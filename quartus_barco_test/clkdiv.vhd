library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

entity ClkDivider is
  port (
    clk_in  : in  std_logic;
    reset   : in  std_logic;
    clk_out : out std_logic
    );
end ClkDivider;

architecture Behavioral of ClkDivider is
  signal temporal : std_logic;
  signal counter  : unsigned(2 downto 0);
begin
  frequency_divider : process (reset, clk_in)
  begin
    if (reset = '1') then
      temporal <= '0';
      counter  <= to_unsigned(0, counter'length);
    elsif rising_edge(clk_in) then
      if (counter = 7) then
        temporal <= not(temporal);
        counter  <= to_unsigned(0, counter'length);
      else
        counter <= counter + 1;
      end if;
    end if;
  end process;

  clk_out <= temporal;
end Behavioral;
