#ifndef STRIPPREVIEWWIDGET_H
#define STRIPPREVIEWWIDGET_H

#include <QWidget>
#include <QBrush>
#include <QPen>
#include <QPalette>
#include <QPainter>
#include <QDebug>

#include "definitions.h"

class StripPreviewWidget : public QWidget
{
    Q_OBJECT
public:
    StripPreviewWidget(const unsigned char* data, QWidget *parent = 0);

    QSize minimumSizeHint() const;
    QSize sizeHint() const;

    void set_data(const unsigned char* data);

signals:

public slots:
    void paintEvent(QPaintEvent *);

private:
    const unsigned char * data;
};

#endif // STRIPPREVIEWWIDGET_H
