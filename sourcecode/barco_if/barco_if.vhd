-------------------------------------------------------------------------------
-- Title      : Interface to BARCO Led Stripes
-- Project    : 
-------------------------------------------------------------------------------
-- File       : barco_if.vhd
-- Author     : Bastli 
-- Company    : Bastli
-- Created    : 2014-08-06
-- Last update: 2014-09-19
-- Platform   : Xilinx ISIM (simulation), Xilinx (synthesis)
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: InAddr_DI expects an address in the range 0 to 111, higher
-- addresses lead to undefined behaviour!
-------------------------------------------------------------------------------
-- Copyright (c) 2014 Bastli
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-08-06  1.0      bastli  Created
-------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-------------------------------------------------------------------------------
entity BarcoIf is

  port (
    Reset_RI         : in  std_logic;
    ---------------------------------------------------------------------------
    InClk_CI         : in  std_logic;
    InAddr_DI        : in  unsigned(6 downto 0);
    Red_DI           : in  std_logic_vector(11 downto 0);
    Green_DI         : in  std_logic_vector(11 downto 0);
    Blue_DI          : in  std_logic_vector(11 downto 0);
    Valid_SI         : in  std_logic;
    ---------------------------------------------------------------------------
    StripeClk_CI     : in  std_logic;
    StripeData_DO    : out std_logic_vector(3 downto 0);
    StripeRed_DO     : out std_logic;
    StripeGreen_DO   : out std_logic;
    StripeBlue_DO    : out std_logic;
    StripeGE_DO      : out std_logic;
    StripeSegment_DO : out std_logic_vector(1 downto 0)
    );

end entity BarcoIf;

-------------------------------------------------------------------------------

architecture rtl of BarcoIf is

  constant PWM_WIDTH : natural := 12;
  -----------------------------------------------------------------------------
  -- RAM
  -----------------------------------------------------------------------------
  type LOCAL_MEMORY_T is array (0 to 31) of std_logic_vector(3*PWM_WIDTH-1 downto 0);

  shared variable local_ram0 : LOCAL_MEMORY_T;
  shared variable local_ram1 : LOCAL_MEMORY_T;
  shared variable local_ram2 : LOCAL_MEMORY_T;
  shared variable local_ram3 : LOCAL_MEMORY_T;

  -- We have the three colors red, green, blue
  -- The MSB PWM_WIDTH bits are red, the bits in the middle green, the LSB bits
  -- are blue
  type ARRAY_RECORD_T is array(0 to 3) of std_logic_vector(3*PWM_WIDTH-1 downto 0);
  signal RamOut_D     : ARRAY_RECORD_T;
  signal RamOutAddr_D : unsigned(4 downto 0);

  signal RamSel_D    : std_logic_vector(1 downto 0);
  signal RamInAddr_D : unsigned(4 downto 0);
  signal InAddrInt_D : unsigned(6 downto 0);
  signal RamIn_D     : std_logic_vector(3*PWM_WIDTH-1 downto 0);


  -----------------------------------------------------------------------------
  -- PWM control
  -----------------------------------------------------------------------------
  type PWM_ARRAY_T is array(0 to 3) of std_logic_vector(PWM_WIDTH-1 downto 0);
  signal RamOutInt_D  : PWM_ARRAY_T;
  signal PWMData_D    : PWM_ARRAY_T;
  signal RamData_D    : std_logic_vector(3 downto 0);
  signal RamDataInt_D : std_logic_vector(3 downto 0);

  -----------------------------------------------------------------------------
  -- FSM
  -----------------------------------------------------------------------------
  type state_t is (reset, reset_color1, reset_wait1, reset_reg0, reset_reg0_data,
                   reset_wait2, reset_reg9, reset_reg9_data,
                   init_red_wait, init_red, init_red_color,
                   init_green_wait, init_green, init_green_color,
                   init_blue_wait, init_blue, init_blue_color,
                   start, addr, data, data_wait, wait_reset);


  -- signals
  signal ColorEn_D            : std_logic;
  signal ColorAllEn_D         : std_logic;
  signal FsmData_D            : std_logic;
  signal GlobalEn_D           : std_logic;
  signal OutReg_DP, OutReg_DN : PWM_ARRAY_T;

  -- registers
  -- Addr holds 2:0 the address that is output, 4:3 the segment, 6:5 the color
  -- Color: 0 means red, 1 means green, 2 means blue
  signal Addr_DP, Addr_DN   : unsigned(6 downto 0);
  signal State_DP, State_DN : state_t;

  signal WaitCounter_DP, WaitCounter_DN : unsigned(3 downto 0);

begin  -- architecture

  -----------------------------------------------------------------------------
  -- RAM, Two-Port
  -- We use 4 RAMs in parallel to feed the 4 data lines
  -- The 4 RAMs are interleaved, so the addresses look a bit weird
  -----------------------------------------------------------------------------

  writeRamPort : process (InClk_CI) is
  begin  -- process
    if rising_edge(InClk_CI) then       -- rising clock edge

      if Valid_SI = '1' then
        case RamSel_D is
          -----------------------------------------------------------------------
          when "00" =>
            local_ram0(to_integer(RamInAddr_D)) := RamIn_D;

          ---------------------------------------------------------------------
          when "01" =>
            local_ram1(to_integer(RamInAddr_D)) := RamIn_D;

          ---------------------------------------------------------------------
          when "10" =>
            local_ram2(to_integer(RamInAddr_D)) := RamIn_D;

          ---------------------------------------------------------------------
          when "11" =>
            local_ram3(to_integer(RamInAddr_D)) := RamIn_D;

          ---------------------------------------------------------------------
          when others => null;
        end case;

      end if;
    end if;
  end process;

  RamInAddr_D <= InAddrInt_D(6 downto 5) & InAddrInt_D(2 downto 0);
  RamSel_D    <= std_logic_vector(InAddrInt_D(4 downto 3));
  RamIn_D     <= Red_DI & Green_DI & Blue_DI;

  readRamPort : process (StripeClk_CI) is
  begin
    if (rising_edge(StripeClk_CI)) then
      RamOut_D(0) <= local_ram0(to_integer(RamOutAddr_D));
      RamOut_D(1) <= local_ram1(to_integer(RamOutAddr_D));
      RamOut_D(2) <= local_ram2(to_integer(RamOutAddr_D));
      RamOut_D(3) <= local_ram3(to_integer(RamOutAddr_D));
    end if;
  end process;


  RamOutAddr_D <= Addr_DP(4 downto 0);

  -----------------------------------------------------------------------------
  -- Calculate RAM address for input
  -----------------------------------------------------------------------------
  ramAddrComb : process (InAddr_DI) is
    variable addr_temp : unsigned(6 downto 0);
  begin  -- process ramAddrComb
    if InAddr_DI >= 84 then
      addr_temp := InAddr_DI + 12;
    elsif InAddr_DI >= 56 then
      addr_temp := InAddr_DI + 8;
    elsif InAddr_DI >= 28 then
      addr_temp := InAddr_DI + 4;
    else
      addr_temp := InAddr_DI;
    end if;

    if addr_temp(4 downto 0) >= 21 then
      InAddrInt_D <= addr_temp + 4;
    elsif addr_temp(4 downto 0) >= 7 then
      InAddrInt_D <= addr_temp + 2;
    else
      InAddrInt_D <= addr_temp;
    end if;
  end process ramAddrComb;

  -----------------------------------------------------------------------------
  -- PWM Control
  -----------------------------------------------------------------------------

  dataGen : for i in 0 to 3 generate
    with Addr_DP(6 downto 5) select
      RamOutInt_D(i) <=
      RamOut_D(i)(3*PWM_WIDTH-1 downto 2*PWM_WIDTH) when "00",    -- red
      RamOut_D(i)(2*PWM_WIDTH-1 downto 1*PWM_WIDTH) when "01",    -- green
      RamOut_D(i)(1*PWM_WIDTH-1 downto 0*PWM_WIDTH) when others;  -- blue
  end generate dataGen;

  bitReverse : for i in 0 to 11 generate
    PWMData_D(0)(i) <= RamOutInt_D(0)(PWM_WIDTH-1 - i);
    PWMData_D(1)(i) <= RamOutInt_D(1)(PWM_WIDTH-1 - i);
    PWMData_D(2)(i) <= RamOutInt_D(2)(PWM_WIDTH-1 - i);
    PWMData_D(3)(i) <= RamOutInt_D(3)(PWM_WIDTH-1 - i);
  end generate bitReverse;


  -----------------------------------------------------------------------------
  -- FSM
  -----------------------------------------------------------------------------

  fsmReg : process (StripeClk_CI, Reset_RI) is
  begin  -- process fsmReg
    if Reset_RI = '1' then              -- asynchronous reset (active high)
      State_DP       <= reset;
      Addr_DP        <= (others => '0');
      WaitCounter_DP <= (others => '0');
      OutReg_DP      <= (others => (others => '0'));

    elsif rising_edge(StripeClk_CI) then  -- rising clock edge
      State_DP       <= State_DN;
      Addr_DP        <= Addr_DN;
      WaitCounter_DP <= WaitCounter_DN;
      OutReg_DP      <= OutReg_DN;

    end if;
  end process fsmReg;


  -----------------------------------------------------------------------------
  -- This is the main FSM
  -- The state diagram looks like this
  --
  --             reset --------------------
  --              /\                       >
  --              |                     reset_color1
  --              |                          |
  --              |                          \/
  --              |                     reset_wait1
  --              |                          |
  --              |                          \/
  --              |                     reset_reg0
  --              |                          |
  --              |                          \/
  --              |                     reset_reg0_data
  --              |                          |
  --              |                          \/
  --              |                     reset_wait2
  --              |                          |
  --              |                          \/
  --              |                     reset_reg9
  --              |                          |
  --              |                          \/
  --              |                     reset_reg9_data
  --              |                          |
  --              |                          \/
  --              |                     init_red_wait
  --              |                          |
  --              |                          \/
  --              |                     init_red
  --              |                          |
  --              |                          \/
  --              |                     init_red_color
  --              |                          |
  --              |                          \/
  --              |                     init_green_wait
  --              |                          |
  --              |                          \/
  --              |                     init_green
  --              |                          |
  --             wait_reset                  \/
  --               <                    init_green_color
  --                \                        |
  --                 \                       \/
  --                  \                 init_blue_wait
  --                   \                     |
  --                    \                    \/
  --                     \              init_blue
  --                      \                  |
  --                       \                 \/
  --                        \           init_blue_color
  --                         \               |
  --                          \              \/
  --                           \           start
  --                            \            |
  --                             \           \/
  --                              \        addr <-
  --                               \         |    \
  --                                \        \/    \
  --                                 \     data     |
  --                                  \      |      |
  --                                   \     \/    /
  --                                       data_wait
  -----------------------------------------------------------------------------
  fsmComb : process (Addr_DP, OutReg_DP(0)(PWM_WIDTH-2 downto 0),
                     OutReg_DP(1)(PWM_WIDTH-2 downto 0),
                     OutReg_DP(2)(PWM_WIDTH-2 downto 0),
                     OutReg_DP(3)(PWM_WIDTH-2 downto 0), PWMData_D, State_DP,
                     WaitCounter_DP) is
    variable temp_addr : unsigned(3 downto 0);
  begin  -- process fsmComb
    State_DN       <= State_DP;
    Addr_DN        <= Addr_DP;
    WaitCounter_DN <= to_unsigned(0, WaitCounter_DN'length);
    GlobalEn_D     <= '0';
    ColorEn_D      <= '0';
    ColorAllEn_D   <= '0';
    OutReg_DN(0)   <= OutReg_DP(0)(PWM_WIDTH-2 downto 0) & '0';
    OutReg_DN(1)   <= OutReg_DP(1)(PWM_WIDTH-2 downto 0) & '0';
    OutReg_DN(2)   <= OutReg_DP(2)(PWM_WIDTH-2 downto 0) & '0';
    OutReg_DN(3)   <= OutReg_DP(3)(PWM_WIDTH-2 downto 0) & '0';

    -- bug in vhdl compiler of ise... so we do it like this
    temp_addr := ('0' & Addr_DP(2 downto 0)) + 1;

    case State_DP is
      -------------------------------------------------------------------------
      when reset =>
        GlobalEn_D <= '1';

        if WaitCounter_DP = 0 then
          OutReg_DN(0) <= "1111" & x"00";
          OutReg_DN(1) <= "1111" & x"00";
          OutReg_DN(2) <= "1111" & x"00";
          OutReg_DN(3) <= "1111" & x"00";
        end if;

        if WaitCounter_DP = 4 then
          State_DN <= reset_color1;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when reset_color1 =>
        GlobalEn_D   <= '1';
        ColorAllEn_D <= '1';

        if WaitCounter_DP = 2 then
          State_DN <= reset_wait1;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when reset_wait1 =>
        if WaitCounter_DP = 2 then
          State_DN <= reset_reg0;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when reset_reg0 =>
        GlobalEn_D <= '1';

        if WaitCounter_DP = 4 then
          State_DN <= reset_reg0_data;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when reset_reg0_data =>
        GlobalEn_D   <= '1';
        ColorAllEn_D <= '1';

        if WaitCounter_DP = 11 then
          State_DN <= reset_wait2;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when reset_wait2 =>
        if WaitCounter_DP = 0 then
          OutReg_DN(0) <= "1111001" & "00000";
          OutReg_DN(1) <= "1111001" & "00000";
          OutReg_DN(2) <= "1111001" & "00000";
          OutReg_DN(3) <= "1111001" & "00000";
        end if;

        if WaitCounter_DP = 1 then
          State_DN <= reset_reg9;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when reset_reg9 =>
        GlobalEn_D <= '1';

        if WaitCounter_DP = 4 then
          State_DN <= reset_reg9_data;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when reset_reg9_data =>
        GlobalEn_D   <= '1';
        ColorAllEn_D <= '1';

        if WaitCounter_DP = 0 then
          OutReg_DN(0) <= "110101011000";
          OutReg_DN(1) <= "110101011000";
          OutReg_DN(2) <= "110101011000";
          OutReg_DN(3) <= "110101011000";
        end if;

        if WaitCounter_DP = 11 then
          State_DN <= init_red_wait;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when init_red_wait =>
        if WaitCounter_DP = 0 then
          OutReg_DN(0) <= "111010100000";
          OutReg_DN(1) <= "111010100000";
          OutReg_DN(2) <= "111010100000";
          OutReg_DN(3) <= "111010100000";
        end if;

        if WaitCounter_DP = 2 then
          State_DN <= init_red;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when init_red =>
        GlobalEn_D <= '1';
        Addr_DN    <= to_unsigned(0, Addr_DN'length);

        if WaitCounter_DP = 4 then
          State_DN <= init_red_color;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when init_red_color =>
        ColorEn_D  <= '1';
        GlobalEn_D <= '1';

        if WaitCounter_DP = 2 then
          State_DN <= init_green_wait;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when init_green_wait =>
        State_DN <= init_green;

      -----------------------------------------------------------------------
      when init_green =>
        GlobalEn_D <= '1';
        Addr_DN    <= to_unsigned(32, Addr_DN'length);

        if WaitCounter_DP = 0 then
          OutReg_DN(0) <= "010100000000";
          OutReg_DN(1) <= "010100000000";
          OutReg_DN(2) <= "010100000000";
          OutReg_DN(3) <= "010100000000";
        end if;

        if WaitCounter_DP = 4 then
          State_DN <= init_green_color;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when init_green_color =>
        ColorEn_D  <= '1';
        GlobalEn_D <= '1';

        if WaitCounter_DP = 2 then
          State_DN <= init_blue_wait;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when init_blue_wait =>
        State_DN <= init_blue;

      -----------------------------------------------------------------------
      when init_blue =>
        GlobalEn_D <= '1';
        Addr_DN    <= to_unsigned(64, Addr_DN'length);

        if WaitCounter_DP = 0 then
          OutReg_DN(0) <= "010100000000";
          OutReg_DN(1) <= "010100000000";
          OutReg_DN(2) <= "010100000000";
          OutReg_DN(3) <= "010100000000";
        end if;

        if WaitCounter_DP = 4 then
          State_DN <= init_blue_color;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when init_blue_color =>
        ColorEn_D  <= '1';
        GlobalEn_D <= '1';

        if WaitCounter_DP = 2 then
          State_DN <= start;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when start =>
        Addr_DN <= to_unsigned(0, Addr_DN'length);

        if WaitCounter_DP = 11 then
          State_DN <= addr;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when addr =>
        if WaitCounter_DP = 0 then
          OutReg_DN(0) <= '0' & std_logic_vector(temp_addr) & "0000000";
          OutReg_DN(1) <= '0' & std_logic_vector(temp_addr) & "0000000";
          OutReg_DN(2) <= '0' & std_logic_vector(temp_addr) & "0000000";
          OutReg_DN(3) <= '0' & std_logic_vector(temp_addr) & "0000000";
        end if;

        if WaitCounter_DP = 4 then
          State_DN <= data;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when data =>
        if WaitCounter_DP = 0 then
          OutReg_DN <= PWMData_D;
        end if;

        ColorEn_D <= '1';

        if WaitCounter_DP = 11 then
          State_DN <= data_wait;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when data_wait =>
        if WaitCounter_DP = 2 then
          if Addr_DP = "1011111" then
            State_DN <= wait_reset;
          else
            Addr_DN  <= Addr_DP + 1;
            State_DN <= addr;
          end if;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when wait_reset =>
        if WaitCounter_DP = 15 then
          State_DN <= reset;
        else
          WaitCounter_DN <= WaitCounter_DP + 1;
        end if;


      -----------------------------------------------------------------------
      when others => null;
    end case;
  end process fsmComb;


  -----------------------------------------------------------------------------
  -- Output assignements
  -----------------------------------------------------------------------------

  -- the two MSBs are used to select the segment of the LED stripe
  -- each segment consists of 4x 7 LEDs
  -- we use a grey code here
  with Addr_DP(4 downto 3) select
    StripeSegment_DO <=
    "11" when "00",
    "01" when "01",
    "00" when "10",
    "10" when "11";

  StripeGE_DO <= GlobalEn_D;

  colorsComb : process (Addr_DP(6 downto 5), ColorAllEn_D, ColorEn_D) is
  begin  -- process colorsComb
    if ColorAllEn_D = '1' then
      StripeRed_DO   <= '1';
      StripeGreen_DO <= '1';
      StripeBlue_DO  <= '1';
    else
      if Addr_DP(6 downto 5) = "00" then
        StripeRed_DO <= ColorEn_D;
      else
        StripeRed_DO <= '0';
      end if;

      if Addr_DP(6 downto 5) = "01" then
        StripeGreen_DO <= ColorEn_D;
      else
        StripeGreen_DO <= '0';
      end if;

      if Addr_DP(6 downto 5) = "10" then
        StripeBlue_DO <= ColorEn_D;
      else
        StripeBlue_DO <= '0';
      end if;
    end if;

  end process colorsComb;

  StripeData_DO(0) <= OutReg_DN(0)(PWM_WIDTH-1);
  StripeData_DO(1) <= OutReg_DN(1)(PWM_WIDTH-1);
  StripeData_DO(2) <= OutReg_DN(2)(PWM_WIDTH-1);
  StripeData_DO(3) <= OutReg_DN(3)(PWM_WIDTH-1);


end architecture rtl;
