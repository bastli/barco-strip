#ifndef BARCOSERIALPORT_H
#define BARCOSERIALPORT_H

#include <QWidget>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QStandardItemModel>
#include <QGridLayout>
#include <QMessageBox>
#include <QDebug>
#include <QSettings>

#include "definitions.h"
#include "strippreviewwidget.h"

Q_DECLARE_METATYPE(QSerialPortInfo)


class BarcoSerialPort : public QWidget
{
    Q_OBJECT

public:
    explicit BarcoSerialPort(int strip_nr, QGridLayout *gui_table, QStandardItemModel *port_model, QWidget *parent = 0);
    ~BarcoSerialPort();

    void send_data(const unsigned char data[], int size);

    inline int getNR() { return this->strip_nr; }

signals:

public slots:
    void connect_port(const QSerialPortInfo& port_info);
    void disconnect_port();

    void handle_serial_error(QSerialPort::SerialPortError error);

    void send_test_pattern();
    void send_null_pattern();

    void handle_connect_button_press();
    void connect_to_selected();

    void select_port(const QString &port_name);

    inline unsigned char* getBuffer() { return this->current_frame_data; }
    inline unsigned char* getTestBuffer() { return this->serial_test_pattern; }
    void sendCurrentFrame();

    void store_port_assignment();

    void enable_preview(bool enabled = true);
    bool show_preview() { return data_preview->isVisible(); }

private:
    void disconnect_error(const QString& message);

    QSerialPort port;
    const int strip_nr;
    QStandardItemModel * port_model;

    unsigned char * serial_test_pattern;
    unsigned char * serial_null_pattern;

    unsigned char * current_frame_data;

    // gui elements
    QLabel * label_name;
    QLabel * label_status;
    QComboBox * dropdown_port;
    QPushButton * button_connect;
    QPushButton * button_test;
    StripPreviewWidget * data_preview;

    // permanent settings (port assignment)
    QSettings settings;
};

#endif // BARCOSERIALPORT_H
