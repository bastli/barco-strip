
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_uart.h"
#include "altera_avalon_uart_regs.h"
#include "sys/alt_dev.h"

ALTERA_AVALON_UART_INSTANCE ( UART_0, uart_0);

inline void uart_init() {
	ALTERA_AVALON_UART_INIT ( UART_0, uart_0);
	IOWR_ALTERA_AVALON_UART_DIVISOR(UART_0_BASE, 64);
}

inline int uart_read(char* ptr, int len, int flags) {
	return altera_avalon_uart_read(&uart_0, ptr, len, flags);
}

inline int uart_write(const char* ptr, int len, int flags) {
	return altera_avalon_uart_write(&uart_0, ptr, len, flags);
}
