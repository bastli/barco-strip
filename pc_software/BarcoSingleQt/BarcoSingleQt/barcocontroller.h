#ifndef BARCOSERIALCONTROLLER_H
#define BARCOSERIALCONTROLLER_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QStandardItemModel>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QColor>
#include <QList>
#include <QtNetwork>
#include <QElapsedTimer>
#include <QTimer>
#include <QSettings>

#include <QMainWindow>

#include "barcoserialport.h"
#include "patterngenerator.h"

namespace Ui {
    class BarcoController;
}

bool compareSerialPortInfo(const QSerialPortInfo& i1, const QSerialPortInfo& i2);

class BarcoController : public QMainWindow
{
    Q_OBJECT
public:
    explicit BarcoController(QWidget *parent = 0);
    ~BarcoController();

    bool filter_port(const QSerialPortInfo& info);
    QList<QSerialPortInfo> filter_ports(const QList<QSerialPortInfo> &ports);

    QHostAddress getMyIP();

    bool isServerRunning();

signals:

public slots:
    void connect_all_serial();
    BarcoSerialPort *add_strip();
    void remove_strip();

    void updateSerialPorts();

    void startServer();
    void stopServer();


    void on_button_connect_all_clicked();
    void on_button_add_strip_clicked();
    void on_button_remove_strip_clicked();
    void on_button_rescan_serial_clicked();

    void handleNewClientConnection();
    void handleClientDisconnect();
    void handleIncomingNetworkData();

private slots:
    void on_button_server_enable_clicked();
    void on_button_test_all_clicked();
    void dropdown_test_pattern_changed(int index);

    void on_checkbox_autoconnect_stateChanged(int arg1);
    void on_checkbox_remember_assigment_stateChanged(int arg1);
    void on_checkbox_show_live_toggled(bool checked);
    void on_checkbox_test_clear_toggled(bool checked);
    void on_checkbox_test_simult_toggled(bool checked);

private:

    // GUI

    Ui::BarcoController * ui;
    QGridLayout * main_layout;


    // DATA

    QByteArray current_frame;

    QList<PatternGenerator*> pattern_generators;
    void addPattern(QString desc, PatternGenerator* gen);


    // SERIAL

    QStandardItemModel* serial_port_model;
    QList<BarcoSerialPort*> serialports;


    // NETWORK

    QTcpServer* server;
    QTcpSocket* client;

    bool receiveSingleByte(unsigned char& byte);
    bool receiveMoreBytes(unsigned char* dest, qint64 size);

    int rx_count;
    QElapsedTimer rx_timer; // for profiling network speed

    char* devnull;

    // SETTINGS

    QSettings settings;
};

#endif // BARCOSERIALCONTROLLER_H
