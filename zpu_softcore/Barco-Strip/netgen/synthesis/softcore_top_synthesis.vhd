--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: softcore_top_synthesis.vhd
-- /___/   /\     Timestamp: Mon Aug 11 12:31:40 2014
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm softcore_top -w -dir netgen/synthesis -ofmt vhdl -sim softcore_top.ngc softcore_top_synthesis.vhd 
-- Device	: xc6slx16-2-csg324
-- Input file	: softcore_top.ngc
-- Output file	: C:\barco-strip\zpu_softcore\Barco-Strip\netgen\synthesis\softcore_top_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: softcore_top
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity softcore_top is
  port (
    CLK_IN1 : in STD_LOGIC := 'X'; 
    reset : in STD_LOGIC := 'X'; 
    uart_rx1 : in STD_LOGIC := 'X'; 
    clk_test_out : out STD_LOGIC; 
    uart_tx1 : out STD_LOGIC; 
    ioButtons : in STD_LOGIC_VECTOR ( 3 downto 0 ); 
    ioLEDs : out STD_LOGIC_VECTOR ( 3 downto 0 ) 
  );
end softcore_top;

architecture Structure of softcore_top is
  signal ioButtons_3_IBUF_0 : STD_LOGIC; 
  signal ioButtons_2_IBUF_1 : STD_LOGIC; 
  signal ioButtons_1_IBUF_2 : STD_LOGIC; 
  signal ioButtons_0_IBUF_3 : STD_LOGIC; 
  signal reset_IBUF_5 : STD_LOGIC; 
  signal uart_rx1_IBUF_6 : STD_LOGIC; 
  signal intIO_inst_LED_3_7 : STD_LOGIC; 
  signal intIO_inst_LED_2_8 : STD_LOGIC; 
  signal intIO_inst_LED_1_9 : STD_LOGIC; 
  signal intIO_inst_LED_0_10 : STD_LOGIC; 
  signal clk_test_out_OBUF_11 : STD_LOGIC; 
  signal core_master_out_addr_31_Q : STD_LOGIC; 
  signal core_master_out_addr_27_Q : STD_LOGIC; 
  signal core_master_out_addr_26_Q : STD_LOGIC; 
  signal core_master_out_addr_25_Q : STD_LOGIC; 
  signal core_master_out_addr_24_Q : STD_LOGIC; 
  signal core_master_out_addr_23_Q : STD_LOGIC; 
  signal core_master_out_addr_22_Q : STD_LOGIC; 
  signal core_master_out_addr_21_Q : STD_LOGIC; 
  signal core_master_out_addr_20_Q : STD_LOGIC; 
  signal core_master_out_addr_19_Q : STD_LOGIC; 
  signal core_master_out_addr_18_Q : STD_LOGIC; 
  signal core_master_out_addr_17_Q : STD_LOGIC; 
  signal core_master_out_addr_16_Q : STD_LOGIC; 
  signal core_master_out_addr_15_Q : STD_LOGIC; 
  signal core_master_out_addr_14_Q : STD_LOGIC; 
  signal core_master_out_addr_13_Q : STD_LOGIC; 
  signal core_master_out_addr_12_Q : STD_LOGIC; 
  signal core_master_out_addr_11_Q : STD_LOGIC; 
  signal core_master_out_addr_3_Q : STD_LOGIC; 
  signal core_master_out_addr_2_Q : STD_LOGIC; 
  signal core_master_out_addr_1_Q : STD_LOGIC; 
  signal core_master_out_addr_0_Q : STD_LOGIC; 
  signal core_master_out_we_42 : STD_LOGIC; 
  signal core_state_43 : STD_LOGIC; 
  signal uart_inst_1_tx_txBufferReady_44 : STD_LOGIC; 
  signal uart1_in_cyc : STD_LOGIC; 
  signal intIO_in_cyc : STD_LOGIC; 
  signal master_in_ack : STD_LOGIC; 
  signal N0 : STD_LOGIC; 
  signal dummy_out_ack : STD_LOGIC; 
  signal generatoer_clkfbout_buf : STD_LOGIC; 
  signal generatoer_clkout0 : STD_LOGIC; 
  signal generatoer_clkfbout : STD_LOGIC; 
  signal generatoer_clkin1 : STD_LOGIC; 
  signal master_out_addr_31_PWR_9_o_equal_4_o_31_1 : STD_LOGIC; 
  signal intIO_inst_slave_in_cyc_slave_in_addr_1_AND_62_o : STD_LOGIC; 
  signal core_n0066_inv12_56 : STD_LOGIC; 
  signal core_n0066_inv1 : STD_LOGIC; 
  signal core_n0069_inv : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_readEnable_59 : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_writeEnable_60 : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_0_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_1_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_2_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_3_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_11_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_12_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_13_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_14_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_15_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_16_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_17_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_18_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_19_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_20_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_21_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_22_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_23_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_24_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_25_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_26_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_27_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_28_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_29_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_30_Q : STD_LOGIC; 
  signal core_zpu_core_foo_out_mem_addr_31_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n02821531 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282152_96 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282121_97 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98 : STD_LOGIC; 
  signal core_zpu_core_foo_Mram_n040723 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282114 : STD_LOGIC; 
  signal core_zpu_core_foo_n0515_4_1 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT281_105 : STD_LOGIC; 
  signal core_zpu_core_foo_Mram_n040721 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282112 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282111 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_31_Q_109 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_30_Q_110 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_30_Q_111 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_29_Q_112 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_29_Q_113 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_28_Q_114 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_28_Q_115 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_27_Q_116 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_27_Q_117 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_26_Q_118 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_26_Q_119 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_25_Q_120 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_25_Q_121 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_24_Q_122 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_24_Q_123 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_23_Q_124 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_23_Q_125 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_22_Q_126 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_22_Q_127 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_21_Q_128 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_21_Q_129 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_20_Q_130 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_20_Q_131 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_19_Q_132 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_19_Q_133 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_18_Q_134 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_18_Q_135 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_17_Q_136 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_17_Q_137 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_16_Q_138 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_16_Q_139 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_15_Q_140 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_15_Q_141 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_14_Q_142 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_14_Q_143 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_13_Q_144 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_13_Q_145 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_12_Q_146 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_12_Q_147 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_11_Q_148 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_11_Q_149 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_10_Q_150 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_10_Q_151 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_9_Q_152 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_9_Q_153 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_8_Q_154 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_8_Q_155 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_7_Q_156 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_7_Q_157 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_6_Q_158 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_6_Q_159 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_5_Q_160 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_5_Q_161 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_4_Q_162 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_4_Q_163 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_3_Q_164 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_3_Q_165 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_2_Q_166 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_2_Q_167 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_1_Q_168 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_1_Q_169 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_0_Q_170 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_0_Q_171 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_27_Q_172 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_26_Q_173 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_25_Q_174 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_24_Q_175 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_23_Q_176 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_22_Q_177 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_21_Q_178 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_20_Q_179 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_19_Q_180 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_18_Q_181 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_17_Q_182 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_16_Q_183 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_15_Q_184 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_14_Q_185 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_13_Q_186 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_12_Q_187 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_11_Q_188 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_10_Q_189 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_9_Q_190 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_8_Q_191 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_7_Q_192 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_6_Q_193 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_5_Q_194 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_4_Q_195 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_3_Q_196 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_2_Q_197 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_1_Q_198 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_0_Q_199 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_lut_0_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_10_Q_201 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_9_Q_202 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_8_Q_203 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_7_Q_204 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_6_Q_205 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_5_Q_206 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_4_Q_207 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_4_Q_208 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_3_Q_209 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_3_Q_210 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_2_Q_211 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_2_Q_212 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_1_Q_213 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_1_Q_214 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_0_Q_215 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_0_Q_216 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_28_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_27_Q_218 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_27_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_26_Q_220 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_26_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_25_Q_222 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_25_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_24_Q_224 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_24_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_23_Q_226 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_23_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_22_Q_228 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_22_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_21_Q_230 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_21_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_20_Q_232 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_20_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_19_Q_234 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_19_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_18_Q_236 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_18_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_17_Q_238 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_17_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_16_Q_240 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_16_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_15_Q_242 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_15_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_14_Q_244 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_14_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_13_Q_246 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_13_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_12_Q_248 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_12_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_11_Q_250 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_11_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_10_Q_252 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_10_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_9_Q_254 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_9_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_8_Q_256 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_8_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_7_Q_258 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_7_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_6_Q_260 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_6_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_5_Q_262 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_5_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_4_Q_264 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_4_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_3_Q_266 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_3_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_2_Q_268 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_2_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_1_Q_270 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_1_Q : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_0_Q_272 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_29_Q_273 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_28_Q_274 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_27_Q_275 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_26_Q_276 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_25_Q_277 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_24_Q_278 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_23_Q_279 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_22_Q_280 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_21_Q_281 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_20_Q_282 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_19_Q_283 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_18_Q_284 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_17_Q_285 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_16_Q_286 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_15_Q_287 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_14_Q_288 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_13_Q_289 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_12_Q_290 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_11_Q_291 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_10_Q_292 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_9_Q_293 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_8_Q_294 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_7_Q_295 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_6_Q_296 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_5_Q_297 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_4_Q_298 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_3_Q_299 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_2_Q_300 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_1_Q_301 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_0_Q_302 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_lut_0_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd5_In_304 : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd3_In1_305 : STD_LOGIC; 
  signal core_zpu_core_foo_Mram_n04071 : STD_LOGIC; 
  signal core_zpu_core_foo_Mram_n0407 : STD_LOGIC; 
  signal core_zpu_core_foo_n0372_inv_308 : STD_LOGIC; 
  signal core_zpu_core_foo_reset_inv : STD_LOGIC; 
  signal core_zpu_core_foo_n0224_0 : STD_LOGIC; 
  signal core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_1_Q : STD_LOGIC; 
  signal core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_2_Q : STD_LOGIC; 
  signal core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_3_Q : STD_LOGIC; 
  signal core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_4_Q : STD_LOGIC; 
  signal core_zpu_core_foo_n0526 : STD_LOGIC; 
  signal core_zpu_core_foo_n0515 : STD_LOGIC; 
  signal core_zpu_core_foo_n0425 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_0_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_1_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_2_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_3_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_4_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_5_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_6_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_7_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_8_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_9_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_10_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_11_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_12_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_13_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_14_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_15_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_16_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_17_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_18_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_19_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_20_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_21_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_22_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_23_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_24_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_25_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_26_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_27_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_28_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_29_Q : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_30_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_GND_14_o_Mux_81_o : STD_LOGIC; 
  signal core_zpu_core_foo_n0457 : STD_LOGIC; 
  signal core_zpu_core_foo_n0435 : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_0_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_1_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_2_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_3_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_4_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_5_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_6_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_7_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_8_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_9_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_10_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_11_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_12_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_13_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_14_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_15_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_16_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_17_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_18_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_19_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_20_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_21_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_22_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_23_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_24_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_25_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_26_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_27_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_28_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_0_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_1_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_2_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_3_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_4_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_5_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_6_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_7_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_8_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_9_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_10_Q : STD_LOGIC; 
  signal core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_11_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_0_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_1_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_2_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_3_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_4_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_5_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_6_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_7_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_8_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_9_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_10_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_11_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_0_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_1_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_2_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_3_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_4_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_5_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_6_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_7_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_8_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_9_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_10_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_11_Q : STD_LOGIC; 
  signal core_zpu_core_foo_n0267 : STD_LOGIC; 
  signal core_zpu_core_foo_n0224 : STD_LOGIC; 
  signal core_zpu_core_foo_n0565 : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_0_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_1_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_2_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_3_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_4_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_5_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_6_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_7_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_8_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_9_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_10_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_11_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_12_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_13_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_14_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_15_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_16_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_17_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_18_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_19_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_20_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_21_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_22_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_23_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_24_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_25_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_26_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_27_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_28_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_29_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_30_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_31_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_0_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_1_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_2_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_3_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_4_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_5_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_6_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_7_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_8_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_9_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_10_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_11_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_12_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_13_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_14_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_15_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_16_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_17_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_18_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_19_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_20_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_21_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_22_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_23_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_24_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_25_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_26_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_27_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_28_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_29_Q : STD_LOGIC; 
  signal core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_30_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_0_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_1_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_2_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_3_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_4_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_5_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_6_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_7_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_8_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_9_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_10_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_11_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_12_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_13_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_14_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_15_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_16_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_17_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_18_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_19_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_20_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_21_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_22_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_23_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_24_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_25_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_26_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_27_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_28_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_29_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_30_Q : STD_LOGIC; 
  signal core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_31_Q : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_GND_14_o_Mux_76_o : STD_LOGIC; 
  signal core_zpu_core_foo_state_4_GND_14_o_Mux_82_o : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd5_583 : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd4_584 : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd3_585 : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd2_586 : STD_LOGIC; 
  signal core_zpu_core_foo_idim_flag_621 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWriteEnable_724 : STD_LOGIC; 
  signal core_zpu_core_foo_memAWriteEnable_725 : STD_LOGIC; 
  signal uart_inst_1_Mmux_slave_out_data11 : STD_LOGIC; 
  signal uart_inst_1_tx_timer_GND_22_o_GND_22_o_MUX_247_o : STD_LOGIC; 
  signal uart_inst_1_uart_write : STD_LOGIC; 
  signal uart_inst_1_tx_timer_output_819 : STD_LOGIC; 
  signal uart_inst_1_rx_timer_output_820 : STD_LOGIC; 
  signal uart_inst_1_rx_byteAvail_821 : STD_LOGIC; 
  signal uart_inst_1_rx_Madd_rxProc_bitpos_3_GND_19_o_add_16_OUT_xor_1_11 : STD_LOGIC; 
  signal uart_inst_1_rx_n0128_inv : STD_LOGIC; 
  signal uart_inst_1_rx_n0124_inv : STD_LOGIC; 
  signal uart_inst_1_rx_n0109_inv : STD_LOGIC; 
  signal uart_inst_1_rx_n0119_inv_838 : STD_LOGIC; 
  signal uart_inst_1_rx_rxProc_bitpos_3_GND_19_o_add_16_OUT_0_Q : STD_LOGIC; 
  signal uart_inst_1_rx_rxProc_bitpos_3_GND_19_o_add_16_OUT_1_Q : STD_LOGIC; 
  signal uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_0_Q : STD_LOGIC; 
  signal uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_1_Q : STD_LOGIC; 
  signal uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_2_Q : STD_LOGIC; 
  signal uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_3_Q : STD_LOGIC; 
  signal uart_inst_1_rx_receive_0_rxd_MUX_222_o : STD_LOGIC; 
  signal uart_inst_1_rx_receive_1_rxd_MUX_221_o : STD_LOGIC; 
  signal uart_inst_1_rx_receive_2_rxd_MUX_220_o : STD_LOGIC; 
  signal uart_inst_1_rx_receive_3_rxd_MUX_219_o : STD_LOGIC; 
  signal uart_inst_1_rx_receive_4_rxd_MUX_218_o : STD_LOGIC; 
  signal uart_inst_1_rx_receive_5_rxd_MUX_217_o : STD_LOGIC; 
  signal uart_inst_1_rx_receive_6_rxd_MUX_216_o : STD_LOGIC; 
  signal uart_inst_1_rx_receive_7_rxd_MUX_215_o : STD_LOGIC; 
  signal uart_inst_1_rx_byteAvail_rxProc_bitpos_3_MUX_223_o : STD_LOGIC; 
  signal uart_inst_1_rx_n0101 : STD_LOGIC; 
  signal uart_inst_1_tx_n0095_inv : STD_LOGIC; 
  signal uart_inst_1_tx_n0078_inv : STD_LOGIC; 
  signal uart_inst_1_tx_n0068_inv : STD_LOGIC; 
  signal uart_inst_1_tx_txProc_bitpos_3_GND_20_o_Mux_6_o : STD_LOGIC; 
  signal uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_0_Q : STD_LOGIC; 
  signal uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_1_Q : STD_LOGIC; 
  signal uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_2_Q : STD_LOGIC; 
  signal uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_3_Q : STD_LOGIC; 
  signal uart_inst_1_tx_GND_20_o_transmit_7_Mux_4_o : STD_LOGIC; 
  signal uart_inst_1_tx_BufferLoaded_txProc_bitpos_3_MUX_235_o : STD_LOGIC; 
  signal uart_inst_1_tx_BufferLoaded_879 : STD_LOGIC; 
  signal uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT41 : STD_LOGIC; 
  signal uart_inst_1_rx_timer_GND_21_o_GND_21_o_MUX_245_o : STD_LOGIC; 
  signal uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_0_Q : STD_LOGIC; 
  signal uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_1_Q : STD_LOGIC; 
  signal uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_2_Q : STD_LOGIC; 
  signal uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_3_Q : STD_LOGIC; 
  signal uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_4_Q : STD_LOGIC; 
  signal uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_5_Q : STD_LOGIC; 
  signal uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_6_Q : STD_LOGIC; 
  signal master_out_addr_31_PWR_9_o_equal_4_o_31_11_912 : STD_LOGIC; 
  signal master_out_addr_31_PWR_9_o_equal_4_o_31_12_913 : STD_LOGIC; 
  signal N2 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT99 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT992_916 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT993_917 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT994_918 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT995_919 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT998 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT9910 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT89 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT892 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT893_924 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT894_925 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT895_926 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT898 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT8910 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1081 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1082_930 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1085 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1087 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1051 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1052_934 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1055 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1057 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1021 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1022_938 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1025 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1027 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT96 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT962_942 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT963_943 : STD_LOGIC; 
  signal N4 : STD_LOGIC; 
  signal N6 : STD_LOGIC; 
  signal N8 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT2 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT3 : STD_LOGIC; 
  signal N10 : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd4_In1_950 : STD_LOGIC; 
  signal N12 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT93 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT933 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT934_954 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT935_955 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT936_956 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT86 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT863 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT864_959 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT865_960 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT866_961 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT82 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT823 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT824_964 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT825_965 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT826_966 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT78_967 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT783 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT784_969 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT785_970 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT786_971 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT74_972 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT743 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT744_974 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT745_975 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT746_976 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT70 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT703 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT704_979 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT705_980 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT706_981 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT7 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT73 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT75_984 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT76_985 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT77_986 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT66 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT663 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT664_989 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT665_990 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT666_991 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT62 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT623 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT624_994 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT625_995 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT626_996 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT58 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT583 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT584_999 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT585_1000 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT586_1001 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT54 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT543 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT544_1004 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT545_1005 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT546_1006 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT50 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT503 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT504_1009 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT505_1010 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT506_1011 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT46 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT462 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT463_1014 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT464_1015 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT465_1016 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT4611 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT43 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT433 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT434_1020 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT435_1021 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT436_1022 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT39 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT393 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT394_1025 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT395_1026 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT396_1027 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT35_1028 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT353 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT354_1030 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT355_1031 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT356_1032 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT31_1033 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT313 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT314_1035 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT315_1036 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT316_1037 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT3 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT32 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT33_1040 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT34_1041 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT36_1042 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT37_1043 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT38_1044 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT310 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT320_1046 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT321_1047 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT27 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT273 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT274_1050 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT275_1051 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT276_1052 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT23 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT233 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT234_1055 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT235_1056 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT236_1057 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT19 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT193 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT194_1060 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT195_1061 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT196_1062 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT15 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT153 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT154_1065 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT155_1066 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT156_1067 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT120 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1203 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1204_1070 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1205_1071 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1206_1072 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT116_1073 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1161_1074 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1162_1075 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1163_1076 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1164_1077 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1167 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1168_1079 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1169_1080 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT11610_1081 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1121 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1122_1083 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1125 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1126_1086 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1127_1087 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1128_1088 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT11 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT114_1090 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT115_1091 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT117_1092 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT118_1093 : STD_LOGIC; 
  signal N14 : STD_LOGIC; 
  signal N16 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n02829 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028291_1097 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028287 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282871_1099 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028284 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282841_1101 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028281 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282811_1103 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028278 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282781_1105 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028275 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282751_1107 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028272 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282721_1109 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028269 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282691_1111 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028263_1112 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282631_1113 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n02826 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028261_1115 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n02823 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028231_1117 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028212 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n0282122_1119 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT8 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT81_1121 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT6 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT61_1123 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT4 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT41_1125 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT24 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT241_1127 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT22_1128 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT221_1129 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT20 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT201_1131 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT2 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT21_1133 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT18 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT181_1135 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT16 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT161_1137 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT14 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT141_1139 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT12 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT121_1141 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT10 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT105 : STD_LOGIC; 
  signal N18 : STD_LOGIC; 
  signal N20 : STD_LOGIC; 
  signal N22 : STD_LOGIC; 
  signal N24 : STD_LOGIC; 
  signal N26 : STD_LOGIC; 
  signal N28 : STD_LOGIC; 
  signal N30 : STD_LOGIC; 
  signal N32 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028239 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028236 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028233 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028230 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028227 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028224 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028221 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028218 : STD_LOGIC; 
  signal core_zpu_core_foo_Mmux_n028215 : STD_LOGIC; 
  signal N34 : STD_LOGIC; 
  signal N38 : STD_LOGIC; 
  signal N40 : STD_LOGIC; 
  signal N42 : STD_LOGIC; 
  signal N44 : STD_LOGIC; 
  signal core_state_glue_set_1178 : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd2_glue_set_1179 : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd3_glue_set_1180 : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd5_glue_set_1181 : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd4_glue_set_1182 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_0_glue_set_1183 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_31_glue_set_1184 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_30_glue_set_1185 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_29_glue_set_1186 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_28_glue_set_1187 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_27_glue_set_1188 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_26_glue_set_1189 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_25_glue_set_1190 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_24_glue_set_1191 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_23_glue_set_1192 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_22_glue_set_1193 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_21_glue_set_1194 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_20_glue_set_1195 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_19_glue_set_1196 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_18_glue_set_1197 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_17_glue_set_1198 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_16_glue_set_1199 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_15_glue_set_1200 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_14_glue_set_1201 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_13_glue_set_1202 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_12_glue_set_1203 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_11_glue_set_1204 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_10_glue_set_1205 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_9_glue_set_1206 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_8_glue_set_1207 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_7_glue_set_1208 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_6_glue_set_1209 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_5_glue_set_1210 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_4_glue_set_1211 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_3_glue_set_1212 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_2_glue_set_1213 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_1_glue_set_1214 : STD_LOGIC; 
  signal core_zpu_core_foo_memBWrite_0_glue_set_1215 : STD_LOGIC; 
  signal uart_inst_1_tx_timer_countGen_counter_cnt_1_glue_set_1216 : STD_LOGIC; 
  signal uart_inst_1_tx_timer_countGen_counter_cnt_0_glue_set_1217 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_27_rt_1218 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_26_rt_1219 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_25_rt_1220 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_24_rt_1221 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_23_rt_1222 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_22_rt_1223 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_21_rt_1224 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_20_rt_1225 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_19_rt_1226 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_18_rt_1227 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_17_rt_1228 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_16_rt_1229 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_15_rt_1230 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_14_rt_1231 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_13_rt_1232 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_12_rt_1233 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_11_rt_1234 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_10_rt_1235 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_9_rt_1236 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_8_rt_1237 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_7_rt_1238 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_6_rt_1239 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_5_rt_1240 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_4_rt_1241 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_3_rt_1242 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_2_rt_1243 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_1_rt_1244 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_10_rt_1245 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_9_rt_1246 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_8_rt_1247 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_7_rt_1248 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_6_rt_1249 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_5_rt_1250 : STD_LOGIC; 
  signal core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_0_rt_1251 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_29_rt_1252 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_28_rt_1253 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_27_rt_1254 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_26_rt_1255 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_25_rt_1256 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_24_rt_1257 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_23_rt_1258 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_22_rt_1259 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_21_rt_1260 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_20_rt_1261 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_19_rt_1262 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_18_rt_1263 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_17_rt_1264 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_16_rt_1265 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_15_rt_1266 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_14_rt_1267 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_13_rt_1268 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_12_rt_1269 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_11_rt_1270 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_10_rt_1271 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_9_rt_1272 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_8_rt_1273 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_7_rt_1274 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_6_rt_1275 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_5_rt_1276 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_4_rt_1277 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_3_rt_1278 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_2_rt_1279 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_1_rt_1280 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_28_rt_1281 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_11_rt_1282 : STD_LOGIC; 
  signal core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_30_rt_1283 : STD_LOGIC; 
  signal N46 : STD_LOGIC; 
  signal N50 : STD_LOGIC; 
  signal N52 : STD_LOGIC; 
  signal N54 : STD_LOGIC; 
  signal N56 : STD_LOGIC; 
  signal N57 : STD_LOGIC; 
  signal N59 : STD_LOGIC; 
  signal N60 : STD_LOGIC; 
  signal N62 : STD_LOGIC; 
  signal N63 : STD_LOGIC; 
  signal N65 : STD_LOGIC; 
  signal N66 : STD_LOGIC; 
  signal N68 : STD_LOGIC; 
  signal N69 : STD_LOGIC; 
  signal N71 : STD_LOGIC; 
  signal N72 : STD_LOGIC; 
  signal N74 : STD_LOGIC; 
  signal N75 : STD_LOGIC; 
  signal N77 : STD_LOGIC; 
  signal N78 : STD_LOGIC; 
  signal N80 : STD_LOGIC; 
  signal N81 : STD_LOGIC; 
  signal N83 : STD_LOGIC; 
  signal N84 : STD_LOGIC; 
  signal N86 : STD_LOGIC; 
  signal N87 : STD_LOGIC; 
  signal N89 : STD_LOGIC; 
  signal N90 : STD_LOGIC; 
  signal N91 : STD_LOGIC; 
  signal N93 : STD_LOGIC; 
  signal N94 : STD_LOGIC; 
  signal N96 : STD_LOGIC; 
  signal N97 : STD_LOGIC; 
  signal N99 : STD_LOGIC; 
  signal N100 : STD_LOGIC; 
  signal N102 : STD_LOGIC; 
  signal N103 : STD_LOGIC; 
  signal N105 : STD_LOGIC; 
  signal N106 : STD_LOGIC; 
  signal N108 : STD_LOGIC; 
  signal N109 : STD_LOGIC; 
  signal N114 : STD_LOGIC; 
  signal N115 : STD_LOGIC; 
  signal N117 : STD_LOGIC; 
  signal N118 : STD_LOGIC; 
  signal N120 : STD_LOGIC; 
  signal N121 : STD_LOGIC; 
  signal N123 : STD_LOGIC; 
  signal N124 : STD_LOGIC; 
  signal N126 : STD_LOGIC; 
  signal N127 : STD_LOGIC; 
  signal N129 : STD_LOGIC; 
  signal N130 : STD_LOGIC; 
  signal N132 : STD_LOGIC; 
  signal N133 : STD_LOGIC; 
  signal N135 : STD_LOGIC; 
  signal N136 : STD_LOGIC; 
  signal N138 : STD_LOGIC; 
  signal N139 : STD_LOGIC; 
  signal N141 : STD_LOGIC; 
  signal N142 : STD_LOGIC; 
  signal N144 : STD_LOGIC; 
  signal N145 : STD_LOGIC; 
  signal N147 : STD_LOGIC; 
  signal N148 : STD_LOGIC; 
  signal N150 : STD_LOGIC; 
  signal N151 : STD_LOGIC; 
  signal N153 : STD_LOGIC; 
  signal N155 : STD_LOGIC; 
  signal N157 : STD_LOGIC; 
  signal N159 : STD_LOGIC; 
  signal N161 : STD_LOGIC; 
  signal N165 : STD_LOGIC; 
  signal N166 : STD_LOGIC; 
  signal N168 : STD_LOGIC; 
  signal N169 : STD_LOGIC; 
  signal N171 : STD_LOGIC; 
  signal N172 : STD_LOGIC; 
  signal N174 : STD_LOGIC; 
  signal N175 : STD_LOGIC; 
  signal N177 : STD_LOGIC; 
  signal N178 : STD_LOGIC; 
  signal N180 : STD_LOGIC; 
  signal N181 : STD_LOGIC; 
  signal N183 : STD_LOGIC; 
  signal N184 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_1_1370 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_3_1_1371 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_2_1_1372 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_0_1_1373 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_1_1_1374 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_3_2_1375 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_4_2_1376 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_2_2_1377 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_0_2_1378 : STD_LOGIC; 
  signal core_zpu_core_foo_idim_flag_1_1379 : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd2_1_1380 : STD_LOGIC; 
  signal core_zpu_core_foo_state_FSM_FFd4_1_1381 : STD_LOGIC; 
  signal core_master_out_we_rstpot_1382 : STD_LOGIC; 
  signal core_zpu_core_foo_decodedOpcode_3_3_1383 : STD_LOGIC; 
  signal N207 : STD_LOGIC; 
  signal N208 : STD_LOGIC; 
  signal N209 : STD_LOGIC; 
  signal N210 : STD_LOGIC; 
  signal N211 : STD_LOGIC; 
  signal N212 : STD_LOGIC; 
  signal N213 : STD_LOGIC; 
  signal N214 : STD_LOGIC; 
  signal N215 : STD_LOGIC; 
  signal N216 : STD_LOGIC; 
  signal N217 : STD_LOGIC; 
  signal N218 : STD_LOGIC; 
  signal intIO_inst_LED_3_1_1396 : STD_LOGIC; 
  signal intIO_inst_LED_2_1_1397 : STD_LOGIC; 
  signal intIO_inst_LED_1_1_1398 : STD_LOGIC; 
  signal intIO_inst_LED_0_1_1399 : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPA_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPA_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPA_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPA_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPB_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPB_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPB_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPB_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPA_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPA_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPA_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPA_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPB_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPB_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPB_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPB_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPA_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPA_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPA_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPA_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPB_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPB_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPB_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPB_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPA_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPA_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPA_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPA_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPB_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPB_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPB_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPB_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPA_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPA_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPA_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPA_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPB_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPB_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPB_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPB_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPA_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPA_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPA_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPA_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPB_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPB_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPB_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPB_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPA_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPA_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPA_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPA_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPB_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPB_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPB_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPB_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPA_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPA_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPA_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPA_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPB_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPB_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPB_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPB_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_31_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_30_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_29_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_28_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_27_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_26_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_25_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_24_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_23_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_22_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_21_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_20_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_19_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_18_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_17_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_16_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_15_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_14_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_13_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_12_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_11_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_10_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_generatoer_pll_base_inst_CLKOUT3_UNCONNECTED : STD_LOGIC; 
  signal NLW_generatoer_pll_base_inst_CLKOUT1_UNCONNECTED : STD_LOGIC; 
  signal NLW_generatoer_pll_base_inst_CLKOUT4_UNCONNECTED : STD_LOGIC; 
  signal NLW_generatoer_pll_base_inst_CLKOUT5_UNCONNECTED : STD_LOGIC; 
  signal NLW_generatoer_pll_base_inst_CLKOUT2_UNCONNECTED : STD_LOGIC; 
  signal NLW_generatoer_pll_base_inst_LOCKED_UNCONNECTED : STD_LOGIC; 
  signal core_master_out_data : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal core_zpu_core_foo_mem_write : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal core_zpu_core_foo_decodeControl_tOpcode : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal core_zpu_core_foo_n0282 : STD_LOGIC_VECTOR ( 30 downto 2 ); 
  signal core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT : STD_LOGIC_VECTOR ( 28 downto 0 ); 
  signal core_zpu_core_foo_decodedOpcode : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal core_zpu_core_foo_sp : STD_LOGIC_VECTOR ( 30 downto 2 ); 
  signal core_zpu_core_foo_pc : STD_LOGIC_VECTOR ( 30 downto 0 ); 
  signal core_zpu_core_foo_opcode : STD_LOGIC_VECTOR ( 6 downto 0 ); 
  signal core_zpu_core_foo_memBWrite : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal core_zpu_core_foo_memAWrite : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal core_zpu_core_foo_memBAddr : STD_LOGIC_VECTOR ( 13 downto 2 ); 
  signal core_zpu_core_foo_memAAddr : STD_LOGIC_VECTOR ( 13 downto 2 ); 
  signal core_zpu_core_foo_memBRead_stdlogic : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal core_zpu_core_foo_memARead_stdlogic : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal uart_inst_1_tx_timer_countGen_counter_cnt : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal uart_inst_1_rx_dataOut : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal uart_inst_1_rx_rxProc_bitpos : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal uart_inst_1_rx_rxProc_samplecnt : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal uart_inst_1_rx_receive : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal uart_inst_1_tx_txProc_bitpos : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal uart_inst_1_tx_transmit : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal uart_inst_1_tx_transmitBuffer : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal uart_inst_1_rx_timer_countGen_counter_cnt : STD_LOGIC_VECTOR ( 6 downto 0 ); 
begin
  XST_VCC : VCC
    port map (
      P => N0
    );
  XST_GND : GND
    port map (
      G => dummy_out_ack
    );
  generatoer_clkout1_buf : BUFG
    port map (
      O => clk_test_out_OBUF_11,
      I => generatoer_clkout0
    );
  generatoer_clkf_buf : BUFG
    port map (
      O => generatoer_clkfbout_buf,
      I => generatoer_clkfbout
    );
  generatoer_clkin1_buf : IBUFG
    generic map(
      CAPACITANCE => "DONT_CARE",
      IBUF_DELAY_VALUE => "0",
      IBUF_LOW_PWR => TRUE,
      IOSTANDARD => "DEFAULT"
    )
    port map (
      I => CLK_IN1,
      O => generatoer_clkin1
    );
  intIO_inst_LED_3 : LDC
    port map (
      CLR => reset_IBUF_5,
      D => core_master_out_data(3),
      G => intIO_inst_slave_in_cyc_slave_in_addr_1_AND_62_o,
      Q => intIO_inst_LED_3_7
    );
  intIO_inst_LED_0 : LDC
    port map (
      CLR => reset_IBUF_5,
      D => core_master_out_data(0),
      G => intIO_inst_slave_in_cyc_slave_in_addr_1_AND_62_o,
      Q => intIO_inst_LED_0_10
    );
  intIO_inst_LED_1 : LDC
    port map (
      CLR => reset_IBUF_5,
      D => core_master_out_data(1),
      G => intIO_inst_slave_in_cyc_slave_in_addr_1_AND_62_o,
      Q => intIO_inst_LED_1_9
    );
  intIO_inst_LED_2 : LDC
    port map (
      CLR => reset_IBUF_5,
      D => core_master_out_data(2),
      G => intIO_inst_slave_in_cyc_slave_in_addr_1_AND_62_o,
      Q => intIO_inst_LED_2_8
    );
  core_master_out_addr_31 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_31_Q,
      Q => core_master_out_addr_31_Q
    );
  core_master_out_addr_27 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_27_Q,
      Q => core_master_out_addr_27_Q
    );
  core_master_out_addr_26 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_26_Q,
      Q => core_master_out_addr_26_Q
    );
  core_master_out_addr_25 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_25_Q,
      Q => core_master_out_addr_25_Q
    );
  core_master_out_addr_24 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_24_Q,
      Q => core_master_out_addr_24_Q
    );
  core_master_out_addr_23 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_23_Q,
      Q => core_master_out_addr_23_Q
    );
  core_master_out_addr_22 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_22_Q,
      Q => core_master_out_addr_22_Q
    );
  core_master_out_addr_21 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_21_Q,
      Q => core_master_out_addr_21_Q
    );
  core_master_out_addr_20 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_20_Q,
      Q => core_master_out_addr_20_Q
    );
  core_master_out_addr_19 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_19_Q,
      Q => core_master_out_addr_19_Q
    );
  core_master_out_addr_18 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_18_Q,
      Q => core_master_out_addr_18_Q
    );
  core_master_out_addr_17 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_17_Q,
      Q => core_master_out_addr_17_Q
    );
  core_master_out_addr_16 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_16_Q,
      Q => core_master_out_addr_16_Q
    );
  core_master_out_addr_15 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_15_Q,
      Q => core_master_out_addr_15_Q
    );
  core_master_out_addr_14 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_14_Q,
      Q => core_master_out_addr_14_Q
    );
  core_master_out_addr_13 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_13_Q,
      Q => core_master_out_addr_13_Q
    );
  core_master_out_addr_12 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_12_Q,
      Q => core_master_out_addr_12_Q
    );
  core_master_out_addr_11 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_11_Q,
      Q => core_master_out_addr_11_Q
    );
  core_master_out_addr_3 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_3_Q,
      Q => core_master_out_addr_3_Q
    );
  core_master_out_addr_2 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_2_Q,
      Q => core_master_out_addr_2_Q
    );
  core_master_out_addr_1 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_1_Q,
      Q => core_master_out_addr_1_Q
    );
  core_master_out_addr_0 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_out_mem_addr_0_Q,
      Q => core_master_out_addr_0_Q
    );
  core_master_out_data_7 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_mem_write(7),
      Q => core_master_out_data(7)
    );
  core_master_out_data_6 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_mem_write(6),
      Q => core_master_out_data(6)
    );
  core_master_out_data_5 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_mem_write(5),
      Q => core_master_out_data(5)
    );
  core_master_out_data_4 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_mem_write(4),
      Q => core_master_out_data(4)
    );
  core_master_out_data_3 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_mem_write(3),
      Q => core_master_out_data(3)
    );
  core_master_out_data_2 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_mem_write(2),
      Q => core_master_out_data(2)
    );
  core_master_out_data_1 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_mem_write(1),
      Q => core_master_out_data(1)
    );
  core_master_out_data_0 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_n0069_inv,
      D => core_zpu_core_foo_mem_write(0),
      Q => core_master_out_data(0)
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_31_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_30_Q_110,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_31_Q_109,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_31_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_31_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(31),
      I1 => core_zpu_core_foo_memBRead_stdlogic(31),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_31_Q_109
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_30_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_29_Q_112,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_30_Q_111,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_30_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_30_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_29_Q_112,
      DI => core_zpu_core_foo_memARead_stdlogic(30),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_30_Q_111,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_30_Q_110
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_30_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(30),
      I1 => core_zpu_core_foo_memBRead_stdlogic(30),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_30_Q_111
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_29_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_28_Q_114,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_29_Q_113,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_29_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_29_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_28_Q_114,
      DI => core_zpu_core_foo_memARead_stdlogic(29),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_29_Q_113,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_29_Q_112
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_29_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(29),
      I1 => core_zpu_core_foo_memBRead_stdlogic(29),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_29_Q_113
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_28_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_27_Q_116,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_28_Q_115,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_28_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_28_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_27_Q_116,
      DI => core_zpu_core_foo_memARead_stdlogic(28),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_28_Q_115,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_28_Q_114
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_28_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(28),
      I1 => core_zpu_core_foo_memBRead_stdlogic(28),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_28_Q_115
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_27_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_26_Q_118,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_27_Q_117,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_27_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_27_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_26_Q_118,
      DI => core_zpu_core_foo_memARead_stdlogic(27),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_27_Q_117,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_27_Q_116
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_27_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(27),
      I1 => core_zpu_core_foo_memBRead_stdlogic(27),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_27_Q_117
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_26_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_25_Q_120,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_26_Q_119,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_26_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_26_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_25_Q_120,
      DI => core_zpu_core_foo_memARead_stdlogic(26),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_26_Q_119,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_26_Q_118
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_26_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(26),
      I1 => core_zpu_core_foo_memBRead_stdlogic(26),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_26_Q_119
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_25_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_24_Q_122,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_25_Q_121,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_25_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_25_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_24_Q_122,
      DI => core_zpu_core_foo_memARead_stdlogic(25),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_25_Q_121,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_25_Q_120
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_25_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(25),
      I1 => core_zpu_core_foo_memBRead_stdlogic(25),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_25_Q_121
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_24_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_23_Q_124,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_24_Q_123,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_24_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_24_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_23_Q_124,
      DI => core_zpu_core_foo_memARead_stdlogic(24),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_24_Q_123,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_24_Q_122
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_24_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(24),
      I1 => core_zpu_core_foo_memARead_stdlogic(24),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_24_Q_123
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_23_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_22_Q_126,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_23_Q_125,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_23_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_23_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_22_Q_126,
      DI => core_zpu_core_foo_memARead_stdlogic(23),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_23_Q_125,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_23_Q_124
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_23_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(23),
      I1 => core_zpu_core_foo_memARead_stdlogic(23),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_23_Q_125
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_22_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_21_Q_128,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_22_Q_127,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_22_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_22_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_21_Q_128,
      DI => core_zpu_core_foo_memARead_stdlogic(22),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_22_Q_127,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_22_Q_126
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_22_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(22),
      I1 => core_zpu_core_foo_memARead_stdlogic(22),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_22_Q_127
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_21_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_20_Q_130,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_21_Q_129,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_21_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_21_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_20_Q_130,
      DI => core_zpu_core_foo_memARead_stdlogic(21),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_21_Q_129,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_21_Q_128
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_21_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(21),
      I1 => core_zpu_core_foo_memARead_stdlogic(21),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_21_Q_129
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_20_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_19_Q_132,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_20_Q_131,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_20_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_20_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_19_Q_132,
      DI => core_zpu_core_foo_memARead_stdlogic(20),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_20_Q_131,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_20_Q_130
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_20_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(20),
      I1 => core_zpu_core_foo_memARead_stdlogic(20),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_20_Q_131
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_19_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_18_Q_134,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_19_Q_133,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_19_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_19_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_18_Q_134,
      DI => core_zpu_core_foo_memARead_stdlogic(19),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_19_Q_133,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_19_Q_132
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_19_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(19),
      I1 => core_zpu_core_foo_memARead_stdlogic(19),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_19_Q_133
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_18_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_17_Q_136,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_18_Q_135,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_18_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_18_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_17_Q_136,
      DI => core_zpu_core_foo_memARead_stdlogic(18),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_18_Q_135,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_18_Q_134
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_18_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(18),
      I1 => core_zpu_core_foo_memARead_stdlogic(18),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_18_Q_135
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_17_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_16_Q_138,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_17_Q_137,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_17_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_17_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_16_Q_138,
      DI => core_zpu_core_foo_memARead_stdlogic(17),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_17_Q_137,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_17_Q_136
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_17_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(17),
      I1 => core_zpu_core_foo_memARead_stdlogic(17),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_17_Q_137
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_16_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_15_Q_140,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_16_Q_139,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_16_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_16_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_15_Q_140,
      DI => core_zpu_core_foo_memARead_stdlogic(16),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_16_Q_139,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_16_Q_138
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_16_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(16),
      I1 => core_zpu_core_foo_memARead_stdlogic(16),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_16_Q_139
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_15_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_14_Q_142,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_15_Q_141,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_15_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_15_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_14_Q_142,
      DI => core_zpu_core_foo_memARead_stdlogic(15),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_15_Q_141,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_15_Q_140
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_15_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(15),
      I1 => core_zpu_core_foo_memARead_stdlogic(15),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_15_Q_141
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_14_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_13_Q_144,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_14_Q_143,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_14_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_14_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_13_Q_144,
      DI => core_zpu_core_foo_memARead_stdlogic(14),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_14_Q_143,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_14_Q_142
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_14_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(14),
      I1 => core_zpu_core_foo_memARead_stdlogic(14),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_14_Q_143
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_13_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_12_Q_146,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_13_Q_145,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_13_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_13_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_12_Q_146,
      DI => core_zpu_core_foo_memARead_stdlogic(13),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_13_Q_145,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_13_Q_144
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_13_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(13),
      I1 => core_zpu_core_foo_memARead_stdlogic(13),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_13_Q_145
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_12_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_11_Q_148,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_12_Q_147,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_12_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_12_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_11_Q_148,
      DI => core_zpu_core_foo_memARead_stdlogic(12),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_12_Q_147,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_12_Q_146
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_12_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(12),
      I1 => core_zpu_core_foo_memARead_stdlogic(12),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_12_Q_147
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_11_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_10_Q_150,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_11_Q_149,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_11_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_11_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_10_Q_150,
      DI => core_zpu_core_foo_memARead_stdlogic(11),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_11_Q_149,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_11_Q_148
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_11_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(11),
      I1 => core_zpu_core_foo_memARead_stdlogic(11),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_11_Q_149
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_10_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_9_Q_152,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_10_Q_151,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_10_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_10_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_9_Q_152,
      DI => core_zpu_core_foo_memARead_stdlogic(10),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_10_Q_151,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_10_Q_150
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_10_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(10),
      I1 => core_zpu_core_foo_memARead_stdlogic(10),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_10_Q_151
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_9_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_8_Q_154,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_9_Q_153,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_9_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_9_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_8_Q_154,
      DI => core_zpu_core_foo_memARead_stdlogic(9),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_9_Q_153,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_9_Q_152
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_9_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(9),
      I1 => core_zpu_core_foo_memARead_stdlogic(9),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_9_Q_153
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_8_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_7_Q_156,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_8_Q_155,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_8_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_8_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_7_Q_156,
      DI => core_zpu_core_foo_memARead_stdlogic(8),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_8_Q_155,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_8_Q_154
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_8_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(8),
      I1 => core_zpu_core_foo_memARead_stdlogic(8),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_8_Q_155
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_7_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_6_Q_158,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_7_Q_157,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_7_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_7_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_6_Q_158,
      DI => core_zpu_core_foo_memARead_stdlogic(7),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_7_Q_157,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_7_Q_156
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_7_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(7),
      I1 => core_zpu_core_foo_memARead_stdlogic(7),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_7_Q_157
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_6_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_5_Q_160,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_6_Q_159,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_6_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_6_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_5_Q_160,
      DI => core_zpu_core_foo_memARead_stdlogic(6),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_6_Q_159,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_6_Q_158
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_6_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(6),
      I1 => core_zpu_core_foo_memARead_stdlogic(6),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_6_Q_159
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_5_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_4_Q_162,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_5_Q_161,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_5_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_5_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_4_Q_162,
      DI => core_zpu_core_foo_memARead_stdlogic(5),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_5_Q_161,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_5_Q_160
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_5_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(5),
      I1 => core_zpu_core_foo_memARead_stdlogic(5),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_5_Q_161
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_4_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_3_Q_164,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_4_Q_163,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_4_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_4_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_3_Q_164,
      DI => core_zpu_core_foo_memARead_stdlogic(4),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_4_Q_163,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_4_Q_162
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_4_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(4),
      I1 => core_zpu_core_foo_memARead_stdlogic(4),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_4_Q_163
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_3_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_2_Q_166,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_3_Q_165,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_3_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_3_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_2_Q_166,
      DI => core_zpu_core_foo_memARead_stdlogic(3),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_3_Q_165,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_3_Q_164
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_3_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(3),
      I1 => core_zpu_core_foo_memARead_stdlogic(3),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_3_Q_165
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_2_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_1_Q_168,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_2_Q_167,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_2_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_2_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_1_Q_168,
      DI => core_zpu_core_foo_memARead_stdlogic(2),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_2_Q_167,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_2_Q_166
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_2_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(2),
      I1 => core_zpu_core_foo_memARead_stdlogic(2),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_2_Q_167
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_1_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_0_Q_170,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_1_Q_169,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_1_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_1_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_0_Q_170,
      DI => core_zpu_core_foo_memARead_stdlogic(1),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_1_Q_169,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_1_Q_168
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_1_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(1),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_1_Q_169
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_xor_0_Q : XORCY
    port map (
      CI => dummy_out_ack,
      LI => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_0_Q_171,
      O => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_0_Q
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_0_Q : MUXCY
    port map (
      CI => dummy_out_ack,
      DI => core_zpu_core_foo_memARead_stdlogic(0),
      S => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_0_Q_171,
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_cy_0_Q_170
    );
  core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_0_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(0),
      I1 => core_zpu_core_foo_memARead_stdlogic(0),
      O => core_zpu_core_foo_Madd_memARead_31_memBRead_31_add_67_OUT_lut_0_Q_171
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_28_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_27_Q_172,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_28_rt_1281,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_28_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_27_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_26_Q_173,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_27_rt_1218,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_27_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_27_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_26_Q_173,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_27_rt_1218,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_27_Q_172
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_26_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_25_Q_174,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_26_rt_1219,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_26_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_26_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_25_Q_174,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_26_rt_1219,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_26_Q_173
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_25_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_24_Q_175,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_25_rt_1220,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_25_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_25_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_24_Q_175,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_25_rt_1220,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_25_Q_174
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_24_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_23_Q_176,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_24_rt_1221,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_24_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_24_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_23_Q_176,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_24_rt_1221,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_24_Q_175
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_23_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_22_Q_177,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_23_rt_1222,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_23_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_23_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_22_Q_177,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_23_rt_1222,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_23_Q_176
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_22_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_21_Q_178,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_22_rt_1223,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_22_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_22_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_21_Q_178,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_22_rt_1223,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_22_Q_177
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_21_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_20_Q_179,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_21_rt_1224,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_21_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_21_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_20_Q_179,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_21_rt_1224,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_21_Q_178
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_20_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_19_Q_180,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_20_rt_1225,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_20_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_20_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_19_Q_180,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_20_rt_1225,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_20_Q_179
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_19_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_18_Q_181,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_19_rt_1226,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_19_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_19_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_18_Q_181,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_19_rt_1226,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_19_Q_180
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_18_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_17_Q_182,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_18_rt_1227,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_18_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_18_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_17_Q_182,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_18_rt_1227,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_18_Q_181
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_17_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_16_Q_183,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_17_rt_1228,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_17_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_17_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_16_Q_183,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_17_rt_1228,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_17_Q_182
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_16_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_15_Q_184,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_16_rt_1229,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_16_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_16_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_15_Q_184,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_16_rt_1229,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_16_Q_183
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_15_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_14_Q_185,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_15_rt_1230,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_15_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_15_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_14_Q_185,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_15_rt_1230,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_15_Q_184
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_14_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_13_Q_186,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_14_rt_1231,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_14_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_14_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_13_Q_186,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_14_rt_1231,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_14_Q_185
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_13_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_12_Q_187,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_13_rt_1232,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_13_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_13_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_12_Q_187,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_13_rt_1232,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_13_Q_186
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_12_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_11_Q_188,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_12_rt_1233,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_12_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_12_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_11_Q_188,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_12_rt_1233,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_12_Q_187
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_11_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_10_Q_189,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_11_rt_1234,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_11_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_11_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_10_Q_189,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_11_rt_1234,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_11_Q_188
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_10_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_9_Q_190,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_10_rt_1235,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_10_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_10_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_9_Q_190,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_10_rt_1235,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_10_Q_189
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_9_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_8_Q_191,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_9_rt_1236,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_9_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_9_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_8_Q_191,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_9_rt_1236,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_9_Q_190
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_8_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_7_Q_192,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_8_rt_1237,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_8_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_8_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_7_Q_192,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_8_rt_1237,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_8_Q_191
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_7_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_6_Q_193,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_7_rt_1238,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_7_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_7_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_6_Q_193,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_7_rt_1238,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_7_Q_192
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_6_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_5_Q_194,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_6_rt_1239,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_6_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_6_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_5_Q_194,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_6_rt_1239,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_6_Q_193
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_5_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_4_Q_195,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_5_rt_1240,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_5_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_5_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_4_Q_195,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_5_rt_1240,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_5_Q_194
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_4_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_3_Q_196,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_4_rt_1241,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_4_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_4_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_3_Q_196,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_4_rt_1241,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_4_Q_195
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_3_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_2_Q_197,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_3_rt_1242,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_3_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_3_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_2_Q_197,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_3_rt_1242,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_3_Q_196
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_2_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_1_Q_198,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_2_rt_1243,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_2_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_2_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_1_Q_198,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_2_rt_1243,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_2_Q_197
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_1_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_0_Q_199,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_1_rt_1244,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_1_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_1_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_0_Q_199,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_1_rt_1244,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_1_Q_198
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_0_Q : XORCY
    port map (
      CI => dummy_out_ack,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_lut_0_Q,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_0_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_0_Q : MUXCY
    port map (
      CI => dummy_out_ack,
      DI => N0,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_lut_0_Q,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_0_Q_199
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_11_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_10_Q_201,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_11_rt_1282,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_11_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_10_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_9_Q_202,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_10_rt_1245,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_10_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_10_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_9_Q_202,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_10_rt_1245,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_10_Q_201
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_9_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_8_Q_203,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_9_rt_1246,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_9_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_9_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_8_Q_203,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_9_rt_1246,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_9_Q_202
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_8_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_7_Q_204,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_8_rt_1247,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_8_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_8_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_7_Q_204,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_8_rt_1247,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_8_Q_203
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_7_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_6_Q_205,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_7_rt_1248,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_7_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_7_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_6_Q_205,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_7_rt_1248,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_7_Q_204
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_6_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_5_Q_206,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_6_rt_1249,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_6_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_6_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_5_Q_206,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_6_rt_1249,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_6_Q_205
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_5_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_4_Q_207,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_5_rt_1250,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_5_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_5_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_4_Q_207,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_5_rt_1250,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_5_Q_206
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_4_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_3_Q_209,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_4_Q_208,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_4_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_4_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_3_Q_209,
      DI => core_zpu_core_foo_sp(6),
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_4_Q_208,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_4_Q_207
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_3_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_2_Q_211,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_3_Q_210,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_3_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_3_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_2_Q_211,
      DI => core_zpu_core_foo_sp(5),
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_3_Q_210,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_3_Q_209
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_3_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_sp(5),
      I1 => core_zpu_core_foo_opcode(3),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_3_Q_210
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_2_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_1_Q_213,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_2_Q_212,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_2_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_2_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_1_Q_213,
      DI => core_zpu_core_foo_sp(4),
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_2_Q_212,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_2_Q_211
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_2_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_sp(4),
      I1 => core_zpu_core_foo_opcode(2),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_2_Q_212
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_1_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_0_Q_215,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_1_Q_214,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_1_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_1_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_0_Q_215,
      DI => core_zpu_core_foo_sp(3),
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_1_Q_214,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_1_Q_213
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_1_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_sp(3),
      I1 => core_zpu_core_foo_opcode(1),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_1_Q_214
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_0_Q : XORCY
    port map (
      CI => dummy_out_ack,
      LI => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_0_Q_216,
      O => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_0_Q
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_0_Q : MUXCY
    port map (
      CI => dummy_out_ack,
      DI => core_zpu_core_foo_sp(2),
      S => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_0_Q_216,
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_0_Q_215
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_0_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => core_zpu_core_foo_sp(2),
      I1 => core_zpu_core_foo_opcode(0),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_0_Q_216
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_28_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_27_Q_218,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_28_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(28)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_27_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_26_Q_220,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_27_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(27)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_27_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_26_Q_220,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_27_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_27_Q_218
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_26_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_25_Q_222,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_26_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(26)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_26_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_25_Q_222,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_26_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_26_Q_220
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_25_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_24_Q_224,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_25_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(25)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_25_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_24_Q_224,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_25_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_25_Q_222
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_24_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_23_Q_226,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_24_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(24)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_24_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_23_Q_226,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_24_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_24_Q_224
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_23_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_22_Q_228,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_23_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(23)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_23_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_22_Q_228,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_23_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_23_Q_226
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_22_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_21_Q_230,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_22_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(22)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_22_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_21_Q_230,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_22_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_22_Q_228
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_21_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_20_Q_232,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_21_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(21)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_21_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_20_Q_232,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_21_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_21_Q_230
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_20_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_19_Q_234,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_20_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(20)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_20_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_19_Q_234,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_20_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_20_Q_232
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_19_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_18_Q_236,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_19_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(19)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_19_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_18_Q_236,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_19_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_19_Q_234
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_18_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_17_Q_238,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_18_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(18)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_18_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_17_Q_238,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_18_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_18_Q_236
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_17_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_16_Q_240,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_17_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(17)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_17_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_16_Q_240,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_17_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_17_Q_238
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_16_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_15_Q_242,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_16_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(16)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_16_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_15_Q_242,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_16_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_16_Q_240
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_15_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_14_Q_244,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_15_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(15)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_15_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_14_Q_244,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_15_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_15_Q_242
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_14_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_13_Q_246,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_14_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(14)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_14_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_13_Q_246,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_14_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_14_Q_244
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_13_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_12_Q_248,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_13_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(13)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_13_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_12_Q_248,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_13_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_13_Q_246
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_12_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_11_Q_250,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_12_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(12)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_12_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_11_Q_250,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_12_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_12_Q_248
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_11_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_10_Q_252,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_11_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(11)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_11_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_10_Q_252,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_11_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_11_Q_250
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_10_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_9_Q_254,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_10_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(10)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_10_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_9_Q_254,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_10_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_10_Q_252
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_9_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_8_Q_256,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_9_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(9)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_9_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_8_Q_256,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_9_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_9_Q_254
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_8_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_7_Q_258,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_8_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(8)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_8_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_7_Q_258,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_8_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_8_Q_256
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_7_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_6_Q_260,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_7_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(7)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_7_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_6_Q_260,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_7_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_7_Q_258
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_6_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_5_Q_262,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_6_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(6)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_6_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_5_Q_262,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_6_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_6_Q_260
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_5_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_4_Q_264,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_5_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(5)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_5_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_4_Q_264,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_5_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_5_Q_262
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_4_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_3_Q_266,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_4_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(4)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_4_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_3_Q_266,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_4_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_4_Q_264
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_3_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_2_Q_268,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_3_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(3)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_3_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_2_Q_268,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_3_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_3_Q_266
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_2_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_1_Q_270,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_2_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(2)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_2_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_1_Q_270,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_2_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_2_Q_268
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_1_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_0_Q_272,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_1_Q,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(1)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_1_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_0_Q_272,
      DI => N0,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_1_Q,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_1_Q_270
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_xor_0_Q : XORCY
    port map (
      CI => N0,
      LI => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_0_rt_1251,
      O => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(0)
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_0_Q : MUXCY
    port map (
      CI => N0,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_0_rt_1251,
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_0_Q_272
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_30_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_29_Q_273,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_30_rt_1283,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_30_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_29_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_28_Q_274,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_29_rt_1252,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_29_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_29_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_28_Q_274,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_29_rt_1252,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_29_Q_273
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_28_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_27_Q_275,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_28_rt_1253,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_28_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_28_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_27_Q_275,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_28_rt_1253,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_28_Q_274
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_27_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_26_Q_276,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_27_rt_1254,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_27_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_27_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_26_Q_276,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_27_rt_1254,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_27_Q_275
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_26_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_25_Q_277,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_26_rt_1255,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_26_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_26_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_25_Q_277,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_26_rt_1255,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_26_Q_276
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_25_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_24_Q_278,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_25_rt_1256,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_25_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_25_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_24_Q_278,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_25_rt_1256,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_25_Q_277
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_24_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_23_Q_279,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_24_rt_1257,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_24_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_24_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_23_Q_279,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_24_rt_1257,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_24_Q_278
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_23_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_22_Q_280,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_23_rt_1258,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_23_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_23_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_22_Q_280,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_23_rt_1258,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_23_Q_279
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_22_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_21_Q_281,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_22_rt_1259,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_22_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_22_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_21_Q_281,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_22_rt_1259,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_22_Q_280
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_21_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_20_Q_282,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_21_rt_1260,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_21_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_21_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_20_Q_282,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_21_rt_1260,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_21_Q_281
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_20_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_19_Q_283,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_20_rt_1261,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_20_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_20_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_19_Q_283,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_20_rt_1261,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_20_Q_282
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_19_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_18_Q_284,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_19_rt_1262,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_19_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_19_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_18_Q_284,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_19_rt_1262,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_19_Q_283
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_18_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_17_Q_285,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_18_rt_1263,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_18_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_18_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_17_Q_285,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_18_rt_1263,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_18_Q_284
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_17_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_16_Q_286,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_17_rt_1264,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_17_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_17_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_16_Q_286,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_17_rt_1264,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_17_Q_285
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_16_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_15_Q_287,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_16_rt_1265,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_16_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_16_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_15_Q_287,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_16_rt_1265,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_16_Q_286
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_15_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_14_Q_288,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_15_rt_1266,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_15_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_15_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_14_Q_288,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_15_rt_1266,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_15_Q_287
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_14_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_13_Q_289,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_14_rt_1267,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_14_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_14_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_13_Q_289,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_14_rt_1267,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_14_Q_288
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_13_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_12_Q_290,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_13_rt_1268,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_13_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_13_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_12_Q_290,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_13_rt_1268,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_13_Q_289
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_12_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_11_Q_291,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_12_rt_1269,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_12_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_12_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_11_Q_291,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_12_rt_1269,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_12_Q_290
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_11_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_10_Q_292,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_11_rt_1270,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_11_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_11_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_10_Q_292,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_11_rt_1270,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_11_Q_291
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_10_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_9_Q_293,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_10_rt_1271,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_10_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_10_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_9_Q_293,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_10_rt_1271,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_10_Q_292
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_9_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_8_Q_294,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_9_rt_1272,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_9_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_9_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_8_Q_294,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_9_rt_1272,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_9_Q_293
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_8_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_7_Q_295,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_8_rt_1273,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_8_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_8_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_7_Q_295,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_8_rt_1273,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_8_Q_294
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_7_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_6_Q_296,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_7_rt_1274,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_7_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_7_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_6_Q_296,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_7_rt_1274,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_7_Q_295
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_6_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_5_Q_297,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_6_rt_1275,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_6_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_6_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_5_Q_297,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_6_rt_1275,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_6_Q_296
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_5_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_4_Q_298,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_5_rt_1276,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_5_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_5_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_4_Q_298,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_5_rt_1276,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_5_Q_297
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_4_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_3_Q_299,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_4_rt_1277,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_4_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_4_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_3_Q_299,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_4_rt_1277,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_4_Q_298
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_3_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_2_Q_300,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_3_rt_1278,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_3_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_3_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_2_Q_300,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_3_rt_1278,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_3_Q_299
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_2_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_1_Q_301,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_2_rt_1279,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_2_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_2_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_1_Q_301,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_2_rt_1279,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_2_Q_300
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_1_Q : XORCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_0_Q_302,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_1_rt_1280,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_1_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_1_Q : MUXCY
    port map (
      CI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_0_Q_302,
      DI => dummy_out_ack,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_1_rt_1280,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_1_Q_301
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_0_Q : XORCY
    port map (
      CI => dummy_out_ack,
      LI => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_lut_0_Q,
      O => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_0_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_0_Q : MUXCY
    port map (
      CI => dummy_out_ack,
      DI => N0,
      S => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_lut_0_Q,
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_0_Q_302
    );
  core_zpu_core_foo_out_mem_readEnable : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_GND_14_o_Mux_81_o,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_out_mem_readEnable_59
    );
  core_zpu_core_foo_mem_write_7 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memBRead_stdlogic(7),
      Q => core_zpu_core_foo_mem_write(7)
    );
  core_zpu_core_foo_mem_write_6 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memBRead_stdlogic(6),
      Q => core_zpu_core_foo_mem_write(6)
    );
  core_zpu_core_foo_mem_write_5 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memBRead_stdlogic(5),
      Q => core_zpu_core_foo_mem_write(5)
    );
  core_zpu_core_foo_mem_write_4 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memBRead_stdlogic(4),
      Q => core_zpu_core_foo_mem_write(4)
    );
  core_zpu_core_foo_mem_write_3 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memBRead_stdlogic(3),
      Q => core_zpu_core_foo_mem_write(3)
    );
  core_zpu_core_foo_mem_write_2 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memBRead_stdlogic(2),
      Q => core_zpu_core_foo_mem_write(2)
    );
  core_zpu_core_foo_mem_write_1 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memBRead_stdlogic(1),
      Q => core_zpu_core_foo_mem_write(1)
    );
  core_zpu_core_foo_mem_write_0 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memBRead_stdlogic(0),
      Q => core_zpu_core_foo_mem_write(0)
    );
  core_zpu_core_foo_out_mem_addr_31 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(31),
      Q => core_zpu_core_foo_out_mem_addr_31_Q
    );
  core_zpu_core_foo_out_mem_addr_30 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(30),
      Q => core_zpu_core_foo_out_mem_addr_30_Q
    );
  core_zpu_core_foo_out_mem_addr_29 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(29),
      Q => core_zpu_core_foo_out_mem_addr_29_Q
    );
  core_zpu_core_foo_out_mem_addr_28 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(28),
      Q => core_zpu_core_foo_out_mem_addr_28_Q
    );
  core_zpu_core_foo_out_mem_addr_27 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(27),
      Q => core_zpu_core_foo_out_mem_addr_27_Q
    );
  core_zpu_core_foo_out_mem_addr_26 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(26),
      Q => core_zpu_core_foo_out_mem_addr_26_Q
    );
  core_zpu_core_foo_out_mem_addr_25 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(25),
      Q => core_zpu_core_foo_out_mem_addr_25_Q
    );
  core_zpu_core_foo_out_mem_addr_24 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(24),
      Q => core_zpu_core_foo_out_mem_addr_24_Q
    );
  core_zpu_core_foo_out_mem_addr_23 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(23),
      Q => core_zpu_core_foo_out_mem_addr_23_Q
    );
  core_zpu_core_foo_out_mem_addr_22 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(22),
      Q => core_zpu_core_foo_out_mem_addr_22_Q
    );
  core_zpu_core_foo_out_mem_addr_21 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(21),
      Q => core_zpu_core_foo_out_mem_addr_21_Q
    );
  core_zpu_core_foo_out_mem_addr_20 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(20),
      Q => core_zpu_core_foo_out_mem_addr_20_Q
    );
  core_zpu_core_foo_out_mem_addr_19 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(19),
      Q => core_zpu_core_foo_out_mem_addr_19_Q
    );
  core_zpu_core_foo_out_mem_addr_18 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(18),
      Q => core_zpu_core_foo_out_mem_addr_18_Q
    );
  core_zpu_core_foo_out_mem_addr_17 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(17),
      Q => core_zpu_core_foo_out_mem_addr_17_Q
    );
  core_zpu_core_foo_out_mem_addr_16 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(16),
      Q => core_zpu_core_foo_out_mem_addr_16_Q
    );
  core_zpu_core_foo_out_mem_addr_15 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(15),
      Q => core_zpu_core_foo_out_mem_addr_15_Q
    );
  core_zpu_core_foo_out_mem_addr_14 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(14),
      Q => core_zpu_core_foo_out_mem_addr_14_Q
    );
  core_zpu_core_foo_out_mem_addr_13 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(13),
      Q => core_zpu_core_foo_out_mem_addr_13_Q
    );
  core_zpu_core_foo_out_mem_addr_12 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(12),
      Q => core_zpu_core_foo_out_mem_addr_12_Q
    );
  core_zpu_core_foo_out_mem_addr_11 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(11),
      Q => core_zpu_core_foo_out_mem_addr_11_Q
    );
  core_zpu_core_foo_out_mem_addr_3 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(3),
      Q => core_zpu_core_foo_out_mem_addr_3_Q
    );
  core_zpu_core_foo_out_mem_addr_2 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(2),
      Q => core_zpu_core_foo_out_mem_addr_2_Q
    );
  core_zpu_core_foo_out_mem_addr_1 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(1),
      Q => core_zpu_core_foo_out_mem_addr_1_Q
    );
  core_zpu_core_foo_out_mem_addr_0 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_memARead_stdlogic(0),
      Q => core_zpu_core_foo_out_mem_addr_0_Q
    );
  core_zpu_core_foo_memAWrite_31 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_31_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(31)
    );
  core_zpu_core_foo_memAWrite_30 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_30_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(30)
    );
  core_zpu_core_foo_memAWrite_29 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_29_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(29)
    );
  core_zpu_core_foo_memAWrite_28 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_28_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(28)
    );
  core_zpu_core_foo_memAWrite_27 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_27_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(27)
    );
  core_zpu_core_foo_memAWrite_26 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_26_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(26)
    );
  core_zpu_core_foo_memAWrite_25 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_25_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(25)
    );
  core_zpu_core_foo_memAWrite_24 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_24_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(24)
    );
  core_zpu_core_foo_memAWrite_23 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_23_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(23)
    );
  core_zpu_core_foo_memAWrite_22 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_22_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(22)
    );
  core_zpu_core_foo_memAWrite_21 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_21_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(21)
    );
  core_zpu_core_foo_memAWrite_20 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_20_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(20)
    );
  core_zpu_core_foo_memAWrite_19 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_19_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(19)
    );
  core_zpu_core_foo_memAWrite_18 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_18_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(18)
    );
  core_zpu_core_foo_memAWrite_17 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_17_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(17)
    );
  core_zpu_core_foo_memAWrite_16 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_16_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(16)
    );
  core_zpu_core_foo_memAWrite_15 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_15_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(15)
    );
  core_zpu_core_foo_memAWrite_14 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_14_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(14)
    );
  core_zpu_core_foo_memAWrite_13 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_13_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(13)
    );
  core_zpu_core_foo_memAWrite_12 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_12_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(12)
    );
  core_zpu_core_foo_memAWrite_11 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_11_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(11)
    );
  core_zpu_core_foo_memAWrite_10 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_10_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(10)
    );
  core_zpu_core_foo_memAWrite_9 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_9_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(9)
    );
  core_zpu_core_foo_memAWrite_8 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_8_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(8)
    );
  core_zpu_core_foo_memAWrite_7 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_7_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(7)
    );
  core_zpu_core_foo_memAWrite_6 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_6_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(6)
    );
  core_zpu_core_foo_memAWrite_5 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_5_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(5)
    );
  core_zpu_core_foo_memAWrite_4 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_4_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(4)
    );
  core_zpu_core_foo_memAWrite_3 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_3_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(3)
    );
  core_zpu_core_foo_memAWrite_2 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_2_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(2)
    );
  core_zpu_core_foo_memAWrite_1 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_1_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(1)
    );
  core_zpu_core_foo_memAWrite_0 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_0_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWrite(0)
    );
  core_zpu_core_foo_memAAddr_13 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_11_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(13)
    );
  core_zpu_core_foo_memAAddr_12 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_10_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(12)
    );
  core_zpu_core_foo_memAAddr_11 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_9_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(11)
    );
  core_zpu_core_foo_memAAddr_10 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_8_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(10)
    );
  core_zpu_core_foo_memAAddr_9 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_7_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(9)
    );
  core_zpu_core_foo_memAAddr_8 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_6_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(8)
    );
  core_zpu_core_foo_memAAddr_7 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_5_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(7)
    );
  core_zpu_core_foo_memAAddr_6 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_4_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(6)
    );
  core_zpu_core_foo_memAAddr_5 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_3_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(5)
    );
  core_zpu_core_foo_memAAddr_4 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_2_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(4)
    );
  core_zpu_core_foo_memAAddr_3 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_1_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(3)
    );
  core_zpu_core_foo_memAAddr_2 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_0_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAAddr(2)
    );
  core_zpu_core_foo_memBAddr_13 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_11_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(13)
    );
  core_zpu_core_foo_memBAddr_12 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_10_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(12)
    );
  core_zpu_core_foo_memBAddr_11 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_9_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(11)
    );
  core_zpu_core_foo_memBAddr_10 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_8_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(10)
    );
  core_zpu_core_foo_memBAddr_9 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_7_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(9)
    );
  core_zpu_core_foo_memBAddr_8 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_6_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(8)
    );
  core_zpu_core_foo_memBAddr_7 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_5_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(7)
    );
  core_zpu_core_foo_memBAddr_6 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_4_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(6)
    );
  core_zpu_core_foo_memBAddr_5 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_3_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(5)
    );
  core_zpu_core_foo_memBAddr_4 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_2_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(4)
    );
  core_zpu_core_foo_memBAddr_3 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_1_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(3)
    );
  core_zpu_core_foo_memBAddr_2 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_0_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBAddr(2)
    );
  core_zpu_core_foo_sp_30 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(30),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(30)
    );
  core_zpu_core_foo_sp_29 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(29),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(29)
    );
  core_zpu_core_foo_sp_28 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(28),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(28)
    );
  core_zpu_core_foo_sp_27 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(27),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(27)
    );
  core_zpu_core_foo_sp_26 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(26),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(26)
    );
  core_zpu_core_foo_sp_25 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(25),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(25)
    );
  core_zpu_core_foo_sp_24 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(24),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(24)
    );
  core_zpu_core_foo_sp_23 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(23),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(23)
    );
  core_zpu_core_foo_sp_22 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(22),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(22)
    );
  core_zpu_core_foo_sp_21 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(21),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(21)
    );
  core_zpu_core_foo_sp_20 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(20),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(20)
    );
  core_zpu_core_foo_sp_19 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(19),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(19)
    );
  core_zpu_core_foo_sp_18 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(18),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(18)
    );
  core_zpu_core_foo_sp_17 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(17),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(17)
    );
  core_zpu_core_foo_sp_16 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(16),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(16)
    );
  core_zpu_core_foo_sp_15 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(15),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(15)
    );
  core_zpu_core_foo_sp_14 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(14),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(14)
    );
  core_zpu_core_foo_sp_13 : FDSE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(13),
      S => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(13)
    );
  core_zpu_core_foo_sp_12 : FDSE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(12),
      S => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(12)
    );
  core_zpu_core_foo_sp_11 : FDSE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(11),
      S => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(11)
    );
  core_zpu_core_foo_sp_10 : FDSE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(10),
      S => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(10)
    );
  core_zpu_core_foo_sp_9 : FDSE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(9),
      S => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(9)
    );
  core_zpu_core_foo_sp_8 : FDSE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(8),
      S => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(8)
    );
  core_zpu_core_foo_sp_7 : FDSE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(7),
      S => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(7)
    );
  core_zpu_core_foo_sp_6 : FDSE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(6),
      S => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(6)
    );
  core_zpu_core_foo_sp_5 : FDSE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(5),
      S => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(5)
    );
  core_zpu_core_foo_sp_4 : FDSE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(4),
      S => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(4)
    );
  core_zpu_core_foo_sp_3 : FDSE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(3),
      S => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(3)
    );
  core_zpu_core_foo_sp_2 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0372_inv_308,
      D => core_zpu_core_foo_n0282(2),
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_sp(2)
    );
  core_zpu_core_foo_memAWriteEnable : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_GND_14_o_Mux_76_o,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memAWriteEnable_725
    );
  core_zpu_core_foo_idim_flag : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_Mram_n04071,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_idim_flag_621
    );
  core_zpu_core_foo_out_mem_writeEnable : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_4_GND_14_o_Mux_82_o,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_out_mem_writeEnable_60
    );
  core_zpu_core_foo_decodedOpcode_4 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_4_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode(4)
    );
  core_zpu_core_foo_decodedOpcode_3 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_3_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode(3)
    );
  core_zpu_core_foo_decodedOpcode_2 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_2_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode(2)
    );
  core_zpu_core_foo_decodedOpcode_1 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_1_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode(1)
    );
  core_zpu_core_foo_pc_30 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_30_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(30)
    );
  core_zpu_core_foo_pc_29 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_29_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(29)
    );
  core_zpu_core_foo_pc_28 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_28_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(28)
    );
  core_zpu_core_foo_pc_27 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_27_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(27)
    );
  core_zpu_core_foo_pc_26 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_26_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(26)
    );
  core_zpu_core_foo_pc_25 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_25_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(25)
    );
  core_zpu_core_foo_pc_24 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_24_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(24)
    );
  core_zpu_core_foo_pc_23 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_23_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(23)
    );
  core_zpu_core_foo_pc_22 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_22_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(22)
    );
  core_zpu_core_foo_pc_21 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_21_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(21)
    );
  core_zpu_core_foo_pc_20 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_20_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(20)
    );
  core_zpu_core_foo_pc_19 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_19_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(19)
    );
  core_zpu_core_foo_pc_18 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_18_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(18)
    );
  core_zpu_core_foo_pc_17 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_17_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(17)
    );
  core_zpu_core_foo_pc_16 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_16_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(16)
    );
  core_zpu_core_foo_pc_15 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_15_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(15)
    );
  core_zpu_core_foo_pc_14 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_14_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(14)
    );
  core_zpu_core_foo_pc_13 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_13_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(13)
    );
  core_zpu_core_foo_pc_12 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_12_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(12)
    );
  core_zpu_core_foo_pc_11 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_11_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(11)
    );
  core_zpu_core_foo_pc_10 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_10_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(10)
    );
  core_zpu_core_foo_pc_9 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_9_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(9)
    );
  core_zpu_core_foo_pc_8 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_8_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(8)
    );
  core_zpu_core_foo_pc_7 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_7_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(7)
    );
  core_zpu_core_foo_pc_6 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_6_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(6)
    );
  core_zpu_core_foo_pc_5 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_5_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(5)
    );
  core_zpu_core_foo_pc_4 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_4_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(4)
    );
  core_zpu_core_foo_pc_3 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_3_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(3)
    );
  core_zpu_core_foo_pc_2 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_2_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(2)
    );
  core_zpu_core_foo_pc_1 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_1_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(1)
    );
  core_zpu_core_foo_pc_0 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_0_Q,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_pc(0)
    );
  core_zpu_core_foo_opcode_6 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_decodeControl_tOpcode(6),
      Q => core_zpu_core_foo_opcode(6)
    );
  core_zpu_core_foo_opcode_5 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_decodeControl_tOpcode(5),
      Q => core_zpu_core_foo_opcode(5)
    );
  core_zpu_core_foo_opcode_4 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_decodeControl_tOpcode(4),
      Q => core_zpu_core_foo_opcode(4)
    );
  core_zpu_core_foo_opcode_3 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_decodeControl_tOpcode(3),
      Q => core_zpu_core_foo_opcode(3)
    );
  core_zpu_core_foo_opcode_2 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_decodeControl_tOpcode(2),
      Q => core_zpu_core_foo_opcode(2)
    );
  core_zpu_core_foo_opcode_1 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_decodeControl_tOpcode(1),
      Q => core_zpu_core_foo_opcode(1)
    );
  core_zpu_core_foo_opcode_0 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_decodeControl_tOpcode(0),
      Q => core_zpu_core_foo_opcode(0)
    );
  core_zpu_core_foo_memBWriteEnable : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_Mram_n0407,
      R => core_zpu_core_foo_n0224_0,
      Q => core_zpu_core_foo_memBWriteEnable_724
    );
  uart_inst_1_tx_timer_output : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => uart_inst_1_tx_timer_GND_22_o_GND_22_o_MUX_247_o,
      R => reset_IBUF_5,
      Q => uart_inst_1_tx_timer_output_819
    );
  uart_inst_1_rx_byteAvail : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0128_inv,
      D => uart_inst_1_rx_byteAvail_rxProc_bitpos_3_MUX_223_o,
      R => reset_IBUF_5,
      Q => uart_inst_1_rx_byteAvail_821
    );
  uart_inst_1_rx_rxProc_bitpos_3 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_timer_output_820,
      D => uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_3_Q,
      R => reset_IBUF_5,
      Q => uart_inst_1_rx_rxProc_bitpos(3)
    );
  uart_inst_1_rx_rxProc_bitpos_2 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_timer_output_820,
      D => uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_2_Q,
      R => reset_IBUF_5,
      Q => uart_inst_1_rx_rxProc_bitpos(2)
    );
  uart_inst_1_rx_rxProc_bitpos_1 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_timer_output_820,
      D => uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_1_Q,
      R => reset_IBUF_5,
      Q => uart_inst_1_rx_rxProc_bitpos(1)
    );
  uart_inst_1_rx_rxProc_bitpos_0 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_timer_output_820,
      D => uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_0_Q,
      R => reset_IBUF_5,
      Q => uart_inst_1_rx_rxProc_bitpos(0)
    );
  uart_inst_1_rx_rxProc_samplecnt_1 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0124_inv,
      D => uart_inst_1_rx_rxProc_bitpos_3_GND_19_o_add_16_OUT_1_Q,
      R => uart_inst_1_rx_n0101,
      Q => uart_inst_1_rx_rxProc_samplecnt(1)
    );
  uart_inst_1_rx_rxProc_samplecnt_0 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0124_inv,
      D => uart_inst_1_rx_rxProc_bitpos_3_GND_19_o_add_16_OUT_0_Q,
      R => uart_inst_1_rx_n0101,
      Q => uart_inst_1_rx_rxProc_samplecnt(0)
    );
  uart_inst_1_rx_dataOut_7 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0109_inv,
      D => uart_inst_1_rx_receive(7),
      Q => uart_inst_1_rx_dataOut(7)
    );
  uart_inst_1_rx_dataOut_6 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0109_inv,
      D => uart_inst_1_rx_receive(6),
      Q => uart_inst_1_rx_dataOut(6)
    );
  uart_inst_1_rx_dataOut_5 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0109_inv,
      D => uart_inst_1_rx_receive(5),
      Q => uart_inst_1_rx_dataOut(5)
    );
  uart_inst_1_rx_dataOut_4 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0109_inv,
      D => uart_inst_1_rx_receive(4),
      Q => uart_inst_1_rx_dataOut(4)
    );
  uart_inst_1_rx_dataOut_3 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0109_inv,
      D => uart_inst_1_rx_receive(3),
      Q => uart_inst_1_rx_dataOut(3)
    );
  uart_inst_1_rx_dataOut_2 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0109_inv,
      D => uart_inst_1_rx_receive(2),
      Q => uart_inst_1_rx_dataOut(2)
    );
  uart_inst_1_rx_dataOut_1 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0109_inv,
      D => uart_inst_1_rx_receive(1),
      Q => uart_inst_1_rx_dataOut(1)
    );
  uart_inst_1_rx_dataOut_0 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0109_inv,
      D => uart_inst_1_rx_receive(0),
      Q => uart_inst_1_rx_dataOut(0)
    );
  uart_inst_1_rx_receive_7 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0119_inv_838,
      D => uart_inst_1_rx_receive_7_rxd_MUX_215_o,
      Q => uart_inst_1_rx_receive(7)
    );
  uart_inst_1_rx_receive_6 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0119_inv_838,
      D => uart_inst_1_rx_receive_6_rxd_MUX_216_o,
      Q => uart_inst_1_rx_receive(6)
    );
  uart_inst_1_rx_receive_5 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0119_inv_838,
      D => uart_inst_1_rx_receive_5_rxd_MUX_217_o,
      Q => uart_inst_1_rx_receive(5)
    );
  uart_inst_1_rx_receive_4 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0119_inv_838,
      D => uart_inst_1_rx_receive_4_rxd_MUX_218_o,
      Q => uart_inst_1_rx_receive(4)
    );
  uart_inst_1_rx_receive_3 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0119_inv_838,
      D => uart_inst_1_rx_receive_3_rxd_MUX_219_o,
      Q => uart_inst_1_rx_receive(3)
    );
  uart_inst_1_rx_receive_2 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0119_inv_838,
      D => uart_inst_1_rx_receive_2_rxd_MUX_220_o,
      Q => uart_inst_1_rx_receive(2)
    );
  uart_inst_1_rx_receive_1 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0119_inv_838,
      D => uart_inst_1_rx_receive_1_rxd_MUX_221_o,
      Q => uart_inst_1_rx_receive(1)
    );
  uart_inst_1_rx_receive_0 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_rx_n0119_inv_838,
      D => uart_inst_1_rx_receive_0_rxd_MUX_222_o,
      Q => uart_inst_1_rx_receive(0)
    );
  uart_inst_1_tx_txBufferReady : FDSE
    generic map(
      INIT => '1'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_timer_output_819,
      D => uart_inst_1_tx_txProc_bitpos_3_GND_20_o_Mux_6_o,
      S => reset_IBUF_5,
      Q => uart_inst_1_tx_txBufferReady_44
    );
  uart_inst_1_tx_txProc_bitpos_3 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_timer_output_819,
      D => uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_3_Q,
      R => reset_IBUF_5,
      Q => uart_inst_1_tx_txProc_bitpos(3)
    );
  uart_inst_1_tx_txProc_bitpos_2 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_timer_output_819,
      D => uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_2_Q,
      R => reset_IBUF_5,
      Q => uart_inst_1_tx_txProc_bitpos(2)
    );
  uart_inst_1_tx_txProc_bitpos_1 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_timer_output_819,
      D => uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_1_Q,
      R => reset_IBUF_5,
      Q => uart_inst_1_tx_txProc_bitpos(1)
    );
  uart_inst_1_tx_txProc_bitpos_0 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_timer_output_819,
      D => uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_0_Q,
      R => reset_IBUF_5,
      Q => uart_inst_1_tx_txProc_bitpos(0)
    );
  uart_inst_1_tx_BufferLoaded : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0095_inv,
      D => uart_inst_1_tx_BufferLoaded_txProc_bitpos_3_MUX_235_o,
      R => reset_IBUF_5,
      Q => uart_inst_1_tx_BufferLoaded_879
    );
  uart_inst_1_tx_transmit_7 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0078_inv,
      D => uart_inst_1_tx_transmitBuffer(7),
      Q => uart_inst_1_tx_transmit(7)
    );
  uart_inst_1_tx_transmit_6 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0078_inv,
      D => uart_inst_1_tx_transmitBuffer(6),
      Q => uart_inst_1_tx_transmit(6)
    );
  uart_inst_1_tx_transmit_5 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0078_inv,
      D => uart_inst_1_tx_transmitBuffer(5),
      Q => uart_inst_1_tx_transmit(5)
    );
  uart_inst_1_tx_transmit_4 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0078_inv,
      D => uart_inst_1_tx_transmitBuffer(4),
      Q => uart_inst_1_tx_transmit(4)
    );
  uart_inst_1_tx_transmit_3 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0078_inv,
      D => uart_inst_1_tx_transmitBuffer(3),
      Q => uart_inst_1_tx_transmit(3)
    );
  uart_inst_1_tx_transmit_2 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0078_inv,
      D => uart_inst_1_tx_transmitBuffer(2),
      Q => uart_inst_1_tx_transmit(2)
    );
  uart_inst_1_tx_transmit_1 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0078_inv,
      D => uart_inst_1_tx_transmitBuffer(1),
      Q => uart_inst_1_tx_transmit(1)
    );
  uart_inst_1_tx_transmit_0 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0078_inv,
      D => uart_inst_1_tx_transmitBuffer(0),
      Q => uart_inst_1_tx_transmit(0)
    );
  uart_inst_1_tx_transmitBuffer_7 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0068_inv,
      D => core_master_out_data(7),
      Q => uart_inst_1_tx_transmitBuffer(7)
    );
  uart_inst_1_tx_transmitBuffer_6 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0068_inv,
      D => core_master_out_data(6),
      Q => uart_inst_1_tx_transmitBuffer(6)
    );
  uart_inst_1_tx_transmitBuffer_5 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0068_inv,
      D => core_master_out_data(5),
      Q => uart_inst_1_tx_transmitBuffer(5)
    );
  uart_inst_1_tx_transmitBuffer_4 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0068_inv,
      D => core_master_out_data(4),
      Q => uart_inst_1_tx_transmitBuffer(4)
    );
  uart_inst_1_tx_transmitBuffer_3 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0068_inv,
      D => core_master_out_data(3),
      Q => uart_inst_1_tx_transmitBuffer(3)
    );
  uart_inst_1_tx_transmitBuffer_2 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0068_inv,
      D => core_master_out_data(2),
      Q => uart_inst_1_tx_transmitBuffer(2)
    );
  uart_inst_1_tx_transmitBuffer_1 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0068_inv,
      D => core_master_out_data(1),
      Q => uart_inst_1_tx_transmitBuffer(1)
    );
  uart_inst_1_tx_transmitBuffer_0 : FDE
    port map (
      C => clk_test_out_OBUF_11,
      CE => uart_inst_1_tx_n0068_inv,
      D => core_master_out_data(0),
      Q => uart_inst_1_tx_transmitBuffer(0)
    );
  uart_inst_1_rx_timer_countGen_counter_cnt_6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_6_Q,
      Q => uart_inst_1_rx_timer_countGen_counter_cnt(6)
    );
  uart_inst_1_rx_timer_countGen_counter_cnt_5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_5_Q,
      Q => uart_inst_1_rx_timer_countGen_counter_cnt(5)
    );
  uart_inst_1_rx_timer_countGen_counter_cnt_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_4_Q,
      Q => uart_inst_1_rx_timer_countGen_counter_cnt(4)
    );
  uart_inst_1_rx_timer_countGen_counter_cnt_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_3_Q,
      Q => uart_inst_1_rx_timer_countGen_counter_cnt(3)
    );
  uart_inst_1_rx_timer_countGen_counter_cnt_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_2_Q,
      Q => uart_inst_1_rx_timer_countGen_counter_cnt(2)
    );
  uart_inst_1_rx_timer_countGen_counter_cnt_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_1_Q,
      Q => uart_inst_1_rx_timer_countGen_counter_cnt(1)
    );
  uart_inst_1_rx_timer_countGen_counter_cnt_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_0_Q,
      Q => uart_inst_1_rx_timer_countGen_counter_cnt(0)
    );
  uart_inst_1_rx_timer_output : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => uart_inst_1_rx_timer_GND_21_o_GND_21_o_MUX_245_o,
      R => reset_IBUF_5,
      Q => uart_inst_1_rx_timer_output_820
    );
  Mmux_master_in_ack11 : LUT6
    generic map(
      INIT => X"2400200004000000"
    )
    port map (
      I0 => core_master_out_addr_20_Q,
      I1 => core_master_out_addr_19_Q,
      I2 => core_master_out_addr_12_Q,
      I3 => master_out_addr_31_PWR_9_o_equal_4_o_31_1,
      I4 => intIO_in_cyc,
      I5 => uart1_in_cyc,
      O => master_in_ack
    );
  intIO_inst_slave_in_cyc_slave_in_addr_1_AND_62_o1 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => intIO_in_cyc,
      I1 => core_master_out_we_42,
      I2 => core_master_out_addr_1_Q,
      I3 => core_master_out_addr_0_Q,
      O => intIO_inst_slave_in_cyc_slave_in_addr_1_AND_62_o
    );
  core_n0069_inv1 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => core_n0066_inv12_56,
      O => core_n0069_inv
    );
  core_zpu_core_foo_n0526_4_1 : LUT4
    generic map(
      INIT => X"0004"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode_4_2_1376,
      I1 => core_zpu_core_foo_decodedOpcode(1),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      O => core_zpu_core_foo_n0526
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT11011 : LUT5
    generic map(
      INIT => X"00000100"
    )
    port map (
      I0 => core_zpu_core_foo_idim_flag_1_1379,
      I1 => core_zpu_core_foo_decodedOpcode_2_2_1377,
      I2 => core_zpu_core_foo_decodedOpcode_3_2_1375,
      I3 => core_zpu_core_foo_opcode(6),
      I4 => core_zpu_core_foo_decodedOpcode_4_2_1376,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101
    );
  core_zpu_core_foo_Mmux_n02821121 : LUT5
    generic map(
      INIT => X"D0D0D0D2"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode_3_3_1383,
      I1 => core_zpu_core_foo_decodedOpcode_4_2_1376,
      I2 => core_zpu_core_foo_decodedOpcode(1),
      I3 => core_zpu_core_foo_decodedOpcode(0),
      I4 => core_zpu_core_foo_decodedOpcode(2),
      O => core_zpu_core_foo_Mmux_n0282112
    );
  core_zpu_core_foo_n0435_4_1 : LUT5
    generic map(
      INIT => X"00000400"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      O => core_zpu_core_foo_n0435
    );
  core_zpu_core_foo_state_FSM_FFd5_In111 : LUT5
    generic map(
      INIT => X"00000004"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_decodedOpcode(4),
      I4 => core_zpu_core_foo_decodedOpcode(3),
      O => core_zpu_core_foo_Mram_n0407
    );
  core_zpu_core_foo_n0457_4_1 : LUT5
    generic map(
      INIT => X"00000020"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      O => core_zpu_core_foo_n0457
    );
  core_zpu_core_foo_Mram_n0407231 : LUT4
    generic map(
      INIT => X"0100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode_0_2_1378,
      I1 => core_zpu_core_foo_decodedOpcode_2_2_1377,
      I2 => core_zpu_core_foo_decodedOpcode_3_2_1375,
      I3 => core_zpu_core_foo_decodedOpcode_4_2_1376,
      O => core_zpu_core_foo_Mram_n040723
    );
  core_zpu_core_foo_n0224_4_1 : LUT4
    generic map(
      INIT => X"0004"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd5_583,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => core_zpu_core_foo_n0224
    );
  core_zpu_core_foo_Mmux_n02821521 : LUT6
    generic map(
      INIT => X"CCCCCCCC0003CCDD"
    )
    port map (
      I0 => core_zpu_core_foo_idim_flag_621,
      I1 => core_zpu_core_foo_decodedOpcode(1),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_decodedOpcode(2),
      I4 => core_zpu_core_foo_decodedOpcode(3),
      I5 => core_zpu_core_foo_decodedOpcode(4),
      O => core_zpu_core_foo_Mmux_n0282152_96
    );
  core_zpu_core_foo_n05651 : LUT4
    generic map(
      INIT => X"4000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_1_1380,
      I1 => core_zpu_core_foo_state_FSM_FFd4_1_1381,
      I2 => core_zpu_core_foo_state_FSM_FFd5_583,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => core_zpu_core_foo_n0565
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT91 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_6_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(8),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_6_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_6_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT81 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_5_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(7),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_5_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_5_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT71 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_4_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(6),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_4_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_4_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT61 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_3_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(5),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_3_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_3_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT51 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_2_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(4),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_2_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_2_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT41 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_1_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(3),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_1_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_1_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT31 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_11_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(13),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_11_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_11_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT21 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_10_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(12),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_10_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_10_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT121 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_9_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(11),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_9_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_9_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT111 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_8_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(10),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_8_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_8_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT101 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_7_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(9),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_7_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_7_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_79_OUT11 : LUT6
    generic map(
      INIT => X"AABFAA8CAAB3AA80"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_0_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_pc(2),
      I5 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_0_Q,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_79_OUT_0_Q
    );
  core_zpu_core_foo_Mmux_n02821211 : LUT6
    generic map(
      INIT => X"FFFFFF1FFFFFFF10"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      I5 => core_zpu_core_foo_Mmux_n0282111,
      O => core_zpu_core_foo_Mmux_n0282121_97
    );
  core_zpu_core_foo_Mmux_decodeControl_tOpcode81 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => core_zpu_core_foo_pc(0),
      I1 => core_zpu_core_foo_pc(1),
      I2 => core_zpu_core_foo_memBRead_stdlogic(23),
      I3 => core_zpu_core_foo_memBRead_stdlogic(7),
      I4 => core_zpu_core_foo_memBRead_stdlogic(15),
      I5 => core_zpu_core_foo_memBRead_stdlogic(31),
      O => core_zpu_core_foo_decodeControl_tOpcode(7)
    );
  core_zpu_core_foo_Mmux_decodeControl_tOpcode71 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => core_zpu_core_foo_pc(0),
      I1 => core_zpu_core_foo_pc(1),
      I2 => core_zpu_core_foo_memBRead_stdlogic(22),
      I3 => core_zpu_core_foo_memBRead_stdlogic(6),
      I4 => core_zpu_core_foo_memBRead_stdlogic(14),
      I5 => core_zpu_core_foo_memBRead_stdlogic(30),
      O => core_zpu_core_foo_decodeControl_tOpcode(6)
    );
  core_zpu_core_foo_Mmux_decodeControl_tOpcode61 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => core_zpu_core_foo_pc(0),
      I1 => core_zpu_core_foo_pc(1),
      I2 => core_zpu_core_foo_memBRead_stdlogic(21),
      I3 => core_zpu_core_foo_memBRead_stdlogic(5),
      I4 => core_zpu_core_foo_memBRead_stdlogic(13),
      I5 => core_zpu_core_foo_memBRead_stdlogic(29),
      O => core_zpu_core_foo_decodeControl_tOpcode(5)
    );
  core_zpu_core_foo_Mmux_decodeControl_tOpcode51 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => core_zpu_core_foo_pc(0),
      I1 => core_zpu_core_foo_pc(1),
      I2 => core_zpu_core_foo_memBRead_stdlogic(20),
      I3 => core_zpu_core_foo_memBRead_stdlogic(4),
      I4 => core_zpu_core_foo_memBRead_stdlogic(12),
      I5 => core_zpu_core_foo_memBRead_stdlogic(28),
      O => core_zpu_core_foo_decodeControl_tOpcode(4)
    );
  core_zpu_core_foo_Mmux_decodeControl_tOpcode41 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => core_zpu_core_foo_pc(0),
      I1 => core_zpu_core_foo_pc(1),
      I2 => core_zpu_core_foo_memBRead_stdlogic(19),
      I3 => core_zpu_core_foo_memBRead_stdlogic(3),
      I4 => core_zpu_core_foo_memBRead_stdlogic(11),
      I5 => core_zpu_core_foo_memBRead_stdlogic(27),
      O => core_zpu_core_foo_decodeControl_tOpcode(3)
    );
  core_zpu_core_foo_Mmux_decodeControl_tOpcode31 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => core_zpu_core_foo_pc(0),
      I1 => core_zpu_core_foo_pc(1),
      I2 => core_zpu_core_foo_memBRead_stdlogic(18),
      I3 => core_zpu_core_foo_memBRead_stdlogic(2),
      I4 => core_zpu_core_foo_memBRead_stdlogic(10),
      I5 => core_zpu_core_foo_memBRead_stdlogic(26),
      O => core_zpu_core_foo_decodeControl_tOpcode(2)
    );
  core_zpu_core_foo_Mmux_decodeControl_tOpcode21 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => core_zpu_core_foo_pc(0),
      I1 => core_zpu_core_foo_pc(1),
      I2 => core_zpu_core_foo_memBRead_stdlogic(17),
      I3 => core_zpu_core_foo_memBRead_stdlogic(1),
      I4 => core_zpu_core_foo_memBRead_stdlogic(9),
      I5 => core_zpu_core_foo_memBRead_stdlogic(25),
      O => core_zpu_core_foo_decodeControl_tOpcode(1)
    );
  core_zpu_core_foo_Mmux_decodeControl_tOpcode11 : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => core_zpu_core_foo_pc(0),
      I1 => core_zpu_core_foo_pc(1),
      I2 => core_zpu_core_foo_memBRead_stdlogic(16),
      I3 => core_zpu_core_foo_memBRead_stdlogic(0),
      I4 => core_zpu_core_foo_memBRead_stdlogic(8),
      I5 => core_zpu_core_foo_memBRead_stdlogic(24),
      O => core_zpu_core_foo_decodeControl_tOpcode(0)
    );
  core_zpu_core_foo_n0515_4_2 : LUT5
    generic map(
      INIT => X"44001101"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode_4_2_1376,
      I1 => core_zpu_core_foo_decodedOpcode(1),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_decodedOpcode(2),
      I4 => core_zpu_core_foo_decodedOpcode(3),
      O => core_zpu_core_foo_n0515
    );
  core_zpu_core_foo_Mmux_n02821111 : LUT4
    generic map(
      INIT => X"FE50"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      O => core_zpu_core_foo_Mmux_n0282111
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT2811 : LUT4
    generic map(
      INIT => X"0007"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_Mram_n040723,
      I2 => core_zpu_core_foo_n0457,
      I3 => core_zpu_core_foo_n0435,
      O => core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT281_105
    );
  core_zpu_core_foo_Mmux_n02821141 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      O => core_zpu_core_foo_Mmux_n0282114
    );
  core_zpu_core_foo_n0515_4_11 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode_4_2_1376,
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_decodedOpcode(3),
      O => core_zpu_core_foo_n0515_4_1
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT281 : LUT6
    generic map(
      INIT => X"EEEEFCCCAAAAF000"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(1),
      I1 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_6_Q,
      I2 => core_zpu_core_foo_memARead_stdlogic(6),
      I3 => core_zpu_core_foo_n0435,
      I4 => core_zpu_core_foo_n0457,
      I5 => core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT281_105,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_6_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT291 : LUT6
    generic map(
      INIT => X"EEEEFCCCAAAAF000"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(2),
      I1 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_7_Q,
      I2 => core_zpu_core_foo_memARead_stdlogic(7),
      I3 => core_zpu_core_foo_n0435,
      I4 => core_zpu_core_foo_n0457,
      I5 => core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT281_105,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_7_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT301 : LUT6
    generic map(
      INIT => X"EEEEFCCCAAAAF000"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(3),
      I1 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_8_Q,
      I2 => core_zpu_core_foo_memARead_stdlogic(8),
      I3 => core_zpu_core_foo_n0435,
      I4 => core_zpu_core_foo_n0457,
      I5 => core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT281_105,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_8_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT311 : LUT6
    generic map(
      INIT => X"EEEEFCCCAAAAF000"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(4),
      I1 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_9_Q,
      I2 => core_zpu_core_foo_memARead_stdlogic(9),
      I3 => core_zpu_core_foo_n0435,
      I4 => core_zpu_core_foo_n0457,
      I5 => core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT281_105,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_9_Q
    );
  core_zpu_core_foo_Mram_n0407211 : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode_3_3_1383,
      I1 => core_zpu_core_foo_decodedOpcode(1),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      O => core_zpu_core_foo_Mram_n040721
    );
  core_zpu_core_foo_Mmux_state_4_GND_14_o_Mux_81_o11 : LUT5
    generic map(
      INIT => X"40000000"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(31),
      I3 => core_zpu_core_foo_n0515_4_1,
      I4 => core_zpu_core_foo_n0224,
      O => core_zpu_core_foo_state_4_GND_14_o_Mux_81_o
    );
  core_zpu_core_foo_state_state_4_GND_14_o_Mux_82_o1 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_state_4_GND_14_o_Mux_82_o
    );
  core_zpu_core_foo_n02671 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => core_zpu_core_foo_decodeControl_tOpcode(7),
      O => core_zpu_core_foo_n0267
    );
  uart_inst_1_Mmux_slave_out_data111 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => core_master_out_addr_3_Q,
      I1 => core_master_out_addr_2_Q,
      I2 => uart1_in_cyc,
      O => uart_inst_1_Mmux_slave_out_data11
    );
  uart_inst_1_uart_write1 : LUT4
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => core_master_out_we_42,
      I1 => core_master_out_addr_2_Q,
      I2 => core_master_out_addr_3_Q,
      I3 => uart1_in_cyc,
      O => uart_inst_1_uart_write
    );
  uart_inst_1_tx_timer_Mmux_GND_22_o_GND_22_o_MUX_247_o11 : LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      I0 => uart_inst_1_rx_timer_output_820,
      I1 => uart_inst_1_tx_timer_countGen_counter_cnt(1),
      I2 => uart_inst_1_tx_timer_countGen_counter_cnt(0),
      O => uart_inst_1_tx_timer_GND_22_o_GND_22_o_MUX_247_o
    );
  uart_inst_1_rx_n01011 : LUT5
    generic map(
      INIT => X"08000000"
    )
    port map (
      I0 => uart_inst_1_rx_timer_output_820,
      I1 => uart_inst_1_rx_rxProc_samplecnt(1),
      I2 => reset_IBUF_5,
      I3 => uart_inst_1_rx_Madd_rxProc_bitpos_3_GND_19_o_add_16_OUT_xor_1_11,
      I4 => uart_inst_1_rx_rxProc_samplecnt(0),
      O => uart_inst_1_rx_n0101
    );
  uart_inst_1_rx_Mmux_receive_5_rxd_MUX_217_o11 : LUT5
    generic map(
      INIT => X"EAAA2AAA"
    )
    port map (
      I0 => uart_inst_1_rx_receive(5),
      I1 => uart_inst_1_rx_rxProc_bitpos(0),
      I2 => uart_inst_1_rx_rxProc_bitpos(1),
      I3 => uart_inst_1_rx_rxProc_bitpos(2),
      I4 => uart_rx1_IBUF_6,
      O => uart_inst_1_rx_receive_5_rxd_MUX_217_o
    );
  uart_inst_1_rx_Mmux_receive_3_rxd_MUX_219_o11 : LUT5
    generic map(
      INIT => X"BAAA8AAA"
    )
    port map (
      I0 => uart_inst_1_rx_receive(3),
      I1 => uart_inst_1_rx_rxProc_bitpos(1),
      I2 => uart_inst_1_rx_rxProc_bitpos(2),
      I3 => uart_inst_1_rx_rxProc_bitpos(0),
      I4 => uart_rx1_IBUF_6,
      O => uart_inst_1_rx_receive_3_rxd_MUX_219_o
    );
  uart_inst_1_rx_Mmux_receive_2_rxd_MUX_220_o11 : LUT5
    generic map(
      INIT => X"ABAAA8AA"
    )
    port map (
      I0 => uart_inst_1_rx_receive(2),
      I1 => uart_inst_1_rx_rxProc_bitpos(0),
      I2 => uart_inst_1_rx_rxProc_bitpos(1),
      I3 => uart_inst_1_rx_rxProc_bitpos(2),
      I4 => uart_rx1_IBUF_6,
      O => uart_inst_1_rx_receive_2_rxd_MUX_220_o
    );
  uart_inst_1_rx_Mmux_receive_4_rxd_MUX_218_o11 : LUT5
    generic map(
      INIT => X"BAAA8AAA"
    )
    port map (
      I0 => uart_inst_1_rx_receive(4),
      I1 => uart_inst_1_rx_rxProc_bitpos(0),
      I2 => uart_inst_1_rx_rxProc_bitpos(1),
      I3 => uart_inst_1_rx_rxProc_bitpos(2),
      I4 => uart_rx1_IBUF_6,
      O => uart_inst_1_rx_receive_4_rxd_MUX_218_o
    );
  uart_inst_1_rx_Mmux_receive_1_rxd_MUX_221_o11 : LUT5
    generic map(
      INIT => X"BAAA8AAA"
    )
    port map (
      I0 => uart_inst_1_rx_receive(1),
      I1 => uart_inst_1_rx_rxProc_bitpos(2),
      I2 => uart_inst_1_rx_rxProc_bitpos(1),
      I3 => uart_inst_1_rx_rxProc_bitpos(0),
      I4 => uart_rx1_IBUF_6,
      O => uart_inst_1_rx_receive_1_rxd_MUX_221_o
    );
  uart_inst_1_rx_Mmux_receive_7_rxd_MUX_215_o11 : LUT5
    generic map(
      INIT => X"ABAAA8AA"
    )
    port map (
      I0 => uart_inst_1_rx_receive(7),
      I1 => uart_inst_1_rx_rxProc_bitpos(2),
      I2 => uart_inst_1_rx_rxProc_bitpos(1),
      I3 => uart_inst_1_rx_rxProc_bitpos(0),
      I4 => uart_rx1_IBUF_6,
      O => uart_inst_1_rx_receive_7_rxd_MUX_215_o
    );
  uart_inst_1_rx_Mmux_receive_6_rxd_MUX_216_o11 : LUT5
    generic map(
      INIT => X"AAABAAA8"
    )
    port map (
      I0 => uart_inst_1_rx_receive(6),
      I1 => uart_inst_1_rx_rxProc_bitpos(0),
      I2 => uart_inst_1_rx_rxProc_bitpos(1),
      I3 => uart_inst_1_rx_rxProc_bitpos(2),
      I4 => uart_rx1_IBUF_6,
      O => uart_inst_1_rx_receive_6_rxd_MUX_216_o
    );
  uart_inst_1_rx_Mmux_receive_0_rxd_MUX_222_o11 : LUT5
    generic map(
      INIT => X"ABAAA8AA"
    )
    port map (
      I0 => uart_inst_1_rx_receive(0),
      I1 => uart_inst_1_rx_rxProc_bitpos(2),
      I2 => uart_inst_1_rx_rxProc_bitpos(0),
      I3 => uart_inst_1_rx_rxProc_bitpos(1),
      I4 => uart_rx1_IBUF_6,
      O => uart_inst_1_rx_receive_0_rxd_MUX_222_o
    );
  uart_inst_1_rx_Mmux_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT21 : LUT6
    generic map(
      INIT => X"6AAA48886AAA6AAA"
    )
    port map (
      I0 => uart_inst_1_rx_rxProc_bitpos(1),
      I1 => uart_inst_1_rx_rxProc_bitpos(0),
      I2 => uart_inst_1_rx_rxProc_samplecnt(0),
      I3 => uart_inst_1_rx_rxProc_samplecnt(1),
      I4 => uart_inst_1_rx_rxProc_bitpos(2),
      I5 => uart_inst_1_rx_rxProc_bitpos(3),
      O => uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_1_Q
    );
  uart_inst_1_rx_n0109_inv1 : LUT6
    generic map(
      INIT => X"0000020000000000"
    )
    port map (
      I0 => uart_inst_1_rx_timer_output_820,
      I1 => reset_IBUF_5,
      I2 => uart_inst_1_rx_rxProc_bitpos(0),
      I3 => uart_inst_1_rx_rxProc_bitpos(1),
      I4 => uart_inst_1_rx_rxProc_bitpos(2),
      I5 => uart_inst_1_rx_rxProc_bitpos(3),
      O => uart_inst_1_rx_n0109_inv
    );
  uart_inst_1_rx_Mmux_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT31 : LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
    port map (
      I0 => uart_inst_1_rx_rxProc_bitpos(2),
      I1 => uart_inst_1_rx_rxProc_bitpos(0),
      I2 => uart_inst_1_rx_rxProc_bitpos(1),
      I3 => uart_inst_1_rx_rxProc_samplecnt(0),
      I4 => uart_inst_1_rx_rxProc_samplecnt(1),
      O => uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_2_Q
    );
  uart_inst_1_rx_Mmux_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT41 : LUT6
    generic map(
      INIT => X"68A8A8A8AAAAAAAA"
    )
    port map (
      I0 => uart_inst_1_rx_rxProc_bitpos(3),
      I1 => uart_inst_1_rx_rxProc_bitpos(2),
      I2 => uart_inst_1_rx_rxProc_bitpos(0),
      I3 => uart_inst_1_rx_rxProc_samplecnt(1),
      I4 => uart_inst_1_rx_rxProc_samplecnt(0),
      I5 => uart_inst_1_rx_rxProc_bitpos(1),
      O => uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_3_Q
    );
  uart_inst_1_rx_Madd_rxProc_bitpos_3_GND_19_o_add_16_OUT_xor_1_12 : LUT3
    generic map(
      INIT => X"28"
    )
    port map (
      I0 => uart_inst_1_rx_Madd_rxProc_bitpos_3_GND_19_o_add_16_OUT_xor_1_11,
      I1 => uart_inst_1_rx_rxProc_samplecnt(0),
      I2 => uart_inst_1_rx_rxProc_samplecnt(1),
      O => uart_inst_1_rx_rxProc_bitpos_3_GND_19_o_add_16_OUT_1_Q
    );
  uart_inst_1_rx_Madd_rxProc_bitpos_3_GND_19_o_add_16_OUT_xor_1_111 : LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
    port map (
      I0 => uart_inst_1_rx_rxProc_bitpos(2),
      I1 => uart_inst_1_rx_rxProc_bitpos(3),
      I2 => uart_rx1_IBUF_6,
      I3 => uart_inst_1_rx_rxProc_bitpos(0),
      I4 => uart_inst_1_rx_rxProc_bitpos(1),
      O => uart_inst_1_rx_Madd_rxProc_bitpos_3_GND_19_o_add_16_OUT_xor_1_11
    );
  uart_inst_1_rx_n0124_inv1 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => uart_inst_1_rx_timer_output_820,
      O => uart_inst_1_rx_n0124_inv
    );
  uart_inst_1_tx_Mmux_txProc_bitpos_3_GND_20_o_mux_11_OUT11 : LUT5
    generic map(
      INIT => X"55555554"
    )
    port map (
      I0 => uart_inst_1_tx_txProc_bitpos(0),
      I1 => uart_inst_1_tx_BufferLoaded_879,
      I2 => uart_inst_1_tx_txProc_bitpos(1),
      I3 => uart_inst_1_tx_txProc_bitpos(2),
      I4 => uart_inst_1_tx_txProc_bitpos(3),
      O => uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_0_Q
    );
  uart_inst_1_tx_Mmux_txProc_bitpos_3_GND_20_o_Mux_6_o11 : LUT5
    generic map(
      INIT => X"AAA8AAAB"
    )
    port map (
      I0 => uart_inst_1_tx_GND_20_o_transmit_7_Mux_4_o,
      I1 => uart_inst_1_tx_txProc_bitpos(3),
      I2 => uart_inst_1_tx_txProc_bitpos(1),
      I3 => uart_inst_1_tx_txProc_bitpos(2),
      I4 => uart_inst_1_tx_txProc_bitpos(0),
      O => uart_inst_1_tx_txProc_bitpos_3_GND_20_o_Mux_6_o
    );
  uart_inst_1_tx_Mmux_txProc_bitpos_3_GND_20_o_mux_11_OUT31 : LUT3
    generic map(
      INIT => X"6A"
    )
    port map (
      I0 => uart_inst_1_tx_txProc_bitpos(2),
      I1 => uart_inst_1_tx_txProc_bitpos(0),
      I2 => uart_inst_1_tx_txProc_bitpos(1),
      O => uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_2_Q
    );
  uart_inst_1_tx_Mmux_txProc_bitpos_3_GND_20_o_mux_11_OUT21 : LUT4
    generic map(
      INIT => X"6466"
    )
    port map (
      I0 => uart_inst_1_tx_txProc_bitpos(0),
      I1 => uart_inst_1_tx_txProc_bitpos(1),
      I2 => uart_inst_1_tx_txProc_bitpos(2),
      I3 => uart_inst_1_tx_txProc_bitpos(3),
      O => uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_1_Q
    );
  uart_inst_1_tx_Mmux_txProc_bitpos_3_GND_20_o_mux_11_OUT41 : LUT4
    generic map(
      INIT => X"6AA2"
    )
    port map (
      I0 => uart_inst_1_tx_txProc_bitpos(3),
      I1 => uart_inst_1_tx_txProc_bitpos(0),
      I2 => uart_inst_1_tx_txProc_bitpos(1),
      I3 => uart_inst_1_tx_txProc_bitpos(2),
      O => uart_inst_1_tx_txProc_bitpos_3_GND_20_o_mux_11_OUT_3_Q
    );
  uart_inst_1_tx_n0078_inv1 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => uart_inst_1_tx_BufferLoaded_txProc_bitpos_3_MUX_235_o,
      O => uart_inst_1_tx_n0078_inv
    );
  uart_inst_1_tx_Mmux_BufferLoaded_txProc_bitpos_3_MUX_235_o11 : LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEFFF"
    )
    port map (
      I0 => uart_inst_1_tx_txProc_bitpos(0),
      I1 => uart_inst_1_tx_txProc_bitpos(2),
      I2 => uart_inst_1_tx_BufferLoaded_879,
      I3 => uart_inst_1_tx_timer_output_819,
      I4 => uart_inst_1_tx_txProc_bitpos(1),
      I5 => uart_inst_1_tx_txProc_bitpos(3),
      O => uart_inst_1_tx_BufferLoaded_txProc_bitpos_3_MUX_235_o
    );
  uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT31 : LUT5
    generic map(
      INIT => X"10101001"
    )
    port map (
      I0 => uart_inst_1_rx_timer_GND_21_o_GND_21_o_MUX_245_o,
      I1 => reset_IBUF_5,
      I2 => uart_inst_1_rx_timer_countGen_counter_cnt(2),
      I3 => uart_inst_1_rx_timer_countGen_counter_cnt(1),
      I4 => uart_inst_1_rx_timer_countGen_counter_cnt(0),
      O => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_2_Q
    );
  uart_inst_1_rx_timer_GND_21_o_GND_21_o_MUX_245_o_6_1 : LUT5
    generic map(
      INIT => X"00000001"
    )
    port map (
      I0 => uart_inst_1_rx_timer_countGen_counter_cnt(3),
      I1 => uart_inst_1_rx_timer_countGen_counter_cnt(4),
      I2 => uart_inst_1_rx_timer_countGen_counter_cnt(5),
      I3 => uart_inst_1_rx_timer_countGen_counter_cnt(6),
      I4 => uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT41,
      O => uart_inst_1_rx_timer_GND_21_o_GND_21_o_MUX_245_o
    );
  uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT411 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => uart_inst_1_rx_timer_countGen_counter_cnt(1),
      I1 => uart_inst_1_rx_timer_countGen_counter_cnt(2),
      I2 => uart_inst_1_rx_timer_countGen_counter_cnt(0),
      O => uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT41
    );
  uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT21 : LUT3
    generic map(
      INIT => X"EB"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => uart_inst_1_rx_timer_countGen_counter_cnt(1),
      I2 => uart_inst_1_rx_timer_countGen_counter_cnt(0),
      O => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_1_Q
    );
  uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT11 : LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => uart_inst_1_rx_timer_countGen_counter_cnt(0),
      O => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_0_Q
    );
  master_out_addr_31_PWR_9_o_equal_4_o_31_11 : LUT6
    generic map(
      INIT => X"0000000000010000"
    )
    port map (
      I0 => core_master_out_addr_11_Q,
      I1 => core_master_out_addr_13_Q,
      I2 => core_master_out_addr_14_Q,
      I3 => core_master_out_addr_16_Q,
      I4 => core_master_out_addr_31_Q,
      I5 => core_master_out_addr_15_Q,
      O => master_out_addr_31_PWR_9_o_equal_4_o_31_11_912
    );
  master_out_addr_31_PWR_9_o_equal_4_o_31_12 : LUT6
    generic map(
      INIT => X"0000000000000001"
    )
    port map (
      I0 => core_master_out_addr_18_Q,
      I1 => core_master_out_addr_17_Q,
      I2 => core_master_out_addr_21_Q,
      I3 => core_master_out_addr_22_Q,
      I4 => core_master_out_addr_24_Q,
      I5 => core_master_out_addr_23_Q,
      O => master_out_addr_31_PWR_9_o_equal_4_o_31_12_913
    );
  master_out_addr_31_PWR_9_o_equal_4_o_31_13 : LUT5
    generic map(
      INIT => X"01000000"
    )
    port map (
      I0 => core_master_out_addr_26_Q,
      I1 => core_master_out_addr_25_Q,
      I2 => core_master_out_addr_27_Q,
      I3 => master_out_addr_31_PWR_9_o_equal_4_o_31_12_913,
      I4 => master_out_addr_31_PWR_9_o_equal_4_o_31_11_912,
      O => master_out_addr_31_PWR_9_o_equal_4_o_31_1
    );
  core_n0066_inv12_SW0 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => core_zpu_core_foo_out_mem_addr_29_Q,
      I1 => core_zpu_core_foo_out_mem_addr_28_Q,
      O => N2
    );
  core_n0066_inv12 : LUT6
    generic map(
      INIT => X"0000000000003200"
    )
    port map (
      I0 => core_zpu_core_foo_out_mem_readEnable_59,
      I1 => core_zpu_core_foo_out_mem_addr_30_Q,
      I2 => core_zpu_core_foo_out_mem_writeEnable_60,
      I3 => core_zpu_core_foo_out_mem_addr_31_Q,
      I4 => core_state_43,
      I5 => N2,
      O => core_n0066_inv12_56
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT991 : LUT5
    generic map(
      INIT => X"A8880000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_memBRead_stdlogic(3),
      I4 => core_zpu_core_foo_memARead_stdlogic(3),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT99
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT992 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => master_out_addr_31_PWR_9_o_equal_4_o_31_1,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT32
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT993 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_master_out_addr_12_Q,
      I1 => core_master_out_addr_20_Q,
      I2 => uart_inst_1_rx_dataOut(3),
      I3 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT992_916
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT994 : LUT6
    generic map(
      INIT => X"00000A0C00000000"
    )
    port map (
      I0 => intIO_inst_LED_3_7,
      I1 => ioButtons_3_IBUF_0,
      I2 => reset_IBUF_5,
      I3 => core_master_out_addr_1_Q,
      I4 => core_master_out_addr_0_Q,
      I5 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT993_917
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT995 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => core_master_out_addr_20_Q,
      I1 => core_master_out_addr_12_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT993_917,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT994_918
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT996 : LUT6
    generic map(
      INIT => X"FFEAFF00FFC0FF00"
    )
    port map (
      I0 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT992_916,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT994_918,
      I2 => intIO_in_cyc,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT99,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT32,
      I5 => uart_inst_1_Mmux_slave_out_data11,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT995_919
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT999 : LUT3
    generic map(
      INIT => X"CA"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(3),
      I1 => core_zpu_core_foo_sp(3),
      I2 => core_zpu_core_foo_decodedOpcode_3_3_1383,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT998
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT891 : LUT5
    generic map(
      INIT => X"A8880000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_memBRead_stdlogic(2),
      I4 => core_zpu_core_foo_memARead_stdlogic(2),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT89
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT893 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_master_out_addr_12_Q,
      I1 => core_master_out_addr_20_Q,
      I2 => uart_inst_1_rx_dataOut(2),
      I3 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT892
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT894 : LUT6
    generic map(
      INIT => X"00000A0C00000000"
    )
    port map (
      I0 => intIO_inst_LED_2_8,
      I1 => ioButtons_2_IBUF_1,
      I2 => reset_IBUF_5,
      I3 => core_master_out_addr_1_Q,
      I4 => core_master_out_addr_0_Q,
      I5 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT893_924
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT895 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => core_master_out_addr_20_Q,
      I1 => core_master_out_addr_12_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT893_924,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT894_925
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT896 : LUT6
    generic map(
      INIT => X"FFEAFF00FFC0FF00"
    )
    port map (
      I0 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT892,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT894_925,
      I2 => intIO_in_cyc,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT89,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT32,
      I5 => uart_inst_1_Mmux_slave_out_data11,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT895_926
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT899 : LUT3
    generic map(
      INIT => X"CA"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(2),
      I1 => core_zpu_core_foo_sp(2),
      I2 => core_zpu_core_foo_decodedOpcode_3_3_1383,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT898
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1082 : LUT6
    generic map(
      INIT => X"0000000008000000"
    )
    port map (
      I0 => uart_inst_1_rx_dataOut(6),
      I1 => core_zpu_core_foo_state_FSM_FFd2_1_1380,
      I2 => core_zpu_core_foo_state_FSM_FFd4_1_1381,
      I3 => core_master_out_addr_20_Q,
      I4 => core_master_out_addr_12_Q,
      I5 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1081
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1086 : LUT3
    generic map(
      INIT => X"CA"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(6),
      I1 => core_zpu_core_foo_sp(6),
      I2 => core_zpu_core_foo_decodedOpcode_3_3_1383,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1085
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1052 : LUT6
    generic map(
      INIT => X"0000000008000000"
    )
    port map (
      I0 => uart_inst_1_rx_dataOut(5),
      I1 => core_zpu_core_foo_state_FSM_FFd2_1_1380,
      I2 => core_zpu_core_foo_state_FSM_FFd4_1_1381,
      I3 => core_master_out_addr_20_Q,
      I4 => core_master_out_addr_12_Q,
      I5 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1051
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1056 : LUT3
    generic map(
      INIT => X"CA"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(5),
      I1 => core_zpu_core_foo_sp(5),
      I2 => core_zpu_core_foo_decodedOpcode_3_3_1383,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1055
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1022 : LUT6
    generic map(
      INIT => X"0000000008000000"
    )
    port map (
      I0 => uart_inst_1_rx_dataOut(4),
      I1 => core_zpu_core_foo_state_FSM_FFd2_1_1380,
      I2 => core_zpu_core_foo_state_FSM_FFd4_1_1381,
      I3 => core_master_out_addr_20_Q,
      I4 => core_master_out_addr_12_Q,
      I5 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1021
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1026 : LUT3
    generic map(
      INIT => X"CA"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(4),
      I1 => core_zpu_core_foo_sp(4),
      I2 => core_zpu_core_foo_decodedOpcode_3_3_1383,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1025
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT961 : LUT5
    generic map(
      INIT => X"A8880000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_memBRead_stdlogic(31),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(31),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT96
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT962 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode_2_2_1377,
      I1 => core_zpu_core_foo_decodedOpcode_3_2_1375,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT964 : LUT6
    generic map(
      INIT => X"0000000E00000002"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(6),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_memARead_stdlogic(24),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT963_943
    );
  core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT5_SW0 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => core_zpu_core_foo_decodeControl_tOpcode(5),
      I1 => core_zpu_core_foo_decodeControl_tOpcode(4),
      O => N4
    );
  core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT5 : LUT6
    generic map(
      INIT => X"00B0000000A00000"
    )
    port map (
      I0 => core_zpu_core_foo_decodeControl_tOpcode(7),
      I1 => core_zpu_core_foo_decodeControl_tOpcode(6),
      I2 => core_zpu_core_foo_decodeControl_tOpcode(2),
      I3 => core_zpu_core_foo_decodeControl_tOpcode(1),
      I4 => core_zpu_core_foo_decodeControl_tOpcode(3),
      I5 => N4,
      O => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_4_Q
    );
  core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT4_SW0 : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => core_zpu_core_foo_decodeControl_tOpcode(6),
      I1 => core_zpu_core_foo_decodeControl_tOpcode(5),
      I2 => core_zpu_core_foo_decodeControl_tOpcode(4),
      O => N6
    );
  core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT4 : LUT6
    generic map(
      INIT => X"00FF5F5000CC4C40"
    )
    port map (
      I0 => core_zpu_core_foo_decodeControl_tOpcode(0),
      I1 => core_zpu_core_foo_decodeControl_tOpcode(7),
      I2 => core_zpu_core_foo_decodeControl_tOpcode(1),
      I3 => core_zpu_core_foo_decodeControl_tOpcode(3),
      I4 => core_zpu_core_foo_decodeControl_tOpcode(2),
      I5 => N6,
      O => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_3_Q
    );
  core_zpu_core_foo_Mmux_state_4_GND_14_o_Mux_76_o1_SW0 : LUT5
    generic map(
      INIT => X"AFEDFEFB"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(1),
      O => N8
    );
  core_zpu_core_foo_Mmux_state_4_GND_14_o_Mux_76_o1 : LUT6
    generic map(
      INIT => X"2C1C2C1E2C0C2C0E"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => N8,
      I5 => master_in_ack,
      O => core_zpu_core_foo_state_4_GND_14_o_Mux_76_o
    );
  core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT21 : LUT4
    generic map(
      INIT => X"2249"
    )
    port map (
      I0 => core_zpu_core_foo_decodeControl_tOpcode(0),
      I1 => core_zpu_core_foo_decodeControl_tOpcode(3),
      I2 => core_zpu_core_foo_decodeControl_tOpcode(1),
      I3 => core_zpu_core_foo_decodeControl_tOpcode(2),
      O => core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT2
    );
  core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT22 : LUT5
    generic map(
      INIT => X"FFAB00AA"
    )
    port map (
      I0 => core_zpu_core_foo_decodeControl_tOpcode(5),
      I1 => core_zpu_core_foo_decodeControl_tOpcode(6),
      I2 => core_zpu_core_foo_decodeControl_tOpcode(4),
      I3 => core_zpu_core_foo_decodeControl_tOpcode(7),
      I4 => core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT2,
      O => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_1_Q
    );
  core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT31 : LUT4
    generic map(
      INIT => X"0471"
    )
    port map (
      I0 => core_zpu_core_foo_decodeControl_tOpcode(0),
      I1 => core_zpu_core_foo_decodeControl_tOpcode(1),
      I2 => core_zpu_core_foo_decodeControl_tOpcode(3),
      I3 => core_zpu_core_foo_decodeControl_tOpcode(2),
      O => core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT3
    );
  core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT32 : LUT5
    generic map(
      INIT => X"BBFF1154"
    )
    port map (
      I0 => core_zpu_core_foo_decodeControl_tOpcode(7),
      I1 => core_zpu_core_foo_decodeControl_tOpcode(5),
      I2 => core_zpu_core_foo_decodeControl_tOpcode(4),
      I3 => core_zpu_core_foo_decodeControl_tOpcode(6),
      I4 => core_zpu_core_foo_Mmux_tOpcode_sel_1_GND_14_o_mux_18_OUT3,
      O => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_2_Q
    );
  core_zpu_core_foo_state_FSM_FFd3_In1_SW0 : LUT6
    generic map(
      INIT => X"0004101000051010"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_memARead_stdlogic(31),
      O => N10
    );
  core_zpu_core_foo_state_FSM_FFd3_In1 : LUT6
    generic map(
      INIT => X"2220B2B02220A2A0"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_state_FSM_FFd5_583,
      I3 => N10,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      I5 => master_in_ack,
      O => core_zpu_core_foo_state_FSM_FFd3_In1_305
    );
  core_zpu_core_foo_state_FSM_FFd4_In1 : LUT6
    generic map(
      INIT => X"0000040000000000"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_state_FSM_FFd4_584,
      O => core_zpu_core_foo_state_FSM_FFd4_In1_950
    );
  core_zpu_core_foo_state_FSM_FFd5_In_SW0 : LUT5
    generic map(
      INIT => X"FFFAF3FF"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      O => N12
    );
  core_zpu_core_foo_state_FSM_FFd5_In : LUT6
    generic map(
      INIT => X"286F282B286F286F"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd5_583,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_Mram_n0407,
      I5 => N12,
      O => core_zpu_core_foo_state_FSM_FFd5_In_304
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT931 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(30),
      I2 => core_zpu_core_foo_memBRead_stdlogic(30),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT93
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT934 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(30),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(1),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT933
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT935 : LUT5
    generic map(
      INIT => X"222A0008"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_memARead_stdlogic(30),
      I4 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_30_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT934_954
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT936 : LUT5
    generic map(
      INIT => X"0A0C0A00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(30),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_memARead_stdlogic(23),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT935_955
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT937 : LUT5
    generic map(
      INIT => X"33113310"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT935_955,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT933,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT934_954,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT936_956
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT861 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(29),
      I2 => core_zpu_core_foo_memBRead_stdlogic(29),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT86
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT864 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(29),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(2),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT863
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT865 : LUT5
    generic map(
      INIT => X"222A0008"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_memARead_stdlogic(29),
      I4 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_29_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT864_959
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT866 : LUT5
    generic map(
      INIT => X"0A0C0A00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(29),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_memARead_stdlogic(22),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT865_960
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT867 : LUT5
    generic map(
      INIT => X"33113310"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT865_960,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT863,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT864_959,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT866_961
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT821 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(28),
      I2 => core_zpu_core_foo_memBRead_stdlogic(28),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT82
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT824 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(28),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(3),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT823
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT825 : LUT5
    generic map(
      INIT => X"222A0008"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_memARead_stdlogic(28),
      I4 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_28_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT824_964
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT826 : LUT5
    generic map(
      INIT => X"0A0C0A00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(28),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_memARead_stdlogic(21),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT825_965
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT827 : LUT5
    generic map(
      INIT => X"33113310"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT825_965,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT823,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT824_964,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT826_966
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT781 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(27),
      I2 => core_zpu_core_foo_memBRead_stdlogic(27),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT78_967
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT784 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(27),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(4),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT783
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT785 : LUT5
    generic map(
      INIT => X"222A0008"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_memARead_stdlogic(27),
      I4 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_27_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT784_969
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT786 : LUT5
    generic map(
      INIT => X"0A0C0A00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(27),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_memARead_stdlogic(20),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT785_970
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT787 : LUT5
    generic map(
      INIT => X"33113310"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT785_970,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT783,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT784_969,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT786_971
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT741 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(26),
      I2 => core_zpu_core_foo_memBRead_stdlogic(26),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT74_972
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT744 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(26),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(5),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT743
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT745 : LUT5
    generic map(
      INIT => X"222A0008"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_memARead_stdlogic(26),
      I4 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_26_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT744_974
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT746 : LUT5
    generic map(
      INIT => X"0A0C0A00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(26),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_memARead_stdlogic(19),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT745_975
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT747 : LUT5
    generic map(
      INIT => X"33113310"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT745_975,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT743,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT744_974,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT746_976
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT701 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(25),
      I2 => core_zpu_core_foo_memBRead_stdlogic(25),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT70
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT704 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(25),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(6),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT703
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT705 : LUT5
    generic map(
      INIT => X"222A0008"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_memARead_stdlogic(25),
      I4 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_25_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT704_979
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT706 : LUT5
    generic map(
      INIT => X"0A0C0A00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(25),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_memARead_stdlogic(18),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT705_980
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT707 : LUT5
    generic map(
      INIT => X"33113310"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT705_980,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT703,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT704_979,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT706_981
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT71 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(10),
      I2 => core_zpu_core_foo_memBRead_stdlogic(10),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT7
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT74 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(10),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(21),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT73
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT75 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(10),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_10_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT75_984
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT76 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(10),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(3),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT76_985
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT77 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT76_985,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT75_984,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT73,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT77_986
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT661 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(24),
      I2 => core_zpu_core_foo_memBRead_stdlogic(24),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT66
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT664 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(24),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(7),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT663
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT665 : LUT5
    generic map(
      INIT => X"222A0008"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_memARead_stdlogic(24),
      I4 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_24_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT664_989
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT666 : LUT5
    generic map(
      INIT => X"0A0C0A00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(24),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_memARead_stdlogic(17),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT665_990
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT667 : LUT5
    generic map(
      INIT => X"33113310"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT665_990,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT663,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT664_989,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT666_991
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT621 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(23),
      I2 => core_zpu_core_foo_memBRead_stdlogic(23),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT62
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT624 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(23),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(8),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT623
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT625 : LUT5
    generic map(
      INIT => X"222A0008"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_memARead_stdlogic(23),
      I4 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_23_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT624_994
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT626 : LUT5
    generic map(
      INIT => X"0A0C0A00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(23),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_memARead_stdlogic(16),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT625_995
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT627 : LUT5
    generic map(
      INIT => X"33113310"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT625_995,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT623,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT624_994,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT626_996
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT581 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(22),
      I2 => core_zpu_core_foo_memBRead_stdlogic(22),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT58
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT584 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(22),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(9),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT583
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT585 : LUT5
    generic map(
      INIT => X"222A0008"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_memARead_stdlogic(22),
      I4 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_22_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT584_999
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT586 : LUT5
    generic map(
      INIT => X"0A0C0A00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(22),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_memARead_stdlogic(15),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT585_1000
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT587 : LUT5
    generic map(
      INIT => X"33113310"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT585_1000,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT583,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT584_999,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT586_1001
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT541 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(21),
      I2 => core_zpu_core_foo_memBRead_stdlogic(21),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT54
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT544 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(21),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(10),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT543
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT545 : LUT5
    generic map(
      INIT => X"222A0008"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_decodedOpcode(0),
      I3 => core_zpu_core_foo_memARead_stdlogic(21),
      I4 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_21_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT544_1004
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT546 : LUT5
    generic map(
      INIT => X"0A0C0A00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(21),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_memARead_stdlogic(14),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT545_1005
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT547 : LUT5
    generic map(
      INIT => X"33113310"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT545_1005,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT543,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT544_1004,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT546_1006
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT501 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(20),
      I2 => core_zpu_core_foo_memBRead_stdlogic(20),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT50
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT504 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(20),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(11),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT503
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT505 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(20),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_20_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT504_1009
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT506 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(20),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(13),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT505_1010
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT507 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT505_1010,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT504_1009,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT503,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT506_1011
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT461 : LUT5
    generic map(
      INIT => X"A8880000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_memBRead_stdlogic(1),
      I4 => core_zpu_core_foo_memARead_stdlogic(1),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT46
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT463 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_master_out_addr_12_Q,
      I1 => core_master_out_addr_20_Q,
      I2 => uart_inst_1_rx_dataOut(1),
      I3 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT462
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT464 : LUT6
    generic map(
      INIT => X"00000A0C00000000"
    )
    port map (
      I0 => intIO_inst_LED_1_9,
      I1 => ioButtons_1_IBUF_2,
      I2 => reset_IBUF_5,
      I3 => core_master_out_addr_1_Q,
      I4 => core_master_out_addr_0_Q,
      I5 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT463_1014
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT465 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => core_master_out_addr_20_Q,
      I1 => core_master_out_addr_12_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT463_1014,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT464_1015
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT466 : LUT6
    generic map(
      INIT => X"FEFAF0F0FCF0F0F0"
    )
    port map (
      I0 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT462,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT464_1015,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT46,
      I3 => intIO_in_cyc,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT32,
      I5 => uart_inst_1_Mmux_slave_out_data11,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT465_1016
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT431 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(19),
      I2 => core_zpu_core_foo_memBRead_stdlogic(19),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT43
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT434 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(19),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(12),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT433
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT435 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(19),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT434_1020
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT436 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(19),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(12),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT435_1021
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT437 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT435_1021,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT434_1020,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT433,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT436_1022
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT391 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(18),
      I2 => core_zpu_core_foo_memBRead_stdlogic(18),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT39
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT394 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(18),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(13),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT393
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT395 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(18),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_18_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT394_1025
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT396 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(18),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(11),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT395_1026
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT397 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT395_1026,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT394_1025,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT393,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT396_1027
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT351 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(17),
      I2 => core_zpu_core_foo_memBRead_stdlogic(17),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT35_1028
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT354 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(17),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(14),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT353
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT355 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(17),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_17_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT354_1030
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT356 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(17),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(10),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT355_1031
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT357 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT355_1031,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT354_1030,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT353,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT356_1032
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT311 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(16),
      I2 => core_zpu_core_foo_memBRead_stdlogic(16),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT31_1033
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT314 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(16),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(15),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT313
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT315 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(16),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_16_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT314_1035
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT316 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(16),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(9),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT315_1036
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT317 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT315_1036,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT314_1035,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT313,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT316_1037
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT31 : LUT5
    generic map(
      INIT => X"A8880000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_1_1381,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_memBRead_stdlogic(0),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(0),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT3
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT33 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_master_out_addr_12_Q,
      I1 => core_master_out_addr_20_Q,
      I2 => uart_inst_1_rx_dataOut(0),
      I3 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT33_1040
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT34 : LUT6
    generic map(
      INIT => X"00000A0C00000000"
    )
    port map (
      I0 => intIO_inst_LED_0_10,
      I1 => ioButtons_0_IBUF_3,
      I2 => reset_IBUF_5,
      I3 => core_master_out_addr_1_Q,
      I4 => core_master_out_addr_0_Q,
      I5 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT34_1041
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT35 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => core_master_out_addr_20_Q,
      I1 => core_master_out_addr_12_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT34_1041,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT36_1042
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT36 : LUT6
    generic map(
      INIT => X"FEFAF0F0FCF0F0F0"
    )
    port map (
      I0 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT33_1040,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT36_1042,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT3,
      I3 => intIO_in_cyc,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT32,
      I5 => uart_inst_1_Mmux_slave_out_data11,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT37_1043
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT37 : LUT6
    generic map(
      INIT => X"88AA00A288880080"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd3_585,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(0),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_memBRead_stdlogic(0),
      I5 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_0_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT38_1044
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT38 : LUT4
    generic map(
      INIT => X"0002"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(0),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_decodedOpcode(4),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT310
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT322 : LUT4
    generic map(
      INIT => X"FF54"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT38_1044,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT321_1047,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT37_1043,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_0_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT271 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(15),
      I2 => core_zpu_core_foo_memBRead_stdlogic(15),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT27
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT274 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(15),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(16),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT273
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT275 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(15),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_15_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT274_1050
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT276 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(15),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(8),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT275_1051
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT277 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT275_1051,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT274_1050,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT273,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT276_1052
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT231 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(14),
      I2 => core_zpu_core_foo_memBRead_stdlogic(14),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT23
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT234 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(14),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(17),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT233
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT235 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(14),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_14_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT234_1055
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT236 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(14),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(7),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT235_1056
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT237 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT235_1056,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT234_1055,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT233,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT236_1057
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT191 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(13),
      I2 => core_zpu_core_foo_memBRead_stdlogic(13),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT19
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT194 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(13),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(18),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT193
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT195 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(13),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_13_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT194_1060
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT196 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(13),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(6),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT195_1061
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT197 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT195_1061,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT194_1060,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT193,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT196_1062
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT151 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(12),
      I2 => core_zpu_core_foo_memBRead_stdlogic(12),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT15
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT154 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(12),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(19),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT153
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT155 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(12),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_12_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT154_1065
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT156 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(12),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(5),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT155_1066
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT157 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT155_1066,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT154_1065,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT153,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT156_1067
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1201 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(9),
      I2 => core_zpu_core_foo_memBRead_stdlogic(9),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT120
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1204 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(9),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(22),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1203
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1205 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(9),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_9_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1204_1070
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1206 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(9),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(2),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1205_1071
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1207 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1205_1071,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1204_1070,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1203,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1206_1072
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1161 : LUT5
    generic map(
      INIT => X"A8880000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_1_1381,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_memBRead_stdlogic(8),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(8),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT116_1073
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1162 : LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => core_master_out_addr_3_Q,
      I1 => core_master_out_addr_2_Q,
      I2 => uart_inst_1_rx_byteAvail_821,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1161_1074
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1163 : LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      I0 => core_master_out_addr_3_Q,
      I1 => uart_inst_1_tx_BufferLoaded_879,
      I2 => core_master_out_addr_2_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1162_1075
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1164 : LUT6
    generic map(
      INIT => X"0008000000000000"
    )
    port map (
      I0 => core_master_out_addr_20_Q,
      I1 => core_master_out_addr_12_Q,
      I2 => core_master_out_addr_19_Q,
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      I5 => master_out_addr_31_PWR_9_o_equal_4_o_31_1,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1163_1076
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1165 : LUT6
    generic map(
      INIT => X"FCF0F0F0FEF0F0F0"
    )
    port map (
      I0 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1162_1075,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1161_1074,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT116_1073,
      I3 => uart1_in_cyc,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1163_1076,
      I5 => uart_inst_1_uart_write,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1164_1077
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1168 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(8),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(23),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1167
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1169 : LUT4
    generic map(
      INIT => X"3074"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_8_Q,
      I3 => core_zpu_core_foo_memARead_stdlogic(8),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1168_1079
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT11610 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(8),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(1),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1169_1080
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT11611 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1169_1080,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1168_1079,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1167,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT11610_1081
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1122 : LUT6
    generic map(
      INIT => X"0000000008000000"
    )
    port map (
      I0 => uart_inst_1_rx_dataOut(7),
      I1 => core_zpu_core_foo_state_FSM_FFd2_1_1380,
      I2 => core_zpu_core_foo_state_FSM_FFd4_1_1381,
      I3 => core_master_out_addr_20_Q,
      I4 => core_master_out_addr_12_Q,
      I5 => core_master_out_addr_19_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1121
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1126 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(7),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(24),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1125
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1127 : LUT4
    generic map(
      INIT => X"3074"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_7_Q,
      I3 => core_zpu_core_foo_memARead_stdlogic(7),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1126_1086
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1128 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(7),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(0),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1127_1087
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1129 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1127_1087,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1126_1086,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1125,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1128_1088
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT111 : LUT5
    generic map(
      INIT => X"88888000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(11),
      I2 => core_zpu_core_foo_memBRead_stdlogic(11),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT11
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT114 : LUT6
    generic map(
      INIT => X"FFFFFFFFFC888888"
    )
    port map (
      I0 => core_zpu_core_foo_pc(11),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(20),
      I4 => core_zpu_core_foo_n0425,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT114_1090
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT115 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_memARead_stdlogic(11),
      I3 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_11_Q,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT115_1091
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT116 : LUT4
    generic map(
      INIT => X"ACA0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(11),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_memARead_stdlogic(4),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT117_1092
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT117 : LUT6
    generic map(
      INIT => X"0F0F0F0F03020100"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(2),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT117_1092,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT115_1091,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT114_1090,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT118_1093
    );
  core_zpu_core_foo_n0372_inv_SW0 : LUT5
    generic map(
      INIT => X"FFFAE0C3"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      O => N14
    );
  core_zpu_core_foo_n0372_inv : LUT6
    generic map(
      INIT => X"0824082008240824"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd5_583,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_Mram_n040723,
      I5 => N14,
      O => core_zpu_core_foo_n0372_inv_308
    );
  core_zpu_core_foo_state_FSM_FFd2_In_SW0 : LUT6
    generic map(
      INIT => X"1101140401011400"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_decodedOpcode(0),
      I4 => core_zpu_core_foo_decodedOpcode(2),
      I5 => core_zpu_core_foo_memARead_stdlogic(31),
      O => N16
    );
  core_zpu_core_foo_Mmux_n028292 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(12),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_decodedOpcode(1),
      O => core_zpu_core_foo_Mmux_n028291_1097
    );
  core_zpu_core_foo_Mmux_n028293 : LUT6
    generic map(
      INIT => X"F1F1F1F011111100"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_10_Q,
      I3 => core_zpu_core_foo_Mmux_n028291_1097,
      I4 => core_zpu_core_foo_Mmux_n02829,
      I5 => core_zpu_core_foo_Mmux_n0282121_97,
      O => core_zpu_core_foo_n0282(12)
    );
  core_zpu_core_foo_Mmux_n0282872 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(9),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_decodedOpcode(1),
      O => core_zpu_core_foo_Mmux_n0282871_1099
    );
  core_zpu_core_foo_Mmux_n0282873 : LUT6
    generic map(
      INIT => X"F1F1F1F011111100"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_7_Q,
      I3 => core_zpu_core_foo_Mmux_n0282871_1099,
      I4 => core_zpu_core_foo_Mmux_n028287,
      I5 => core_zpu_core_foo_Mmux_n0282121_97,
      O => core_zpu_core_foo_n0282(9)
    );
  core_zpu_core_foo_Mmux_n0282842 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(8),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_decodedOpcode(1),
      O => core_zpu_core_foo_Mmux_n0282841_1101
    );
  core_zpu_core_foo_Mmux_n0282843 : LUT6
    generic map(
      INIT => X"F1F1F1F011111100"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_6_Q,
      I3 => core_zpu_core_foo_Mmux_n0282841_1101,
      I4 => core_zpu_core_foo_Mmux_n028284,
      I5 => core_zpu_core_foo_Mmux_n0282121_97,
      O => core_zpu_core_foo_n0282(8)
    );
  core_zpu_core_foo_Mmux_n0282812 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(7),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_decodedOpcode(1),
      O => core_zpu_core_foo_Mmux_n0282811_1103
    );
  core_zpu_core_foo_Mmux_n0282813 : LUT6
    generic map(
      INIT => X"CDCDCDCC05050500"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_5_Q,
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_n0282811_1103,
      I4 => core_zpu_core_foo_Mmux_n028281,
      I5 => core_zpu_core_foo_Mmux_n0282121_97,
      O => core_zpu_core_foo_n0282(7)
    );
  core_zpu_core_foo_Mmux_n0282782 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(6),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_decodedOpcode(1),
      O => core_zpu_core_foo_Mmux_n0282781_1105
    );
  core_zpu_core_foo_Mmux_n0282783 : LUT6
    generic map(
      INIT => X"ABABABAA03030300"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_4_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_n0282781_1105,
      I4 => core_zpu_core_foo_Mmux_n028278,
      I5 => core_zpu_core_foo_Mmux_n0282121_97,
      O => core_zpu_core_foo_n0282(6)
    );
  core_zpu_core_foo_Mmux_n0282752 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(5),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_decodedOpcode(1),
      O => core_zpu_core_foo_Mmux_n0282751_1107
    );
  core_zpu_core_foo_Mmux_n0282753 : LUT6
    generic map(
      INIT => X"ABABABAA03030300"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_3_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_n0282751_1107,
      I4 => core_zpu_core_foo_Mmux_n028275,
      I5 => core_zpu_core_foo_Mmux_n0282121_97,
      O => core_zpu_core_foo_n0282(5)
    );
  core_zpu_core_foo_Mmux_n0282722 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(4),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_decodedOpcode(1),
      O => core_zpu_core_foo_Mmux_n0282721_1109
    );
  core_zpu_core_foo_Mmux_n0282723 : LUT6
    generic map(
      INIT => X"ABABABAA03030300"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_2_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_n0282721_1109,
      I4 => core_zpu_core_foo_Mmux_n028272,
      I5 => core_zpu_core_foo_Mmux_n0282121_97,
      O => core_zpu_core_foo_n0282(4)
    );
  core_zpu_core_foo_Mmux_n0282692 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(3),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_decodedOpcode(1),
      O => core_zpu_core_foo_Mmux_n0282691_1111
    );
  core_zpu_core_foo_Mmux_n0282693 : LUT6
    generic map(
      INIT => X"ABABABAA03030300"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_1_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_n0282691_1111,
      I4 => core_zpu_core_foo_Mmux_n028269,
      I5 => core_zpu_core_foo_Mmux_n0282121_97,
      O => core_zpu_core_foo_n0282(3)
    );
  core_zpu_core_foo_Mmux_n0282632 : LUT5
    generic map(
      INIT => X"FFFF2000"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_decodedOpcode(1),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_memARead_stdlogic(2),
      I4 => core_zpu_core_foo_Mmux_n028263_1112,
      O => core_zpu_core_foo_Mmux_n0282631_1113
    );
  core_zpu_core_foo_Mmux_n0282633 : LUT5
    generic map(
      INIT => X"AB03AA00"
    )
    port map (
      I0 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_0_Q,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_state_FSM_FFd3_585,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => core_zpu_core_foo_Mmux_n0282631_1113,
      O => core_zpu_core_foo_n0282(2)
    );
  core_zpu_core_foo_Mmux_n028262 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(11),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_decodedOpcode(1),
      O => core_zpu_core_foo_Mmux_n028261_1115
    );
  core_zpu_core_foo_Mmux_n028263 : LUT6
    generic map(
      INIT => X"F1F1F1F011111100"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_9_Q,
      I3 => core_zpu_core_foo_Mmux_n028261_1115,
      I4 => core_zpu_core_foo_Mmux_n02826,
      I5 => core_zpu_core_foo_Mmux_n0282121_97,
      O => core_zpu_core_foo_n0282(11)
    );
  core_zpu_core_foo_Mmux_n028232 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_zpu_core_foo_memARead_stdlogic(10),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_decodedOpcode(1),
      O => core_zpu_core_foo_Mmux_n028231_1117
    );
  core_zpu_core_foo_Mmux_n028234 : LUT6
    generic map(
      INIT => X"F1F1F1F011111100"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_8_Q,
      I3 => core_zpu_core_foo_Mmux_n028231_1117,
      I4 => core_zpu_core_foo_Mmux_n02823,
      I5 => core_zpu_core_foo_Mmux_n0282121_97,
      O => core_zpu_core_foo_n0282(10)
    );
  core_zpu_core_foo_Mmux_n0282121 : LUT6
    generic map(
      INIT => X"FFFF200020002000"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_decodedOpcode(1),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_memARead_stdlogic(13),
      I4 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(11),
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n028212
    );
  core_zpu_core_foo_Mmux_n0282123 : LUT6
    generic map(
      INIT => X"F1F1F1F011111100"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_11_Q,
      I3 => core_zpu_core_foo_Mmux_n028212,
      I4 => core_zpu_core_foo_Mmux_n0282122_1119,
      I5 => core_zpu_core_foo_Mmux_n0282121_97,
      O => core_zpu_core_foo_n0282(13)
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT82 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(1),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_1_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT81_1121
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT83 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(3),
      I1 => core_zpu_core_foo_memARead_stdlogic(3),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT8,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT81_1121,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_1_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT62 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(11),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_11_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT61_1123
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT63 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(13),
      I1 => core_zpu_core_foo_memARead_stdlogic(13),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT6,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT61_1123,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_11_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT42 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(10),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_10_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT41_1125
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT43 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(12),
      I1 => core_zpu_core_foo_memARead_stdlogic(12),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT4,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT41_1125,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_10_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT242 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(9),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_9_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT241_1127
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT243 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(11),
      I1 => core_zpu_core_foo_memARead_stdlogic(11),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT24,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT241_1127,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_9_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT222 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(8),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_8_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT221_1129
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT223 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(10),
      I1 => core_zpu_core_foo_memARead_stdlogic(10),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT22_1128,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT221_1129,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_8_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT202 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(7),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_7_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT201_1131
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT203 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(9),
      I1 => core_zpu_core_foo_memARead_stdlogic(9),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT20,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT201_1131,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_7_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT22 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(0),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_0_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT21_1133
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT23 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(2),
      I1 => core_zpu_core_foo_memARead_stdlogic(2),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT2,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT21_1133,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_0_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT182 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(6),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_6_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT181_1135
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT183 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(8),
      I1 => core_zpu_core_foo_memARead_stdlogic(8),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT18,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT181_1135,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_6_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT162 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(5),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_5_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT161_1137
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT163 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(7),
      I1 => core_zpu_core_foo_memARead_stdlogic(7),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT16,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT161_1137,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_5_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT142 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(4),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_4_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT141_1139
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT143 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(6),
      I1 => core_zpu_core_foo_memARead_stdlogic(6),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT14,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT141_1139,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_4_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT122 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(3),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_3_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT121_1141
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT123 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(5),
      I1 => core_zpu_core_foo_memARead_stdlogic(5),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT12,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT121_1141,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_3_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102 : LUT5
    generic map(
      INIT => X"ECA00000"
    )
    port map (
      I0 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(2),
      I1 => core_zpu_core_foo_sp_30_GND_14_o_add_31_OUT_2_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111,
      I3 => core_zpu_core_foo_n0526,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT105
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103 : LUT6
    generic map(
      INIT => X"FFFEFFFCFFFAFFF0"
    )
    port map (
      I0 => core_zpu_core_foo_sp(4),
      I1 => core_zpu_core_foo_memARead_stdlogic(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT10,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT105,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_75_OUT_2_Q
    );
  core_zpu_core_foo_Mmux_n028266_SW0 : LUT6
    generic map(
      INIT => X"01031133050F55FF"
    )
    port map (
      I0 => core_zpu_core_foo_sp(30),
      I1 => core_zpu_core_foo_memARead_stdlogic(30),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(28),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => N18
    );
  core_zpu_core_foo_Mmux_n028266 : LUT5
    generic map(
      INIT => X"F000F111"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_28_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => N18,
      O => core_zpu_core_foo_n0282(30)
    );
  core_zpu_core_foo_Mmux_n028260_SW0 : LUT6
    generic map(
      INIT => X"01031133050F55FF"
    )
    port map (
      I0 => core_zpu_core_foo_sp(29),
      I1 => core_zpu_core_foo_memARead_stdlogic(29),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(27),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => N20
    );
  core_zpu_core_foo_Mmux_n028260 : LUT5
    generic map(
      INIT => X"F000F111"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_27_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => N20,
      O => core_zpu_core_foo_n0282(29)
    );
  core_zpu_core_foo_Mmux_n028257_SW0 : LUT6
    generic map(
      INIT => X"01031133050F55FF"
    )
    port map (
      I0 => core_zpu_core_foo_sp(28),
      I1 => core_zpu_core_foo_memARead_stdlogic(28),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(26),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => N22
    );
  core_zpu_core_foo_Mmux_n028257 : LUT5
    generic map(
      INIT => X"F000F111"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_26_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => N22,
      O => core_zpu_core_foo_n0282(28)
    );
  core_zpu_core_foo_Mmux_n028254_SW0 : LUT6
    generic map(
      INIT => X"01031133050F55FF"
    )
    port map (
      I0 => core_zpu_core_foo_sp(27),
      I1 => core_zpu_core_foo_memARead_stdlogic(27),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(25),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => N24
    );
  core_zpu_core_foo_Mmux_n028254 : LUT5
    generic map(
      INIT => X"F000F111"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_25_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => N24,
      O => core_zpu_core_foo_n0282(27)
    );
  core_zpu_core_foo_Mmux_n028251_SW0 : LUT6
    generic map(
      INIT => X"01031133050F55FF"
    )
    port map (
      I0 => core_zpu_core_foo_sp(26),
      I1 => core_zpu_core_foo_memARead_stdlogic(26),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(24),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => N26
    );
  core_zpu_core_foo_Mmux_n028251 : LUT5
    generic map(
      INIT => X"F000F111"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_24_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => N26,
      O => core_zpu_core_foo_n0282(26)
    );
  core_zpu_core_foo_Mmux_n028248_SW0 : LUT6
    generic map(
      INIT => X"01031133050F55FF"
    )
    port map (
      I0 => core_zpu_core_foo_sp(25),
      I1 => core_zpu_core_foo_memARead_stdlogic(25),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(23),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => N28
    );
  core_zpu_core_foo_Mmux_n028248 : LUT5
    generic map(
      INIT => X"F000F111"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_23_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => N28,
      O => core_zpu_core_foo_n0282(25)
    );
  core_zpu_core_foo_Mmux_n028245_SW0 : LUT6
    generic map(
      INIT => X"01031133050F55FF"
    )
    port map (
      I0 => core_zpu_core_foo_sp(24),
      I1 => core_zpu_core_foo_memARead_stdlogic(24),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(22),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => N30
    );
  core_zpu_core_foo_Mmux_n028245 : LUT5
    generic map(
      INIT => X"F000F111"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_22_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => N30,
      O => core_zpu_core_foo_n0282(24)
    );
  core_zpu_core_foo_Mmux_n028242_SW0 : LUT6
    generic map(
      INIT => X"01031133050F55FF"
    )
    port map (
      I0 => core_zpu_core_foo_sp(23),
      I1 => core_zpu_core_foo_memARead_stdlogic(23),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(21),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => N32
    );
  core_zpu_core_foo_Mmux_n028242 : LUT5
    generic map(
      INIT => X"F000F111"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_21_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => N32,
      O => core_zpu_core_foo_n0282(23)
    );
  core_zpu_core_foo_Mmux_n0282391 : LUT6
    generic map(
      INIT => X"FEFCEECCFAF0AA00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(22),
      I1 => core_zpu_core_foo_memARead_stdlogic(22),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(20),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => core_zpu_core_foo_Mmux_n028239
    );
  core_zpu_core_foo_Mmux_n0282392 : LUT5
    generic map(
      INIT => X"F111F000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_20_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => core_zpu_core_foo_Mmux_n028239,
      O => core_zpu_core_foo_n0282(22)
    );
  core_zpu_core_foo_Mmux_n0282361 : LUT6
    generic map(
      INIT => X"FEFCEECCFAF0AA00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(21),
      I1 => core_zpu_core_foo_memARead_stdlogic(21),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(19),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => core_zpu_core_foo_Mmux_n028236
    );
  core_zpu_core_foo_Mmux_n0282362 : LUT5
    generic map(
      INIT => X"F111F000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_19_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => core_zpu_core_foo_Mmux_n028236,
      O => core_zpu_core_foo_n0282(21)
    );
  core_zpu_core_foo_Mmux_n0282331 : LUT6
    generic map(
      INIT => X"FEFCEECCFAF0AA00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(20),
      I1 => core_zpu_core_foo_memARead_stdlogic(20),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(18),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => core_zpu_core_foo_Mmux_n028233
    );
  core_zpu_core_foo_Mmux_n0282332 : LUT5
    generic map(
      INIT => X"F111F000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_18_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => core_zpu_core_foo_Mmux_n028233,
      O => core_zpu_core_foo_n0282(20)
    );
  core_zpu_core_foo_Mmux_n0282301 : LUT6
    generic map(
      INIT => X"FEFCEECCFAF0AA00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(19),
      I1 => core_zpu_core_foo_memARead_stdlogic(19),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(17),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => core_zpu_core_foo_Mmux_n028230
    );
  core_zpu_core_foo_Mmux_n0282302 : LUT5
    generic map(
      INIT => X"F111F000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_17_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => core_zpu_core_foo_Mmux_n028230,
      O => core_zpu_core_foo_n0282(19)
    );
  core_zpu_core_foo_Mmux_n0282271 : LUT6
    generic map(
      INIT => X"FEFCEECCFAF0AA00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(18),
      I1 => core_zpu_core_foo_memARead_stdlogic(18),
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(16),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => core_zpu_core_foo_Mmux_n028227
    );
  core_zpu_core_foo_Mmux_n0282272 : LUT5
    generic map(
      INIT => X"F111F000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_16_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => core_zpu_core_foo_Mmux_n028227,
      O => core_zpu_core_foo_n0282(18)
    );
  core_zpu_core_foo_Mmux_n0282241 : LUT6
    generic map(
      INIT => X"FEFCFAF0EECCAA00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(17),
      I1 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(15),
      I2 => core_zpu_core_foo_memARead_stdlogic(17),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => core_zpu_core_foo_Mmux_n028224
    );
  core_zpu_core_foo_Mmux_n0282242 : LUT5
    generic map(
      INIT => X"F111F000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_15_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => core_zpu_core_foo_Mmux_n028224,
      O => core_zpu_core_foo_n0282(17)
    );
  core_zpu_core_foo_Mmux_n0282211 : LUT6
    generic map(
      INIT => X"FEFCFAF0EECCAA00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(16),
      I1 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(14),
      I2 => core_zpu_core_foo_memARead_stdlogic(16),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => core_zpu_core_foo_Mmux_n028221
    );
  core_zpu_core_foo_Mmux_n0282212 : LUT5
    generic map(
      INIT => X"F111F000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_14_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => core_zpu_core_foo_Mmux_n028221,
      O => core_zpu_core_foo_n0282(16)
    );
  core_zpu_core_foo_Mmux_n0282181 : LUT6
    generic map(
      INIT => X"FEFCFAF0EECCAA00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(15),
      I1 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(13),
      I2 => core_zpu_core_foo_memARead_stdlogic(15),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => core_zpu_core_foo_Mmux_n028218
    );
  core_zpu_core_foo_Mmux_n0282182 : LUT5
    generic map(
      INIT => X"F111F000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_13_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => core_zpu_core_foo_Mmux_n028218,
      O => core_zpu_core_foo_n0282(15)
    );
  core_zpu_core_foo_Mmux_n0282151 : LUT6
    generic map(
      INIT => X"FEFCFAF0EECCAA00"
    )
    port map (
      I0 => core_zpu_core_foo_sp(14),
      I1 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(12),
      I2 => core_zpu_core_foo_memARead_stdlogic(14),
      I3 => core_zpu_core_foo_Mmux_n02821531,
      I4 => core_zpu_core_foo_Mmux_n0282152_96,
      I5 => core_zpu_core_foo_Mmux_n0282114,
      O => core_zpu_core_foo_Mmux_n028215
    );
  core_zpu_core_foo_Mmux_n0282152 : LUT5
    generic map(
      INIT => X"F111F000"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_sp_30_GND_14_o_add_61_OUT_12_Q,
      I3 => core_zpu_core_foo_Mmux_n0282121_97,
      I4 => core_zpu_core_foo_Mmux_n028215,
      O => core_zpu_core_foo_n0282(14)
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT27 : LUT6
    generic map(
      INIT => X"AAAAAAAAF0F0FFCC"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(0),
      I1 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_5_Q,
      I2 => core_zpu_core_foo_memARead_stdlogic(5),
      I3 => N34,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_5_Q
    );
  uart_inst_1_rx_n0119_inv_SW0 : LUT3
    generic map(
      INIT => X"DF"
    )
    port map (
      I0 => uart_inst_1_rx_rxProc_samplecnt(0),
      I1 => reset_IBUF_5,
      I2 => uart_inst_1_rx_timer_output_820,
      O => N38
    );
  uart_inst_1_rx_n0119_inv : LUT6
    generic map(
      INIT => X"1111111111011010"
    )
    port map (
      I0 => uart_inst_1_rx_rxProc_samplecnt(1),
      I1 => N38,
      I2 => uart_inst_1_rx_rxProc_bitpos(1),
      I3 => uart_inst_1_rx_rxProc_bitpos(0),
      I4 => uart_inst_1_rx_rxProc_bitpos(3),
      I5 => uart_inst_1_rx_rxProc_bitpos(2),
      O => uart_inst_1_rx_n0119_inv_838
    );
  uart_inst_1_rx_Mmux_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT1_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => uart_inst_1_rx_rxProc_samplecnt(1),
      I1 => uart_inst_1_rx_rxProc_samplecnt(0),
      O => N40
    );
  uart_inst_1_rx_Mmux_byteAvail_rxProc_bitpos_3_MUX_223_o1_SW0 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => uart_inst_1_rx_rxProc_bitpos(0),
      I1 => uart_inst_1_rx_rxProc_bitpos(2),
      O => N42
    );
  uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT7_SW0 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => uart_inst_1_rx_timer_countGen_counter_cnt(3),
      I1 => uart_inst_1_rx_timer_countGen_counter_cnt(4),
      O => N44
    );
  uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT7 : LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFA9AA"
    )
    port map (
      I0 => uart_inst_1_rx_timer_countGen_counter_cnt(6),
      I1 => uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT41,
      I2 => uart_inst_1_rx_timer_countGen_counter_cnt(5),
      I3 => N44,
      I4 => uart_inst_1_rx_timer_GND_21_o_GND_21_o_MUX_245_o,
      I5 => reset_IBUF_5,
      O => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_6_Q
    );
  ioButtons_3_IBUF : IBUF
    port map (
      I => ioButtons(3),
      O => ioButtons_3_IBUF_0
    );
  ioButtons_2_IBUF : IBUF
    port map (
      I => ioButtons(2),
      O => ioButtons_2_IBUF_1
    );
  ioButtons_1_IBUF : IBUF
    port map (
      I => ioButtons(1),
      O => ioButtons_1_IBUF_2
    );
  ioButtons_0_IBUF : IBUF
    port map (
      I => ioButtons(0),
      O => ioButtons_0_IBUF_3
    );
  reset_IBUF : IBUF
    port map (
      I => reset,
      O => reset_IBUF_5
    );
  uart_rx1_IBUF : IBUF
    port map (
      I => uart_rx1,
      O => uart_rx1_IBUF_6
    );
  ioLEDs_3_OBUF : OBUF
    port map (
      I => intIO_inst_LED_3_1_1396,
      O => ioLEDs(3)
    );
  ioLEDs_2_OBUF : OBUF
    port map (
      I => intIO_inst_LED_2_1_1397,
      O => ioLEDs(2)
    );
  ioLEDs_1_OBUF : OBUF
    port map (
      I => intIO_inst_LED_1_1_1398,
      O => ioLEDs(1)
    );
  ioLEDs_0_OBUF : OBUF
    port map (
      I => intIO_inst_LED_0_1_1399,
      O => ioLEDs(0)
    );
  clk_test_out_OBUF : OBUF
    port map (
      I => clk_test_out_OBUF_11,
      O => clk_test_out
    );
  uart_tx1_OBUF : OBUF
    port map (
      I => uart_inst_1_tx_txBufferReady_44,
      O => uart_tx1
    );
  core_state : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => core_state_glue_set_1178,
      R => reset_IBUF_5,
      Q => core_state_43
    );
  core_zpu_core_foo_state_FSM_FFd2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_FSM_FFd2_glue_set_1179,
      Q => core_zpu_core_foo_state_FSM_FFd2_586
    );
  core_zpu_core_foo_state_FSM_FFd3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_FSM_FFd3_glue_set_1180,
      Q => core_zpu_core_foo_state_FSM_FFd3_585
    );
  core_zpu_core_foo_state_FSM_FFd5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_FSM_FFd5_glue_set_1181,
      Q => core_zpu_core_foo_state_FSM_FFd5_583
    );
  core_zpu_core_foo_state_FSM_FFd5_glue_set : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => core_zpu_core_foo_state_FSM_FFd5_In_304,
      O => core_zpu_core_foo_state_FSM_FFd5_glue_set_1181
    );
  core_zpu_core_foo_state_FSM_FFd4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_FSM_FFd4_glue_set_1182,
      Q => core_zpu_core_foo_state_FSM_FFd4_584
    );
  core_zpu_core_foo_decodedOpcode_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_decodedOpcode_0_glue_set_1183,
      Q => core_zpu_core_foo_decodedOpcode(0)
    );
  core_zpu_core_foo_memBWrite_31 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_31_glue_set_1184,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(31)
    );
  core_zpu_core_foo_memBWrite_30 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_30_glue_set_1185,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(30)
    );
  core_zpu_core_foo_memBWrite_29 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_29_glue_set_1186,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(29)
    );
  core_zpu_core_foo_memBWrite_28 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_28_glue_set_1187,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(28)
    );
  core_zpu_core_foo_memBWrite_27 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_27_glue_set_1188,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(27)
    );
  core_zpu_core_foo_memBWrite_26 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_26_glue_set_1189,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(26)
    );
  core_zpu_core_foo_memBWrite_25 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_25_glue_set_1190,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(25)
    );
  core_zpu_core_foo_memBWrite_24 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_24_glue_set_1191,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(24)
    );
  core_zpu_core_foo_memBWrite_23 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_23_glue_set_1192,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(23)
    );
  core_zpu_core_foo_memBWrite_22 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_22_glue_set_1193,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(22)
    );
  core_zpu_core_foo_memBWrite_21 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_21_glue_set_1194,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(21)
    );
  core_zpu_core_foo_memBWrite_20 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_20_glue_set_1195,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(20)
    );
  core_zpu_core_foo_memBWrite_19 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_19_glue_set_1196,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(19)
    );
  core_zpu_core_foo_memBWrite_18 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_18_glue_set_1197,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(18)
    );
  core_zpu_core_foo_memBWrite_17 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_17_glue_set_1198,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(17)
    );
  core_zpu_core_foo_memBWrite_16 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_16_glue_set_1199,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(16)
    );
  core_zpu_core_foo_memBWrite_15 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_15_glue_set_1200,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(15)
    );
  core_zpu_core_foo_memBWrite_14 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_14_glue_set_1201,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(14)
    );
  core_zpu_core_foo_memBWrite_13 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_13_glue_set_1202,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(13)
    );
  core_zpu_core_foo_memBWrite_12 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_12_glue_set_1203,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(12)
    );
  core_zpu_core_foo_memBWrite_11 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_11_glue_set_1204,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(11)
    );
  core_zpu_core_foo_memBWrite_10 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_10_glue_set_1205,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(10)
    );
  core_zpu_core_foo_memBWrite_9 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_9_glue_set_1206,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(9)
    );
  core_zpu_core_foo_memBWrite_8 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_8_glue_set_1207,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(8)
    );
  core_zpu_core_foo_memBWrite_7 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_7_glue_set_1208,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(7)
    );
  core_zpu_core_foo_memBWrite_6 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_6_glue_set_1209,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(6)
    );
  core_zpu_core_foo_memBWrite_5 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_5_glue_set_1210,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(5)
    );
  core_zpu_core_foo_memBWrite_4 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_4_glue_set_1211,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(4)
    );
  core_zpu_core_foo_memBWrite_3 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_3_glue_set_1212,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(3)
    );
  core_zpu_core_foo_memBWrite_2 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_2_glue_set_1213,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(2)
    );
  core_zpu_core_foo_memBWrite_1 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_1_glue_set_1214,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(1)
    );
  core_zpu_core_foo_memBWrite_0 : FDR
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_memBWrite_0_glue_set_1215,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_memBWrite(0)
    );
  uart_inst_1_tx_timer_countGen_counter_cnt_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => uart_inst_1_tx_timer_countGen_counter_cnt_1_glue_set_1216,
      Q => uart_inst_1_tx_timer_countGen_counter_cnt(1)
    );
  uart_inst_1_tx_timer_countGen_counter_cnt_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => uart_inst_1_tx_timer_countGen_counter_cnt_0_glue_set_1217,
      Q => uart_inst_1_tx_timer_countGen_counter_cnt(0)
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_27_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(29),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_27_rt_1218
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_26_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(28),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_26_rt_1219
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_25_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(27),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_25_rt_1220
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_24_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(26),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_24_rt_1221
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_23_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(25),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_23_rt_1222
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_22_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(24),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_22_rt_1223
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_21_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(23),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_21_rt_1224
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_20_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(22),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_20_rt_1225
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_19_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(21),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_19_rt_1226
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_18_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(20),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_18_rt_1227
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_17_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(19),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_17_rt_1228
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_16_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(18),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_16_rt_1229
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_15_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(17),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_15_rt_1230
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_14_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(16),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_14_rt_1231
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(15),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_13_rt_1232
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(14),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_12_rt_1233
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(13),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_11_rt_1234
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(12),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_10_rt_1235
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(11),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_9_rt_1236
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(10),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_8_rt_1237
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(9),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_7_rt_1238
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(8),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_6_rt_1239
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(7),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_5_rt_1240
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(6),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_4_rt_1241
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(5),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_3_rt_1242
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(4),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_2_rt_1243
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(3),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_cy_1_rt_1244
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(12),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_10_rt_1245
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(11),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_9_rt_1246
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(10),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_8_rt_1247
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(9),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_7_rt_1248
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(8),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_6_rt_1249
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(7),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_cy_5_rt_1250
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_0_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(2),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_cy_0_rt_1251
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_29_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(29),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_29_rt_1252
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_28_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(28),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_28_rt_1253
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_27_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(27),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_27_rt_1254
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_26_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(26),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_26_rt_1255
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_25_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(25),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_25_rt_1256
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_24_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(24),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_24_rt_1257
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_23_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(23),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_23_rt_1258
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_22_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(22),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_22_rt_1259
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_21_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(21),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_21_rt_1260
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_20_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(20),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_20_rt_1261
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_19_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(19),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_19_rt_1262
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_18_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(18),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_18_rt_1263
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_17_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(17),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_17_rt_1264
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_16_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(16),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_16_rt_1265
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_15_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(15),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_15_rt_1266
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_14_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(14),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_14_rt_1267
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(13),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_13_rt_1268
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(12),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_12_rt_1269
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(11),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_11_rt_1270
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(10),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_10_rt_1271
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(9),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_9_rt_1272
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(8),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_8_rt_1273
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(7),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_7_rt_1274
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(6),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_6_rt_1275
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(5),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_5_rt_1276
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(4),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_4_rt_1277
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(3),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_3_rt_1278
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(2),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_2_rt_1279
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(1),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_cy_1_rt_1280
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_28_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(30),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_xor_28_rt_1281
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_sp(13),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_xor_11_rt_1282
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_30_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => core_zpu_core_foo_pc(30),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_xor_30_rt_1283
    );
  core_zpu_core_foo_state_FSM_FFd3_glue_set : LUT6
    generic map(
      INIT => X"FFFFFFFFBAEEBAAA"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => N46,
      I5 => core_zpu_core_foo_state_FSM_FFd3_In1_305,
      O => core_zpu_core_foo_state_FSM_FFd3_glue_set_1180
    );
  master_out_addr_31_PWR_9_o_equal_4_o_31_13_SW0 : LUT4
    generic map(
      INIT => X"DFFF"
    )
    port map (
      I0 => core_state_43,
      I1 => core_master_out_addr_19_Q,
      I2 => core_master_out_addr_20_Q,
      I3 => core_master_out_addr_12_Q,
      O => N52
    );
  Mmux_uart1_in_cyc11 : LUT6
    generic map(
      INIT => X"0000000001000000"
    )
    port map (
      I0 => core_master_out_addr_25_Q,
      I1 => core_master_out_addr_26_Q,
      I2 => core_master_out_addr_27_Q,
      I3 => master_out_addr_31_PWR_9_o_equal_4_o_31_11_912,
      I4 => master_out_addr_31_PWR_9_o_equal_4_o_31_12_913,
      I5 => N52,
      O => uart1_in_cyc
    );
  master_out_addr_31_PWR_9_o_equal_4_o_31_13_SW1 : LUT4
    generic map(
      INIT => X"FFF7"
    )
    port map (
      I0 => core_state_43,
      I1 => core_master_out_addr_19_Q,
      I2 => core_master_out_addr_20_Q,
      I3 => core_master_out_addr_12_Q,
      O => N54
    );
  Mmux_intIO_in_cyc11 : LUT6
    generic map(
      INIT => X"0000000001000000"
    )
    port map (
      I0 => core_master_out_addr_25_Q,
      I1 => core_master_out_addr_26_Q,
      I2 => core_master_out_addr_27_Q,
      I3 => master_out_addr_31_PWR_9_o_equal_4_o_31_11_912,
      I4 => master_out_addr_31_PWR_9_o_equal_4_o_31_12_913,
      I5 => N54,
      O => intIO_in_cyc
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT932_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(30),
      I1 => core_zpu_core_foo_memARead_stdlogic(30),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N56
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT932_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(30),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(30),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N57
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT938 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N57,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT93,
      I3 => N56,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_30_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT936_956,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_30_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT862_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(29),
      I1 => core_zpu_core_foo_memARead_stdlogic(29),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N59
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT862_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(29),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(29),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N60
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT868 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N60,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT86,
      I3 => N59,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_29_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT866_961,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_29_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT822_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(28),
      I1 => core_zpu_core_foo_memARead_stdlogic(28),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N62
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT822_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(28),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(28),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N63
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT828 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N63,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT82,
      I3 => N62,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_28_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT826_966,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_28_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT782_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(27),
      I1 => core_zpu_core_foo_memARead_stdlogic(27),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N65
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT782_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(27),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(27),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N66
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT788 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N66,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT78_967,
      I3 => N65,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_27_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT786_971,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_27_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT742_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(26),
      I1 => core_zpu_core_foo_memARead_stdlogic(26),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N68
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT742_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(26),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(26),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N69
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT748 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N69,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT74_972,
      I3 => N68,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_26_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT746_976,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_26_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT702_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(25),
      I1 => core_zpu_core_foo_memARead_stdlogic(25),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N71
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT702_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(25),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(25),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N72
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT708 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N72,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT70,
      I3 => N71,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_25_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT706_981,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_25_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT662_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(24),
      I1 => core_zpu_core_foo_memARead_stdlogic(24),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N74
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT662_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(24),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(24),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N75
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT668 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N75,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT66,
      I3 => N74,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_24_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT666_991,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_24_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT622_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(23),
      I1 => core_zpu_core_foo_memARead_stdlogic(23),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N77
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT622_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(23),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(23),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N78
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT628 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N78,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT62,
      I3 => N77,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_23_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT626_996,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_23_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT582_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(22),
      I1 => core_zpu_core_foo_memARead_stdlogic(22),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N80
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT582_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(22),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(22),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N81
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT588 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N81,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT58,
      I3 => N80,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_22_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT586_1001,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_22_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT542_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(21),
      I1 => core_zpu_core_foo_memARead_stdlogic(21),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N83
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT542_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(21),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(21),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N84
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT548 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N84,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT54,
      I3 => N83,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_21_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT546_1006,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_21_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT502_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(20),
      I1 => core_zpu_core_foo_memARead_stdlogic(20),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N86
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT502_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(20),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(20),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N87
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT508 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N87,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT50,
      I3 => N86,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_20_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT506_1011,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_20_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT965_SW0 : LUT4
    generic map(
      INIT => X"F5F4"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT963_943,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT96,
      I3 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT962_942,
      O => N89
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT966 : LUT5
    generic map(
      INIT => X"FD75A820"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd3_585,
      I1 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_31_Q,
      I2 => N90,
      I3 => N91,
      I4 => N89,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_31_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT432_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(19),
      I1 => core_zpu_core_foo_memARead_stdlogic(19),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N93
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT432_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(19),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(19),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N94
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT438 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N94,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT43,
      I3 => N93,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_19_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT436_1022,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_19_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT392_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(18),
      I1 => core_zpu_core_foo_memARead_stdlogic(18),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N96
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT392_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(18),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(18),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N97
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT398 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N97,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT39,
      I3 => N96,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_18_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT396_1027,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_18_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT352_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(17),
      I1 => core_zpu_core_foo_memARead_stdlogic(17),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N99
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT352_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(17),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(17),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N100
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT358 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N100,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT35_1028,
      I3 => N99,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_17_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT356_1032,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_17_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT312_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(16),
      I1 => core_zpu_core_foo_memARead_stdlogic(16),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N102
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT312_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(16),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(16),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N103
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT318 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N103,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT31_1033,
      I3 => N102,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_16_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT316_1037,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_16_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT272_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(15),
      I1 => core_zpu_core_foo_memARead_stdlogic(15),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N105
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT272_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(15),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(15),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N106
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT278 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N106,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT27,
      I3 => N105,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_15_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT276_1052,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_15_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT232_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(14),
      I1 => core_zpu_core_foo_memARead_stdlogic(14),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N108
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT232_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(14),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(14),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N109
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT238 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N109,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT23,
      I3 => N108,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_14_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT236_1057,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_14_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT192_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(13),
      I1 => core_zpu_core_foo_memARead_stdlogic(13),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N114
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT192_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(13),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(13),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N115
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT198 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N115,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT19,
      I3 => N114,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_13_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT196_1062,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_13_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT152_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(12),
      I1 => core_zpu_core_foo_memARead_stdlogic(12),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N117
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT152_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(12),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(12),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N118
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT158 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N118,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT15,
      I3 => N117,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_12_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT156_1067,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_12_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT112_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(11),
      I1 => core_zpu_core_foo_memARead_stdlogic(11),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N120
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT112_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(11),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(11),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N121
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT118 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N121,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT11,
      I3 => N120,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_11_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT118_1093,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_11_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT72_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(10),
      I1 => core_zpu_core_foo_memARead_stdlogic(10),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N123
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT72_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(10),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(10),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N124
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT78 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N124,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT7,
      I3 => N123,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_10_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT77_986,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_10_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1202_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(9),
      I1 => core_zpu_core_foo_memARead_stdlogic(9),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N126
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1202_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(9),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(9),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N127
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1208 : LUT6
    generic map(
      INIT => X"F5F5F5F5F1F1F0F5"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N127,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT120,
      I3 => N126,
      I4 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_9_Q,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1206_1072,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_9_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1166_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(8),
      I1 => core_zpu_core_foo_memARead_stdlogic(8),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N129
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1166_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(8),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(8),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N130
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT11612 : LUT6
    generic map(
      INIT => X"FFFFFFFF55551105"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N130,
      I2 => N129,
      I3 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_8_Q,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT11610_1081,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1164_1077,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_8_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(7),
      I1 => core_zpu_core_foo_memARead_stdlogic(7),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N132
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(7),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(7),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N133
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT11210 : LUT6
    generic map(
      INIT => X"FFFFFFFF55551105"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => N133,
      I2 => N132,
      I3 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_7_Q,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1128_1088,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1122_1083,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_7_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1084_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(6),
      I1 => core_zpu_core_foo_memARead_stdlogic(6),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N135
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1084_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(6),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(6),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N136
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1089 : LUT6
    generic map(
      INIT => X"FFFFFFFF44544555"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1087,
      I2 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_6_Q,
      I3 => N136,
      I4 => N135,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1082_930,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_6_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1054_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(5),
      I1 => core_zpu_core_foo_memARead_stdlogic(5),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N138
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1054_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(5),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(5),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N139
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1059 : LUT6
    generic map(
      INIT => X"FFFFFFFF44544555"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1057,
      I2 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_5_Q,
      I3 => N139,
      I4 => N138,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1052_934,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_5_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1024_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(4),
      I1 => core_zpu_core_foo_memARead_stdlogic(4),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N141
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1024_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(4),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(4),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N142
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1029 : LUT6
    generic map(
      INIT => X"FFFFFFFF44544555"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1027,
      I2 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_4_Q,
      I3 => N142,
      I4 => N141,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1022_938,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_4_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT997_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(3),
      I1 => core_zpu_core_foo_memARead_stdlogic(3),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N144
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT997_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(3),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(3),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N145
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT9912 : LUT6
    generic map(
      INIT => X"FFFFFFFF44544555"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT9910,
      I2 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_3_Q,
      I3 => N145,
      I4 => N144,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT995_919,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_3_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT897_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(2),
      I1 => core_zpu_core_foo_memARead_stdlogic(2),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N147
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT897_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(2),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(2),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N148
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT8912 : LUT6
    generic map(
      INIT => X"FFFFFFFF44544555"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT8910,
      I2 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_2_Q,
      I3 => N148,
      I4 => N147,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT895_926,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_2_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT467_SW0 : LUT5
    generic map(
      INIT => X"51FFFFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(1),
      I2 => core_zpu_core_foo_state_FSM_FFd4_584,
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N150
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT467_SW1 : LUT5
    generic map(
      INIT => X"7704FFFF"
    )
    port map (
      I0 => core_zpu_core_foo_memBRead_stdlogic(1),
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_memARead_stdlogic(1),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N151
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT4613 : LUT6
    generic map(
      INIT => X"FFFFFFFF44544555"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT4611,
      I2 => core_zpu_core_foo_memARead_31_memBRead_31_add_67_OUT_1_Q,
      I3 => N151,
      I4 => N150,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT465_1016,
      O => core_zpu_core_foo_state_4_X_14_o_wide_mux_77_OUT_1_Q
    );
  core_zpu_core_foo_decodedOpcode_0_glue_set_SW0 : LUT5
    generic map(
      INIT => X"0000DA6E"
    )
    port map (
      I0 => core_zpu_core_foo_decodeControl_tOpcode(0),
      I1 => core_zpu_core_foo_decodeControl_tOpcode(1),
      I2 => core_zpu_core_foo_decodeControl_tOpcode(2),
      I3 => core_zpu_core_foo_decodeControl_tOpcode(3),
      I4 => core_zpu_core_foo_decodeControl_tOpcode(4),
      O => N153
    );
  core_zpu_core_foo_decodedOpcode_0_glue_set : LUT6
    generic map(
      INIT => X"DDDDD888DDDDD88D"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodeControl_tOpcode(5),
      I3 => core_zpu_core_foo_decodeControl_tOpcode(6),
      I4 => core_zpu_core_foo_decodeControl_tOpcode(7),
      I5 => N153,
      O => core_zpu_core_foo_decodedOpcode_0_glue_set_1183
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT965_SW1 : LUT6
    generic map(
      INIT => X"F222C020E222C020"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd5_583,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_memARead_stdlogic(31),
      I3 => core_zpu_core_foo_state_FSM_FFd4_584,
      I4 => core_zpu_core_foo_memBRead_stdlogic(31),
      I5 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N90
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT965_SW2 : LUT6
    generic map(
      INIT => X"D5D19091D5919091"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_memARead_stdlogic(31),
      I3 => core_zpu_core_foo_state_FSM_FFd5_583,
      I4 => core_zpu_core_foo_memBRead_stdlogic(31),
      I5 => core_zpu_core_foo_state_FSM_FFd3_585,
      O => N91
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1083_SW0 : LUT5
    generic map(
      INIT => X"20000000"
    )
    port map (
      I0 => core_master_out_addr_2_Q,
      I1 => core_master_out_addr_3_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1081,
      I3 => master_out_addr_31_PWR_9_o_equal_4_o_31_1,
      I4 => uart1_in_cyc,
      O => N155
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1083 : LUT6
    generic map(
      INIT => X"FFFFFFFF88808080"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(6),
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_memBRead_stdlogic(6),
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      I5 => N155,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1082_930
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1053_SW0 : LUT5
    generic map(
      INIT => X"20000000"
    )
    port map (
      I0 => core_master_out_addr_2_Q,
      I1 => core_master_out_addr_3_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1051,
      I3 => master_out_addr_31_PWR_9_o_equal_4_o_31_1,
      I4 => uart1_in_cyc,
      O => N157
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1053 : LUT6
    generic map(
      INIT => X"FFFFFFFF88808080"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(5),
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_memBRead_stdlogic(5),
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      I5 => N157,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1052_934
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1023_SW0 : LUT5
    generic map(
      INIT => X"20000000"
    )
    port map (
      I0 => core_master_out_addr_2_Q,
      I1 => core_master_out_addr_3_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1021,
      I3 => master_out_addr_31_PWR_9_o_equal_4_o_31_1,
      I4 => uart1_in_cyc,
      O => N159
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1023 : LUT6
    generic map(
      INIT => X"FFFFFFFF88808080"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(4),
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_memBRead_stdlogic(4),
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      I5 => N159,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1022_938
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1123_SW0 : LUT5
    generic map(
      INIT => X"20000000"
    )
    port map (
      I0 => core_master_out_addr_2_Q,
      I1 => core_master_out_addr_3_Q,
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1121,
      I3 => master_out_addr_31_PWR_9_o_equal_4_o_31_1,
      I4 => uart1_in_cyc,
      O => N161
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1123 : LUT6
    generic map(
      INIT => X"FFFFFFFF88808080"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_memARead_stdlogic(7),
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_memBRead_stdlogic(7),
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      I5 => N161,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1122_1083
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT963 : LUT6
    generic map(
      INIT => X"A800B81000001010"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_decodedOpcode(4),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1124,
      I3 => core_zpu_core_foo_memARead_stdlogic(0),
      I4 => core_zpu_core_foo_memARead_stdlogic(31),
      I5 => core_zpu_core_foo_n0515_4_1,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT962_942
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT321 : LUT6
    generic map(
      INIT => X"00FF00EC00FF00A0"
    )
    port map (
      I0 => core_zpu_core_foo_pc(0),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT310,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT320_1046,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT321_1047
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_4_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => core_zpu_core_foo_sp(6),
      I1 => core_zpu_core_foo_opcode(4),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_31_OUT_lut_4_Q_208
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT1031 : LUT6
    generic map(
      INIT => X"0000EEEE0000EFEE"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_586,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mram_n04071,
      I3 => core_zpu_core_foo_n0515,
      I4 => core_zpu_core_foo_n0565,
      I5 => core_zpu_core_foo_n0526,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT103_98
    );
  core_zpu_core_foo_Mmux_n0282122 : LUT5
    generic map(
      INIT => X"0B080000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(13),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(11),
      I4 => core_zpu_core_foo_Mram_n040721,
      O => core_zpu_core_foo_Mmux_n0282122_1119
    );
  core_zpu_core_foo_Mmux_n028291 : LUT6
    generic map(
      INIT => X"FF08FF000B080000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(12),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(10),
      I4 => core_zpu_core_foo_Mram_n040721,
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n02829
    );
  core_zpu_core_foo_Mmux_n028261 : LUT6
    generic map(
      INIT => X"FF08FF000B080000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(11),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(9),
      I4 => core_zpu_core_foo_Mram_n040721,
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n02826
    );
  core_zpu_core_foo_Mmux_n028231 : LUT6
    generic map(
      INIT => X"FF08FF000B080000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(10),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(8),
      I4 => core_zpu_core_foo_Mram_n040721,
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n02823
    );
  core_zpu_core_foo_Mmux_n0282871 : LUT6
    generic map(
      INIT => X"FF08FF000B080000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(9),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(7),
      I4 => core_zpu_core_foo_Mram_n040721,
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n028287
    );
  core_zpu_core_foo_Mmux_n0282841 : LUT6
    generic map(
      INIT => X"FF08FF000B080000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(8),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(6),
      I4 => core_zpu_core_foo_Mram_n040721,
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n028284
    );
  core_zpu_core_foo_Mmux_n0282811 : LUT6
    generic map(
      INIT => X"FF08FF000B080000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(7),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(5),
      I4 => core_zpu_core_foo_Mram_n040721,
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n028281
    );
  core_zpu_core_foo_Mmux_n0282781 : LUT6
    generic map(
      INIT => X"FF08FF000B080000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(6),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(4),
      I4 => core_zpu_core_foo_Mram_n040721,
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n028278
    );
  core_zpu_core_foo_Mmux_n0282751 : LUT6
    generic map(
      INIT => X"FF08FF000B080000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(5),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(3),
      I4 => core_zpu_core_foo_Mram_n040721,
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n028275
    );
  core_zpu_core_foo_Mmux_n0282721 : LUT6
    generic map(
      INIT => X"FF08FF000B080000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(4),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(2),
      I4 => core_zpu_core_foo_Mram_n040721,
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n028272
    );
  Mmux_master_in_ack11_SW0 : LUT6
    generic map(
      INIT => X"FFEFFFFFFFFFFFFF"
    )
    port map (
      I0 => core_master_out_addr_25_Q,
      I1 => core_master_out_addr_26_Q,
      I2 => master_out_addr_31_PWR_9_o_equal_4_o_31_11_912,
      I3 => core_master_out_addr_27_Q,
      I4 => master_out_addr_31_PWR_9_o_equal_4_o_31_12_913,
      I5 => core_state_43,
      O => N50
    );
  core_zpu_core_foo_n0425_4_1 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode_2_1_1372,
      I1 => core_zpu_core_foo_decodedOpcode_0_2_1378,
      I2 => core_zpu_core_foo_decodedOpcode_3_1_1371,
      I3 => core_zpu_core_foo_decodedOpcode_4_2_1376,
      O => core_zpu_core_foo_n0425
    );
  core_zpu_core_foo_Mram_n0407111 : LUT5
    generic map(
      INIT => X"00000010"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode_4_1_1370,
      I1 => core_zpu_core_foo_decodedOpcode_1_1_1374,
      I2 => core_zpu_core_foo_decodedOpcode_0_1_1373,
      I3 => core_zpu_core_foo_decodedOpcode_2_1_1372,
      I4 => core_zpu_core_foo_decodedOpcode_3_1_1371,
      O => core_zpu_core_foo_Mram_n04071
    );
  core_zpu_core_foo_Mmux_n0282631 : LUT6
    generic map(
      INIT => X"F0F8F0F000B80000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(2),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(0),
      I3 => core_zpu_core_foo_decodedOpcode(4),
      I4 => core_zpu_core_foo_Mram_n040721,
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n028263_1112
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT4612 : LUT6
    generic map(
      INIT => X"3333103223010000"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_memARead_stdlogic(1),
      I3 => core_zpu_core_foo_memARead_stdlogic(30),
      I4 => N165,
      I5 => N166,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT4611
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT8911_SW0 : LUT6
    generic map(
      INIT => X"AAAAFCFCAAAAFC30"
    )
    port map (
      I0 => core_zpu_core_foo_pc(2),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT898,
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_2_Q,
      O => N168
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT8911_SW1 : LUT6
    generic map(
      INIT => X"AAAA30FCAAAA3030"
    )
    port map (
      I0 => core_zpu_core_foo_pc(2),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT898,
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_2_Q,
      O => N169
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT8911 : LUT6
    generic map(
      INIT => X"3333130220310000"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_memARead_stdlogic(29),
      I3 => core_zpu_core_foo_memARead_stdlogic(2),
      I4 => N168,
      I5 => N169,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT8910
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT9911_SW0 : LUT6
    generic map(
      INIT => X"AAAAFCFCAAAAFC30"
    )
    port map (
      I0 => core_zpu_core_foo_pc(3),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT998,
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_3_Q,
      O => N171
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT9911_SW1 : LUT6
    generic map(
      INIT => X"AAAA30FCAAAA3030"
    )
    port map (
      I0 => core_zpu_core_foo_pc(3),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT998,
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_3_Q,
      O => N172
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT9911 : LUT6
    generic map(
      INIT => X"3333130220310000"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_memARead_stdlogic(28),
      I3 => core_zpu_core_foo_memARead_stdlogic(3),
      I4 => N171,
      I5 => N172,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT9910
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1028_SW0 : LUT6
    generic map(
      INIT => X"AAAAFCFCAAAAFC30"
    )
    port map (
      I0 => core_zpu_core_foo_pc(4),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1025,
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_4_Q,
      O => N174
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1028_SW1 : LUT6
    generic map(
      INIT => X"AAAA30FCAAAA3030"
    )
    port map (
      I0 => core_zpu_core_foo_pc(4),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1025,
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_4_Q,
      O => N175
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1028 : LUT6
    generic map(
      INIT => X"3333130220310000"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_memARead_stdlogic(27),
      I3 => core_zpu_core_foo_memARead_stdlogic(4),
      I4 => N174,
      I5 => N175,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1027
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1058_SW0 : LUT6
    generic map(
      INIT => X"AAAAFCFCAAAAFC30"
    )
    port map (
      I0 => core_zpu_core_foo_pc(5),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1055,
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_5_Q,
      O => N177
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1058_SW1 : LUT6
    generic map(
      INIT => X"AAAA30FCAAAA3030"
    )
    port map (
      I0 => core_zpu_core_foo_pc(5),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1055,
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_5_Q,
      O => N178
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1058 : LUT6
    generic map(
      INIT => X"3333130220310000"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_memARead_stdlogic(26),
      I3 => core_zpu_core_foo_memARead_stdlogic(5),
      I4 => N177,
      I5 => N178,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1057
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1088_SW0 : LUT6
    generic map(
      INIT => X"AAAAFCFCAAAAFC30"
    )
    port map (
      I0 => core_zpu_core_foo_pc(6),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1085,
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_6_Q,
      O => N180
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1088_SW1 : LUT6
    generic map(
      INIT => X"AAAA30FCAAAA3030"
    )
    port map (
      I0 => core_zpu_core_foo_pc(6),
      I1 => core_zpu_core_foo_decodedOpcode(2),
      I2 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1085,
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_6_Q,
      O => N181
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1088 : LUT6
    generic map(
      INIT => X"3333130220310000"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_memARead_stdlogic(25),
      I3 => core_zpu_core_foo_memARead_stdlogic(6),
      I4 => N180,
      I5 => N181,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT1087
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT4612_SW0 : LUT6
    generic map(
      INIT => X"CCCCF0FACCCCF00A"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(1),
      I1 => core_zpu_core_foo_pc(1),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_1_Q,
      O => N165
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT4612_SW1 : LUT6
    generic map(
      INIT => X"CCCC00FACCCC000A"
    )
    port map (
      I0 => core_zpu_core_foo_opcode(1),
      I1 => core_zpu_core_foo_pc(1),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      I5 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_1_Q,
      O => N166
    );
  uart_inst_1_rx_Mmux_byteAvail_rxProc_bitpos_3_MUX_223_o1 : MUXF7
    port map (
      I0 => N183,
      I1 => N184,
      S => uart_inst_1_Mmux_slave_out_data11,
      O => uart_inst_1_rx_byteAvail_rxProc_bitpos_3_MUX_223_o
    );
  uart_inst_1_rx_Mmux_byteAvail_rxProc_bitpos_3_MUX_223_o1_G : LUT6
    generic map(
      INIT => X"8080AA8080808000"
    )
    port map (
      I0 => uart_inst_1_rx_timer_output_820,
      I1 => core_master_out_we_42,
      I2 => uart_inst_1_rx_byteAvail_821,
      I3 => uart_inst_1_rx_rxProc_bitpos(1),
      I4 => N42,
      I5 => uart_inst_1_rx_rxProc_bitpos(3),
      O => N184
    );
  core_zpu_core_foo_state_FSM_FFd3_In_SW1 : LUT5
    generic map(
      INIT => X"04041410"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_decodedOpcode(3),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(0),
      I4 => core_zpu_core_foo_decodedOpcode(1),
      O => N46
    );
  uart_inst_1_tx_n0095_inv1 : LUT5
    generic map(
      INIT => X"20FF00FF"
    )
    port map (
      I0 => core_master_out_we_42,
      I1 => core_master_out_addr_2_Q,
      I2 => core_master_out_addr_3_Q,
      I3 => uart_inst_1_tx_BufferLoaded_txProc_bitpos_3_MUX_235_o,
      I4 => uart1_in_cyc,
      O => uart_inst_1_tx_n0095_inv
    );
  uart_inst_1_tx_n0068_inv1 : LUT5
    generic map(
      INIT => X"00400000"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => core_master_out_we_42,
      I2 => core_master_out_addr_3_Q,
      I3 => core_master_out_addr_2_Q,
      I4 => uart1_in_cyc,
      O => uart_inst_1_tx_n0068_inv
    );
  uart_inst_1_rx_n0128_inv1 : LUT5
    generic map(
      INIT => X"FF10FF00"
    )
    port map (
      I0 => core_master_out_we_42,
      I1 => core_master_out_addr_3_Q,
      I2 => core_master_out_addr_2_Q,
      I3 => uart_inst_1_rx_timer_output_820,
      I4 => uart1_in_cyc,
      O => uart_inst_1_rx_n0128_inv
    );
  core_zpu_core_foo_Mmux_n0282691 : LUT6
    generic map(
      INIT => X"FF08FF000B080000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(3),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_decodedOpcode(4),
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(1),
      I4 => core_zpu_core_foo_Mram_n040721,
      I5 => core_zpu_core_foo_Mmux_n0282112,
      O => core_zpu_core_foo_Mmux_n028269
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT1111 : LUT5
    generic map(
      INIT => X"FFFF0FA4"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT111
    );
  core_zpu_core_foo_Mmux_n028215311 : LUT5
    generic map(
      INIT => X"00000002"
    )
    port map (
      I0 => core_zpu_core_foo_idim_flag_621,
      I1 => core_zpu_core_foo_decodedOpcode(1),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_decodedOpcode(4),
      O => core_zpu_core_foo_Mmux_n02821531
    );
  uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT61 : LUT5
    generic map(
      INIT => X"FFFFAAA9"
    )
    port map (
      I0 => uart_inst_1_rx_timer_countGen_counter_cnt(5),
      I1 => uart_inst_1_rx_timer_countGen_counter_cnt(3),
      I2 => uart_inst_1_rx_timer_countGen_counter_cnt(4),
      I3 => uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT41,
      I4 => reset_IBUF_5,
      O => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_5_Q
    );
  uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT51 : LUT6
    generic map(
      INIT => X"5041504150415040"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT41,
      I2 => uart_inst_1_rx_timer_countGen_counter_cnt(4),
      I3 => uart_inst_1_rx_timer_countGen_counter_cnt(3),
      I4 => uart_inst_1_rx_timer_countGen_counter_cnt(5),
      I5 => uart_inst_1_rx_timer_countGen_counter_cnt(6),
      O => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_4_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT27_SW0 : LUT5
    generic map(
      INIT => X"00000200"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_decodedOpcode(0),
      I2 => core_zpu_core_foo_decodedOpcode(3),
      I3 => core_zpu_core_foo_decodedOpcode(4),
      I4 => core_zpu_core_foo_decodedOpcode(2),
      O => N34
    );
  core_zpu_core_foo_n02241 : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => reset_IBUF_5,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_state_FSM_FFd5_583,
      O => core_zpu_core_foo_n0224_0
    );
  uart_inst_1_rx_Madd_rxProc_bitpos_3_GND_19_o_add_16_OUT_xor_0_11 : LUT6
    generic map(
      INIT => X"00000001FFFFFFFF"
    )
    port map (
      I0 => uart_inst_1_rx_rxProc_bitpos(1),
      I1 => uart_inst_1_rx_rxProc_bitpos(2),
      I2 => uart_inst_1_rx_rxProc_bitpos(3),
      I3 => uart_rx1_IBUF_6,
      I4 => uart_inst_1_rx_rxProc_bitpos(0),
      I5 => uart_inst_1_rx_rxProc_samplecnt(0),
      O => uart_inst_1_rx_rxProc_bitpos_3_GND_19_o_add_16_OUT_0_Q
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT1021 : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd2_1_1380,
      I1 => core_zpu_core_foo_state_FSM_FFd3_585,
      I2 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT241 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(30),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_30_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_30_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT221 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(29),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_29_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_29_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT211 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(28),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_28_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_28_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT201 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(27),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_27_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_27_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT191 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(26),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_26_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_26_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT181 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(25),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_25_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_25_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT171 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(24),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_24_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_24_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT161 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(23),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_23_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_23_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT151 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(22),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_22_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_22_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT141 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(21),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_21_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_21_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT110 : LUT6
    generic map(
      INIT => X"00000000F0F044CC"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_0_Q,
      I2 => core_zpu_core_foo_memARead_stdlogic(0),
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_0_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT210 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(10),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_10_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_10_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT32 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(11),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_11_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_11_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT41 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(12),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_12_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_12_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT51 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(13),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_13_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_13_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT61 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(14),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_14_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_14_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT71 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(15),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_15_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_15_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT81 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(16),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_16_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_16_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT91 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(17),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_17_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_17_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT101 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(18),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_18_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_18_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT111 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(19),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_19_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_19_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT121 : LUT6
    generic map(
      INIT => X"00000000F0F044CC"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_1_Q,
      I2 => core_zpu_core_foo_memARead_stdlogic(1),
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_1_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT131 : LUT6
    generic map(
      INIT => X"00000000CCCC50F0"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_memARead_stdlogic(20),
      I2 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_20_Q,
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_20_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT231 : LUT6
    generic map(
      INIT => X"00000000F0F044CC"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_2_Q,
      I2 => core_zpu_core_foo_memARead_stdlogic(2),
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_2_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT251 : LUT6
    generic map(
      INIT => X"00000000F0F044CC"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_3_Q,
      I2 => core_zpu_core_foo_memARead_stdlogic(3),
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_3_Q
    );
  core_zpu_core_foo_Mmux_decodedOpcode_4_pc_30_wide_mux_52_OUT261 : LUT6
    generic map(
      INIT => X"00000000F0F044CC"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(1),
      I1 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_4_Q,
      I2 => core_zpu_core_foo_memARead_stdlogic(4),
      I3 => core_zpu_core_foo_Mram_n040723,
      I4 => core_zpu_core_foo_n0435,
      I5 => core_zpu_core_foo_n0457,
      O => core_zpu_core_foo_decodedOpcode_4_pc_30_wide_mux_52_OUT_4_Q
    );
  uart_inst_1_rx_Mmux_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT1 : LUT6
    generic map(
      INIT => X"6666266266662667"
    )
    port map (
      I0 => uart_inst_1_rx_rxProc_bitpos(0),
      I1 => N40,
      I2 => uart_inst_1_rx_rxProc_bitpos(3),
      I3 => uart_inst_1_rx_rxProc_bitpos(1),
      I4 => uart_inst_1_rx_rxProc_bitpos(2),
      I5 => uart_rx1_IBUF_6,
      O => uart_inst_1_rx_rxProc_bitpos_3_rxProc_bitpos_3_wide_mux_12_OUT_0_Q
    );
  uart_inst_1_rx_Mmux_byteAvail_rxProc_bitpos_3_MUX_223_o1_F : LUT6
    generic map(
      INIT => X"888888A888888880"
    )
    port map (
      I0 => uart_inst_1_rx_timer_output_820,
      I1 => uart_inst_1_rx_byteAvail_821,
      I2 => uart_inst_1_rx_rxProc_bitpos(1),
      I3 => uart_inst_1_rx_rxProc_bitpos(0),
      I4 => uart_inst_1_rx_rxProc_bitpos(2),
      I5 => uart_inst_1_rx_rxProc_bitpos(3),
      O => N183
    );
  uart_inst_1_tx_timer_countGen_counter_cnt_1_glue_set : LUT4
    generic map(
      INIT => X"FFA6"
    )
    port map (
      I0 => uart_inst_1_tx_timer_countGen_counter_cnt(1),
      I1 => uart_inst_1_rx_timer_output_820,
      I2 => uart_inst_1_tx_timer_countGen_counter_cnt(0),
      I3 => reset_IBUF_5,
      O => uart_inst_1_tx_timer_countGen_counter_cnt_1_glue_set_1216
    );
  uart_inst_1_tx_timer_countGen_counter_cnt_0_glue_set : LUT3
    generic map(
      INIT => X"F6"
    )
    port map (
      I0 => uart_inst_1_rx_timer_output_820,
      I1 => uart_inst_1_tx_timer_countGen_counter_cnt(0),
      I2 => reset_IBUF_5,
      O => uart_inst_1_tx_timer_countGen_counter_cnt_0_glue_set_1217
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT81 : LUT6
    generic map(
      INIT => X"000000B800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(3),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(1),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT8
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT21 : LUT6
    generic map(
      INIT => X"000000B800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(2),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(0),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT2
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT61 : LUT6
    generic map(
      INIT => X"000B000800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(13),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(11),
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT6
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT41 : LUT6
    generic map(
      INIT => X"000B000800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(12),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(10),
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT4
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT241 : LUT6
    generic map(
      INIT => X"000B000800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(11),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(9),
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT24
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT221 : LUT6
    generic map(
      INIT => X"000B000800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(10),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(8),
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT22_1128
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT201 : LUT6
    generic map(
      INIT => X"000B000800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(9),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(7),
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT20
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT181 : LUT6
    generic map(
      INIT => X"000B000800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(8),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(6),
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT18
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT161 : LUT6
    generic map(
      INIT => X"000B000800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(7),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(5),
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT16
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT141 : LUT6
    generic map(
      INIT => X"000B000800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(6),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(4),
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT14
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT121 : LUT6
    generic map(
      INIT => X"00000B0800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(5),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(3),
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT12
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT101 : LUT6
    generic map(
      INIT => X"000000B800000000"
    )
    port map (
      I0 => core_zpu_core_foo_sp(4),
      I1 => core_zpu_core_foo_idim_flag_621,
      I2 => core_zpu_core_foo_GND_14_o_GND_14_o_sub_23_OUT(2),
      I3 => core_zpu_core_foo_state_FSM_FFd2_586,
      I4 => core_zpu_core_foo_state_FSM_FFd3_585,
      I5 => core_zpu_core_foo_Mram_n04071,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT10
    );
  core_zpu_core_foo_memBWrite_31_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(31),
      O => core_zpu_core_foo_memBWrite_31_glue_set_1184
    );
  core_zpu_core_foo_memBWrite_30_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(30),
      O => core_zpu_core_foo_memBWrite_30_glue_set_1185
    );
  core_zpu_core_foo_memBWrite_29_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(29),
      O => core_zpu_core_foo_memBWrite_29_glue_set_1186
    );
  core_zpu_core_foo_memBWrite_28_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(28),
      O => core_zpu_core_foo_memBWrite_28_glue_set_1187
    );
  core_zpu_core_foo_memBWrite_27_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(27),
      O => core_zpu_core_foo_memBWrite_27_glue_set_1188
    );
  core_zpu_core_foo_memBWrite_26_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(26),
      O => core_zpu_core_foo_memBWrite_26_glue_set_1189
    );
  core_zpu_core_foo_memBWrite_25_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(25),
      O => core_zpu_core_foo_memBWrite_25_glue_set_1190
    );
  core_zpu_core_foo_memBWrite_24_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(24),
      O => core_zpu_core_foo_memBWrite_24_glue_set_1191
    );
  core_zpu_core_foo_memBWrite_23_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(23),
      O => core_zpu_core_foo_memBWrite_23_glue_set_1192
    );
  core_zpu_core_foo_memBWrite_22_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(22),
      O => core_zpu_core_foo_memBWrite_22_glue_set_1193
    );
  core_zpu_core_foo_memBWrite_21_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(21),
      O => core_zpu_core_foo_memBWrite_21_glue_set_1194
    );
  core_zpu_core_foo_memBWrite_20_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(20),
      O => core_zpu_core_foo_memBWrite_20_glue_set_1195
    );
  core_zpu_core_foo_memBWrite_19_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(19),
      O => core_zpu_core_foo_memBWrite_19_glue_set_1196
    );
  core_zpu_core_foo_memBWrite_18_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(18),
      O => core_zpu_core_foo_memBWrite_18_glue_set_1197
    );
  core_zpu_core_foo_memBWrite_17_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(17),
      O => core_zpu_core_foo_memBWrite_17_glue_set_1198
    );
  core_zpu_core_foo_memBWrite_16_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(16),
      O => core_zpu_core_foo_memBWrite_16_glue_set_1199
    );
  core_zpu_core_foo_memBWrite_15_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(15),
      O => core_zpu_core_foo_memBWrite_15_glue_set_1200
    );
  core_zpu_core_foo_memBWrite_14_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(14),
      O => core_zpu_core_foo_memBWrite_14_glue_set_1201
    );
  core_zpu_core_foo_memBWrite_13_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(13),
      O => core_zpu_core_foo_memBWrite_13_glue_set_1202
    );
  core_zpu_core_foo_memBWrite_12_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(12),
      O => core_zpu_core_foo_memBWrite_12_glue_set_1203
    );
  core_zpu_core_foo_memBWrite_11_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(11),
      O => core_zpu_core_foo_memBWrite_11_glue_set_1204
    );
  core_zpu_core_foo_memBWrite_10_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(10),
      O => core_zpu_core_foo_memBWrite_10_glue_set_1205
    );
  core_zpu_core_foo_memBWrite_9_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(9),
      O => core_zpu_core_foo_memBWrite_9_glue_set_1206
    );
  core_zpu_core_foo_memBWrite_8_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(8),
      O => core_zpu_core_foo_memBWrite_8_glue_set_1207
    );
  core_zpu_core_foo_memBWrite_7_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(7),
      O => core_zpu_core_foo_memBWrite_7_glue_set_1208
    );
  core_zpu_core_foo_memBWrite_6_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(6),
      O => core_zpu_core_foo_memBWrite_6_glue_set_1209
    );
  core_zpu_core_foo_memBWrite_5_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(5),
      O => core_zpu_core_foo_memBWrite_5_glue_set_1210
    );
  core_zpu_core_foo_memBWrite_4_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(4),
      O => core_zpu_core_foo_memBWrite_4_glue_set_1211
    );
  core_zpu_core_foo_memBWrite_3_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(3),
      O => core_zpu_core_foo_memBWrite_3_glue_set_1212
    );
  core_zpu_core_foo_memBWrite_2_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(2),
      O => core_zpu_core_foo_memBWrite_2_glue_set_1213
    );
  core_zpu_core_foo_memBWrite_1_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(1),
      O => core_zpu_core_foo_memBWrite_1_glue_set_1214
    );
  core_zpu_core_foo_memBWrite_0_glue_set : LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd4_584,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => core_zpu_core_foo_state_FSM_FFd2_586,
      I3 => core_zpu_core_foo_state_FSM_FFd3_585,
      I4 => core_zpu_core_foo_memARead_stdlogic(0),
      O => core_zpu_core_foo_memBWrite_0_glue_set_1215
    );
  uart_inst_1_rx_timer_Mmux_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT42 : LUT5
    generic map(
      INIT => X"FFFFAAA9"
    )
    port map (
      I0 => uart_inst_1_rx_timer_countGen_counter_cnt(3),
      I1 => uart_inst_1_rx_timer_countGen_counter_cnt(1),
      I2 => uart_inst_1_rx_timer_countGen_counter_cnt(2),
      I3 => uart_inst_1_rx_timer_countGen_counter_cnt(0),
      I4 => reset_IBUF_5,
      O => uart_inst_1_rx_timer_countGen_counter_cnt_6_PWR_39_o_mux_4_OUT_3_Q
    );
  core_zpu_core_foo_decodedOpcode_4_1 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_4_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode_4_1_1370
    );
  core_zpu_core_foo_decodedOpcode_3_1 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_3_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode_3_1_1371
    );
  core_zpu_core_foo_decodedOpcode_2_1 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_2_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode_2_1_1372
    );
  core_zpu_core_foo_decodedOpcode_0_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_decodedOpcode_0_glue_set_1183,
      Q => core_zpu_core_foo_decodedOpcode_0_1_1373
    );
  core_zpu_core_foo_decodedOpcode_1_1 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_1_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode_1_1_1374
    );
  core_zpu_core_foo_decodedOpcode_3_2 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_3_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode_3_2_1375
    );
  core_zpu_core_foo_decodedOpcode_4_2 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_4_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode_4_2_1376
    );
  core_zpu_core_foo_decodedOpcode_2_2 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_2_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode_2_2_1377
    );
  core_zpu_core_foo_decodedOpcode_0_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_decodedOpcode_0_glue_set_1183,
      Q => core_zpu_core_foo_decodedOpcode_0_2_1378
    );
  core_zpu_core_foo_idim_flag_1 : FDRE
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_n0224,
      D => core_zpu_core_foo_Mram_n04071,
      R => reset_IBUF_5,
      Q => core_zpu_core_foo_idim_flag_1_1379
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT1041 : LUT6
    generic map(
      INIT => X"FFFF1000FFFF0000"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode_4_2_1376,
      I1 => core_zpu_core_foo_decodedOpcode(1),
      I2 => core_zpu_core_foo_decodedOpcode(2),
      I3 => core_zpu_core_foo_decodedOpcode(3),
      I4 => core_zpu_core_foo_n0565,
      I5 => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT102_101,
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_75_OUT104
    );
  core_zpu_core_foo_state_FSM_FFd2_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_FSM_FFd2_glue_set_1179,
      Q => core_zpu_core_foo_state_FSM_FFd2_1_1380
    );
  core_zpu_core_foo_state_FSM_FFd4_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      D => core_zpu_core_foo_state_FSM_FFd4_glue_set_1182,
      Q => core_zpu_core_foo_state_FSM_FFd4_1_1381
    );
  core_master_out_we : FD
    port map (
      C => clk_test_out_OBUF_11,
      D => core_master_out_we_rstpot_1382,
      Q => core_master_out_we_42
    );
  core_master_out_we_rstpot : LUT6
    generic map(
      INIT => X"0044444400505050"
    )
    port map (
      I0 => reset_IBUF_5,
      I1 => core_zpu_core_foo_out_mem_writeEnable_60,
      I2 => core_master_out_we_42,
      I3 => core_state_43,
      I4 => master_in_ack,
      I5 => core_n0066_inv1,
      O => core_master_out_we_rstpot_1382
    );
  core_zpu_core_foo_decodedOpcode_3_3 : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk_test_out_OBUF_11,
      CE => core_zpu_core_foo_reset_inv,
      D => core_zpu_core_foo_tOpcode_sel_1_GND_14_o_mux_18_OUT_3_Q,
      R => core_zpu_core_foo_n0267,
      Q => core_zpu_core_foo_decodedOpcode_3_3_1383
    );
  uart_inst_1_tx_Mmux_GND_20_o_transmit_7_Mux_4_o_2_f7 : MUXF7
    port map (
      I0 => N207,
      I1 => N208,
      S => uart_inst_1_tx_txProc_bitpos(0),
      O => uart_inst_1_tx_GND_20_o_transmit_7_Mux_4_o
    );
  uart_inst_1_tx_Mmux_GND_20_o_transmit_7_Mux_4_o_2_f7_F : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => uart_inst_1_tx_txProc_bitpos(2),
      I1 => uart_inst_1_tx_txProc_bitpos(1),
      I2 => uart_inst_1_tx_transmit(2),
      I3 => uart_inst_1_tx_transmit(4),
      I4 => uart_inst_1_tx_transmit(0),
      I5 => uart_inst_1_tx_transmit(6),
      O => N207
    );
  uart_inst_1_tx_Mmux_GND_20_o_transmit_7_Mux_4_o_2_f7_G : LUT6
    generic map(
      INIT => X"FD75B931EC64A820"
    )
    port map (
      I0 => uart_inst_1_tx_txProc_bitpos(1),
      I1 => uart_inst_1_tx_txProc_bitpos(2),
      I2 => uart_inst_1_tx_transmit(1),
      I3 => uart_inst_1_tx_transmit(5),
      I4 => uart_inst_1_tx_transmit(3),
      I5 => uart_inst_1_tx_transmit(7),
      O => N208
    );
  core_zpu_core_foo_state_FSM_FFd2_glue_set : MUXF7
    port map (
      I0 => N209,
      I1 => N210,
      S => master_in_ack,
      O => core_zpu_core_foo_state_FSM_FFd2_glue_set_1179
    );
  core_zpu_core_foo_state_FSM_FFd2_glue_set_F : LUT6
    generic map(
      INIT => X"FFFFFFFF4545C5C1"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd3_585,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_state_FSM_FFd5_583,
      I3 => N16,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      I5 => reset_IBUF_5,
      O => N209
    );
  core_zpu_core_foo_state_FSM_FFd2_glue_set_G : LUT6
    generic map(
      INIT => X"FFFFFFFF4440D5D1"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd3_585,
      I1 => core_zpu_core_foo_state_FSM_FFd4_584,
      I2 => core_zpu_core_foo_state_FSM_FFd5_583,
      I3 => N16,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      I5 => reset_IBUF_5,
      O => N210
    );
  core_state_glue_set : MUXF7
    port map (
      I0 => N211,
      I1 => N212,
      S => core_state_43,
      O => core_state_glue_set_1178
    );
  core_state_glue_set_F : LUT5
    generic map(
      INIT => X"02020200"
    )
    port map (
      I0 => core_zpu_core_foo_out_mem_addr_31_Q,
      I1 => core_zpu_core_foo_out_mem_addr_30_Q,
      I2 => N2,
      I3 => core_zpu_core_foo_out_mem_writeEnable_60,
      I4 => core_zpu_core_foo_out_mem_readEnable_59,
      O => N211
    );
  core_state_glue_set_G : LUT6
    generic map(
      INIT => X"FFFFFFFFEE7FFF7F"
    )
    port map (
      I0 => core_master_out_addr_12_Q,
      I1 => core_master_out_addr_20_Q,
      I2 => uart1_in_cyc,
      I3 => core_master_out_addr_19_Q,
      I4 => intIO_in_cyc,
      I5 => N50,
      O => N212
    );
  core_n0066_inv11 : MUXF7
    port map (
      I0 => N213,
      I1 => N214,
      S => core_state_43,
      O => core_n0066_inv1
    );
  core_n0066_inv11_F : LUT5
    generic map(
      INIT => X"02020200"
    )
    port map (
      I0 => core_zpu_core_foo_out_mem_addr_31_Q,
      I1 => core_zpu_core_foo_out_mem_addr_30_Q,
      I2 => N2,
      I3 => core_zpu_core_foo_out_mem_writeEnable_60,
      I4 => core_zpu_core_foo_out_mem_readEnable_59,
      O => N213
    );
  core_n0066_inv11_G : LUT6
    generic map(
      INIT => X"0820002008000000"
    )
    port map (
      I0 => master_out_addr_31_PWR_9_o_equal_4_o_31_1,
      I1 => core_master_out_addr_12_Q,
      I2 => core_master_out_addr_19_Q,
      I3 => core_master_out_addr_20_Q,
      I4 => uart1_in_cyc,
      I5 => intIO_in_cyc,
      O => N214
    );
  core_zpu_core_foo_state_FSM_FFd4_glue_set : MUXF7
    port map (
      I0 => N215,
      I1 => N216,
      S => core_zpu_core_foo_state_FSM_FFd4_584,
      O => core_zpu_core_foo_state_FSM_FFd4_glue_set_1182
    );
  core_zpu_core_foo_state_FSM_FFd4_glue_set_F : LUT6
    generic map(
      INIT => X"FFFFFFFF55551101"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd3_585,
      I1 => core_zpu_core_foo_state_FSM_FFd2_586,
      I2 => core_zpu_core_foo_state_FSM_FFd5_583,
      I3 => master_in_ack,
      I4 => core_zpu_core_foo_state_FSM_FFd4_In1_950,
      I5 => reset_IBUF_5,
      O => N215
    );
  core_zpu_core_foo_state_FSM_FFd4_glue_set_G : LUT6
    generic map(
      INIT => X"FFFFFFFF7777DDDC"
    )
    port map (
      I0 => core_zpu_core_foo_state_FSM_FFd3_585,
      I1 => core_zpu_core_foo_state_FSM_FFd5_583,
      I2 => N10,
      I3 => core_zpu_core_foo_state_FSM_FFd4_In1_950,
      I4 => core_zpu_core_foo_state_FSM_FFd2_586,
      I5 => reset_IBUF_5,
      O => N216
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT320 : MUXF7
    port map (
      I0 => N217,
      I1 => N218,
      S => core_zpu_core_foo_decodedOpcode(3),
      O => core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT320_1046
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT320_F : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(4),
      I1 => core_zpu_core_foo_pc_30_GND_14_o_add_21_OUT_0_Q,
      O => N217
    );
  core_zpu_core_foo_Mmux_state_4_X_14_o_wide_mux_77_OUT320_G : LUT5
    generic map(
      INIT => X"FF010101"
    )
    port map (
      I0 => core_zpu_core_foo_decodedOpcode(0),
      I1 => core_zpu_core_foo_decodedOpcode_4_2_1376,
      I2 => core_zpu_core_foo_memARead_stdlogic(0),
      I3 => core_zpu_core_foo_memARead_stdlogic(31),
      I4 => core_zpu_core_foo_n0425,
      O => N218
    );
  core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_lut_0_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(2),
      O => core_zpu_core_foo_Madd_sp_30_GND_14_o_add_61_OUT_lut_0_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_28_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(30),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_28_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_27_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(29),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_27_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_26_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(28),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_26_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_25_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(27),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_25_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_24_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(26),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_24_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_23_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(25),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_23_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_22_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(24),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_22_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_21_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(23),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_21_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_20_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(22),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_20_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_19_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(21),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_19_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_18_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(20),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_18_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_17_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(19),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_17_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_16_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(18),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_16_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_15_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(17),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_15_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_14_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(16),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_14_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_13_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(15),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_13_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_12_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(14),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_12_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_11_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(13),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_11_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_10_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(12),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_10_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_9_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(11),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_9_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_8_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(10),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_8_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_7_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(9),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_7_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_6_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(8),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_6_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_5_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(7),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_5_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_4_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(6),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_4_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_3_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(5),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_3_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_2_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(4),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_2_Q
    );
  core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_1_INV_0 : INV
    port map (
      I => core_zpu_core_foo_sp(3),
      O => core_zpu_core_foo_Msub_GND_14_o_GND_14_o_sub_23_OUT_28_0_lut_1_Q
    );
  core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_lut_0_INV_0 : INV
    port map (
      I => core_zpu_core_foo_pc(0),
      O => core_zpu_core_foo_Madd_pc_30_GND_14_o_add_21_OUT_lut_0_Q
    );
  core_zpu_core_foo_reset_inv1_INV_0 : INV
    port map (
      I => reset_IBUF_5,
      O => core_zpu_core_foo_reset_inv
    );
  core_zpu_core_foo_memory_RAMB16_S4_S4_inst0 : RAMB16BWER
    generic map(
      DATA_WIDTH_A => 4,
      DATA_WIDTH_B => 4,
      DOA_REG => 0,
      DOB_REG => 0,
      EN_RSTRAM_A => TRUE,
      EN_RSTRAM_B => TRUE,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RSTTYPE => "SYNC",
      RST_PRIORITY_A => "CE",
      RST_PRIORITY_B => "CE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "SPARTAN6",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST"
    )
    port map (
      REGCEA => dummy_out_ack,
      CLKA => clk_test_out_OBUF_11,
      ENB => N0,
      RSTB => dummy_out_ack,
      CLKB => clk_test_out_OBUF_11,
      REGCEB => dummy_out_ack,
      RSTA => dummy_out_ack,
      ENA => N0,
      DIPA(3) => dummy_out_ack,
      DIPA(2) => dummy_out_ack,
      DIPA(1) => dummy_out_ack,
      DIPA(0) => dummy_out_ack,
      WEA(3) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(2) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(1) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(0) => core_zpu_core_foo_memAWriteEnable_725,
      DOA(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_31_UNCONNECTED,
      DOA(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_30_UNCONNECTED,
      DOA(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_29_UNCONNECTED,
      DOA(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_28_UNCONNECTED,
      DOA(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_27_UNCONNECTED,
      DOA(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_26_UNCONNECTED,
      DOA(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_25_UNCONNECTED,
      DOA(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_24_UNCONNECTED,
      DOA(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_23_UNCONNECTED,
      DOA(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_22_UNCONNECTED,
      DOA(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_21_UNCONNECTED,
      DOA(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_20_UNCONNECTED,
      DOA(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_19_UNCONNECTED,
      DOA(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_18_UNCONNECTED,
      DOA(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_17_UNCONNECTED,
      DOA(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_16_UNCONNECTED,
      DOA(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_15_UNCONNECTED,
      DOA(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_14_UNCONNECTED,
      DOA(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_13_UNCONNECTED,
      DOA(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_12_UNCONNECTED,
      DOA(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_11_UNCONNECTED,
      DOA(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_10_UNCONNECTED,
      DOA(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_9_UNCONNECTED,
      DOA(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_8_UNCONNECTED,
      DOA(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_7_UNCONNECTED,
      DOA(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_6_UNCONNECTED,
      DOA(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_5_UNCONNECTED,
      DOA(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOA_4_UNCONNECTED,
      DOA(3) => core_zpu_core_foo_memARead_stdlogic(3),
      DOA(2) => core_zpu_core_foo_memARead_stdlogic(2),
      DOA(1) => core_zpu_core_foo_memARead_stdlogic(1),
      DOA(0) => core_zpu_core_foo_memARead_stdlogic(0),
      ADDRA(13) => dummy_out_ack,
      ADDRA(12) => dummy_out_ack,
      ADDRA(11) => core_zpu_core_foo_memAAddr(13),
      ADDRA(10) => core_zpu_core_foo_memAAddr(12),
      ADDRA(9) => core_zpu_core_foo_memAAddr(11),
      ADDRA(8) => core_zpu_core_foo_memAAddr(10),
      ADDRA(7) => core_zpu_core_foo_memAAddr(9),
      ADDRA(6) => core_zpu_core_foo_memAAddr(8),
      ADDRA(5) => core_zpu_core_foo_memAAddr(7),
      ADDRA(4) => core_zpu_core_foo_memAAddr(6),
      ADDRA(3) => core_zpu_core_foo_memAAddr(5),
      ADDRA(2) => core_zpu_core_foo_memAAddr(4),
      ADDRA(1) => core_zpu_core_foo_memAAddr(3),
      ADDRA(0) => core_zpu_core_foo_memAAddr(2),
      ADDRB(13) => dummy_out_ack,
      ADDRB(12) => dummy_out_ack,
      ADDRB(11) => core_zpu_core_foo_memBAddr(13),
      ADDRB(10) => core_zpu_core_foo_memBAddr(12),
      ADDRB(9) => core_zpu_core_foo_memBAddr(11),
      ADDRB(8) => core_zpu_core_foo_memBAddr(10),
      ADDRB(7) => core_zpu_core_foo_memBAddr(9),
      ADDRB(6) => core_zpu_core_foo_memBAddr(8),
      ADDRB(5) => core_zpu_core_foo_memBAddr(7),
      ADDRB(4) => core_zpu_core_foo_memBAddr(6),
      ADDRB(3) => core_zpu_core_foo_memBAddr(5),
      ADDRB(2) => core_zpu_core_foo_memBAddr(4),
      ADDRB(1) => core_zpu_core_foo_memBAddr(3),
      ADDRB(0) => core_zpu_core_foo_memBAddr(2),
      DIB(31) => dummy_out_ack,
      DIB(30) => dummy_out_ack,
      DIB(29) => dummy_out_ack,
      DIB(28) => dummy_out_ack,
      DIB(27) => dummy_out_ack,
      DIB(26) => dummy_out_ack,
      DIB(25) => dummy_out_ack,
      DIB(24) => dummy_out_ack,
      DIB(23) => dummy_out_ack,
      DIB(22) => dummy_out_ack,
      DIB(21) => dummy_out_ack,
      DIB(20) => dummy_out_ack,
      DIB(19) => dummy_out_ack,
      DIB(18) => dummy_out_ack,
      DIB(17) => dummy_out_ack,
      DIB(16) => dummy_out_ack,
      DIB(15) => dummy_out_ack,
      DIB(14) => dummy_out_ack,
      DIB(13) => dummy_out_ack,
      DIB(12) => dummy_out_ack,
      DIB(11) => dummy_out_ack,
      DIB(10) => dummy_out_ack,
      DIB(9) => dummy_out_ack,
      DIB(8) => dummy_out_ack,
      DIB(7) => dummy_out_ack,
      DIB(6) => dummy_out_ack,
      DIB(5) => dummy_out_ack,
      DIB(4) => dummy_out_ack,
      DIB(3) => core_zpu_core_foo_memBWrite(3),
      DIB(2) => core_zpu_core_foo_memBWrite(2),
      DIB(1) => core_zpu_core_foo_memBWrite(1),
      DIB(0) => core_zpu_core_foo_memBWrite(0),
      DOPA(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPA_3_UNCONNECTED,
      DOPA(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPA_2_UNCONNECTED,
      DOPA(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPA_1_UNCONNECTED,
      DOPA(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPA_0_UNCONNECTED,
      DIPB(3) => dummy_out_ack,
      DIPB(2) => dummy_out_ack,
      DIPB(1) => dummy_out_ack,
      DIPB(0) => dummy_out_ack,
      DOPB(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPB_3_UNCONNECTED,
      DOPB(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPB_2_UNCONNECTED,
      DOPB(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPB_1_UNCONNECTED,
      DOPB(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOPB_0_UNCONNECTED,
      DOB(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_31_UNCONNECTED,
      DOB(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_30_UNCONNECTED,
      DOB(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_29_UNCONNECTED,
      DOB(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_28_UNCONNECTED,
      DOB(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_27_UNCONNECTED,
      DOB(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_26_UNCONNECTED,
      DOB(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_25_UNCONNECTED,
      DOB(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_24_UNCONNECTED,
      DOB(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_23_UNCONNECTED,
      DOB(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_22_UNCONNECTED,
      DOB(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_21_UNCONNECTED,
      DOB(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_20_UNCONNECTED,
      DOB(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_19_UNCONNECTED,
      DOB(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_18_UNCONNECTED,
      DOB(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_17_UNCONNECTED,
      DOB(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_16_UNCONNECTED,
      DOB(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_15_UNCONNECTED,
      DOB(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_14_UNCONNECTED,
      DOB(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_13_UNCONNECTED,
      DOB(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_12_UNCONNECTED,
      DOB(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_11_UNCONNECTED,
      DOB(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_10_UNCONNECTED,
      DOB(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_9_UNCONNECTED,
      DOB(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_8_UNCONNECTED,
      DOB(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_7_UNCONNECTED,
      DOB(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_6_UNCONNECTED,
      DOB(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_5_UNCONNECTED,
      DOB(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst0_DOB_4_UNCONNECTED,
      DOB(3) => core_zpu_core_foo_memBRead_stdlogic(3),
      DOB(2) => core_zpu_core_foo_memBRead_stdlogic(2),
      DOB(1) => core_zpu_core_foo_memBRead_stdlogic(1),
      DOB(0) => core_zpu_core_foo_memBRead_stdlogic(0),
      WEB(3) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(2) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(1) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(0) => core_zpu_core_foo_memBWriteEnable_724,
      DIA(31) => dummy_out_ack,
      DIA(30) => dummy_out_ack,
      DIA(29) => dummy_out_ack,
      DIA(28) => dummy_out_ack,
      DIA(27) => dummy_out_ack,
      DIA(26) => dummy_out_ack,
      DIA(25) => dummy_out_ack,
      DIA(24) => dummy_out_ack,
      DIA(23) => dummy_out_ack,
      DIA(22) => dummy_out_ack,
      DIA(21) => dummy_out_ack,
      DIA(20) => dummy_out_ack,
      DIA(19) => dummy_out_ack,
      DIA(18) => dummy_out_ack,
      DIA(17) => dummy_out_ack,
      DIA(16) => dummy_out_ack,
      DIA(15) => dummy_out_ack,
      DIA(14) => dummy_out_ack,
      DIA(13) => dummy_out_ack,
      DIA(12) => dummy_out_ack,
      DIA(11) => dummy_out_ack,
      DIA(10) => dummy_out_ack,
      DIA(9) => dummy_out_ack,
      DIA(8) => dummy_out_ack,
      DIA(7) => dummy_out_ack,
      DIA(6) => dummy_out_ack,
      DIA(5) => dummy_out_ack,
      DIA(4) => dummy_out_ack,
      DIA(3) => core_zpu_core_foo_memAWrite(3),
      DIA(2) => core_zpu_core_foo_memAWrite(2),
      DIA(1) => core_zpu_core_foo_memAWrite(1),
      DIA(0) => core_zpu_core_foo_memAWrite(0)
    );
  core_zpu_core_foo_memory_RAMB16_S4_S4_inst1 : RAMB16BWER
    generic map(
      DATA_WIDTH_A => 4,
      DATA_WIDTH_B => 4,
      DOA_REG => 0,
      DOB_REG => 0,
      EN_RSTRAM_A => TRUE,
      EN_RSTRAM_B => TRUE,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RSTTYPE => "SYNC",
      RST_PRIORITY_A => "CE",
      RST_PRIORITY_B => "CE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "SPARTAN6",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST"
    )
    port map (
      REGCEA => dummy_out_ack,
      CLKA => clk_test_out_OBUF_11,
      ENB => N0,
      RSTB => dummy_out_ack,
      CLKB => clk_test_out_OBUF_11,
      REGCEB => dummy_out_ack,
      RSTA => dummy_out_ack,
      ENA => N0,
      DIPA(3) => dummy_out_ack,
      DIPA(2) => dummy_out_ack,
      DIPA(1) => dummy_out_ack,
      DIPA(0) => dummy_out_ack,
      WEA(3) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(2) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(1) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(0) => core_zpu_core_foo_memAWriteEnable_725,
      DOA(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_31_UNCONNECTED,
      DOA(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_30_UNCONNECTED,
      DOA(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_29_UNCONNECTED,
      DOA(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_28_UNCONNECTED,
      DOA(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_27_UNCONNECTED,
      DOA(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_26_UNCONNECTED,
      DOA(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_25_UNCONNECTED,
      DOA(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_24_UNCONNECTED,
      DOA(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_23_UNCONNECTED,
      DOA(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_22_UNCONNECTED,
      DOA(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_21_UNCONNECTED,
      DOA(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_20_UNCONNECTED,
      DOA(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_19_UNCONNECTED,
      DOA(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_18_UNCONNECTED,
      DOA(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_17_UNCONNECTED,
      DOA(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_16_UNCONNECTED,
      DOA(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_15_UNCONNECTED,
      DOA(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_14_UNCONNECTED,
      DOA(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_13_UNCONNECTED,
      DOA(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_12_UNCONNECTED,
      DOA(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_11_UNCONNECTED,
      DOA(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_10_UNCONNECTED,
      DOA(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_9_UNCONNECTED,
      DOA(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_8_UNCONNECTED,
      DOA(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_7_UNCONNECTED,
      DOA(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_6_UNCONNECTED,
      DOA(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_5_UNCONNECTED,
      DOA(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOA_4_UNCONNECTED,
      DOA(3) => core_zpu_core_foo_memARead_stdlogic(7),
      DOA(2) => core_zpu_core_foo_memARead_stdlogic(6),
      DOA(1) => core_zpu_core_foo_memARead_stdlogic(5),
      DOA(0) => core_zpu_core_foo_memARead_stdlogic(4),
      ADDRA(13) => dummy_out_ack,
      ADDRA(12) => dummy_out_ack,
      ADDRA(11) => core_zpu_core_foo_memAAddr(13),
      ADDRA(10) => core_zpu_core_foo_memAAddr(12),
      ADDRA(9) => core_zpu_core_foo_memAAddr(11),
      ADDRA(8) => core_zpu_core_foo_memAAddr(10),
      ADDRA(7) => core_zpu_core_foo_memAAddr(9),
      ADDRA(6) => core_zpu_core_foo_memAAddr(8),
      ADDRA(5) => core_zpu_core_foo_memAAddr(7),
      ADDRA(4) => core_zpu_core_foo_memAAddr(6),
      ADDRA(3) => core_zpu_core_foo_memAAddr(5),
      ADDRA(2) => core_zpu_core_foo_memAAddr(4),
      ADDRA(1) => core_zpu_core_foo_memAAddr(3),
      ADDRA(0) => core_zpu_core_foo_memAAddr(2),
      ADDRB(13) => dummy_out_ack,
      ADDRB(12) => dummy_out_ack,
      ADDRB(11) => core_zpu_core_foo_memBAddr(13),
      ADDRB(10) => core_zpu_core_foo_memBAddr(12),
      ADDRB(9) => core_zpu_core_foo_memBAddr(11),
      ADDRB(8) => core_zpu_core_foo_memBAddr(10),
      ADDRB(7) => core_zpu_core_foo_memBAddr(9),
      ADDRB(6) => core_zpu_core_foo_memBAddr(8),
      ADDRB(5) => core_zpu_core_foo_memBAddr(7),
      ADDRB(4) => core_zpu_core_foo_memBAddr(6),
      ADDRB(3) => core_zpu_core_foo_memBAddr(5),
      ADDRB(2) => core_zpu_core_foo_memBAddr(4),
      ADDRB(1) => core_zpu_core_foo_memBAddr(3),
      ADDRB(0) => core_zpu_core_foo_memBAddr(2),
      DIB(31) => dummy_out_ack,
      DIB(30) => dummy_out_ack,
      DIB(29) => dummy_out_ack,
      DIB(28) => dummy_out_ack,
      DIB(27) => dummy_out_ack,
      DIB(26) => dummy_out_ack,
      DIB(25) => dummy_out_ack,
      DIB(24) => dummy_out_ack,
      DIB(23) => dummy_out_ack,
      DIB(22) => dummy_out_ack,
      DIB(21) => dummy_out_ack,
      DIB(20) => dummy_out_ack,
      DIB(19) => dummy_out_ack,
      DIB(18) => dummy_out_ack,
      DIB(17) => dummy_out_ack,
      DIB(16) => dummy_out_ack,
      DIB(15) => dummy_out_ack,
      DIB(14) => dummy_out_ack,
      DIB(13) => dummy_out_ack,
      DIB(12) => dummy_out_ack,
      DIB(11) => dummy_out_ack,
      DIB(10) => dummy_out_ack,
      DIB(9) => dummy_out_ack,
      DIB(8) => dummy_out_ack,
      DIB(7) => dummy_out_ack,
      DIB(6) => dummy_out_ack,
      DIB(5) => dummy_out_ack,
      DIB(4) => dummy_out_ack,
      DIB(3) => core_zpu_core_foo_memBWrite(7),
      DIB(2) => core_zpu_core_foo_memBWrite(6),
      DIB(1) => core_zpu_core_foo_memBWrite(5),
      DIB(0) => core_zpu_core_foo_memBWrite(4),
      DOPA(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPA_3_UNCONNECTED,
      DOPA(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPA_2_UNCONNECTED,
      DOPA(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPA_1_UNCONNECTED,
      DOPA(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPA_0_UNCONNECTED,
      DIPB(3) => dummy_out_ack,
      DIPB(2) => dummy_out_ack,
      DIPB(1) => dummy_out_ack,
      DIPB(0) => dummy_out_ack,
      DOPB(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPB_3_UNCONNECTED,
      DOPB(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPB_2_UNCONNECTED,
      DOPB(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPB_1_UNCONNECTED,
      DOPB(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOPB_0_UNCONNECTED,
      DOB(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_31_UNCONNECTED,
      DOB(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_30_UNCONNECTED,
      DOB(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_29_UNCONNECTED,
      DOB(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_28_UNCONNECTED,
      DOB(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_27_UNCONNECTED,
      DOB(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_26_UNCONNECTED,
      DOB(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_25_UNCONNECTED,
      DOB(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_24_UNCONNECTED,
      DOB(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_23_UNCONNECTED,
      DOB(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_22_UNCONNECTED,
      DOB(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_21_UNCONNECTED,
      DOB(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_20_UNCONNECTED,
      DOB(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_19_UNCONNECTED,
      DOB(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_18_UNCONNECTED,
      DOB(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_17_UNCONNECTED,
      DOB(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_16_UNCONNECTED,
      DOB(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_15_UNCONNECTED,
      DOB(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_14_UNCONNECTED,
      DOB(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_13_UNCONNECTED,
      DOB(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_12_UNCONNECTED,
      DOB(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_11_UNCONNECTED,
      DOB(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_10_UNCONNECTED,
      DOB(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_9_UNCONNECTED,
      DOB(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_8_UNCONNECTED,
      DOB(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_7_UNCONNECTED,
      DOB(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_6_UNCONNECTED,
      DOB(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_5_UNCONNECTED,
      DOB(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst1_DOB_4_UNCONNECTED,
      DOB(3) => core_zpu_core_foo_memBRead_stdlogic(7),
      DOB(2) => core_zpu_core_foo_memBRead_stdlogic(6),
      DOB(1) => core_zpu_core_foo_memBRead_stdlogic(5),
      DOB(0) => core_zpu_core_foo_memBRead_stdlogic(4),
      WEB(3) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(2) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(1) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(0) => core_zpu_core_foo_memBWriteEnable_724,
      DIA(31) => dummy_out_ack,
      DIA(30) => dummy_out_ack,
      DIA(29) => dummy_out_ack,
      DIA(28) => dummy_out_ack,
      DIA(27) => dummy_out_ack,
      DIA(26) => dummy_out_ack,
      DIA(25) => dummy_out_ack,
      DIA(24) => dummy_out_ack,
      DIA(23) => dummy_out_ack,
      DIA(22) => dummy_out_ack,
      DIA(21) => dummy_out_ack,
      DIA(20) => dummy_out_ack,
      DIA(19) => dummy_out_ack,
      DIA(18) => dummy_out_ack,
      DIA(17) => dummy_out_ack,
      DIA(16) => dummy_out_ack,
      DIA(15) => dummy_out_ack,
      DIA(14) => dummy_out_ack,
      DIA(13) => dummy_out_ack,
      DIA(12) => dummy_out_ack,
      DIA(11) => dummy_out_ack,
      DIA(10) => dummy_out_ack,
      DIA(9) => dummy_out_ack,
      DIA(8) => dummy_out_ack,
      DIA(7) => dummy_out_ack,
      DIA(6) => dummy_out_ack,
      DIA(5) => dummy_out_ack,
      DIA(4) => dummy_out_ack,
      DIA(3) => core_zpu_core_foo_memAWrite(7),
      DIA(2) => core_zpu_core_foo_memAWrite(6),
      DIA(1) => core_zpu_core_foo_memAWrite(5),
      DIA(0) => core_zpu_core_foo_memAWrite(4)
    );
  core_zpu_core_foo_memory_RAMB16_S4_S4_inst2 : RAMB16BWER
    generic map(
      DATA_WIDTH_A => 4,
      DATA_WIDTH_B => 4,
      DOA_REG => 0,
      DOB_REG => 0,
      EN_RSTRAM_A => TRUE,
      EN_RSTRAM_B => TRUE,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RSTTYPE => "SYNC",
      RST_PRIORITY_A => "CE",
      RST_PRIORITY_B => "CE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "SPARTAN6",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST"
    )
    port map (
      REGCEA => dummy_out_ack,
      CLKA => clk_test_out_OBUF_11,
      ENB => N0,
      RSTB => dummy_out_ack,
      CLKB => clk_test_out_OBUF_11,
      REGCEB => dummy_out_ack,
      RSTA => dummy_out_ack,
      ENA => N0,
      DIPA(3) => dummy_out_ack,
      DIPA(2) => dummy_out_ack,
      DIPA(1) => dummy_out_ack,
      DIPA(0) => dummy_out_ack,
      WEA(3) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(2) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(1) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(0) => core_zpu_core_foo_memAWriteEnable_725,
      DOA(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_31_UNCONNECTED,
      DOA(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_30_UNCONNECTED,
      DOA(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_29_UNCONNECTED,
      DOA(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_28_UNCONNECTED,
      DOA(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_27_UNCONNECTED,
      DOA(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_26_UNCONNECTED,
      DOA(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_25_UNCONNECTED,
      DOA(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_24_UNCONNECTED,
      DOA(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_23_UNCONNECTED,
      DOA(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_22_UNCONNECTED,
      DOA(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_21_UNCONNECTED,
      DOA(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_20_UNCONNECTED,
      DOA(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_19_UNCONNECTED,
      DOA(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_18_UNCONNECTED,
      DOA(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_17_UNCONNECTED,
      DOA(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_16_UNCONNECTED,
      DOA(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_15_UNCONNECTED,
      DOA(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_14_UNCONNECTED,
      DOA(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_13_UNCONNECTED,
      DOA(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_12_UNCONNECTED,
      DOA(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_11_UNCONNECTED,
      DOA(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_10_UNCONNECTED,
      DOA(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_9_UNCONNECTED,
      DOA(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_8_UNCONNECTED,
      DOA(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_7_UNCONNECTED,
      DOA(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_6_UNCONNECTED,
      DOA(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_5_UNCONNECTED,
      DOA(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOA_4_UNCONNECTED,
      DOA(3) => core_zpu_core_foo_memARead_stdlogic(11),
      DOA(2) => core_zpu_core_foo_memARead_stdlogic(10),
      DOA(1) => core_zpu_core_foo_memARead_stdlogic(9),
      DOA(0) => core_zpu_core_foo_memARead_stdlogic(8),
      ADDRA(13) => dummy_out_ack,
      ADDRA(12) => dummy_out_ack,
      ADDRA(11) => core_zpu_core_foo_memAAddr(13),
      ADDRA(10) => core_zpu_core_foo_memAAddr(12),
      ADDRA(9) => core_zpu_core_foo_memAAddr(11),
      ADDRA(8) => core_zpu_core_foo_memAAddr(10),
      ADDRA(7) => core_zpu_core_foo_memAAddr(9),
      ADDRA(6) => core_zpu_core_foo_memAAddr(8),
      ADDRA(5) => core_zpu_core_foo_memAAddr(7),
      ADDRA(4) => core_zpu_core_foo_memAAddr(6),
      ADDRA(3) => core_zpu_core_foo_memAAddr(5),
      ADDRA(2) => core_zpu_core_foo_memAAddr(4),
      ADDRA(1) => core_zpu_core_foo_memAAddr(3),
      ADDRA(0) => core_zpu_core_foo_memAAddr(2),
      ADDRB(13) => dummy_out_ack,
      ADDRB(12) => dummy_out_ack,
      ADDRB(11) => core_zpu_core_foo_memBAddr(13),
      ADDRB(10) => core_zpu_core_foo_memBAddr(12),
      ADDRB(9) => core_zpu_core_foo_memBAddr(11),
      ADDRB(8) => core_zpu_core_foo_memBAddr(10),
      ADDRB(7) => core_zpu_core_foo_memBAddr(9),
      ADDRB(6) => core_zpu_core_foo_memBAddr(8),
      ADDRB(5) => core_zpu_core_foo_memBAddr(7),
      ADDRB(4) => core_zpu_core_foo_memBAddr(6),
      ADDRB(3) => core_zpu_core_foo_memBAddr(5),
      ADDRB(2) => core_zpu_core_foo_memBAddr(4),
      ADDRB(1) => core_zpu_core_foo_memBAddr(3),
      ADDRB(0) => core_zpu_core_foo_memBAddr(2),
      DIB(31) => dummy_out_ack,
      DIB(30) => dummy_out_ack,
      DIB(29) => dummy_out_ack,
      DIB(28) => dummy_out_ack,
      DIB(27) => dummy_out_ack,
      DIB(26) => dummy_out_ack,
      DIB(25) => dummy_out_ack,
      DIB(24) => dummy_out_ack,
      DIB(23) => dummy_out_ack,
      DIB(22) => dummy_out_ack,
      DIB(21) => dummy_out_ack,
      DIB(20) => dummy_out_ack,
      DIB(19) => dummy_out_ack,
      DIB(18) => dummy_out_ack,
      DIB(17) => dummy_out_ack,
      DIB(16) => dummy_out_ack,
      DIB(15) => dummy_out_ack,
      DIB(14) => dummy_out_ack,
      DIB(13) => dummy_out_ack,
      DIB(12) => dummy_out_ack,
      DIB(11) => dummy_out_ack,
      DIB(10) => dummy_out_ack,
      DIB(9) => dummy_out_ack,
      DIB(8) => dummy_out_ack,
      DIB(7) => dummy_out_ack,
      DIB(6) => dummy_out_ack,
      DIB(5) => dummy_out_ack,
      DIB(4) => dummy_out_ack,
      DIB(3) => core_zpu_core_foo_memBWrite(11),
      DIB(2) => core_zpu_core_foo_memBWrite(10),
      DIB(1) => core_zpu_core_foo_memBWrite(9),
      DIB(0) => core_zpu_core_foo_memBWrite(8),
      DOPA(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPA_3_UNCONNECTED,
      DOPA(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPA_2_UNCONNECTED,
      DOPA(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPA_1_UNCONNECTED,
      DOPA(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPA_0_UNCONNECTED,
      DIPB(3) => dummy_out_ack,
      DIPB(2) => dummy_out_ack,
      DIPB(1) => dummy_out_ack,
      DIPB(0) => dummy_out_ack,
      DOPB(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPB_3_UNCONNECTED,
      DOPB(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPB_2_UNCONNECTED,
      DOPB(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPB_1_UNCONNECTED,
      DOPB(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOPB_0_UNCONNECTED,
      DOB(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_31_UNCONNECTED,
      DOB(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_30_UNCONNECTED,
      DOB(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_29_UNCONNECTED,
      DOB(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_28_UNCONNECTED,
      DOB(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_27_UNCONNECTED,
      DOB(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_26_UNCONNECTED,
      DOB(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_25_UNCONNECTED,
      DOB(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_24_UNCONNECTED,
      DOB(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_23_UNCONNECTED,
      DOB(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_22_UNCONNECTED,
      DOB(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_21_UNCONNECTED,
      DOB(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_20_UNCONNECTED,
      DOB(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_19_UNCONNECTED,
      DOB(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_18_UNCONNECTED,
      DOB(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_17_UNCONNECTED,
      DOB(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_16_UNCONNECTED,
      DOB(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_15_UNCONNECTED,
      DOB(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_14_UNCONNECTED,
      DOB(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_13_UNCONNECTED,
      DOB(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_12_UNCONNECTED,
      DOB(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_11_UNCONNECTED,
      DOB(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_10_UNCONNECTED,
      DOB(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_9_UNCONNECTED,
      DOB(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_8_UNCONNECTED,
      DOB(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_7_UNCONNECTED,
      DOB(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_6_UNCONNECTED,
      DOB(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_5_UNCONNECTED,
      DOB(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst2_DOB_4_UNCONNECTED,
      DOB(3) => core_zpu_core_foo_memBRead_stdlogic(11),
      DOB(2) => core_zpu_core_foo_memBRead_stdlogic(10),
      DOB(1) => core_zpu_core_foo_memBRead_stdlogic(9),
      DOB(0) => core_zpu_core_foo_memBRead_stdlogic(8),
      WEB(3) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(2) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(1) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(0) => core_zpu_core_foo_memBWriteEnable_724,
      DIA(31) => dummy_out_ack,
      DIA(30) => dummy_out_ack,
      DIA(29) => dummy_out_ack,
      DIA(28) => dummy_out_ack,
      DIA(27) => dummy_out_ack,
      DIA(26) => dummy_out_ack,
      DIA(25) => dummy_out_ack,
      DIA(24) => dummy_out_ack,
      DIA(23) => dummy_out_ack,
      DIA(22) => dummy_out_ack,
      DIA(21) => dummy_out_ack,
      DIA(20) => dummy_out_ack,
      DIA(19) => dummy_out_ack,
      DIA(18) => dummy_out_ack,
      DIA(17) => dummy_out_ack,
      DIA(16) => dummy_out_ack,
      DIA(15) => dummy_out_ack,
      DIA(14) => dummy_out_ack,
      DIA(13) => dummy_out_ack,
      DIA(12) => dummy_out_ack,
      DIA(11) => dummy_out_ack,
      DIA(10) => dummy_out_ack,
      DIA(9) => dummy_out_ack,
      DIA(8) => dummy_out_ack,
      DIA(7) => dummy_out_ack,
      DIA(6) => dummy_out_ack,
      DIA(5) => dummy_out_ack,
      DIA(4) => dummy_out_ack,
      DIA(3) => core_zpu_core_foo_memAWrite(11),
      DIA(2) => core_zpu_core_foo_memAWrite(10),
      DIA(1) => core_zpu_core_foo_memAWrite(9),
      DIA(0) => core_zpu_core_foo_memAWrite(8)
    );
  core_zpu_core_foo_memory_RAMB16_S4_S4_inst3 : RAMB16BWER
    generic map(
      DATA_WIDTH_A => 4,
      DATA_WIDTH_B => 4,
      DOA_REG => 0,
      DOB_REG => 0,
      EN_RSTRAM_A => TRUE,
      EN_RSTRAM_B => TRUE,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RSTTYPE => "SYNC",
      RST_PRIORITY_A => "CE",
      RST_PRIORITY_B => "CE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "SPARTAN6",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST"
    )
    port map (
      REGCEA => dummy_out_ack,
      CLKA => clk_test_out_OBUF_11,
      ENB => N0,
      RSTB => dummy_out_ack,
      CLKB => clk_test_out_OBUF_11,
      REGCEB => dummy_out_ack,
      RSTA => dummy_out_ack,
      ENA => N0,
      DIPA(3) => dummy_out_ack,
      DIPA(2) => dummy_out_ack,
      DIPA(1) => dummy_out_ack,
      DIPA(0) => dummy_out_ack,
      WEA(3) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(2) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(1) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(0) => core_zpu_core_foo_memAWriteEnable_725,
      DOA(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_31_UNCONNECTED,
      DOA(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_30_UNCONNECTED,
      DOA(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_29_UNCONNECTED,
      DOA(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_28_UNCONNECTED,
      DOA(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_27_UNCONNECTED,
      DOA(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_26_UNCONNECTED,
      DOA(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_25_UNCONNECTED,
      DOA(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_24_UNCONNECTED,
      DOA(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_23_UNCONNECTED,
      DOA(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_22_UNCONNECTED,
      DOA(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_21_UNCONNECTED,
      DOA(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_20_UNCONNECTED,
      DOA(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_19_UNCONNECTED,
      DOA(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_18_UNCONNECTED,
      DOA(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_17_UNCONNECTED,
      DOA(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_16_UNCONNECTED,
      DOA(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_15_UNCONNECTED,
      DOA(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_14_UNCONNECTED,
      DOA(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_13_UNCONNECTED,
      DOA(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_12_UNCONNECTED,
      DOA(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_11_UNCONNECTED,
      DOA(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_10_UNCONNECTED,
      DOA(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_9_UNCONNECTED,
      DOA(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_8_UNCONNECTED,
      DOA(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_7_UNCONNECTED,
      DOA(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_6_UNCONNECTED,
      DOA(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_5_UNCONNECTED,
      DOA(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOA_4_UNCONNECTED,
      DOA(3) => core_zpu_core_foo_memARead_stdlogic(15),
      DOA(2) => core_zpu_core_foo_memARead_stdlogic(14),
      DOA(1) => core_zpu_core_foo_memARead_stdlogic(13),
      DOA(0) => core_zpu_core_foo_memARead_stdlogic(12),
      ADDRA(13) => dummy_out_ack,
      ADDRA(12) => dummy_out_ack,
      ADDRA(11) => core_zpu_core_foo_memAAddr(13),
      ADDRA(10) => core_zpu_core_foo_memAAddr(12),
      ADDRA(9) => core_zpu_core_foo_memAAddr(11),
      ADDRA(8) => core_zpu_core_foo_memAAddr(10),
      ADDRA(7) => core_zpu_core_foo_memAAddr(9),
      ADDRA(6) => core_zpu_core_foo_memAAddr(8),
      ADDRA(5) => core_zpu_core_foo_memAAddr(7),
      ADDRA(4) => core_zpu_core_foo_memAAddr(6),
      ADDRA(3) => core_zpu_core_foo_memAAddr(5),
      ADDRA(2) => core_zpu_core_foo_memAAddr(4),
      ADDRA(1) => core_zpu_core_foo_memAAddr(3),
      ADDRA(0) => core_zpu_core_foo_memAAddr(2),
      ADDRB(13) => dummy_out_ack,
      ADDRB(12) => dummy_out_ack,
      ADDRB(11) => core_zpu_core_foo_memBAddr(13),
      ADDRB(10) => core_zpu_core_foo_memBAddr(12),
      ADDRB(9) => core_zpu_core_foo_memBAddr(11),
      ADDRB(8) => core_zpu_core_foo_memBAddr(10),
      ADDRB(7) => core_zpu_core_foo_memBAddr(9),
      ADDRB(6) => core_zpu_core_foo_memBAddr(8),
      ADDRB(5) => core_zpu_core_foo_memBAddr(7),
      ADDRB(4) => core_zpu_core_foo_memBAddr(6),
      ADDRB(3) => core_zpu_core_foo_memBAddr(5),
      ADDRB(2) => core_zpu_core_foo_memBAddr(4),
      ADDRB(1) => core_zpu_core_foo_memBAddr(3),
      ADDRB(0) => core_zpu_core_foo_memBAddr(2),
      DIB(31) => dummy_out_ack,
      DIB(30) => dummy_out_ack,
      DIB(29) => dummy_out_ack,
      DIB(28) => dummy_out_ack,
      DIB(27) => dummy_out_ack,
      DIB(26) => dummy_out_ack,
      DIB(25) => dummy_out_ack,
      DIB(24) => dummy_out_ack,
      DIB(23) => dummy_out_ack,
      DIB(22) => dummy_out_ack,
      DIB(21) => dummy_out_ack,
      DIB(20) => dummy_out_ack,
      DIB(19) => dummy_out_ack,
      DIB(18) => dummy_out_ack,
      DIB(17) => dummy_out_ack,
      DIB(16) => dummy_out_ack,
      DIB(15) => dummy_out_ack,
      DIB(14) => dummy_out_ack,
      DIB(13) => dummy_out_ack,
      DIB(12) => dummy_out_ack,
      DIB(11) => dummy_out_ack,
      DIB(10) => dummy_out_ack,
      DIB(9) => dummy_out_ack,
      DIB(8) => dummy_out_ack,
      DIB(7) => dummy_out_ack,
      DIB(6) => dummy_out_ack,
      DIB(5) => dummy_out_ack,
      DIB(4) => dummy_out_ack,
      DIB(3) => core_zpu_core_foo_memBWrite(15),
      DIB(2) => core_zpu_core_foo_memBWrite(14),
      DIB(1) => core_zpu_core_foo_memBWrite(13),
      DIB(0) => core_zpu_core_foo_memBWrite(12),
      DOPA(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPA_3_UNCONNECTED,
      DOPA(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPA_2_UNCONNECTED,
      DOPA(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPA_1_UNCONNECTED,
      DOPA(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPA_0_UNCONNECTED,
      DIPB(3) => dummy_out_ack,
      DIPB(2) => dummy_out_ack,
      DIPB(1) => dummy_out_ack,
      DIPB(0) => dummy_out_ack,
      DOPB(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPB_3_UNCONNECTED,
      DOPB(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPB_2_UNCONNECTED,
      DOPB(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPB_1_UNCONNECTED,
      DOPB(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOPB_0_UNCONNECTED,
      DOB(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_31_UNCONNECTED,
      DOB(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_30_UNCONNECTED,
      DOB(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_29_UNCONNECTED,
      DOB(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_28_UNCONNECTED,
      DOB(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_27_UNCONNECTED,
      DOB(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_26_UNCONNECTED,
      DOB(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_25_UNCONNECTED,
      DOB(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_24_UNCONNECTED,
      DOB(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_23_UNCONNECTED,
      DOB(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_22_UNCONNECTED,
      DOB(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_21_UNCONNECTED,
      DOB(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_20_UNCONNECTED,
      DOB(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_19_UNCONNECTED,
      DOB(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_18_UNCONNECTED,
      DOB(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_17_UNCONNECTED,
      DOB(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_16_UNCONNECTED,
      DOB(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_15_UNCONNECTED,
      DOB(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_14_UNCONNECTED,
      DOB(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_13_UNCONNECTED,
      DOB(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_12_UNCONNECTED,
      DOB(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_11_UNCONNECTED,
      DOB(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_10_UNCONNECTED,
      DOB(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_9_UNCONNECTED,
      DOB(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_8_UNCONNECTED,
      DOB(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_7_UNCONNECTED,
      DOB(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_6_UNCONNECTED,
      DOB(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_5_UNCONNECTED,
      DOB(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst3_DOB_4_UNCONNECTED,
      DOB(3) => core_zpu_core_foo_memBRead_stdlogic(15),
      DOB(2) => core_zpu_core_foo_memBRead_stdlogic(14),
      DOB(1) => core_zpu_core_foo_memBRead_stdlogic(13),
      DOB(0) => core_zpu_core_foo_memBRead_stdlogic(12),
      WEB(3) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(2) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(1) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(0) => core_zpu_core_foo_memBWriteEnable_724,
      DIA(31) => dummy_out_ack,
      DIA(30) => dummy_out_ack,
      DIA(29) => dummy_out_ack,
      DIA(28) => dummy_out_ack,
      DIA(27) => dummy_out_ack,
      DIA(26) => dummy_out_ack,
      DIA(25) => dummy_out_ack,
      DIA(24) => dummy_out_ack,
      DIA(23) => dummy_out_ack,
      DIA(22) => dummy_out_ack,
      DIA(21) => dummy_out_ack,
      DIA(20) => dummy_out_ack,
      DIA(19) => dummy_out_ack,
      DIA(18) => dummy_out_ack,
      DIA(17) => dummy_out_ack,
      DIA(16) => dummy_out_ack,
      DIA(15) => dummy_out_ack,
      DIA(14) => dummy_out_ack,
      DIA(13) => dummy_out_ack,
      DIA(12) => dummy_out_ack,
      DIA(11) => dummy_out_ack,
      DIA(10) => dummy_out_ack,
      DIA(9) => dummy_out_ack,
      DIA(8) => dummy_out_ack,
      DIA(7) => dummy_out_ack,
      DIA(6) => dummy_out_ack,
      DIA(5) => dummy_out_ack,
      DIA(4) => dummy_out_ack,
      DIA(3) => core_zpu_core_foo_memAWrite(15),
      DIA(2) => core_zpu_core_foo_memAWrite(14),
      DIA(1) => core_zpu_core_foo_memAWrite(13),
      DIA(0) => core_zpu_core_foo_memAWrite(12)
    );
  core_zpu_core_foo_memory_RAMB16_S4_S4_inst4 : RAMB16BWER
    generic map(
      DATA_WIDTH_A => 4,
      DATA_WIDTH_B => 4,
      DOA_REG => 0,
      DOB_REG => 0,
      EN_RSTRAM_A => TRUE,
      EN_RSTRAM_B => TRUE,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RSTTYPE => "SYNC",
      RST_PRIORITY_A => "CE",
      RST_PRIORITY_B => "CE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "SPARTAN6",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST"
    )
    port map (
      REGCEA => dummy_out_ack,
      CLKA => clk_test_out_OBUF_11,
      ENB => N0,
      RSTB => dummy_out_ack,
      CLKB => clk_test_out_OBUF_11,
      REGCEB => dummy_out_ack,
      RSTA => dummy_out_ack,
      ENA => N0,
      DIPA(3) => dummy_out_ack,
      DIPA(2) => dummy_out_ack,
      DIPA(1) => dummy_out_ack,
      DIPA(0) => dummy_out_ack,
      WEA(3) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(2) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(1) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(0) => core_zpu_core_foo_memAWriteEnable_725,
      DOA(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_31_UNCONNECTED,
      DOA(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_30_UNCONNECTED,
      DOA(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_29_UNCONNECTED,
      DOA(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_28_UNCONNECTED,
      DOA(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_27_UNCONNECTED,
      DOA(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_26_UNCONNECTED,
      DOA(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_25_UNCONNECTED,
      DOA(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_24_UNCONNECTED,
      DOA(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_23_UNCONNECTED,
      DOA(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_22_UNCONNECTED,
      DOA(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_21_UNCONNECTED,
      DOA(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_20_UNCONNECTED,
      DOA(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_19_UNCONNECTED,
      DOA(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_18_UNCONNECTED,
      DOA(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_17_UNCONNECTED,
      DOA(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_16_UNCONNECTED,
      DOA(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_15_UNCONNECTED,
      DOA(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_14_UNCONNECTED,
      DOA(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_13_UNCONNECTED,
      DOA(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_12_UNCONNECTED,
      DOA(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_11_UNCONNECTED,
      DOA(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_10_UNCONNECTED,
      DOA(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_9_UNCONNECTED,
      DOA(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_8_UNCONNECTED,
      DOA(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_7_UNCONNECTED,
      DOA(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_6_UNCONNECTED,
      DOA(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_5_UNCONNECTED,
      DOA(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOA_4_UNCONNECTED,
      DOA(3) => core_zpu_core_foo_memARead_stdlogic(19),
      DOA(2) => core_zpu_core_foo_memARead_stdlogic(18),
      DOA(1) => core_zpu_core_foo_memARead_stdlogic(17),
      DOA(0) => core_zpu_core_foo_memARead_stdlogic(16),
      ADDRA(13) => dummy_out_ack,
      ADDRA(12) => dummy_out_ack,
      ADDRA(11) => core_zpu_core_foo_memAAddr(13),
      ADDRA(10) => core_zpu_core_foo_memAAddr(12),
      ADDRA(9) => core_zpu_core_foo_memAAddr(11),
      ADDRA(8) => core_zpu_core_foo_memAAddr(10),
      ADDRA(7) => core_zpu_core_foo_memAAddr(9),
      ADDRA(6) => core_zpu_core_foo_memAAddr(8),
      ADDRA(5) => core_zpu_core_foo_memAAddr(7),
      ADDRA(4) => core_zpu_core_foo_memAAddr(6),
      ADDRA(3) => core_zpu_core_foo_memAAddr(5),
      ADDRA(2) => core_zpu_core_foo_memAAddr(4),
      ADDRA(1) => core_zpu_core_foo_memAAddr(3),
      ADDRA(0) => core_zpu_core_foo_memAAddr(2),
      ADDRB(13) => dummy_out_ack,
      ADDRB(12) => dummy_out_ack,
      ADDRB(11) => core_zpu_core_foo_memBAddr(13),
      ADDRB(10) => core_zpu_core_foo_memBAddr(12),
      ADDRB(9) => core_zpu_core_foo_memBAddr(11),
      ADDRB(8) => core_zpu_core_foo_memBAddr(10),
      ADDRB(7) => core_zpu_core_foo_memBAddr(9),
      ADDRB(6) => core_zpu_core_foo_memBAddr(8),
      ADDRB(5) => core_zpu_core_foo_memBAddr(7),
      ADDRB(4) => core_zpu_core_foo_memBAddr(6),
      ADDRB(3) => core_zpu_core_foo_memBAddr(5),
      ADDRB(2) => core_zpu_core_foo_memBAddr(4),
      ADDRB(1) => core_zpu_core_foo_memBAddr(3),
      ADDRB(0) => core_zpu_core_foo_memBAddr(2),
      DIB(31) => dummy_out_ack,
      DIB(30) => dummy_out_ack,
      DIB(29) => dummy_out_ack,
      DIB(28) => dummy_out_ack,
      DIB(27) => dummy_out_ack,
      DIB(26) => dummy_out_ack,
      DIB(25) => dummy_out_ack,
      DIB(24) => dummy_out_ack,
      DIB(23) => dummy_out_ack,
      DIB(22) => dummy_out_ack,
      DIB(21) => dummy_out_ack,
      DIB(20) => dummy_out_ack,
      DIB(19) => dummy_out_ack,
      DIB(18) => dummy_out_ack,
      DIB(17) => dummy_out_ack,
      DIB(16) => dummy_out_ack,
      DIB(15) => dummy_out_ack,
      DIB(14) => dummy_out_ack,
      DIB(13) => dummy_out_ack,
      DIB(12) => dummy_out_ack,
      DIB(11) => dummy_out_ack,
      DIB(10) => dummy_out_ack,
      DIB(9) => dummy_out_ack,
      DIB(8) => dummy_out_ack,
      DIB(7) => dummy_out_ack,
      DIB(6) => dummy_out_ack,
      DIB(5) => dummy_out_ack,
      DIB(4) => dummy_out_ack,
      DIB(3) => core_zpu_core_foo_memBWrite(19),
      DIB(2) => core_zpu_core_foo_memBWrite(18),
      DIB(1) => core_zpu_core_foo_memBWrite(17),
      DIB(0) => core_zpu_core_foo_memBWrite(16),
      DOPA(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPA_3_UNCONNECTED,
      DOPA(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPA_2_UNCONNECTED,
      DOPA(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPA_1_UNCONNECTED,
      DOPA(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPA_0_UNCONNECTED,
      DIPB(3) => dummy_out_ack,
      DIPB(2) => dummy_out_ack,
      DIPB(1) => dummy_out_ack,
      DIPB(0) => dummy_out_ack,
      DOPB(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPB_3_UNCONNECTED,
      DOPB(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPB_2_UNCONNECTED,
      DOPB(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPB_1_UNCONNECTED,
      DOPB(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOPB_0_UNCONNECTED,
      DOB(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_31_UNCONNECTED,
      DOB(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_30_UNCONNECTED,
      DOB(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_29_UNCONNECTED,
      DOB(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_28_UNCONNECTED,
      DOB(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_27_UNCONNECTED,
      DOB(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_26_UNCONNECTED,
      DOB(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_25_UNCONNECTED,
      DOB(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_24_UNCONNECTED,
      DOB(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_23_UNCONNECTED,
      DOB(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_22_UNCONNECTED,
      DOB(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_21_UNCONNECTED,
      DOB(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_20_UNCONNECTED,
      DOB(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_19_UNCONNECTED,
      DOB(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_18_UNCONNECTED,
      DOB(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_17_UNCONNECTED,
      DOB(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_16_UNCONNECTED,
      DOB(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_15_UNCONNECTED,
      DOB(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_14_UNCONNECTED,
      DOB(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_13_UNCONNECTED,
      DOB(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_12_UNCONNECTED,
      DOB(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_11_UNCONNECTED,
      DOB(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_10_UNCONNECTED,
      DOB(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_9_UNCONNECTED,
      DOB(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_8_UNCONNECTED,
      DOB(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_7_UNCONNECTED,
      DOB(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_6_UNCONNECTED,
      DOB(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_5_UNCONNECTED,
      DOB(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst4_DOB_4_UNCONNECTED,
      DOB(3) => core_zpu_core_foo_memBRead_stdlogic(19),
      DOB(2) => core_zpu_core_foo_memBRead_stdlogic(18),
      DOB(1) => core_zpu_core_foo_memBRead_stdlogic(17),
      DOB(0) => core_zpu_core_foo_memBRead_stdlogic(16),
      WEB(3) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(2) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(1) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(0) => core_zpu_core_foo_memBWriteEnable_724,
      DIA(31) => dummy_out_ack,
      DIA(30) => dummy_out_ack,
      DIA(29) => dummy_out_ack,
      DIA(28) => dummy_out_ack,
      DIA(27) => dummy_out_ack,
      DIA(26) => dummy_out_ack,
      DIA(25) => dummy_out_ack,
      DIA(24) => dummy_out_ack,
      DIA(23) => dummy_out_ack,
      DIA(22) => dummy_out_ack,
      DIA(21) => dummy_out_ack,
      DIA(20) => dummy_out_ack,
      DIA(19) => dummy_out_ack,
      DIA(18) => dummy_out_ack,
      DIA(17) => dummy_out_ack,
      DIA(16) => dummy_out_ack,
      DIA(15) => dummy_out_ack,
      DIA(14) => dummy_out_ack,
      DIA(13) => dummy_out_ack,
      DIA(12) => dummy_out_ack,
      DIA(11) => dummy_out_ack,
      DIA(10) => dummy_out_ack,
      DIA(9) => dummy_out_ack,
      DIA(8) => dummy_out_ack,
      DIA(7) => dummy_out_ack,
      DIA(6) => dummy_out_ack,
      DIA(5) => dummy_out_ack,
      DIA(4) => dummy_out_ack,
      DIA(3) => core_zpu_core_foo_memAWrite(19),
      DIA(2) => core_zpu_core_foo_memAWrite(18),
      DIA(1) => core_zpu_core_foo_memAWrite(17),
      DIA(0) => core_zpu_core_foo_memAWrite(16)
    );
  core_zpu_core_foo_memory_RAMB16_S4_S4_inst5 : RAMB16BWER
    generic map(
      DATA_WIDTH_A => 4,
      DATA_WIDTH_B => 4,
      DOA_REG => 0,
      DOB_REG => 0,
      EN_RSTRAM_A => TRUE,
      EN_RSTRAM_B => TRUE,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RSTTYPE => "SYNC",
      RST_PRIORITY_A => "CE",
      RST_PRIORITY_B => "CE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "SPARTAN6",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST"
    )
    port map (
      REGCEA => dummy_out_ack,
      CLKA => clk_test_out_OBUF_11,
      ENB => N0,
      RSTB => dummy_out_ack,
      CLKB => clk_test_out_OBUF_11,
      REGCEB => dummy_out_ack,
      RSTA => dummy_out_ack,
      ENA => N0,
      DIPA(3) => dummy_out_ack,
      DIPA(2) => dummy_out_ack,
      DIPA(1) => dummy_out_ack,
      DIPA(0) => dummy_out_ack,
      WEA(3) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(2) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(1) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(0) => core_zpu_core_foo_memAWriteEnable_725,
      DOA(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_31_UNCONNECTED,
      DOA(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_30_UNCONNECTED,
      DOA(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_29_UNCONNECTED,
      DOA(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_28_UNCONNECTED,
      DOA(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_27_UNCONNECTED,
      DOA(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_26_UNCONNECTED,
      DOA(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_25_UNCONNECTED,
      DOA(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_24_UNCONNECTED,
      DOA(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_23_UNCONNECTED,
      DOA(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_22_UNCONNECTED,
      DOA(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_21_UNCONNECTED,
      DOA(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_20_UNCONNECTED,
      DOA(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_19_UNCONNECTED,
      DOA(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_18_UNCONNECTED,
      DOA(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_17_UNCONNECTED,
      DOA(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_16_UNCONNECTED,
      DOA(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_15_UNCONNECTED,
      DOA(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_14_UNCONNECTED,
      DOA(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_13_UNCONNECTED,
      DOA(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_12_UNCONNECTED,
      DOA(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_11_UNCONNECTED,
      DOA(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_10_UNCONNECTED,
      DOA(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_9_UNCONNECTED,
      DOA(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_8_UNCONNECTED,
      DOA(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_7_UNCONNECTED,
      DOA(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_6_UNCONNECTED,
      DOA(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_5_UNCONNECTED,
      DOA(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOA_4_UNCONNECTED,
      DOA(3) => core_zpu_core_foo_memARead_stdlogic(23),
      DOA(2) => core_zpu_core_foo_memARead_stdlogic(22),
      DOA(1) => core_zpu_core_foo_memARead_stdlogic(21),
      DOA(0) => core_zpu_core_foo_memARead_stdlogic(20),
      ADDRA(13) => dummy_out_ack,
      ADDRA(12) => dummy_out_ack,
      ADDRA(11) => core_zpu_core_foo_memAAddr(13),
      ADDRA(10) => core_zpu_core_foo_memAAddr(12),
      ADDRA(9) => core_zpu_core_foo_memAAddr(11),
      ADDRA(8) => core_zpu_core_foo_memAAddr(10),
      ADDRA(7) => core_zpu_core_foo_memAAddr(9),
      ADDRA(6) => core_zpu_core_foo_memAAddr(8),
      ADDRA(5) => core_zpu_core_foo_memAAddr(7),
      ADDRA(4) => core_zpu_core_foo_memAAddr(6),
      ADDRA(3) => core_zpu_core_foo_memAAddr(5),
      ADDRA(2) => core_zpu_core_foo_memAAddr(4),
      ADDRA(1) => core_zpu_core_foo_memAAddr(3),
      ADDRA(0) => core_zpu_core_foo_memAAddr(2),
      ADDRB(13) => dummy_out_ack,
      ADDRB(12) => dummy_out_ack,
      ADDRB(11) => core_zpu_core_foo_memBAddr(13),
      ADDRB(10) => core_zpu_core_foo_memBAddr(12),
      ADDRB(9) => core_zpu_core_foo_memBAddr(11),
      ADDRB(8) => core_zpu_core_foo_memBAddr(10),
      ADDRB(7) => core_zpu_core_foo_memBAddr(9),
      ADDRB(6) => core_zpu_core_foo_memBAddr(8),
      ADDRB(5) => core_zpu_core_foo_memBAddr(7),
      ADDRB(4) => core_zpu_core_foo_memBAddr(6),
      ADDRB(3) => core_zpu_core_foo_memBAddr(5),
      ADDRB(2) => core_zpu_core_foo_memBAddr(4),
      ADDRB(1) => core_zpu_core_foo_memBAddr(3),
      ADDRB(0) => core_zpu_core_foo_memBAddr(2),
      DIB(31) => dummy_out_ack,
      DIB(30) => dummy_out_ack,
      DIB(29) => dummy_out_ack,
      DIB(28) => dummy_out_ack,
      DIB(27) => dummy_out_ack,
      DIB(26) => dummy_out_ack,
      DIB(25) => dummy_out_ack,
      DIB(24) => dummy_out_ack,
      DIB(23) => dummy_out_ack,
      DIB(22) => dummy_out_ack,
      DIB(21) => dummy_out_ack,
      DIB(20) => dummy_out_ack,
      DIB(19) => dummy_out_ack,
      DIB(18) => dummy_out_ack,
      DIB(17) => dummy_out_ack,
      DIB(16) => dummy_out_ack,
      DIB(15) => dummy_out_ack,
      DIB(14) => dummy_out_ack,
      DIB(13) => dummy_out_ack,
      DIB(12) => dummy_out_ack,
      DIB(11) => dummy_out_ack,
      DIB(10) => dummy_out_ack,
      DIB(9) => dummy_out_ack,
      DIB(8) => dummy_out_ack,
      DIB(7) => dummy_out_ack,
      DIB(6) => dummy_out_ack,
      DIB(5) => dummy_out_ack,
      DIB(4) => dummy_out_ack,
      DIB(3) => core_zpu_core_foo_memBWrite(23),
      DIB(2) => core_zpu_core_foo_memBWrite(22),
      DIB(1) => core_zpu_core_foo_memBWrite(21),
      DIB(0) => core_zpu_core_foo_memBWrite(20),
      DOPA(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPA_3_UNCONNECTED,
      DOPA(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPA_2_UNCONNECTED,
      DOPA(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPA_1_UNCONNECTED,
      DOPA(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPA_0_UNCONNECTED,
      DIPB(3) => dummy_out_ack,
      DIPB(2) => dummy_out_ack,
      DIPB(1) => dummy_out_ack,
      DIPB(0) => dummy_out_ack,
      DOPB(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPB_3_UNCONNECTED,
      DOPB(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPB_2_UNCONNECTED,
      DOPB(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPB_1_UNCONNECTED,
      DOPB(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOPB_0_UNCONNECTED,
      DOB(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_31_UNCONNECTED,
      DOB(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_30_UNCONNECTED,
      DOB(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_29_UNCONNECTED,
      DOB(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_28_UNCONNECTED,
      DOB(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_27_UNCONNECTED,
      DOB(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_26_UNCONNECTED,
      DOB(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_25_UNCONNECTED,
      DOB(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_24_UNCONNECTED,
      DOB(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_23_UNCONNECTED,
      DOB(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_22_UNCONNECTED,
      DOB(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_21_UNCONNECTED,
      DOB(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_20_UNCONNECTED,
      DOB(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_19_UNCONNECTED,
      DOB(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_18_UNCONNECTED,
      DOB(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_17_UNCONNECTED,
      DOB(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_16_UNCONNECTED,
      DOB(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_15_UNCONNECTED,
      DOB(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_14_UNCONNECTED,
      DOB(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_13_UNCONNECTED,
      DOB(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_12_UNCONNECTED,
      DOB(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_11_UNCONNECTED,
      DOB(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_10_UNCONNECTED,
      DOB(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_9_UNCONNECTED,
      DOB(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_8_UNCONNECTED,
      DOB(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_7_UNCONNECTED,
      DOB(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_6_UNCONNECTED,
      DOB(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_5_UNCONNECTED,
      DOB(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst5_DOB_4_UNCONNECTED,
      DOB(3) => core_zpu_core_foo_memBRead_stdlogic(23),
      DOB(2) => core_zpu_core_foo_memBRead_stdlogic(22),
      DOB(1) => core_zpu_core_foo_memBRead_stdlogic(21),
      DOB(0) => core_zpu_core_foo_memBRead_stdlogic(20),
      WEB(3) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(2) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(1) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(0) => core_zpu_core_foo_memBWriteEnable_724,
      DIA(31) => dummy_out_ack,
      DIA(30) => dummy_out_ack,
      DIA(29) => dummy_out_ack,
      DIA(28) => dummy_out_ack,
      DIA(27) => dummy_out_ack,
      DIA(26) => dummy_out_ack,
      DIA(25) => dummy_out_ack,
      DIA(24) => dummy_out_ack,
      DIA(23) => dummy_out_ack,
      DIA(22) => dummy_out_ack,
      DIA(21) => dummy_out_ack,
      DIA(20) => dummy_out_ack,
      DIA(19) => dummy_out_ack,
      DIA(18) => dummy_out_ack,
      DIA(17) => dummy_out_ack,
      DIA(16) => dummy_out_ack,
      DIA(15) => dummy_out_ack,
      DIA(14) => dummy_out_ack,
      DIA(13) => dummy_out_ack,
      DIA(12) => dummy_out_ack,
      DIA(11) => dummy_out_ack,
      DIA(10) => dummy_out_ack,
      DIA(9) => dummy_out_ack,
      DIA(8) => dummy_out_ack,
      DIA(7) => dummy_out_ack,
      DIA(6) => dummy_out_ack,
      DIA(5) => dummy_out_ack,
      DIA(4) => dummy_out_ack,
      DIA(3) => core_zpu_core_foo_memAWrite(23),
      DIA(2) => core_zpu_core_foo_memAWrite(22),
      DIA(1) => core_zpu_core_foo_memAWrite(21),
      DIA(0) => core_zpu_core_foo_memAWrite(20)
    );
  core_zpu_core_foo_memory_RAMB16_S4_S4_inst6 : RAMB16BWER
    generic map(
      DATA_WIDTH_A => 4,
      DATA_WIDTH_B => 4,
      DOA_REG => 0,
      DOB_REG => 0,
      EN_RSTRAM_A => TRUE,
      EN_RSTRAM_B => TRUE,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RSTTYPE => "SYNC",
      RST_PRIORITY_A => "CE",
      RST_PRIORITY_B => "CE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "SPARTAN6",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST"
    )
    port map (
      REGCEA => dummy_out_ack,
      CLKA => clk_test_out_OBUF_11,
      ENB => N0,
      RSTB => dummy_out_ack,
      CLKB => clk_test_out_OBUF_11,
      REGCEB => dummy_out_ack,
      RSTA => dummy_out_ack,
      ENA => N0,
      DIPA(3) => dummy_out_ack,
      DIPA(2) => dummy_out_ack,
      DIPA(1) => dummy_out_ack,
      DIPA(0) => dummy_out_ack,
      WEA(3) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(2) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(1) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(0) => core_zpu_core_foo_memAWriteEnable_725,
      DOA(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_31_UNCONNECTED,
      DOA(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_30_UNCONNECTED,
      DOA(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_29_UNCONNECTED,
      DOA(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_28_UNCONNECTED,
      DOA(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_27_UNCONNECTED,
      DOA(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_26_UNCONNECTED,
      DOA(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_25_UNCONNECTED,
      DOA(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_24_UNCONNECTED,
      DOA(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_23_UNCONNECTED,
      DOA(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_22_UNCONNECTED,
      DOA(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_21_UNCONNECTED,
      DOA(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_20_UNCONNECTED,
      DOA(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_19_UNCONNECTED,
      DOA(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_18_UNCONNECTED,
      DOA(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_17_UNCONNECTED,
      DOA(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_16_UNCONNECTED,
      DOA(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_15_UNCONNECTED,
      DOA(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_14_UNCONNECTED,
      DOA(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_13_UNCONNECTED,
      DOA(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_12_UNCONNECTED,
      DOA(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_11_UNCONNECTED,
      DOA(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_10_UNCONNECTED,
      DOA(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_9_UNCONNECTED,
      DOA(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_8_UNCONNECTED,
      DOA(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_7_UNCONNECTED,
      DOA(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_6_UNCONNECTED,
      DOA(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_5_UNCONNECTED,
      DOA(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOA_4_UNCONNECTED,
      DOA(3) => core_zpu_core_foo_memARead_stdlogic(27),
      DOA(2) => core_zpu_core_foo_memARead_stdlogic(26),
      DOA(1) => core_zpu_core_foo_memARead_stdlogic(25),
      DOA(0) => core_zpu_core_foo_memARead_stdlogic(24),
      ADDRA(13) => dummy_out_ack,
      ADDRA(12) => dummy_out_ack,
      ADDRA(11) => core_zpu_core_foo_memAAddr(13),
      ADDRA(10) => core_zpu_core_foo_memAAddr(12),
      ADDRA(9) => core_zpu_core_foo_memAAddr(11),
      ADDRA(8) => core_zpu_core_foo_memAAddr(10),
      ADDRA(7) => core_zpu_core_foo_memAAddr(9),
      ADDRA(6) => core_zpu_core_foo_memAAddr(8),
      ADDRA(5) => core_zpu_core_foo_memAAddr(7),
      ADDRA(4) => core_zpu_core_foo_memAAddr(6),
      ADDRA(3) => core_zpu_core_foo_memAAddr(5),
      ADDRA(2) => core_zpu_core_foo_memAAddr(4),
      ADDRA(1) => core_zpu_core_foo_memAAddr(3),
      ADDRA(0) => core_zpu_core_foo_memAAddr(2),
      ADDRB(13) => dummy_out_ack,
      ADDRB(12) => dummy_out_ack,
      ADDRB(11) => core_zpu_core_foo_memBAddr(13),
      ADDRB(10) => core_zpu_core_foo_memBAddr(12),
      ADDRB(9) => core_zpu_core_foo_memBAddr(11),
      ADDRB(8) => core_zpu_core_foo_memBAddr(10),
      ADDRB(7) => core_zpu_core_foo_memBAddr(9),
      ADDRB(6) => core_zpu_core_foo_memBAddr(8),
      ADDRB(5) => core_zpu_core_foo_memBAddr(7),
      ADDRB(4) => core_zpu_core_foo_memBAddr(6),
      ADDRB(3) => core_zpu_core_foo_memBAddr(5),
      ADDRB(2) => core_zpu_core_foo_memBAddr(4),
      ADDRB(1) => core_zpu_core_foo_memBAddr(3),
      ADDRB(0) => core_zpu_core_foo_memBAddr(2),
      DIB(31) => dummy_out_ack,
      DIB(30) => dummy_out_ack,
      DIB(29) => dummy_out_ack,
      DIB(28) => dummy_out_ack,
      DIB(27) => dummy_out_ack,
      DIB(26) => dummy_out_ack,
      DIB(25) => dummy_out_ack,
      DIB(24) => dummy_out_ack,
      DIB(23) => dummy_out_ack,
      DIB(22) => dummy_out_ack,
      DIB(21) => dummy_out_ack,
      DIB(20) => dummy_out_ack,
      DIB(19) => dummy_out_ack,
      DIB(18) => dummy_out_ack,
      DIB(17) => dummy_out_ack,
      DIB(16) => dummy_out_ack,
      DIB(15) => dummy_out_ack,
      DIB(14) => dummy_out_ack,
      DIB(13) => dummy_out_ack,
      DIB(12) => dummy_out_ack,
      DIB(11) => dummy_out_ack,
      DIB(10) => dummy_out_ack,
      DIB(9) => dummy_out_ack,
      DIB(8) => dummy_out_ack,
      DIB(7) => dummy_out_ack,
      DIB(6) => dummy_out_ack,
      DIB(5) => dummy_out_ack,
      DIB(4) => dummy_out_ack,
      DIB(3) => core_zpu_core_foo_memBWrite(27),
      DIB(2) => core_zpu_core_foo_memBWrite(26),
      DIB(1) => core_zpu_core_foo_memBWrite(25),
      DIB(0) => core_zpu_core_foo_memBWrite(24),
      DOPA(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPA_3_UNCONNECTED,
      DOPA(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPA_2_UNCONNECTED,
      DOPA(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPA_1_UNCONNECTED,
      DOPA(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPA_0_UNCONNECTED,
      DIPB(3) => dummy_out_ack,
      DIPB(2) => dummy_out_ack,
      DIPB(1) => dummy_out_ack,
      DIPB(0) => dummy_out_ack,
      DOPB(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPB_3_UNCONNECTED,
      DOPB(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPB_2_UNCONNECTED,
      DOPB(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPB_1_UNCONNECTED,
      DOPB(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOPB_0_UNCONNECTED,
      DOB(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_31_UNCONNECTED,
      DOB(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_30_UNCONNECTED,
      DOB(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_29_UNCONNECTED,
      DOB(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_28_UNCONNECTED,
      DOB(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_27_UNCONNECTED,
      DOB(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_26_UNCONNECTED,
      DOB(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_25_UNCONNECTED,
      DOB(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_24_UNCONNECTED,
      DOB(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_23_UNCONNECTED,
      DOB(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_22_UNCONNECTED,
      DOB(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_21_UNCONNECTED,
      DOB(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_20_UNCONNECTED,
      DOB(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_19_UNCONNECTED,
      DOB(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_18_UNCONNECTED,
      DOB(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_17_UNCONNECTED,
      DOB(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_16_UNCONNECTED,
      DOB(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_15_UNCONNECTED,
      DOB(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_14_UNCONNECTED,
      DOB(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_13_UNCONNECTED,
      DOB(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_12_UNCONNECTED,
      DOB(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_11_UNCONNECTED,
      DOB(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_10_UNCONNECTED,
      DOB(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_9_UNCONNECTED,
      DOB(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_8_UNCONNECTED,
      DOB(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_7_UNCONNECTED,
      DOB(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_6_UNCONNECTED,
      DOB(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_5_UNCONNECTED,
      DOB(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst6_DOB_4_UNCONNECTED,
      DOB(3) => core_zpu_core_foo_memBRead_stdlogic(27),
      DOB(2) => core_zpu_core_foo_memBRead_stdlogic(26),
      DOB(1) => core_zpu_core_foo_memBRead_stdlogic(25),
      DOB(0) => core_zpu_core_foo_memBRead_stdlogic(24),
      WEB(3) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(2) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(1) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(0) => core_zpu_core_foo_memBWriteEnable_724,
      DIA(31) => dummy_out_ack,
      DIA(30) => dummy_out_ack,
      DIA(29) => dummy_out_ack,
      DIA(28) => dummy_out_ack,
      DIA(27) => dummy_out_ack,
      DIA(26) => dummy_out_ack,
      DIA(25) => dummy_out_ack,
      DIA(24) => dummy_out_ack,
      DIA(23) => dummy_out_ack,
      DIA(22) => dummy_out_ack,
      DIA(21) => dummy_out_ack,
      DIA(20) => dummy_out_ack,
      DIA(19) => dummy_out_ack,
      DIA(18) => dummy_out_ack,
      DIA(17) => dummy_out_ack,
      DIA(16) => dummy_out_ack,
      DIA(15) => dummy_out_ack,
      DIA(14) => dummy_out_ack,
      DIA(13) => dummy_out_ack,
      DIA(12) => dummy_out_ack,
      DIA(11) => dummy_out_ack,
      DIA(10) => dummy_out_ack,
      DIA(9) => dummy_out_ack,
      DIA(8) => dummy_out_ack,
      DIA(7) => dummy_out_ack,
      DIA(6) => dummy_out_ack,
      DIA(5) => dummy_out_ack,
      DIA(4) => dummy_out_ack,
      DIA(3) => core_zpu_core_foo_memAWrite(27),
      DIA(2) => core_zpu_core_foo_memAWrite(26),
      DIA(1) => core_zpu_core_foo_memAWrite(25),
      DIA(0) => core_zpu_core_foo_memAWrite(24)
    );
  core_zpu_core_foo_memory_RAMB16_S4_S4_inst7 : RAMB16BWER
    generic map(
      DATA_WIDTH_A => 4,
      DATA_WIDTH_B => 4,
      DOA_REG => 0,
      DOB_REG => 0,
      EN_RSTRAM_A => TRUE,
      EN_RSTRAM_B => TRUE,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      RSTTYPE => "SYNC",
      RST_PRIORITY_A => "CE",
      RST_PRIORITY_B => "CE",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "SPARTAN6",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST"
    )
    port map (
      REGCEA => dummy_out_ack,
      CLKA => clk_test_out_OBUF_11,
      ENB => N0,
      RSTB => dummy_out_ack,
      CLKB => clk_test_out_OBUF_11,
      REGCEB => dummy_out_ack,
      RSTA => dummy_out_ack,
      ENA => N0,
      DIPA(3) => dummy_out_ack,
      DIPA(2) => dummy_out_ack,
      DIPA(1) => dummy_out_ack,
      DIPA(0) => dummy_out_ack,
      WEA(3) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(2) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(1) => core_zpu_core_foo_memAWriteEnable_725,
      WEA(0) => core_zpu_core_foo_memAWriteEnable_725,
      DOA(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_31_UNCONNECTED,
      DOA(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_30_UNCONNECTED,
      DOA(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_29_UNCONNECTED,
      DOA(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_28_UNCONNECTED,
      DOA(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_27_UNCONNECTED,
      DOA(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_26_UNCONNECTED,
      DOA(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_25_UNCONNECTED,
      DOA(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_24_UNCONNECTED,
      DOA(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_23_UNCONNECTED,
      DOA(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_22_UNCONNECTED,
      DOA(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_21_UNCONNECTED,
      DOA(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_20_UNCONNECTED,
      DOA(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_19_UNCONNECTED,
      DOA(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_18_UNCONNECTED,
      DOA(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_17_UNCONNECTED,
      DOA(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_16_UNCONNECTED,
      DOA(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_15_UNCONNECTED,
      DOA(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_14_UNCONNECTED,
      DOA(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_13_UNCONNECTED,
      DOA(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_12_UNCONNECTED,
      DOA(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_11_UNCONNECTED,
      DOA(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_10_UNCONNECTED,
      DOA(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_9_UNCONNECTED,
      DOA(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_8_UNCONNECTED,
      DOA(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_7_UNCONNECTED,
      DOA(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_6_UNCONNECTED,
      DOA(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_5_UNCONNECTED,
      DOA(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOA_4_UNCONNECTED,
      DOA(3) => core_zpu_core_foo_memARead_stdlogic(31),
      DOA(2) => core_zpu_core_foo_memARead_stdlogic(30),
      DOA(1) => core_zpu_core_foo_memARead_stdlogic(29),
      DOA(0) => core_zpu_core_foo_memARead_stdlogic(28),
      ADDRA(13) => dummy_out_ack,
      ADDRA(12) => dummy_out_ack,
      ADDRA(11) => core_zpu_core_foo_memAAddr(13),
      ADDRA(10) => core_zpu_core_foo_memAAddr(12),
      ADDRA(9) => core_zpu_core_foo_memAAddr(11),
      ADDRA(8) => core_zpu_core_foo_memAAddr(10),
      ADDRA(7) => core_zpu_core_foo_memAAddr(9),
      ADDRA(6) => core_zpu_core_foo_memAAddr(8),
      ADDRA(5) => core_zpu_core_foo_memAAddr(7),
      ADDRA(4) => core_zpu_core_foo_memAAddr(6),
      ADDRA(3) => core_zpu_core_foo_memAAddr(5),
      ADDRA(2) => core_zpu_core_foo_memAAddr(4),
      ADDRA(1) => core_zpu_core_foo_memAAddr(3),
      ADDRA(0) => core_zpu_core_foo_memAAddr(2),
      ADDRB(13) => dummy_out_ack,
      ADDRB(12) => dummy_out_ack,
      ADDRB(11) => core_zpu_core_foo_memBAddr(13),
      ADDRB(10) => core_zpu_core_foo_memBAddr(12),
      ADDRB(9) => core_zpu_core_foo_memBAddr(11),
      ADDRB(8) => core_zpu_core_foo_memBAddr(10),
      ADDRB(7) => core_zpu_core_foo_memBAddr(9),
      ADDRB(6) => core_zpu_core_foo_memBAddr(8),
      ADDRB(5) => core_zpu_core_foo_memBAddr(7),
      ADDRB(4) => core_zpu_core_foo_memBAddr(6),
      ADDRB(3) => core_zpu_core_foo_memBAddr(5),
      ADDRB(2) => core_zpu_core_foo_memBAddr(4),
      ADDRB(1) => core_zpu_core_foo_memBAddr(3),
      ADDRB(0) => core_zpu_core_foo_memBAddr(2),
      DIB(31) => dummy_out_ack,
      DIB(30) => dummy_out_ack,
      DIB(29) => dummy_out_ack,
      DIB(28) => dummy_out_ack,
      DIB(27) => dummy_out_ack,
      DIB(26) => dummy_out_ack,
      DIB(25) => dummy_out_ack,
      DIB(24) => dummy_out_ack,
      DIB(23) => dummy_out_ack,
      DIB(22) => dummy_out_ack,
      DIB(21) => dummy_out_ack,
      DIB(20) => dummy_out_ack,
      DIB(19) => dummy_out_ack,
      DIB(18) => dummy_out_ack,
      DIB(17) => dummy_out_ack,
      DIB(16) => dummy_out_ack,
      DIB(15) => dummy_out_ack,
      DIB(14) => dummy_out_ack,
      DIB(13) => dummy_out_ack,
      DIB(12) => dummy_out_ack,
      DIB(11) => dummy_out_ack,
      DIB(10) => dummy_out_ack,
      DIB(9) => dummy_out_ack,
      DIB(8) => dummy_out_ack,
      DIB(7) => dummy_out_ack,
      DIB(6) => dummy_out_ack,
      DIB(5) => dummy_out_ack,
      DIB(4) => dummy_out_ack,
      DIB(3) => core_zpu_core_foo_memBWrite(31),
      DIB(2) => core_zpu_core_foo_memBWrite(30),
      DIB(1) => core_zpu_core_foo_memBWrite(29),
      DIB(0) => core_zpu_core_foo_memBWrite(28),
      DOPA(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPA_3_UNCONNECTED,
      DOPA(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPA_2_UNCONNECTED,
      DOPA(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPA_1_UNCONNECTED,
      DOPA(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPA_0_UNCONNECTED,
      DIPB(3) => dummy_out_ack,
      DIPB(2) => dummy_out_ack,
      DIPB(1) => dummy_out_ack,
      DIPB(0) => dummy_out_ack,
      DOPB(3) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPB_3_UNCONNECTED,
      DOPB(2) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPB_2_UNCONNECTED,
      DOPB(1) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPB_1_UNCONNECTED,
      DOPB(0) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOPB_0_UNCONNECTED,
      DOB(31) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_31_UNCONNECTED,
      DOB(30) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_30_UNCONNECTED,
      DOB(29) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_29_UNCONNECTED,
      DOB(28) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_28_UNCONNECTED,
      DOB(27) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_27_UNCONNECTED,
      DOB(26) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_26_UNCONNECTED,
      DOB(25) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_25_UNCONNECTED,
      DOB(24) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_24_UNCONNECTED,
      DOB(23) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_23_UNCONNECTED,
      DOB(22) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_22_UNCONNECTED,
      DOB(21) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_21_UNCONNECTED,
      DOB(20) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_20_UNCONNECTED,
      DOB(19) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_19_UNCONNECTED,
      DOB(18) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_18_UNCONNECTED,
      DOB(17) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_17_UNCONNECTED,
      DOB(16) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_16_UNCONNECTED,
      DOB(15) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_15_UNCONNECTED,
      DOB(14) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_14_UNCONNECTED,
      DOB(13) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_13_UNCONNECTED,
      DOB(12) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_12_UNCONNECTED,
      DOB(11) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_11_UNCONNECTED,
      DOB(10) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_10_UNCONNECTED,
      DOB(9) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_9_UNCONNECTED,
      DOB(8) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_8_UNCONNECTED,
      DOB(7) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_7_UNCONNECTED,
      DOB(6) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_6_UNCONNECTED,
      DOB(5) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_5_UNCONNECTED,
      DOB(4) => NLW_core_zpu_core_foo_memory_RAMB16_S4_S4_inst7_DOB_4_UNCONNECTED,
      DOB(3) => core_zpu_core_foo_memBRead_stdlogic(31),
      DOB(2) => core_zpu_core_foo_memBRead_stdlogic(30),
      DOB(1) => core_zpu_core_foo_memBRead_stdlogic(29),
      DOB(0) => core_zpu_core_foo_memBRead_stdlogic(28),
      WEB(3) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(2) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(1) => core_zpu_core_foo_memBWriteEnable_724,
      WEB(0) => core_zpu_core_foo_memBWriteEnable_724,
      DIA(31) => dummy_out_ack,
      DIA(30) => dummy_out_ack,
      DIA(29) => dummy_out_ack,
      DIA(28) => dummy_out_ack,
      DIA(27) => dummy_out_ack,
      DIA(26) => dummy_out_ack,
      DIA(25) => dummy_out_ack,
      DIA(24) => dummy_out_ack,
      DIA(23) => dummy_out_ack,
      DIA(22) => dummy_out_ack,
      DIA(21) => dummy_out_ack,
      DIA(20) => dummy_out_ack,
      DIA(19) => dummy_out_ack,
      DIA(18) => dummy_out_ack,
      DIA(17) => dummy_out_ack,
      DIA(16) => dummy_out_ack,
      DIA(15) => dummy_out_ack,
      DIA(14) => dummy_out_ack,
      DIA(13) => dummy_out_ack,
      DIA(12) => dummy_out_ack,
      DIA(11) => dummy_out_ack,
      DIA(10) => dummy_out_ack,
      DIA(9) => dummy_out_ack,
      DIA(8) => dummy_out_ack,
      DIA(7) => dummy_out_ack,
      DIA(6) => dummy_out_ack,
      DIA(5) => dummy_out_ack,
      DIA(4) => dummy_out_ack,
      DIA(3) => core_zpu_core_foo_memAWrite(31),
      DIA(2) => core_zpu_core_foo_memAWrite(30),
      DIA(1) => core_zpu_core_foo_memAWrite(29),
      DIA(0) => core_zpu_core_foo_memAWrite(28)
    );
  intIO_inst_LED_3_1 : LDC
    port map (
      CLR => reset_IBUF_5,
      D => core_master_out_data(3),
      G => intIO_inst_slave_in_cyc_slave_in_addr_1_AND_62_o,
      Q => intIO_inst_LED_3_1_1396
    );
  intIO_inst_LED_2_1 : LDC
    port map (
      CLR => reset_IBUF_5,
      D => core_master_out_data(2),
      G => intIO_inst_slave_in_cyc_slave_in_addr_1_AND_62_o,
      Q => intIO_inst_LED_2_1_1397
    );
  intIO_inst_LED_1_1 : LDC
    port map (
      CLR => reset_IBUF_5,
      D => core_master_out_data(1),
      G => intIO_inst_slave_in_cyc_slave_in_addr_1_AND_62_o,
      Q => intIO_inst_LED_1_1_1398
    );
  intIO_inst_LED_0_1 : LDC
    port map (
      CLR => reset_IBUF_5,
      D => core_master_out_data(0),
      G => intIO_inst_slave_in_cyc_slave_in_addr_1_AND_62_o,
      Q => intIO_inst_LED_0_1_1399
    );
  generatoer_pll_base_inst : PLL_BASE
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT => 37,
      CLKFBOUT_PHASE => 0.000000,
      CLKIN_PERIOD => 37.037000,
      CLKOUT0_DIVIDE => 20,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT1_DIVIDE => 1,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLK_FEEDBACK => "CLKFBOUT",
      COMPENSATION => "SYSTEM_SYNCHRONOUS",
      DIVCLK_DIVIDE => 1,
      REF_JITTER => 0.010000,
      RESET_ON_LOSS_OF_LOCK => FALSE
    )
    port map (
      CLKIN => generatoer_clkin1,
      CLKFBIN => generatoer_clkfbout_buf,
      CLKOUT0 => generatoer_clkout0,
      CLKFBOUT => generatoer_clkfbout,
      RST => reset_IBUF_5,
      CLKOUT3 => NLW_generatoer_pll_base_inst_CLKOUT3_UNCONNECTED,
      CLKOUT1 => NLW_generatoer_pll_base_inst_CLKOUT1_UNCONNECTED,
      CLKOUT4 => NLW_generatoer_pll_base_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_generatoer_pll_base_inst_CLKOUT5_UNCONNECTED,
      CLKOUT2 => NLW_generatoer_pll_base_inst_CLKOUT2_UNCONNECTED,
      LOCKED => NLW_generatoer_pll_base_inst_LOCKED_UNCONNECTED
    );

end Structure;

