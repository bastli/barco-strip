/* 
 * "Small Hello World" example. 
 * 
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example 
 * designs. It requires a STDOUT  device in your system's hardware. 
 *
 * The purpose of this example is to demonstrate the smallest possible Hello 
 * World application, using the Nios II HAL library.  The memory footprint
 * of this hosted application is ~332 bytes by default using the standard 
 * reference design.  For a more fully featured Hello World application
 * example, see the example titled "Hello World".
 *
 * The memory footprint of this example has been reduced by making the
 * following changes to the normal "Hello World" example.
 * Check in the Nios II Software Developers Manual for a more complete 
 * description.
 * 
 * In the SW Application project (small_hello_world):
 *
 *  - In the C/C++ Build page
 * 
 *    - Set the Optimization Level to -Os
 * 
 * In System Library project (small_hello_world_syslib):
 *  - In the C/C++ Build page
 * 
 *    - Set the Optimization Level to -Os
 * 
 *    - Define the preprocessor option ALT_NO_INSTRUCTION_EMULATION 
 *      This removes software exception handling, which means that you cannot 
 *      run code compiled for Nios II cpu with a hardware multiplier on a core 
 *      without a the multiply unit. Check the Nios II Software Developers 
 *      Manual for more details.
 *
 *  - In the System Library page:
 *    - Set Periodic system timer and Timestamp timer to none
 *      This prevents the automatic inclusion of the timer driver.
 *
 *    - Set Max file descriptors to 4
 *      This reduces the size of the file handle pool.
 *
 *    - Check Main function does not exit
 *    - Uncheck Clean exit (flush buffers)
 *      This removes the unneeded call to exit when main returns, since it
 *      won't.
 *
 *    - Check Don't use C++
 *      This builds without the C++ support code.
 *
 *    - Check Small C library
 *      This uses a reduced functionality C library, which lacks  
 *      support for buffering, file IO, floating point and getch(), etc. 
 *      Check the Nios II Software Developers Manual for a complete list.
 *
 *    - Check Reduced device drivers
 *      This uses reduced functionality drivers if they're available. For the
 *      standard design this means you get polled UART and JTAG UART drivers,
 *      no support for the LCD driver and you lose the ability to program 
 *      CFI compliant flash devices.
 *
 *    - Check Access device drivers directly
 *      This bypasses the device file system to access device drivers directly.
 *      This eliminates the space required for the device file system services.
 *      It also provides a HAL version of libc services that access the drivers
 *      directly, further reducing space. Only a limited number of libc
 *      functions are available in this configuration.
 *
 */

#define BARCO_MAX_ADDR			111
#include "sys/alt_stdio.h"
#include "sys/alt_dev.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "uart/uart.h"
#include "stdint.h"

volatile int * LEDS_ptr = (volatile int *) PIO_0_BASE;
volatile int * BARCO_ptr = (volatile int *) BARCO_AVALON_0_BASE;

#define SET_LED(addr, red, green, blue) {*(BARCO_ptr + 2 * addr) = red | (green << 16); \
										 *(BARCO_ptr + 2 * addr + 1) = blue;}

// gamma correction, from 8 bit to 12 bit
uint16_t led_gamma(uint8_t in);

void waitPattern() {
	unsigned char buf;
	unsigned int counter = 0;

	while(counter != 4) {
		uart_read(&buf, 1, 0);

		switch(counter) {
		case 0:
			if(buf == 0x0F)
				counter++;
			else
				counter = 0;
			break;

		case 1:
			if(buf == 0xF1)
				counter++;
			else
				counter = 0;
			break;

		case 2:
			if(buf == 0x03)
				counter++;
			else
				counter = 0;
			break;

		case 3:
			if(buf == 0xF2)
				counter++;
			else
				counter = 0;
			break;
		}
	}
}

void readBurst(uint8_t enabled) {

	char buf[10];
	unsigned int i;

	for (i = 0; i <= BARCO_MAX_ADDR; i++) {
		uart_read(buf, 3, 0);

		if(enabled)
			SET_LED(i, led_gamma(buf[0]), led_gamma(buf[1]), led_gamma(buf[2]));
	}
}


int main() {

	unsigned int led_cnt = 0;
	// initialize uart
	uart_init();

	*(LEDS_ptr) = 0x02;

	unsigned int i;

	for (i = 0; i <= BARCO_MAX_ADDR; i++) {
		SET_LED(i, 0, 0, 0);
	}

	/* Event loop never exits. */
	while (1) {

		// detect magic pattern
		waitPattern();

		// read all led values from rs485
		readBurst(1);

		if (led_cnt > 3) {
			*(LEDS_ptr) = *(LEDS_ptr) ^ 0x1;
			led_cnt = 0;
		}

		led_cnt++;

	}

	return 0;
}
