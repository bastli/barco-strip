/* IO-Adrressen */
volatile int* uart_rx 			= (volatile int*)0x80101004;
volatile int* uart_tx 			= (volatile int*)0x80101008;

volatile int* keyboard 			= (volatile int*)0x8010000C;

volatile int* intIOBTN			= (volatile int*)0x80080000;
volatile int* intIOSW			= (volatile int*)0x80080001;
volatile int* intIOLED			= (volatile int*)0x80080002;
volatile int* intIOSSEG			= (volatile int*)0x80080003;

volatile int* vga				= (volatile int*)0x800E0000;

volatile int* sram_adr			= (volatile int*)0x800C0000;
volatile int* sram_data			= (volatile int*)0x800C0001;
volatile int* sram_read_data	= (volatile int*)0x800C0003;

volatile int* interrupt_stat 	= (volatile int*)0x80000004;
volatile int* interrupt_enable 	= (volatile int*)0x80000008;
volatile int* interrupt_ctrl 	= (volatile int*)0x8000000C;

int interrupt = 0;
int data = 0;
int global_start_index_lauftext = 0;

void _zpu_interrupt(void);
void uart_recv(void);
void keyboard_recv(void);
int scancode2ascii (int scancode);
void uart_send(char character);
void uart_send_string(char* string);
void scrolling_text(char *string, int iSize);
void s_seg(char *string);
void init(void);
int main();

