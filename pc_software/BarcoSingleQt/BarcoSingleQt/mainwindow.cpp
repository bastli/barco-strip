#if 0
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->status_ip    ->setStyleSheet(status_default_style + "background-color: " + COLOR_ERR.name());
    ui->status_phy   ->setStyleSheet(status_default_style + "background-color: " + COLOR_ERR.name());
    ui->status_port  ->setStyleSheet(status_default_style + "background-color: " + COLOR_ERR.name());
    ui->status_status->setStyleSheet(status_default_style + "background-color: " + COLOR_ERR.name());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_button_connect_all_clicked()
{
    ui->serialcontroller->connect_all_serial();
}

void MainWindow::on_button_add_strip_clicked()
{
    ui->serialcontroller->add_strip();
}

void MainWindow::on_button_remove_strip_clicked()
{
    ui->serialcontroller->remove_strip();
}

void MainWindow::on_button_rescan_serial_clicked()
{
    ui->serialcontroller->updateSerialPorts();
}

#endif
