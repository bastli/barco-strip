#include "barcoserialport.h"

BarcoSerialPort::BarcoSerialPort(int strip_nr, QGridLayout* gui_table, QStandardItemModel *port_model, QWidget *parent) :
    QWidget(parent),
    strip_nr(strip_nr),
    port_model(port_model)
{
    // label, just showing "Strip 12" or something
    label_name = new QLabel(tr("Strip %1").arg(strip_nr), this);
    gui_table->addWidget(label_name, strip_nr*2, 0);
    // make sure widget is destroyed if this strip is as table takes ownership of widget
    connect(this, SIGNAL(destroyed()), label_name, SLOT(deleteLater()));

    // label for currently connected port
    label_status = new QLabel(tr("not connected"), this);
    label_status->setStyleSheet(QString("background-color: %1; %2").arg(COLOR_ERR.name()).arg(status_default_style));
    gui_table->addWidget(label_status, strip_nr*2, 1);
    connect(this, SIGNAL(destroyed()), label_status, SLOT(deleteLater()));

    // dropdown to choose serial port
    dropdown_port = new QComboBox(this);
    dropdown_port->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    dropdown_port->setInsertPolicy(QComboBox::InsertAlphabetically);
    dropdown_port->setModel(this->port_model);
    gui_table->addWidget(dropdown_port, strip_nr*2, 2);
    connect(this, SIGNAL(destroyed()), dropdown_port, SLOT(deleteLater()));

    // connect button
    button_connect = new QPushButton("connect", this);
    connect(button_connect, SIGNAL(clicked()), this, SLOT(handle_connect_button_press()));
    gui_table->addWidget(button_connect, strip_nr*2, 3);
    connect(this, SIGNAL(destroyed()), button_connect, SLOT(deleteLater()));

    // test button
    button_test = new QPushButton("test", this);
    button_test->setDisabled(true);
    connect(button_test, SIGNAL(clicked()), this, SLOT(send_test_pattern()));
    gui_table->addWidget(button_test, strip_nr*2, 4);
    connect(this, SIGNAL(destroyed()), button_test, SLOT(deleteLater()));


    // initialize test pattern
    //memset(serial_test_pattern, 0, sizeof(serial_test_pattern));
    serial_test_pattern = (unsigned char*) malloc(BARCO_DATA_PAYLOAD_SIZE);

    for (int i = 0; i < BARCO_LEDS_PER_STRIP; i++) {
        for (int c = 0; c < BARCO_COL_NUM; c++) {
            serial_test_pattern[i * BARCO_COL_NUM + c] = i*2;
        }
    }

    serial_null_pattern = (unsigned char *) malloc(BARCO_DATA_PAYLOAD_SIZE);
    memset(serial_null_pattern, 0, BARCO_DATA_PAYLOAD_SIZE);

    current_frame_data = (unsigned char *) malloc(BARCO_DATA_PAYLOAD_SIZE);
    memset(current_frame_data, 0, BARCO_DATA_PAYLOAD_SIZE);

    connect(&this->port, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(handle_serial_error(QSerialPort::SerialPortError)));


    data_preview = new StripPreviewWidget(this->current_frame_data, this);
    gui_table->addWidget(data_preview, strip_nr*2+1, 0, 1, 5);

    qDebug() << "create strip with number" << strip_nr;
}

BarcoSerialPort::~BarcoSerialPort()
{
    if (serial_test_pattern) free(serial_test_pattern);
    if (serial_null_pattern) free(serial_null_pattern);
    if (current_frame_data)  free(current_frame_data);
}

void BarcoSerialPort::connect_port(const QSerialPortInfo& port_info)
{
    // ignore if already opened
    if (this->port.isOpen())
        return;

    this->port.setPort(port_info);
    this->port.setBaudRate(BARCO_BAUDRATE);

    if (this->port.open(QIODevice::WriteOnly)) {
        this->port.setBaudRate(BARCO_BAUDRATE);
        this->label_status->setText(port.portName());
        this->label_status->setStyleSheet(QString("background-color: %1; %2").arg(COLOR_OK.name()).arg(status_default_style));
        this->button_connect->setText(tr("disconnect"));
        this->dropdown_port->setDisabled(true);
        this->button_test->setEnabled(true);

        // disable item in model as well, so it can't be selected with other dropdowns
        int idx = this->dropdown_port->currentIndex();
        QStandardItem* i = ((QStandardItemModel*) dropdown_port->model())->item(idx);
        i->setFlags(i->flags() & ~(Qt::ItemIsEnabled));

        qDebug() << "connected to" << port_info.portName() << "at" << port_info.systemLocation();

        store_port_assignment();

    } else {
        // open failed
        this->disconnect_error(QObject::tr("Cannot connect to %1: %2").arg(port.portName()).arg(port.errorString()));
    }
}

void BarcoSerialPort::disconnect_port()
{
    if (!this->port.isOpen())
        return;

    this->port.close();
    this->dropdown_port->setEnabled(true);
    this->label_status->setText("disconnected");
    this->button_connect->setText(tr("connect"));
    this->label_status->setStyleSheet(QString("background-color: %1; %2").arg(COLOR_ERR.name()).arg(status_default_style));
    this->button_test->setDisabled(true);

    // enable item in model, so it is selectable again

    QStandardItemModel* model = (QStandardItemModel*) dropdown_port->model();
    QList<QStandardItem*> items = model->findItems(port.portName());

    if (items.isEmpty())
        qDebug() << "WARNING: cannot reactivate dropdown port '" << port.portName() << "': not found.";
    else if (items.size() > 1)
        qDebug() << "ERROR: cannot reactivate dropdown port '" << port.portName() << "': found" << items.size() << "entries.";
    else {
        items.first()->setFlags(items.first()->flags() | Qt::ItemIsEnabled);
    }

    qDebug() << "disconnected from" << port.portName();
}

void BarcoSerialPort::disconnect_error(const QString& message)
{
    this->disconnect_port();
    this->label_status->setText("<b>ERROR</b>");
    qDebug() << message;
}

void BarcoSerialPort::handle_serial_error(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::WriteError) {
         disconnect_error(tr("An I/O error occurred while writing the data to port %1, error: %2").arg(this->port.portName()).arg(port.errorString()));
    }
}

void BarcoSerialPort::send_data(const unsigned char data[], int size)
{
    // don't sent if not connected to some serial port...
    if (!this->port.isOpen())
        return;

    //qDebug() << "sending" << size << "bytes to strip" << this->strip_nr << "at" << port.portName();

    // TODO
    int d = port.write((const char*)data, size);
    if (d < 0) {
        disconnect_error(tr("Failed to write data to port %1. Error: %2").arg(port.portName()).arg(port.errorString()));
    } else if (d != size) {
        disconnect_error(tr("Failed to write %1 bytes to port %2. Error: %3").arg(size).arg(port.portName()).arg(port.errorString()));
    }
}

void BarcoSerialPort::send_test_pattern()
{
    send_data(serial_sync_pattern, sizeof(serial_sync_pattern));
    send_data(serial_test_pattern, BARCO_DATA_PAYLOAD_SIZE);

    if (show_preview())
        data_preview->set_data(serial_test_pattern);
}

void BarcoSerialPort::send_null_pattern()
{
    send_data(serial_sync_pattern, sizeof(serial_sync_pattern));
    send_data(serial_null_pattern, BARCO_DATA_PAYLOAD_SIZE);

    if (show_preview())
        data_preview->set_data(serial_null_pattern);
}

void BarcoSerialPort::sendCurrentFrame()
{
    //qDebug() << "send frame on strip" << this->strip_nr << "from data at" << (quint64) current_frame_data;
    //qDebug() << QByteArray((char*)current_frame_data, BARCO_DATA_PAYLOAD_SIZE).toHex();

    send_data(serial_sync_pattern, sizeof(serial_sync_pattern));
    send_data(current_frame_data, BARCO_DATA_PAYLOAD_SIZE);

    if (show_preview())
        data_preview->set_data(current_frame_data);
}

void BarcoSerialPort::handle_connect_button_press()
{
    if (this->port.isOpen()) {
        this->disconnect_port();
    } else {
        connect_to_selected();
    }
}

void BarcoSerialPort::connect_to_selected() {
    QSerialPortInfo p = this->dropdown_port->currentData(Qt::UserRole+1).value<QSerialPortInfo>();
    this->connect_port(p);
}

void BarcoSerialPort::select_port(const QString& port_name)
{
    QList<QStandardItem*> items = ((QStandardItemModel*) this->dropdown_port->model())->findItems(port_name);

    // invalid port (try to refresh ports manually)
    if (items.isEmpty())
        return;

    if (items.size() > 1) {
        qDebug() << "ERROR: more than one port in model for name '" << port_name << "' found!";
    } else {
        this->dropdown_port->setCurrentIndex(items.first()->index().row());
    }
}

void BarcoSerialPort::store_port_assignment()
{
    settings.setValue("port_assignments/" + QString::number(this->strip_nr), this->port.portName());
}

void BarcoSerialPort::enable_preview(bool enabled)
{
    this->data_preview->setVisible(enabled);
}
