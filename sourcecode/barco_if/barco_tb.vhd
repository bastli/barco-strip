
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity BarcoTb is

end entity BarcoTb;

architecture behavioural of BarcoTb is

  constant clk_phase_high : time := 50 ns;
  constant clk_phase_low  : time := 50 ns;

  constant stimuli_application_time  : time := 20 ns;
  constant response_acquisition_time : time := 80 ns;


  constant stripe_clk_phase_high : time := 500 ns;
  constant stripe_clk_phase_low  : time := 500 ns;

  constant PWM_WIDTH : natural := 12;

  -----------------------------------------------------------------------------
  -- procedure
  -----------------------------------------------------------------------------
  procedure SetLed(signal Clk_C    : in  std_logic;
                   signal Valid_S  : out std_logic;
                   signal InAddr_D : out std_logic_vector;
                   signal Red_D    : out std_logic_vector;
                   signal Green_D  : out std_logic_vector;
                   signal Blue_D   : out std_logic_vector;
                   constant addr   : in  integer;
                   constant red    : in  std_logic_vector(PWM_WIDTH-1 downto 0);
                   constant green  : in  std_logic_vector(PWM_WIDTH-1 downto 0);
                   constant blue   : in  std_logic_vector(PWM_WIDTH-1 downto 0)
                   )
  is
  begin
    wait until rising_edge(Clk_C);
    wait for stimuli_application_time;

    InAddr_D <= std_logic_vector(to_unsigned(addr, InAddr_D'length));
    Red_D    <= red;
    Green_D  <= green;
    Blue_D   <= blue;

    Valid_S <= '1';

    wait until rising_edge(Clk_C);
    wait for stimuli_application_time;

    Valid_S <= '0';

  end SetLed;

  -----------------------------------------------------------------------------
  -- components
  -----------------------------------------------------------------------------

  component BarcoIf is
    port (
      Reset_RI         : in  std_logic;
      InClk_CI         : in  std_logic;
      InAddr_DI        : in  std_logic_vector(6 downto 0);
      Red_DI           : in  std_logic_vector(PWM_WIDTH-1 downto 0);
      Green_DI         : in  std_logic_vector(PWM_WIDTH-1 downto 0);
      Blue_DI          : in  std_logic_vector(PWM_WIDTH-1 downto 0);
      Valid_SI         : in  std_logic;
      StripeClk_CI     : in  std_logic;
      StripeData_DO    : out std_logic_vector(3 downto 0);
      StripeRed_DO     : out std_logic;
      StripeGreen_DO   : out std_logic;
      StripeBlue_DO    : out std_logic;
      StripeGE_DO      : out std_logic;
      StripeSegment_DO : out std_logic_vector(1 downto 0));
  end component BarcoIf;

  -----------------------------------------------------------------------------
  -- signals
  -----------------------------------------------------------------------------

  signal Clk_C   : std_logic := '0';
  signal Reset_R : std_logic := '0';

  signal InAddr_D : std_logic_vector(6 downto 0)           := (others => '0');
  signal Red_D    : std_logic_vector(PWM_WIDTH-1 downto 0) := (others => '0');
  signal Green_D  : std_logic_vector(PWM_WIDTH-1 downto 0) := (others => '0');
  signal Blue_D   : std_logic_vector(PWM_WIDTH-1 downto 0) := (others => '0');
  signal Valid_S  : std_logic                              := '0';


  signal StripeClk_C     : std_logic;
  signal StripeData_D    : std_logic_vector(3 downto 0);
  signal StripeRed_D     : std_logic;
  signal StripeGreen_D   : std_logic;
  signal StripeBlue_D    : std_logic;
  signal StripeGE_D      : std_logic;
  signal StripeSegment_D : std_logic_vector(1 downto 0);

begin  -- architecture behavioural

  BarcoIfInst : BarcoIf
    port map (
      Reset_RI         => Reset_R,
      InClk_CI         => Clk_C,
      InAddr_DI        => InAddr_D,
      Red_DI           => Red_D,
      Green_DI         => Green_D,
      Blue_DI          => Blue_D,
      Valid_SI         => Valid_S,
      StripeClk_CI     => StripeClk_C,
      StripeData_DO    => StripeData_D,
      StripeRed_DO     => StripeRed_D,
      StripeGreen_DO   => StripeGreen_D,
      StripeBlue_DO    => StripeBlue_D,
      StripeGE_DO      => StripeGE_D,
      StripeSegment_DO => StripeSegment_D);


  -----------------------------------------------------------------------------

  InClkGen : process is
  begin  -- process
    loop
      Clk_C <= '1';
      wait for clk_phase_high;
      Clk_C <= '0';
      wait for clk_phase_low;
    end loop;
  end process InClkGen;

  -----------------------------------------------------------------------------

  StripeClkGen : process is
  begin  -- process
    loop
      StripeClk_C <= '1';
      wait for stripe_clk_phase_high;
      StripeClk_C <= '0';
      wait for stripe_clk_phase_low;
    end loop;
  end process StripeClkGen;


  -----------------------------------------------------------------------------
  StimApplication : process is
    variable i : integer;
  begin  -- process StimApplication
    Reset_R <= '1';

    wait until rising_edge(Clk_C);
    wait until rising_edge(Clk_C);
    wait until rising_edge(Clk_C);

    Reset_R <= '0';

    for i in 0 to 127 loop
      SetLed(Clk_C, Valid_S, InAddr_D, Red_D, Green_D, Blue_D,
             i, x"000", x"000", x"000");
    end loop;  -- i

    for i in 32 to 63 loop
      SetLed(Clk_C, Valid_S, InAddr_D, Red_D, Green_D, Blue_D,
             i, x"FFF", x"FFF", x"FFF");
    end loop;  -- i

    --SetLed(Clk_C, Valid_S, InAddr_D, Red_D, Green_D, Blue_D,
    --       0, "0110", "1111", "0000");

    --SetLed(Clk_C, Valid_S, InAddr_D, Red_D, Green_D, Blue_D,
    --       1, "0001", "1111", "1010");

    --SetLed(Clk_C, Valid_S, InAddr_D, Red_D, Green_D, Blue_D,
    --       6, "0001", "1111", "1010");


    --SetLed(Clk_C, Valid_S, InAddr_D, Red_D, Green_D, Blue_D,
    --       112, "0011", "1111", "1010");

    --SetLed(Clk_C, Valid_S, InAddr_D, Red_D, Green_D, Blue_D,
    --       118, "0011", "1111", "1010");

    --SetLed(Clk_C, Valid_S, InAddr_D, Red_D, Green_D, Blue_D,
    --       120, "0011", "1111", "1010");

    --SetLed(Clk_C, Valid_S, InAddr_D, Red_D, Green_D, Blue_D,
    --       126, "0011", "1111", "1010");

    wait;
  end process StimApplication;



end architecture behavioural;
