
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-------------------------------------------------------------------------------
entity LedCounter is

  port (
    Clk_CI   : in  std_logic;
    Reset_RI : in  std_logic;
    Addr_DO  : out unsigned(6 downto 0);
    Red_DO   : out std_logic_vector(11 downto 0);
    Green_DO : out std_logic_vector(11 downto 0);
    Blue_DO  : out std_logic_vector(11 downto 0);
    Valid_SO : out std_logic
    );

end entity LedCounter;

-------------------------------------------------------------------------------

architecture rtl of LedCounter is
  constant COUNTER_MAX : natural := 1000000;
  type state_t is (wait_set, set, wait_unset, unset);

  signal State_DP, State_DN           : state_t;
  signal Counter_DP, Counter_DN       : unsigned(31 downto 0);
  signal LedCounter_DP, LedCounter_DN : unsigned(6 downto 0);

begin  -- architecture rtl

  fsmReg : process (Clk_CI, Reset_RI) is
  begin  -- process fsmReg
    if Reset_RI = '1' then              -- asynchronous reset (active high)
      State_DP      <= wait_set;
      Counter_DP    <= to_unsigned(0, Counter_DP'length);
      LedCounter_DP <= to_unsigned(0, LedCounter_DP'length);
    elsif rising_edge(Clk_CI) then      -- rising clock edge
      State_DP      <= State_DN;
      Counter_DP    <= Counter_DN;
      LedCounter_DP <= LedCounter_DN;
    end if;
  end process fsmReg;

  fsmComb : process (Counter_DP, LedCounter_DP, State_DP) is
  begin  -- process fsmComb
    State_DN      <= State_DP;
    Counter_DN    <= Counter_DP + 1;
    LedCounter_DN <= LedCounter_DP;

    Red_DO   <= (others => '0');
    Green_DO <= (others => '0');
    Blue_DO  <= (others => '0');
    Valid_SO <= '0';

    case State_DP is
      -------------------------------------------------------------------------
      when wait_set =>
        if Counter_DP = COUNTER_MAX then
          State_DN   <= set;
          Counter_DN <= to_unsigned(0, Counter_DN'length);
        end if;

      -----------------------------------------------------------------------
      when set =>
        Red_DO   <= (others => '1');
        Green_DO <= (others => '0');
        Blue_DO  <= (others => '0');
        Valid_SO <= '1';

        if LedCounter_DP = 111 then
          LedCounter_DN <= to_unsigned(0, LedCounter_DN'length);
          State_DN      <= wait_unset;
        else
          State_DN      <= wait_set;
          LedCounter_DN <= LedCounter_DP + 1;
        end if;
        -----------------------------------------------------------------------

      when wait_unset =>
        if Counter_DP = COUNTER_MAX then
          State_DN   <= unset;
          Counter_DN <= to_unsigned(0, Counter_DN'length);
        end if;

      -----------------------------------------------------------------------
      when unset =>
        Red_DO   <= (others => '0');
        Green_DO <= (others => '0');
        Blue_DO  <= (others => '1');
        Valid_SO <= '1';


        if LedCounter_DP = 111 then
          LedCounter_DN <= to_unsigned(0, LedCounter_DN'length);
          State_DN      <= wait_set;
        else
          State_DN      <= wait_unset;
          LedCounter_DN <= LedCounter_DP + 1;
        end if;

      -----------------------------------------------------------------------
      when others => null;
    end case;
  end process fsmComb;


  -----------------------------------------------------------------------------
  -- output assignements
  -----------------------------------------------------------------------------

  Addr_DO <= LedCounter_DP;


end architecture rtl;
