
//-----------------------------------------------------------------------------
// UART
// this is just a small wrapper, so that we do not have to deal with the HAL directly
// For more information look at the source files and as most functions just pass
// the arguments to an internal function
//-----------------------------------------------------------------------------


void uart_init();

int uart_read(char* ptr, int len, int flags);

int uart_write(const char* ptr, int len, int flags);
