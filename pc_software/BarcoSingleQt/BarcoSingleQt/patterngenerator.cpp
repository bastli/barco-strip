#include "patterngenerator.h"

void PatternGenerator::generate(const int& strip_id, unsigned char *dest)
{
    for (int i = 0; i < BARCO_LEDS_PER_STRIP; i++) {
        this->generate(strip_id, i,
                       dest[i*BARCO_COL_NUM + 0],
                       dest[i*BARCO_COL_NUM + 1],
                       dest[i*BARCO_COL_NUM + 2]);
    }
}

void PatternGenerator::generate(const int &strip_id, const int i, unsigned char& r, unsigned char& g, unsigned char& b)
{
    Q_UNUSED(strip_id);
    Q_UNUSED(i);

    r = 0; g = 0; b = 0;
}

void ConstPatternGenerator::generate(const int &strip_id, const int i, unsigned char &r, unsigned char &g, unsigned char &b)
{
    Q_UNUSED(strip_id);
    Q_UNUSED(i);

    r = color.red();
    g = color.green();
    b = color.blue();
}

void RampPatternGenerator::generate(const int &strip_id, const int i, unsigned char &r, unsigned char &g, unsigned char &b)
{
    Q_UNUSED(strip_id);

    float p = ((float) i) / BARCO_LEDS_PER_STRIP;

    r = p * color.red();
    g = p * color.green();
    b = p * color.blue();
}

void NumberPatternGenerator::generate(const int &strip_id, const int i, unsigned char &r, unsigned char &g, unsigned char &b)
{
    r =   0; g =   0; b =   0;

    if (i%2 == 0 && i/2 <= strip_id) {
        r = 255; g = 255; b = 255;
    } else {
        int j = BARCO_LEDS_PER_STRIP - i - 1; // inverse order

        if (j%2 == 0 && j/2 <= strip_id) {
            r = 255; g = 255; b = 255;
        }
    }
}
