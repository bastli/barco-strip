#include "barcocontroller.h"
#include "ui_barcocontroller.h"

bool compareSerialPortInfo(const QSerialPortInfo& i1, const QSerialPortInfo& i2)
{
    return i1.portName() < i2.portName();
}

BarcoController::BarcoController(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::BarcoController),
    server(NULL), client(NULL)
{
    ui->setupUi(this);
    ui->status_ip    ->setStyleSheet(status_default_style + "background-color: " + COLOR_ERR.name());
    ui->status_port  ->setStyleSheet(status_default_style + "background-color: " + COLOR_ERR.name());
    ui->status_status->setStyleSheet(status_default_style + "background-color: " + COLOR_ERR.name());

    main_layout = ui->device_table;

    serial_port_model = new QStandardItemModel(this);

    updateSerialPorts();

    // per default, create as many strips as available serial ports
    QList<QSerialPortInfo> available_ports = this->filter_ports(QSerialPortInfo::availablePorts());
    //for (int i = 0; i < available_ports.length(); i++) {
    int i = 0;
    foreach (const QSerialPortInfo& info, available_ports) {
        BarcoSerialPort* port = this->add_strip(); //new BarcoSerialPort(i, main_layout, serial_port_model, this->show_live_preview, this);

        // try to guess which port to use
        QString port_name = info.portName();

        if (settings.value("restore_port_assignment", false).toBool()) {
            QVariant v = settings.value("port_assignments/" + QString::number(i));

            if (v.isValid())
                port_name = v.toString();
        }

        port->select_port(port_name);

        ++i;
    }

    server = new QTcpServer(this);

    startServer();

    // data for unconnected strips goes here
    devnull = (char*) malloc(BARCO_COL_NUM * BARCO_LEDS_PER_STRIP);


    // create some test patterns
    addPattern("counter (1 based)",    new NumberPatternGenerator(this));

    addPattern("constant WHITE", new ConstPatternGenerator(Qt::white, this));
    addPattern("constant RED",   new ConstPatternGenerator(Qt::red, this));
    addPattern("constant GREEN", new ConstPatternGenerator(Qt::green, this));
    addPattern("constant BLUE" , new ConstPatternGenerator(Qt::blue, this));

    addPattern("ramp WHITE", new RampPatternGenerator(Qt::white, this));
    addPattern("ramp RED",   new RampPatternGenerator(Qt::red, this));
    addPattern("ramp GREEN", new RampPatternGenerator(Qt::green, this));
    addPattern("ramp BLUE" , new RampPatternGenerator(Qt::blue, this));


    // restore permanent settings

    ui->checkbox_show_live->setChecked(settings.value("show_live_data", true).toBool());
    ui->dropdown_test_pattern->setCurrentIndex(settings.value("test_pattern", 0).toInt());

    ui->checkbox_test_clear->setChecked(settings.value("test_clear_after_delay", true).toBool());
    ui->checkbox_test_simult->setChecked(settings.value("test_simultaneously", true).toBool());

    // autoconnect to all available ports if requested
    if (settings.value("autoconnect", false).toBool()) {
        this->connect_all_serial();
        ui->checkbox_autoconnect->setChecked(true);
    }

    ui->checkbox_remember_assigment->setChecked(settings.value("restore_port_assignment", false).toBool());

    dropdown_test_pattern_changed(ui->dropdown_test_pattern->currentIndex());

    connect(ui->dropdown_test_pattern, SIGNAL(currentIndexChanged(int)), this, SLOT(dropdown_test_pattern_changed(int)));
}

BarcoController::~BarcoController()
{
    if (devnull) free(devnull);
    delete ui;
}

void BarcoController::addPattern(QString desc, PatternGenerator* gen)
{
    this->pattern_generators.push_back(gen);
    this->ui->dropdown_test_pattern->addItem(desc, QVariant::fromValue((void*) gen));
}


void BarcoController::startServer()
{
    if (!server->listen(QHostAddress::Any, DEFAULT_TCP_PORT)) {
        qDebug() << "failed to starten server on port" << DEFAULT_TCP_PORT;
        qDebug() << server->errorString();

        QMessageBox::critical(this, "Failed to start server",
                              tr("Failed to start server on port %1:\n%2").arg(DEFAULT_TCP_PORT).arg(server->errorString()));

        ui->status_status->setText(server->errorString());
        ui->button_server_enable->setText(tr("start server"));
    } else {
        qDebug() << "server listening on address" << server->serverAddress() << "and port" << server->serverPort();
        qDebug() << "this device's IP is" << getMyIP().toString();

        ui->status_ip->setText(getMyIP().toString());
        ui->status_port->setText(QString::number(server->serverPort()));
        ui->status_status->setText("waiting for connection");
        ui->status_status->setStyleSheet(status_default_style + "background-color: " + COLOR_WAITING.name());

        ui->status_ip    ->setStyleSheet(status_default_style + "background-color: " + COLOR_OK.name());
        ui->status_port  ->setStyleSheet(status_default_style + "background-color: " + COLOR_OK.name());

        connect(server, SIGNAL(newConnection()), this, SLOT(handleNewClientConnection()));
        ui->button_server_enable->setText(tr("stop server"));
    }
}

void BarcoController::stopServer()
{
    if (client && client->isOpen()) {
        this->client->close();
        this->client->deleteLater();
        this->client = NULL;

        // handleClientDisconnect should do the rest
    }

    if (server->isListening())
        server->close();

    this->ui->status_status->setText("not running");
    ui->status_status->setStyleSheet(status_default_style + "background-color: " + COLOR_ERR.name());

    rx_timer.invalidate();
    this->ui->button_server_enable->setText(tr("start server"));
}

QHostAddress BarcoController::getMyIP()
{
    // use the first non-localhost IPv4 address
    foreach (QHostAddress address, QNetworkInterface::allAddresses()) {
        if (address != QHostAddress::LocalHost && address.toIPv4Address()) {
            return address;
        }
    }

    // if we did not find one, use IPv4 localhost
    return QHostAddress(QHostAddress::LocalHost);
}

void BarcoController::updateSerialPorts()
{
    const QList<QSerialPortInfo> available_ports = QSerialPortInfo::availablePorts();

    // remove ports if necessary
    if (serial_port_model->rowCount() > 0) {
        for (int i = 0; i < serial_port_model->rowCount(); ++i) {
            // check if old ports still exist
            bool exists = false;
            foreach (const QSerialPortInfo& p, available_ports) {
                if (p.portName() == serial_port_model->item(i)->text()) {
                    exists = true; // found it, it's still there!
                    break;
                }
            }

            if (!exists) {
                serial_port_model->removeRow(i);
            }
        }
    }

    // add new ports
    foreach (const QSerialPortInfo& port, available_ports) {

        if (serial_port_model->findItems(port.portName()).isEmpty()) {

            QStandardItem * item = new QStandardItem(port.portName());
            item->setData(port.portName(), Qt::DisplayRole);
            item->setData(QVariant::fromValue(port));
            serial_port_model->appendRow(item);
        }
    }

    serial_port_model->sort(0);

    for(int i = 0; i < serial_port_model->rowCount(); i++) {
        qDebug() << "port" << i << "=" << serial_port_model->item(i)->text();
    }
}

void BarcoController::connect_all_serial()
{
    foreach (BarcoSerialPort* p, this->serialports) {
        p->connect_to_selected();
    }
}

BarcoSerialPort* BarcoController::add_strip()
{
    BarcoSerialPort* port = new BarcoSerialPort(serialports.size(), main_layout, serial_port_model, this);
    port->enable_preview(this->ui->checkbox_show_live->isChecked());
    this->serialports.push_back(port);

    dropdown_test_pattern_changed(ui->dropdown_test_pattern->currentIndex());

    return port;
}

void BarcoController::remove_strip()
{
    if (!this->serialports.isEmpty()) {
        BarcoSerialPort* last = serialports.last();
        serialports.removeLast();
        last->deleteLater();
    }
}

// returns true if name is a serial port we want (e.g. "ttyUSB1")
bool BarcoController::filter_port(const QSerialPortInfo &info)
{
    return info.portName().contains("USB", Qt::CaseInsensitive)
        || (info.vendorIdentifier() == 0x067b && info.productIdentifier() == 0x2303);
}

QList<QSerialPortInfo> BarcoController::filter_ports(const QList<QSerialPortInfo>& ports)
{
    QList<QSerialPortInfo> result;

    foreach (const QSerialPortInfo& i, ports) {
        if (filter_port(i))
            result.push_back(i);
    }

    // order ports alphabetically
    qSort(result.begin(), result.end(), compareSerialPortInfo);

    return result;
}

void BarcoController::on_button_connect_all_clicked()
{
    this->connect_all_serial();
}

void BarcoController::on_button_add_strip_clicked()
{
    this->add_strip();
}

void BarcoController::on_button_remove_strip_clicked()
{
    this->remove_strip();
}

void BarcoController::on_button_rescan_serial_clicked()
{
    this->updateSerialPorts();
}

void BarcoController::handleNewClientConnection()
{
    // we already have a connection, discard this new attempt
    if (this->client && this->client->isOpen()) {
        qDebug() << "ignoring connection attempt: server is already connected";
        return;
    }


    this->client = this->server->nextPendingConnection();

    if (!client) {
        qDebug() << "ERROR: failed to get next pending TCP connection:" << server->errorString();

        this->ui->status_status->setText("connection failed");
        ui->status_status->setStyleSheet(status_default_style + "background-color: " + COLOR_ERR.name());

        return;
    }

    qDebug() << "new client connected:" << client->peerAddress();

    connect(client, SIGNAL(disconnected()), this, SLOT(handleClientDisconnect()));
    connect(client, SIGNAL(readyRead()),    this, SLOT(handleIncomingNetworkData()));

    this->ui->status_status->setText(tr("connected to %1").arg(client->peerAddress().toString()));
    ui->status_status->setStyleSheet(status_default_style + "background-color: " + COLOR_OK.name());

    rx_timer.start();
    rx_count = 0;

    qDebug() << "stop listening";
    server->close();
}

void BarcoController::handleClientDisconnect()
{
    qDebug() << "client disconnected";

    if (client) {
        this->client->deleteLater();
        this->client = NULL;
    }

    this->ui->status_status->setText("disconnected");
    ui->status_status->setStyleSheet(status_default_style + "background-color: " + COLOR_ERR.name());

    rx_timer.invalidate();

    startServer();
}

bool BarcoController::receiveSingleByte(unsigned char& byte)
{
    if (client->bytesAvailable() == 0 && !client->waitForReadyRead(100)) // 100ms timeout
        return false;

    qint64 count = client->read((char*) &byte, 1);
    if (count != 1) {
        qDebug() << "ERROR: cannot read single byte from network. got" << count << "bytes. Error:" << client->errorString();
        return false;
    } else
        return true;
}

bool BarcoController::receiveMoreBytes(unsigned char* dest, qint64 size)
{
    if (!client) {
        qDebug() << "ERROR: cannot receive bytes: no connected client.";
        return false;
    }

    while (size > 0) {
        qint64 read = client->read((char*) dest, size);

        if (read < 0) {
            qDebug() << "ERROR: failed to receive" << size << "bytes:" << client->errorString();
            return false;
        } else {
            dest += read;
            size -= read;
        }
    }

    return true;
}

void BarcoController::handleIncomingNetworkData()
{
    if (!client) {
        qDebug() << "ERROR: cannot handle incoming network data: no open socket.";
        return;
    }

    if (client->bytesAvailable() == 0)
        return;

    unsigned int header_state = 0;
    while (header_state < sizeof(network_sync_pattern)) {

        // receive single byte
        unsigned char d = 0;
        if (!receiveSingleByte(d)) return;

        if (d == network_sync_pattern[header_state]) {
            ++header_state;
        } else {
            header_state = 0;
        }
    }

    unsigned char address = 0;
    if (!receiveSingleByte(address)) return;

    rx_count++;
    if (rx_count == 100) {

        float time = ((float) rx_timer.restart()) / rx_count; // ms per packet

        float fps   = 1000 / (time * 15); // we currently get data for all 15 strips
        float kbytes = (BARCO_COL_NUM + BARCO_LEDS_PER_STRIP + sizeof(network_sync_pattern) + 1) / time;

        float percent = kbytes * 1000 / BARCO_BAUDRATE;

        ui->status_transferrate->setText(tr("%1 FPS\n%2 kB/s\%3 %% of serial link").arg(fps).arg(kbytes).arg(percent*100));

        rx_count = 0;
    }

    if (address >= serialports.size()) {
        // silently ignore data for strips we don't have
        //qDebug() << "WARNING: ignoring data for address" << address << "no strip connected with this number.";
        client->read(devnull, BARCO_COL_NUM * BARCO_LEDS_PER_STRIP);
        return;
    }

    BarcoSerialPort* port = serialports.at((int) address);
    if (receiveMoreBytes(port->getBuffer(), BARCO_COL_NUM * BARCO_LEDS_PER_STRIP)) {

        //qDebug() << "updating port" << port->getNR();

        // signal update to port
        port->sendCurrentFrame();

        //qDebug() << "send frame on strip" << port->getNR() << "from data at" << (quint64) port->getBuffer();
        //qDebug() << QByteArray((char*)port->getBuffer(), BARCO_COL_NUM*BARCO_LEDS_PER_STRIP).toHex();


    } else
        qDebug() << "failed to read whole frame";
}

bool BarcoController::isServerRunning()
{
    return this->server->isListening() || (this->client && this->client->isOpen());
}

void BarcoController::on_button_server_enable_clicked()
{
    if (isServerRunning())
        stopServer();
    else
        startServer();
}

void BarcoController::on_button_test_all_clicked()
{
    foreach (BarcoSerialPort* port, this->serialports) {
        if (ui->checkbox_test_simult->isChecked()) {

            port->send_test_pattern();
            if (ui->checkbox_test_clear->isChecked())
                QTimer::singleShot(1000, port, SLOT(send_null_pattern()));

        } else {

            QTimer::singleShot(1000 * port->getNR(), port, SLOT(send_test_pattern()));
            if (ui->checkbox_test_clear->isChecked())
                QTimer::singleShot(1000 + 1000 * port->getNR(), port, SLOT(send_null_pattern()));
        }
    }
}

void BarcoController::dropdown_test_pattern_changed(int index)
{
    // TODO: use ui->dropdown_test_pattern->itemData() somehow
    PatternGenerator* gen = this->pattern_generators.at(index);

    //qDebug() << "changing pattern to" << index;
    settings.setValue("test_pattern", index);

    if (!gen) {
        qDebug() << "ERROR: no pattern generator found with index" << index;
        return;
    }

    foreach (BarcoSerialPort* port, this->serialports) {
        gen->generate(port->getNR(), port->getTestBuffer());
    }
}

void BarcoController::on_checkbox_autoconnect_stateChanged(int arg1)
{
    Q_UNUSED(arg1);

    settings.setValue("autoconnect", ui->checkbox_autoconnect->isChecked());
}

void BarcoController::on_checkbox_remember_assigment_stateChanged(int arg1)
{
    Q_UNUSED(arg1);

    settings.setValue("restore_port_assignment", ui->checkbox_remember_assigment->isChecked());
}

void BarcoController::on_checkbox_show_live_toggled(bool checked)
{
    settings.setValue("show_live_data", checked);

    foreach (BarcoSerialPort* port, this->serialports) {
        port->enable_preview(checked);
    }
}

void BarcoController::on_checkbox_test_clear_toggled(bool checked)
{
    settings.setValue("test_clear_after_delay", checked);
}

void BarcoController::on_checkbox_test_simult_toggled(bool checked)
{
    settings.setValue("test_simultaneously", checked);
}
