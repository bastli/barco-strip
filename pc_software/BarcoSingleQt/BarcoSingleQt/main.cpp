#include "barcocontroller.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("AMIV Bastli");
    QCoreApplication::setOrganizationDomain("bastli.ch");
    QCoreApplication::setApplicationName("BarcoSingleQt");

    QApplication a(argc, argv);

    BarcoController c;
    c.show();

    return a.exec();
}
