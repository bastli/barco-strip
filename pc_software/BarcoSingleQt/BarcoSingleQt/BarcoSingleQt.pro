#-------------------------------------------------
#
# Project created by QtCreator 2014-10-21T16:57:23
#
#-------------------------------------------------

QT       += core gui serialport network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BarcoSingleQt
TEMPLATE = app


SOURCES += main.cpp\
    barcoserialport.cpp \
    barcocontroller.cpp \
    patterngenerator.cpp \
    strippreviewwidget.cpp

HEADERS  += \
    definitions.h \
    barcoserialport.h \
    barcocontroller.h \
    patterngenerator.h \
    strippreviewwidget.h

FORMS    += \
    barcocontroller.ui

OTHER_FILES += \
    TODO
