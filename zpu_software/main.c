


volatile int* intIOLED			= (volatile int*)0x80080002;
volatile int* intIOBTN			= (volatile int*)0x80080000;

volatile int* uart_rx 			= (volatile int*)0x80101004;
volatile int* uart_tx 			= (volatile int*)0x80101008;

volatile int* barco_base		= (volatile int*)0x80060000;

#define BARCO_MAX_ADDR			111

void uart_send(char character) {
	while ((*uart_tx & 0x100) == 0);
	*uart_tx = character;
}

void uart_send_string(char* string) {
	int i = 0;
	while (string[i]) uart_send(string[i++]);
}


int main()
{

unsigned int prev_state;
unsigned int prev_btn_state;
unsigned int ledmask = 0x00FFFFFF;
unsigned int upper_bd = 0;
unsigned int lower_bd = 0;
	
	uart_send_string("Init LEDS with 0xFF");
	for(unsigned int i=0; i<=BARCO_MAX_ADDR; i++){
	
	*(barco_base+i)=0x00FFFFFF;
	
	
	
	
	}
	
	while (1){
	   

	  
	  switch(*intIOBTN){
	  
	  case 1:
		lower_bd=0;
	  break;
	  
	  
	  
	  case 2:
	 lower_bd=28;
	
	
	  break;
	  
	  
	  
	  case 4:
	  	lower_bd=56;
	

	
	
	  break;
	  
	 
	  
	  case 8:
	 lower_bd=84;
	
	
	
	  break;
	  
	  default:
	  
	  break;
	  }
	  
	  
	*intIOLED = *intIOBTN;
	
	for(int i=0; i<=BARCO_MAX_ADDR; i++){
	
	if(i>=lower_bd && i<lower_bd+28){
	*(barco_base+i)=0x00FFFFFF;
	}
	else{
	*(barco_base+i)= 0x0;
	
	}
	
	
	
	}
  }
	
	return 0;
}

