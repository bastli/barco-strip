#include "strippreviewwidget.h"

StripPreviewWidget::StripPreviewWidget(const unsigned char* data, QWidget *parent)
    : QWidget(parent), data(data)
{
}

QSize StripPreviewWidget::minimumSizeHint() const
{
    return QSize(BARCO_LEDS_PER_STRIP, 8);
}

QSize StripPreviewWidget::sizeHint() const
{
    // prefered height of widget
    return QSize(BARCO_LEDS_PER_STRIP, 8);
}

void StripPreviewWidget::set_data(const unsigned char* data)
{
    this->data = data;
    this->update();
}

void StripPreviewWidget::paintEvent(QPaintEvent * )
{
    QPainter painter(this);

    if (!data)
        return;

    int pixelwidth = ((float) width()) / BARCO_LEDS_PER_STRIP;

    for (int i = 0; i < BARCO_LEDS_PER_STRIP; i++) {

        painter.fillRect(i*pixelwidth+1, 1, pixelwidth-1, height(),
                         QColor(data[i*BARCO_COL_NUM+0],
                                data[i*BARCO_COL_NUM+1],
                                data[i*BARCO_COL_NUM+2])
                         );
    }
}
