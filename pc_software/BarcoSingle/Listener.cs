﻿#define ShowFPS

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Windows.Media;
using System.IO.Ports;
using System.IO;



namespace BarcoSingle
{
    class Listener : IDisposable
    {
        private IPEndPoint _endpoint;

        const int bufferSize = 1000;
        const int timeOut = 1000;
        readonly byte[] magicPattern = new byte[] { 0xb1, 0x6b, 0x00, 0xb5 };
        const int adressByte = 1;
        const int colorDataBytes = 112 * 3;
        const int stripeDataSize = 112 * 3 + 1 + 4;

        private readonly byte[] syncPattern = { 0x0f, 0xf1, 0x03, 0xf2 };

        byte[] byteBuffer = new byte[1];
        byte[] ledBuffer = new byte[colorDataBytes*15];

        public void OutputRun(string name, int address)
        {
            SerialPort serialPort = new SerialPort(name)
            {
                BaudRate = 500000,
                Parity = Parity.None,
                DataBits = 8,
                StopBits = StopBits.One,
                WriteBufferSize = 1200,
                WriteTimeout = 100,
            };

            serialPort.Open();
            if (serialPort.IsOpen)
            {
                while (true)
                {
                    serialPort.Write(syncPattern, 0, 4);
                    serialPort.Write(ledBuffer, address * colorDataBytes, colorDataBytes);
                }
            }


            return;
        }

        public void Run(int port)
        {
            

            _endpoint = new IPEndPoint(IPAddress.Any, port);
            using (Socket _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                _listener.Bind(_endpoint);
                _listener.Listen(1);

                while (true)
                {

                    System.Diagnostics.Debug.Print(String.Format("My IP is {0}", MyIP()));
                    System.Diagnostics.Debug.Print(String.Format("Waiting for connection on {0}:{1}", _endpoint.Address.ToString(), port));

                    //wait for connection (blocking)
                    using (var _connection = _listener.Accept())
                    {

                        bool connected = true;

                        while (connected)
                        {
                            System.Diagnostics.Debug.Print("Connection established!");

                            _connection.ReceiveTimeout = timeOut;

                            while (true)
                            {
                                try
                                {
                                    int headerCount = 0;

                                    while (headerCount < 4)
                                    {
                                        if (_connection.Receive(byteBuffer, 0, 1, SocketFlags.None) > 0)
                                        {
                                            if (byteBuffer[0] == magicPattern[headerCount])
                                                headerCount++;
                                            else
                                                headerCount = 0;
                                        }
                                    }

                                    while (_connection.Receive(byteBuffer, 0, 1, SocketFlags.None) < 1)
                                    { }

                                    byte addr = byteBuffer[0];

                                    int ledCount = 0;
                                    int offset = addr * colorDataBytes;

                                    while (true)
                                    {
                                        ledCount += _connection.Receive(ledBuffer, offset + ledCount, colorDataBytes - ledCount, SocketFlags.None);
                                        if (ledCount >= colorDataBytes)
                                            break;
                                    }
                                }
                                catch (SocketException e)
                                {
                                    System.Diagnostics.Debug.Print(String.Format("Socket Error: {0}", e.Message));
                                    connected = false;
                                    break;
                                }
                            }

                        }
                    }
                }
            }
        }

        private string MyIP()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        public void Dispose()
        {
        }
    }
}
