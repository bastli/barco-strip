// qsys_barco.v


`timescale 1 ps / 1 ps
module qsys_barco (
		input  wire        reset_reset_n,                       //                      reset.reset_n
		input  wire        clk_clk,                             //                        clk.clk
		output wire [1:0]  pio_0_external_connection_export,    //  pio_0_external_connection.export
		input  wire        uart_0_external_connection_rxd,      // uart_0_external_connection.rxd
		output wire        uart_0_external_connection_txd,      //                           .txd
		output wire [11:0] barco_avalon_0_conduit_end_Red_DO,   // barco_avalon_0_conduit_end.Red_DO
		output wire [11:0] barco_avalon_0_conduit_end_Green_DO, //                           .Green_DO
		output wire [11:0] barco_avalon_0_conduit_end_Blue_DO,  //                           .Blue_DO
		output wire        barco_avalon_0_conduit_end_Valid_SO, //                           .Valid_SO
		output wire [6:0]  barco_avalon_0_conduit_end_Addr_DO   //                           .Addr_DO
	);

	wire          nios2_qsys_0_instruction_master_waitrequest;                                                                    // nios2_qsys_0_instruction_master_translator:av_waitrequest -> nios2_qsys_0:i_waitrequest
	wire   [24:0] nios2_qsys_0_instruction_master_address;                                                                        // nios2_qsys_0:i_address -> nios2_qsys_0_instruction_master_translator:av_address
	wire          nios2_qsys_0_instruction_master_read;                                                                           // nios2_qsys_0:i_read -> nios2_qsys_0_instruction_master_translator:av_read
	wire   [31:0] nios2_qsys_0_instruction_master_readdata;                                                                       // nios2_qsys_0_instruction_master_translator:av_readdata -> nios2_qsys_0:i_readdata
	wire          nios2_qsys_0_data_master_waitrequest;                                                                           // nios2_qsys_0_data_master_translator:av_waitrequest -> nios2_qsys_0:d_waitrequest
	wire   [31:0] nios2_qsys_0_data_master_writedata;                                                                             // nios2_qsys_0:d_writedata -> nios2_qsys_0_data_master_translator:av_writedata
	wire   [24:0] nios2_qsys_0_data_master_address;                                                                               // nios2_qsys_0:d_address -> nios2_qsys_0_data_master_translator:av_address
	wire          nios2_qsys_0_data_master_write;                                                                                 // nios2_qsys_0:d_write -> nios2_qsys_0_data_master_translator:av_write
	wire          nios2_qsys_0_data_master_read;                                                                                  // nios2_qsys_0:d_read -> nios2_qsys_0_data_master_translator:av_read
	wire   [31:0] nios2_qsys_0_data_master_readdata;                                                                              // nios2_qsys_0_data_master_translator:av_readdata -> nios2_qsys_0:d_readdata
	wire          nios2_qsys_0_data_master_debugaccess;                                                                           // nios2_qsys_0:jtag_debug_module_debugaccess_to_roms -> nios2_qsys_0_data_master_translator:av_debugaccess
	wire    [3:0] nios2_qsys_0_data_master_byteenable;                                                                            // nios2_qsys_0:d_byteenable -> nios2_qsys_0_data_master_translator:av_byteenable
	wire   [31:0] nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_writedata;                                        // nios2_qsys_0_jtag_debug_module_translator:av_writedata -> nios2_qsys_0:jtag_debug_module_writedata
	wire    [8:0] nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_address;                                          // nios2_qsys_0_jtag_debug_module_translator:av_address -> nios2_qsys_0:jtag_debug_module_address
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_chipselect;                                       // nios2_qsys_0_jtag_debug_module_translator:av_chipselect -> nios2_qsys_0:jtag_debug_module_select
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_write;                                            // nios2_qsys_0_jtag_debug_module_translator:av_write -> nios2_qsys_0:jtag_debug_module_write
	wire   [31:0] nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_readdata;                                         // nios2_qsys_0:jtag_debug_module_readdata -> nios2_qsys_0_jtag_debug_module_translator:av_readdata
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_begintransfer;                                    // nios2_qsys_0_jtag_debug_module_translator:av_begintransfer -> nios2_qsys_0:jtag_debug_module_begintransfer
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_debugaccess;                                      // nios2_qsys_0_jtag_debug_module_translator:av_debugaccess -> nios2_qsys_0:jtag_debug_module_debugaccess
	wire    [3:0] nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_byteenable;                                       // nios2_qsys_0_jtag_debug_module_translator:av_byteenable -> nios2_qsys_0:jtag_debug_module_byteenable
	wire   [15:0] uart_0_s1_translator_avalon_anti_slave_0_writedata;                                                             // uart_0_s1_translator:av_writedata -> uart_0:writedata
	wire    [2:0] uart_0_s1_translator_avalon_anti_slave_0_address;                                                               // uart_0_s1_translator:av_address -> uart_0:address
	wire          uart_0_s1_translator_avalon_anti_slave_0_chipselect;                                                            // uart_0_s1_translator:av_chipselect -> uart_0:chipselect
	wire          uart_0_s1_translator_avalon_anti_slave_0_write;                                                                 // uart_0_s1_translator:av_write -> uart_0:write_n
	wire          uart_0_s1_translator_avalon_anti_slave_0_read;                                                                  // uart_0_s1_translator:av_read -> uart_0:read_n
	wire   [15:0] uart_0_s1_translator_avalon_anti_slave_0_readdata;                                                              // uart_0:readdata -> uart_0_s1_translator:av_readdata
	wire          uart_0_s1_translator_avalon_anti_slave_0_begintransfer;                                                         // uart_0_s1_translator:av_begintransfer -> uart_0:begintransfer
	wire   [31:0] onchip_memory2_0_s1_translator_avalon_anti_slave_0_writedata;                                                   // onchip_memory2_0_s1_translator:av_writedata -> onchip_memory2_0:writedata
	wire    [8:0] onchip_memory2_0_s1_translator_avalon_anti_slave_0_address;                                                     // onchip_memory2_0_s1_translator:av_address -> onchip_memory2_0:address
	wire          onchip_memory2_0_s1_translator_avalon_anti_slave_0_chipselect;                                                  // onchip_memory2_0_s1_translator:av_chipselect -> onchip_memory2_0:chipselect
	wire          onchip_memory2_0_s1_translator_avalon_anti_slave_0_clken;                                                       // onchip_memory2_0_s1_translator:av_clken -> onchip_memory2_0:clken
	wire          onchip_memory2_0_s1_translator_avalon_anti_slave_0_write;                                                       // onchip_memory2_0_s1_translator:av_write -> onchip_memory2_0:write
	wire   [31:0] onchip_memory2_0_s1_translator_avalon_anti_slave_0_readdata;                                                    // onchip_memory2_0:readdata -> onchip_memory2_0_s1_translator:av_readdata
	wire    [3:0] onchip_memory2_0_s1_translator_avalon_anti_slave_0_byteenable;                                                  // onchip_memory2_0_s1_translator:av_byteenable -> onchip_memory2_0:byteenable
	wire   [31:0] pio_0_s1_translator_avalon_anti_slave_0_writedata;                                                              // pio_0_s1_translator:av_writedata -> pio_0:writedata
	wire    [2:0] pio_0_s1_translator_avalon_anti_slave_0_address;                                                                // pio_0_s1_translator:av_address -> pio_0:address
	wire          pio_0_s1_translator_avalon_anti_slave_0_chipselect;                                                             // pio_0_s1_translator:av_chipselect -> pio_0:chipselect
	wire          pio_0_s1_translator_avalon_anti_slave_0_write;                                                                  // pio_0_s1_translator:av_write -> pio_0:write_n
	wire   [31:0] pio_0_s1_translator_avalon_anti_slave_0_readdata;                                                               // pio_0:readdata -> pio_0_s1_translator:av_readdata
	wire          barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_waitrequest;                                         // barco_avalon_0:avs_s0_waitrequest -> barco_avalon_0_barco_avalon_translator:av_waitrequest
	wire   [63:0] barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_writedata;                                           // barco_avalon_0_barco_avalon_translator:av_writedata -> barco_avalon_0:avs_s0_writedata
	wire    [6:0] barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_address;                                             // barco_avalon_0_barco_avalon_translator:av_address -> barco_avalon_0:avs_s0_address
	wire          barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_write;                                               // barco_avalon_0_barco_avalon_translator:av_write -> barco_avalon_0:avs_s0_write
	wire          barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_read;                                                // barco_avalon_0_barco_avalon_translator:av_read -> barco_avalon_0:avs_s0_read
	wire   [63:0] barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_readdata;                                            // barco_avalon_0:avs_s0_readdata -> barco_avalon_0_barco_avalon_translator:av_readdata
	wire          barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_readdatavalid;                                       // barco_avalon_0:avs_s0_readdatavalid -> barco_avalon_0_barco_avalon_translator:av_readdatavalid
	wire    [7:0] barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_byteenable;                                          // barco_avalon_0_barco_avalon_translator:av_byteenable -> barco_avalon_0:avs_s0_byteEnable
	wire   [31:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_writedata;                             // epcs_flash_controller_0_epcs_control_port_translator:av_writedata -> epcs_flash_controller_0:writedata
	wire    [8:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_address;                               // epcs_flash_controller_0_epcs_control_port_translator:av_address -> epcs_flash_controller_0:address
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_chipselect;                            // epcs_flash_controller_0_epcs_control_port_translator:av_chipselect -> epcs_flash_controller_0:chipselect
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_write;                                 // epcs_flash_controller_0_epcs_control_port_translator:av_write -> epcs_flash_controller_0:write_n
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_read;                                  // epcs_flash_controller_0_epcs_control_port_translator:av_read -> epcs_flash_controller_0:read_n
	wire   [31:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_readdata;                              // epcs_flash_controller_0:readdata -> epcs_flash_controller_0_epcs_control_port_translator:av_readdata
	wire          nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_waitrequest;                               // nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:av_waitrequest -> nios2_qsys_0_instruction_master_translator:uav_waitrequest
	wire    [2:0] nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_burstcount;                                // nios2_qsys_0_instruction_master_translator:uav_burstcount -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:av_burstcount
	wire   [31:0] nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_writedata;                                 // nios2_qsys_0_instruction_master_translator:uav_writedata -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:av_writedata
	wire   [24:0] nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_address;                                   // nios2_qsys_0_instruction_master_translator:uav_address -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:av_address
	wire          nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_lock;                                      // nios2_qsys_0_instruction_master_translator:uav_lock -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:av_lock
	wire          nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_write;                                     // nios2_qsys_0_instruction_master_translator:uav_write -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:av_write
	wire          nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_read;                                      // nios2_qsys_0_instruction_master_translator:uav_read -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:av_read
	wire   [31:0] nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_readdata;                                  // nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:av_readdata -> nios2_qsys_0_instruction_master_translator:uav_readdata
	wire          nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_debugaccess;                               // nios2_qsys_0_instruction_master_translator:uav_debugaccess -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:av_debugaccess
	wire    [3:0] nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_byteenable;                                // nios2_qsys_0_instruction_master_translator:uav_byteenable -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:av_byteenable
	wire          nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_readdatavalid;                             // nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:av_readdatavalid -> nios2_qsys_0_instruction_master_translator:uav_readdatavalid
	wire          nios2_qsys_0_data_master_translator_avalon_universal_master_0_waitrequest;                                      // nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:av_waitrequest -> nios2_qsys_0_data_master_translator:uav_waitrequest
	wire    [2:0] nios2_qsys_0_data_master_translator_avalon_universal_master_0_burstcount;                                       // nios2_qsys_0_data_master_translator:uav_burstcount -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:av_burstcount
	wire   [31:0] nios2_qsys_0_data_master_translator_avalon_universal_master_0_writedata;                                        // nios2_qsys_0_data_master_translator:uav_writedata -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:av_writedata
	wire   [24:0] nios2_qsys_0_data_master_translator_avalon_universal_master_0_address;                                          // nios2_qsys_0_data_master_translator:uav_address -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:av_address
	wire          nios2_qsys_0_data_master_translator_avalon_universal_master_0_lock;                                             // nios2_qsys_0_data_master_translator:uav_lock -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:av_lock
	wire          nios2_qsys_0_data_master_translator_avalon_universal_master_0_write;                                            // nios2_qsys_0_data_master_translator:uav_write -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:av_write
	wire          nios2_qsys_0_data_master_translator_avalon_universal_master_0_read;                                             // nios2_qsys_0_data_master_translator:uav_read -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:av_read
	wire   [31:0] nios2_qsys_0_data_master_translator_avalon_universal_master_0_readdata;                                         // nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:av_readdata -> nios2_qsys_0_data_master_translator:uav_readdata
	wire          nios2_qsys_0_data_master_translator_avalon_universal_master_0_debugaccess;                                      // nios2_qsys_0_data_master_translator:uav_debugaccess -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:av_debugaccess
	wire    [3:0] nios2_qsys_0_data_master_translator_avalon_universal_master_0_byteenable;                                       // nios2_qsys_0_data_master_translator:uav_byteenable -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:av_byteenable
	wire          nios2_qsys_0_data_master_translator_avalon_universal_master_0_readdatavalid;                                    // nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:av_readdatavalid -> nios2_qsys_0_data_master_translator:uav_readdatavalid
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_waitrequest;                        // nios2_qsys_0_jtag_debug_module_translator:uav_waitrequest -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:m0_waitrequest
	wire    [2:0] nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_burstcount;                         // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:m0_burstcount -> nios2_qsys_0_jtag_debug_module_translator:uav_burstcount
	wire   [31:0] nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_writedata;                          // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:m0_writedata -> nios2_qsys_0_jtag_debug_module_translator:uav_writedata
	wire   [24:0] nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_address;                            // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:m0_address -> nios2_qsys_0_jtag_debug_module_translator:uav_address
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_write;                              // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:m0_write -> nios2_qsys_0_jtag_debug_module_translator:uav_write
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_lock;                               // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:m0_lock -> nios2_qsys_0_jtag_debug_module_translator:uav_lock
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_read;                               // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:m0_read -> nios2_qsys_0_jtag_debug_module_translator:uav_read
	wire   [31:0] nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_readdata;                           // nios2_qsys_0_jtag_debug_module_translator:uav_readdata -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:m0_readdata
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_readdatavalid;                      // nios2_qsys_0_jtag_debug_module_translator:uav_readdatavalid -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:m0_readdatavalid
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_debugaccess;                        // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:m0_debugaccess -> nios2_qsys_0_jtag_debug_module_translator:uav_debugaccess
	wire    [3:0] nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_byteenable;                         // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:m0_byteenable -> nios2_qsys_0_jtag_debug_module_translator:uav_byteenable
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_endofpacket;                 // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rf_source_endofpacket -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo:in_endofpacket
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_valid;                       // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rf_source_valid -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo:in_valid
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_startofpacket;               // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rf_source_startofpacket -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo:in_startofpacket
	wire   [82:0] nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_data;                        // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rf_source_data -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo:in_data
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_ready;                       // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo:in_ready -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rf_source_ready
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket;              // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo:out_endofpacket -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rf_sink_endofpacket
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid;                    // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo:out_valid -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rf_sink_valid
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket;            // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo:out_startofpacket -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rf_sink_startofpacket
	wire   [82:0] nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data;                     // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo:out_data -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rf_sink_data
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready;                    // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rf_sink_ready -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo:out_ready
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid;                  // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rdata_fifo_src_valid -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_valid
	wire   [31:0] nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data;                   // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rdata_fifo_src_data -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_data
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready;                  // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_ready -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rdata_fifo_src_ready
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_m0_waitrequest;                                             // uart_0_s1_translator:uav_waitrequest -> uart_0_s1_translator_avalon_universal_slave_0_agent:m0_waitrequest
	wire    [2:0] uart_0_s1_translator_avalon_universal_slave_0_agent_m0_burstcount;                                              // uart_0_s1_translator_avalon_universal_slave_0_agent:m0_burstcount -> uart_0_s1_translator:uav_burstcount
	wire   [31:0] uart_0_s1_translator_avalon_universal_slave_0_agent_m0_writedata;                                               // uart_0_s1_translator_avalon_universal_slave_0_agent:m0_writedata -> uart_0_s1_translator:uav_writedata
	wire   [24:0] uart_0_s1_translator_avalon_universal_slave_0_agent_m0_address;                                                 // uart_0_s1_translator_avalon_universal_slave_0_agent:m0_address -> uart_0_s1_translator:uav_address
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_m0_write;                                                   // uart_0_s1_translator_avalon_universal_slave_0_agent:m0_write -> uart_0_s1_translator:uav_write
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_m0_lock;                                                    // uart_0_s1_translator_avalon_universal_slave_0_agent:m0_lock -> uart_0_s1_translator:uav_lock
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_m0_read;                                                    // uart_0_s1_translator_avalon_universal_slave_0_agent:m0_read -> uart_0_s1_translator:uav_read
	wire   [31:0] uart_0_s1_translator_avalon_universal_slave_0_agent_m0_readdata;                                                // uart_0_s1_translator:uav_readdata -> uart_0_s1_translator_avalon_universal_slave_0_agent:m0_readdata
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_m0_readdatavalid;                                           // uart_0_s1_translator:uav_readdatavalid -> uart_0_s1_translator_avalon_universal_slave_0_agent:m0_readdatavalid
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_m0_debugaccess;                                             // uart_0_s1_translator_avalon_universal_slave_0_agent:m0_debugaccess -> uart_0_s1_translator:uav_debugaccess
	wire    [3:0] uart_0_s1_translator_avalon_universal_slave_0_agent_m0_byteenable;                                              // uart_0_s1_translator_avalon_universal_slave_0_agent:m0_byteenable -> uart_0_s1_translator:uav_byteenable
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_endofpacket;                                      // uart_0_s1_translator_avalon_universal_slave_0_agent:rf_source_endofpacket -> uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_endofpacket
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_valid;                                            // uart_0_s1_translator_avalon_universal_slave_0_agent:rf_source_valid -> uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_valid
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_startofpacket;                                    // uart_0_s1_translator_avalon_universal_slave_0_agent:rf_source_startofpacket -> uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_startofpacket
	wire   [82:0] uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_data;                                             // uart_0_s1_translator_avalon_universal_slave_0_agent:rf_source_data -> uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_data
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_ready;                                            // uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_ready -> uart_0_s1_translator_avalon_universal_slave_0_agent:rf_source_ready
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket;                                   // uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_endofpacket -> uart_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_endofpacket
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid;                                         // uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_valid -> uart_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_valid
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket;                                 // uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_startofpacket -> uart_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_startofpacket
	wire   [82:0] uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data;                                          // uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_data -> uart_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_data
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready;                                         // uart_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_ready -> uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_ready
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid;                                       // uart_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_src_valid -> uart_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_valid
	wire   [31:0] uart_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data;                                        // uart_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_src_data -> uart_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_data
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready;                                       // uart_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_ready -> uart_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_src_ready
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_waitrequest;                                   // onchip_memory2_0_s1_translator:uav_waitrequest -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:m0_waitrequest
	wire    [2:0] onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_burstcount;                                    // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:m0_burstcount -> onchip_memory2_0_s1_translator:uav_burstcount
	wire   [31:0] onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_writedata;                                     // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:m0_writedata -> onchip_memory2_0_s1_translator:uav_writedata
	wire   [24:0] onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_address;                                       // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:m0_address -> onchip_memory2_0_s1_translator:uav_address
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_write;                                         // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:m0_write -> onchip_memory2_0_s1_translator:uav_write
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_lock;                                          // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:m0_lock -> onchip_memory2_0_s1_translator:uav_lock
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_read;                                          // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:m0_read -> onchip_memory2_0_s1_translator:uav_read
	wire   [31:0] onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_readdata;                                      // onchip_memory2_0_s1_translator:uav_readdata -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:m0_readdata
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_readdatavalid;                                 // onchip_memory2_0_s1_translator:uav_readdatavalid -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:m0_readdatavalid
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_debugaccess;                                   // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:m0_debugaccess -> onchip_memory2_0_s1_translator:uav_debugaccess
	wire    [3:0] onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_byteenable;                                    // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:m0_byteenable -> onchip_memory2_0_s1_translator:uav_byteenable
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_endofpacket;                            // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rf_source_endofpacket -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_endofpacket
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_valid;                                  // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rf_source_valid -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_valid
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_startofpacket;                          // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rf_source_startofpacket -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_startofpacket
	wire   [82:0] onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_data;                                   // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rf_source_data -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_data
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_ready;                                  // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_ready -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rf_source_ready
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket;                         // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_endofpacket -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_endofpacket
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid;                               // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_valid -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_valid
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket;                       // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_startofpacket -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_startofpacket
	wire   [82:0] onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data;                                // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_data -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_data
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready;                               // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_ready -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_ready
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid;                             // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_src_valid -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_valid
	wire   [31:0] onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data;                              // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_src_data -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_data
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready;                             // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_ready -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_src_ready
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_m0_waitrequest;                                              // pio_0_s1_translator:uav_waitrequest -> pio_0_s1_translator_avalon_universal_slave_0_agent:m0_waitrequest
	wire    [2:0] pio_0_s1_translator_avalon_universal_slave_0_agent_m0_burstcount;                                               // pio_0_s1_translator_avalon_universal_slave_0_agent:m0_burstcount -> pio_0_s1_translator:uav_burstcount
	wire   [31:0] pio_0_s1_translator_avalon_universal_slave_0_agent_m0_writedata;                                                // pio_0_s1_translator_avalon_universal_slave_0_agent:m0_writedata -> pio_0_s1_translator:uav_writedata
	wire   [24:0] pio_0_s1_translator_avalon_universal_slave_0_agent_m0_address;                                                  // pio_0_s1_translator_avalon_universal_slave_0_agent:m0_address -> pio_0_s1_translator:uav_address
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_m0_write;                                                    // pio_0_s1_translator_avalon_universal_slave_0_agent:m0_write -> pio_0_s1_translator:uav_write
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_m0_lock;                                                     // pio_0_s1_translator_avalon_universal_slave_0_agent:m0_lock -> pio_0_s1_translator:uav_lock
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_m0_read;                                                     // pio_0_s1_translator_avalon_universal_slave_0_agent:m0_read -> pio_0_s1_translator:uav_read
	wire   [31:0] pio_0_s1_translator_avalon_universal_slave_0_agent_m0_readdata;                                                 // pio_0_s1_translator:uav_readdata -> pio_0_s1_translator_avalon_universal_slave_0_agent:m0_readdata
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_m0_readdatavalid;                                            // pio_0_s1_translator:uav_readdatavalid -> pio_0_s1_translator_avalon_universal_slave_0_agent:m0_readdatavalid
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_m0_debugaccess;                                              // pio_0_s1_translator_avalon_universal_slave_0_agent:m0_debugaccess -> pio_0_s1_translator:uav_debugaccess
	wire    [3:0] pio_0_s1_translator_avalon_universal_slave_0_agent_m0_byteenable;                                               // pio_0_s1_translator_avalon_universal_slave_0_agent:m0_byteenable -> pio_0_s1_translator:uav_byteenable
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_endofpacket;                                       // pio_0_s1_translator_avalon_universal_slave_0_agent:rf_source_endofpacket -> pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_endofpacket
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_valid;                                             // pio_0_s1_translator_avalon_universal_slave_0_agent:rf_source_valid -> pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_valid
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_startofpacket;                                     // pio_0_s1_translator_avalon_universal_slave_0_agent:rf_source_startofpacket -> pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_startofpacket
	wire   [82:0] pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_data;                                              // pio_0_s1_translator_avalon_universal_slave_0_agent:rf_source_data -> pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_data
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_ready;                                             // pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:in_ready -> pio_0_s1_translator_avalon_universal_slave_0_agent:rf_source_ready
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket;                                    // pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_endofpacket -> pio_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_endofpacket
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid;                                          // pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_valid -> pio_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_valid
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket;                                  // pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_startofpacket -> pio_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_startofpacket
	wire   [82:0] pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data;                                           // pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_data -> pio_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_data
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready;                                          // pio_0_s1_translator_avalon_universal_slave_0_agent:rf_sink_ready -> pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:out_ready
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid;                                        // pio_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_src_valid -> pio_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_valid
	wire   [31:0] pio_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data;                                         // pio_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_src_data -> pio_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_data
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready;                                        // pio_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_ready -> pio_0_s1_translator_avalon_universal_slave_0_agent:rdata_fifo_src_ready
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_waitrequest;                           // barco_avalon_0_barco_avalon_translator:uav_waitrequest -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:m0_waitrequest
	wire    [3:0] barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_burstcount;                            // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:m0_burstcount -> barco_avalon_0_barco_avalon_translator:uav_burstcount
	wire   [63:0] barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_writedata;                             // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:m0_writedata -> barco_avalon_0_barco_avalon_translator:uav_writedata
	wire   [24:0] barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_address;                               // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:m0_address -> barco_avalon_0_barco_avalon_translator:uav_address
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_write;                                 // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:m0_write -> barco_avalon_0_barco_avalon_translator:uav_write
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_lock;                                  // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:m0_lock -> barco_avalon_0_barco_avalon_translator:uav_lock
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_read;                                  // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:m0_read -> barco_avalon_0_barco_avalon_translator:uav_read
	wire   [63:0] barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_readdata;                              // barco_avalon_0_barco_avalon_translator:uav_readdata -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:m0_readdata
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_readdatavalid;                         // barco_avalon_0_barco_avalon_translator:uav_readdatavalid -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:m0_readdatavalid
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_debugaccess;                           // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:m0_debugaccess -> barco_avalon_0_barco_avalon_translator:uav_debugaccess
	wire    [7:0] barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_byteenable;                            // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:m0_byteenable -> barco_avalon_0_barco_avalon_translator:uav_byteenable
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_endofpacket;                    // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rf_source_endofpacket -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo:in_endofpacket
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_valid;                          // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rf_source_valid -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo:in_valid
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_startofpacket;                  // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rf_source_startofpacket -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo:in_startofpacket
	wire  [118:0] barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_data;                           // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rf_source_data -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo:in_data
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_ready;                          // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo:in_ready -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rf_source_ready
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket;                 // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo:out_endofpacket -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rf_sink_endofpacket
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid;                       // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo:out_valid -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rf_sink_valid
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket;               // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo:out_startofpacket -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rf_sink_startofpacket
	wire  [118:0] barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data;                        // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo:out_data -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rf_sink_data
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready;                       // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rf_sink_ready -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo:out_ready
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid;                     // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rdata_fifo_src_valid -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_valid
	wire   [63:0] barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data;                      // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rdata_fifo_src_data -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_data
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready;                     // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_ready -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rdata_fifo_src_ready
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_waitrequest;             // epcs_flash_controller_0_epcs_control_port_translator:uav_waitrequest -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:m0_waitrequest
	wire    [2:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_burstcount;              // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:m0_burstcount -> epcs_flash_controller_0_epcs_control_port_translator:uav_burstcount
	wire   [31:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_writedata;               // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:m0_writedata -> epcs_flash_controller_0_epcs_control_port_translator:uav_writedata
	wire   [24:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_address;                 // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:m0_address -> epcs_flash_controller_0_epcs_control_port_translator:uav_address
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_write;                   // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:m0_write -> epcs_flash_controller_0_epcs_control_port_translator:uav_write
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_lock;                    // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:m0_lock -> epcs_flash_controller_0_epcs_control_port_translator:uav_lock
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_read;                    // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:m0_read -> epcs_flash_controller_0_epcs_control_port_translator:uav_read
	wire   [31:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_readdata;                // epcs_flash_controller_0_epcs_control_port_translator:uav_readdata -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:m0_readdata
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_readdatavalid;           // epcs_flash_controller_0_epcs_control_port_translator:uav_readdatavalid -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:m0_readdatavalid
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_debugaccess;             // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:m0_debugaccess -> epcs_flash_controller_0_epcs_control_port_translator:uav_debugaccess
	wire    [3:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_byteenable;              // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:m0_byteenable -> epcs_flash_controller_0_epcs_control_port_translator:uav_byteenable
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_endofpacket;      // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rf_source_endofpacket -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo:in_endofpacket
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_valid;            // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rf_source_valid -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo:in_valid
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_startofpacket;    // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rf_source_startofpacket -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo:in_startofpacket
	wire   [82:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_data;             // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rf_source_data -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo:in_data
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_ready;            // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo:in_ready -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rf_source_ready
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket;   // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo:out_endofpacket -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rf_sink_endofpacket
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid;         // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo:out_valid -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rf_sink_valid
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket; // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo:out_startofpacket -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rf_sink_startofpacket
	wire   [82:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data;          // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo:out_data -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rf_sink_data
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready;         // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rf_sink_ready -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo:out_ready
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid;       // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rdata_fifo_src_valid -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_valid
	wire   [31:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data;        // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rdata_fifo_src_data -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_data
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready;       // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rdata_fifo_sink_ready -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rdata_fifo_src_ready
	wire          nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_endofpacket;                      // nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:cp_endofpacket -> addr_router:sink_endofpacket
	wire          nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_valid;                            // nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:cp_valid -> addr_router:sink_valid
	wire          nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_startofpacket;                    // nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:cp_startofpacket -> addr_router:sink_startofpacket
	wire   [81:0] nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_data;                             // nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:cp_data -> addr_router:sink_data
	wire          nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_ready;                            // addr_router:sink_ready -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:cp_ready
	wire          nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_endofpacket;                             // nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:cp_endofpacket -> addr_router_001:sink_endofpacket
	wire          nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_valid;                                   // nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:cp_valid -> addr_router_001:sink_valid
	wire          nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_startofpacket;                           // nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:cp_startofpacket -> addr_router_001:sink_startofpacket
	wire   [81:0] nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_data;                                    // nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:cp_data -> addr_router_001:sink_data
	wire          nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_ready;                                   // addr_router_001:sink_ready -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:cp_ready
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_endofpacket;                        // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rp_endofpacket -> id_router:sink_endofpacket
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_valid;                              // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rp_valid -> id_router:sink_valid
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_startofpacket;                      // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rp_startofpacket -> id_router:sink_startofpacket
	wire   [81:0] nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_data;                               // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rp_data -> id_router:sink_data
	wire          nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_ready;                              // id_router:sink_ready -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:rp_ready
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rp_endofpacket;                                             // uart_0_s1_translator_avalon_universal_slave_0_agent:rp_endofpacket -> id_router_001:sink_endofpacket
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rp_valid;                                                   // uart_0_s1_translator_avalon_universal_slave_0_agent:rp_valid -> id_router_001:sink_valid
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rp_startofpacket;                                           // uart_0_s1_translator_avalon_universal_slave_0_agent:rp_startofpacket -> id_router_001:sink_startofpacket
	wire   [81:0] uart_0_s1_translator_avalon_universal_slave_0_agent_rp_data;                                                    // uart_0_s1_translator_avalon_universal_slave_0_agent:rp_data -> id_router_001:sink_data
	wire          uart_0_s1_translator_avalon_universal_slave_0_agent_rp_ready;                                                   // id_router_001:sink_ready -> uart_0_s1_translator_avalon_universal_slave_0_agent:rp_ready
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_endofpacket;                                   // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rp_endofpacket -> id_router_002:sink_endofpacket
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_valid;                                         // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rp_valid -> id_router_002:sink_valid
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_startofpacket;                                 // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rp_startofpacket -> id_router_002:sink_startofpacket
	wire   [81:0] onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_data;                                          // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rp_data -> id_router_002:sink_data
	wire          onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_ready;                                         // id_router_002:sink_ready -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:rp_ready
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rp_endofpacket;                                              // pio_0_s1_translator_avalon_universal_slave_0_agent:rp_endofpacket -> id_router_003:sink_endofpacket
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rp_valid;                                                    // pio_0_s1_translator_avalon_universal_slave_0_agent:rp_valid -> id_router_003:sink_valid
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rp_startofpacket;                                            // pio_0_s1_translator_avalon_universal_slave_0_agent:rp_startofpacket -> id_router_003:sink_startofpacket
	wire   [81:0] pio_0_s1_translator_avalon_universal_slave_0_agent_rp_data;                                                     // pio_0_s1_translator_avalon_universal_slave_0_agent:rp_data -> id_router_003:sink_data
	wire          pio_0_s1_translator_avalon_universal_slave_0_agent_rp_ready;                                                    // id_router_003:sink_ready -> pio_0_s1_translator_avalon_universal_slave_0_agent:rp_ready
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_endofpacket;                           // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rp_endofpacket -> id_router_004:sink_endofpacket
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_valid;                                 // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rp_valid -> id_router_004:sink_valid
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_startofpacket;                         // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rp_startofpacket -> id_router_004:sink_startofpacket
	wire  [117:0] barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_data;                                  // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rp_data -> id_router_004:sink_data
	wire          barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_ready;                                 // id_router_004:sink_ready -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:rp_ready
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_endofpacket;             // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rp_endofpacket -> id_router_005:sink_endofpacket
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_valid;                   // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rp_valid -> id_router_005:sink_valid
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_startofpacket;           // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rp_startofpacket -> id_router_005:sink_startofpacket
	wire   [81:0] epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_data;                    // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rp_data -> id_router_005:sink_data
	wire          epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_ready;                   // id_router_005:sink_ready -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:rp_ready
	wire          addr_router_src_endofpacket;                                                                                    // addr_router:src_endofpacket -> limiter:cmd_sink_endofpacket
	wire          addr_router_src_valid;                                                                                          // addr_router:src_valid -> limiter:cmd_sink_valid
	wire          addr_router_src_startofpacket;                                                                                  // addr_router:src_startofpacket -> limiter:cmd_sink_startofpacket
	wire   [81:0] addr_router_src_data;                                                                                           // addr_router:src_data -> limiter:cmd_sink_data
	wire    [5:0] addr_router_src_channel;                                                                                        // addr_router:src_channel -> limiter:cmd_sink_channel
	wire          addr_router_src_ready;                                                                                          // limiter:cmd_sink_ready -> addr_router:src_ready
	wire          limiter_rsp_src_endofpacket;                                                                                    // limiter:rsp_src_endofpacket -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:rp_endofpacket
	wire          limiter_rsp_src_valid;                                                                                          // limiter:rsp_src_valid -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:rp_valid
	wire          limiter_rsp_src_startofpacket;                                                                                  // limiter:rsp_src_startofpacket -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:rp_startofpacket
	wire   [81:0] limiter_rsp_src_data;                                                                                           // limiter:rsp_src_data -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:rp_data
	wire    [5:0] limiter_rsp_src_channel;                                                                                        // limiter:rsp_src_channel -> nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:rp_channel
	wire          limiter_rsp_src_ready;                                                                                          // nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:rp_ready -> limiter:rsp_src_ready
	wire          addr_router_001_src_endofpacket;                                                                                // addr_router_001:src_endofpacket -> limiter_001:cmd_sink_endofpacket
	wire          addr_router_001_src_valid;                                                                                      // addr_router_001:src_valid -> limiter_001:cmd_sink_valid
	wire          addr_router_001_src_startofpacket;                                                                              // addr_router_001:src_startofpacket -> limiter_001:cmd_sink_startofpacket
	wire   [81:0] addr_router_001_src_data;                                                                                       // addr_router_001:src_data -> limiter_001:cmd_sink_data
	wire    [5:0] addr_router_001_src_channel;                                                                                    // addr_router_001:src_channel -> limiter_001:cmd_sink_channel
	wire          addr_router_001_src_ready;                                                                                      // limiter_001:cmd_sink_ready -> addr_router_001:src_ready
	wire          limiter_001_rsp_src_endofpacket;                                                                                // limiter_001:rsp_src_endofpacket -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:rp_endofpacket
	wire          limiter_001_rsp_src_valid;                                                                                      // limiter_001:rsp_src_valid -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:rp_valid
	wire          limiter_001_rsp_src_startofpacket;                                                                              // limiter_001:rsp_src_startofpacket -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:rp_startofpacket
	wire   [81:0] limiter_001_rsp_src_data;                                                                                       // limiter_001:rsp_src_data -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:rp_data
	wire    [5:0] limiter_001_rsp_src_channel;                                                                                    // limiter_001:rsp_src_channel -> nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:rp_channel
	wire          limiter_001_rsp_src_ready;                                                                                      // nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:rp_ready -> limiter_001:rsp_src_ready
	wire          rst_controller_reset_out_reset;                                                                                 // rst_controller:reset_out -> [addr_router:reset, addr_router_001:reset, barco_avalon_0:reset, barco_avalon_0_barco_avalon_translator:reset, barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:reset, barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo:reset, cmd_xbar_demux:reset, cmd_xbar_demux_001:reset, cmd_xbar_mux:reset, cmd_xbar_mux_001:reset, cmd_xbar_mux_004:reset, cmd_xbar_mux_005:reset, epcs_flash_controller_0:reset_n, epcs_flash_controller_0_epcs_control_port_translator:reset, epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:reset, epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo:reset, id_router:reset, id_router_001:reset, id_router_004:reset, id_router_005:reset, irq_mapper:reset, limiter:reset, limiter_001:reset, nios2_qsys_0:reset_n, nios2_qsys_0_data_master_translator:reset, nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent:reset, nios2_qsys_0_instruction_master_translator:reset, nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent:reset, nios2_qsys_0_jtag_debug_module_translator:reset, nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:reset, nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo:reset, rsp_xbar_demux:reset, rsp_xbar_demux_001:reset, rsp_xbar_demux_004:reset, rsp_xbar_demux_005:reset, rsp_xbar_mux:reset, rsp_xbar_mux_001:reset, uart_0:reset_n, uart_0_s1_translator:reset, uart_0_s1_translator_avalon_universal_slave_0_agent:reset, uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:reset, width_adapter:reset, width_adapter_001:reset]
	wire          nios2_qsys_0_jtag_debug_module_reset_reset;                                                                     // nios2_qsys_0:jtag_debug_module_resetrequest -> rst_controller:reset_in0
	wire          rst_controller_001_reset_out_reset;                                                                             // rst_controller_001:reset_out -> [cmd_xbar_mux_002:reset, cmd_xbar_mux_003:reset, id_router_002:reset, id_router_003:reset, onchip_memory2_0:reset, onchip_memory2_0_s1_translator:reset, onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:reset, onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:reset, pio_0:reset_n, pio_0_s1_translator:reset, pio_0_s1_translator_avalon_universal_slave_0_agent:reset, pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo:reset, rsp_xbar_demux_002:reset, rsp_xbar_demux_003:reset]
	wire          cmd_xbar_demux_src0_endofpacket;                                                                                // cmd_xbar_demux:src0_endofpacket -> cmd_xbar_mux:sink0_endofpacket
	wire          cmd_xbar_demux_src0_valid;                                                                                      // cmd_xbar_demux:src0_valid -> cmd_xbar_mux:sink0_valid
	wire          cmd_xbar_demux_src0_startofpacket;                                                                              // cmd_xbar_demux:src0_startofpacket -> cmd_xbar_mux:sink0_startofpacket
	wire   [81:0] cmd_xbar_demux_src0_data;                                                                                       // cmd_xbar_demux:src0_data -> cmd_xbar_mux:sink0_data
	wire    [5:0] cmd_xbar_demux_src0_channel;                                                                                    // cmd_xbar_demux:src0_channel -> cmd_xbar_mux:sink0_channel
	wire          cmd_xbar_demux_src0_ready;                                                                                      // cmd_xbar_mux:sink0_ready -> cmd_xbar_demux:src0_ready
	wire          cmd_xbar_demux_src1_endofpacket;                                                                                // cmd_xbar_demux:src1_endofpacket -> cmd_xbar_mux_001:sink0_endofpacket
	wire          cmd_xbar_demux_src1_valid;                                                                                      // cmd_xbar_demux:src1_valid -> cmd_xbar_mux_001:sink0_valid
	wire          cmd_xbar_demux_src1_startofpacket;                                                                              // cmd_xbar_demux:src1_startofpacket -> cmd_xbar_mux_001:sink0_startofpacket
	wire   [81:0] cmd_xbar_demux_src1_data;                                                                                       // cmd_xbar_demux:src1_data -> cmd_xbar_mux_001:sink0_data
	wire    [5:0] cmd_xbar_demux_src1_channel;                                                                                    // cmd_xbar_demux:src1_channel -> cmd_xbar_mux_001:sink0_channel
	wire          cmd_xbar_demux_src1_ready;                                                                                      // cmd_xbar_mux_001:sink0_ready -> cmd_xbar_demux:src1_ready
	wire          cmd_xbar_demux_src2_endofpacket;                                                                                // cmd_xbar_demux:src2_endofpacket -> cmd_xbar_mux_002:sink0_endofpacket
	wire          cmd_xbar_demux_src2_valid;                                                                                      // cmd_xbar_demux:src2_valid -> cmd_xbar_mux_002:sink0_valid
	wire          cmd_xbar_demux_src2_startofpacket;                                                                              // cmd_xbar_demux:src2_startofpacket -> cmd_xbar_mux_002:sink0_startofpacket
	wire   [81:0] cmd_xbar_demux_src2_data;                                                                                       // cmd_xbar_demux:src2_data -> cmd_xbar_mux_002:sink0_data
	wire    [5:0] cmd_xbar_demux_src2_channel;                                                                                    // cmd_xbar_demux:src2_channel -> cmd_xbar_mux_002:sink0_channel
	wire          cmd_xbar_demux_src2_ready;                                                                                      // cmd_xbar_mux_002:sink0_ready -> cmd_xbar_demux:src2_ready
	wire          cmd_xbar_demux_src3_endofpacket;                                                                                // cmd_xbar_demux:src3_endofpacket -> cmd_xbar_mux_003:sink0_endofpacket
	wire          cmd_xbar_demux_src3_valid;                                                                                      // cmd_xbar_demux:src3_valid -> cmd_xbar_mux_003:sink0_valid
	wire          cmd_xbar_demux_src3_startofpacket;                                                                              // cmd_xbar_demux:src3_startofpacket -> cmd_xbar_mux_003:sink0_startofpacket
	wire   [81:0] cmd_xbar_demux_src3_data;                                                                                       // cmd_xbar_demux:src3_data -> cmd_xbar_mux_003:sink0_data
	wire    [5:0] cmd_xbar_demux_src3_channel;                                                                                    // cmd_xbar_demux:src3_channel -> cmd_xbar_mux_003:sink0_channel
	wire          cmd_xbar_demux_src3_ready;                                                                                      // cmd_xbar_mux_003:sink0_ready -> cmd_xbar_demux:src3_ready
	wire          cmd_xbar_demux_src4_endofpacket;                                                                                // cmd_xbar_demux:src4_endofpacket -> cmd_xbar_mux_004:sink0_endofpacket
	wire          cmd_xbar_demux_src4_valid;                                                                                      // cmd_xbar_demux:src4_valid -> cmd_xbar_mux_004:sink0_valid
	wire          cmd_xbar_demux_src4_startofpacket;                                                                              // cmd_xbar_demux:src4_startofpacket -> cmd_xbar_mux_004:sink0_startofpacket
	wire   [81:0] cmd_xbar_demux_src4_data;                                                                                       // cmd_xbar_demux:src4_data -> cmd_xbar_mux_004:sink0_data
	wire    [5:0] cmd_xbar_demux_src4_channel;                                                                                    // cmd_xbar_demux:src4_channel -> cmd_xbar_mux_004:sink0_channel
	wire          cmd_xbar_demux_src4_ready;                                                                                      // cmd_xbar_mux_004:sink0_ready -> cmd_xbar_demux:src4_ready
	wire          cmd_xbar_demux_src5_endofpacket;                                                                                // cmd_xbar_demux:src5_endofpacket -> cmd_xbar_mux_005:sink0_endofpacket
	wire          cmd_xbar_demux_src5_valid;                                                                                      // cmd_xbar_demux:src5_valid -> cmd_xbar_mux_005:sink0_valid
	wire          cmd_xbar_demux_src5_startofpacket;                                                                              // cmd_xbar_demux:src5_startofpacket -> cmd_xbar_mux_005:sink0_startofpacket
	wire   [81:0] cmd_xbar_demux_src5_data;                                                                                       // cmd_xbar_demux:src5_data -> cmd_xbar_mux_005:sink0_data
	wire    [5:0] cmd_xbar_demux_src5_channel;                                                                                    // cmd_xbar_demux:src5_channel -> cmd_xbar_mux_005:sink0_channel
	wire          cmd_xbar_demux_src5_ready;                                                                                      // cmd_xbar_mux_005:sink0_ready -> cmd_xbar_demux:src5_ready
	wire          cmd_xbar_demux_001_src0_endofpacket;                                                                            // cmd_xbar_demux_001:src0_endofpacket -> cmd_xbar_mux:sink1_endofpacket
	wire          cmd_xbar_demux_001_src0_valid;                                                                                  // cmd_xbar_demux_001:src0_valid -> cmd_xbar_mux:sink1_valid
	wire          cmd_xbar_demux_001_src0_startofpacket;                                                                          // cmd_xbar_demux_001:src0_startofpacket -> cmd_xbar_mux:sink1_startofpacket
	wire   [81:0] cmd_xbar_demux_001_src0_data;                                                                                   // cmd_xbar_demux_001:src0_data -> cmd_xbar_mux:sink1_data
	wire    [5:0] cmd_xbar_demux_001_src0_channel;                                                                                // cmd_xbar_demux_001:src0_channel -> cmd_xbar_mux:sink1_channel
	wire          cmd_xbar_demux_001_src0_ready;                                                                                  // cmd_xbar_mux:sink1_ready -> cmd_xbar_demux_001:src0_ready
	wire          cmd_xbar_demux_001_src1_endofpacket;                                                                            // cmd_xbar_demux_001:src1_endofpacket -> cmd_xbar_mux_001:sink1_endofpacket
	wire          cmd_xbar_demux_001_src1_valid;                                                                                  // cmd_xbar_demux_001:src1_valid -> cmd_xbar_mux_001:sink1_valid
	wire          cmd_xbar_demux_001_src1_startofpacket;                                                                          // cmd_xbar_demux_001:src1_startofpacket -> cmd_xbar_mux_001:sink1_startofpacket
	wire   [81:0] cmd_xbar_demux_001_src1_data;                                                                                   // cmd_xbar_demux_001:src1_data -> cmd_xbar_mux_001:sink1_data
	wire    [5:0] cmd_xbar_demux_001_src1_channel;                                                                                // cmd_xbar_demux_001:src1_channel -> cmd_xbar_mux_001:sink1_channel
	wire          cmd_xbar_demux_001_src1_ready;                                                                                  // cmd_xbar_mux_001:sink1_ready -> cmd_xbar_demux_001:src1_ready
	wire          cmd_xbar_demux_001_src2_endofpacket;                                                                            // cmd_xbar_demux_001:src2_endofpacket -> cmd_xbar_mux_002:sink1_endofpacket
	wire          cmd_xbar_demux_001_src2_valid;                                                                                  // cmd_xbar_demux_001:src2_valid -> cmd_xbar_mux_002:sink1_valid
	wire          cmd_xbar_demux_001_src2_startofpacket;                                                                          // cmd_xbar_demux_001:src2_startofpacket -> cmd_xbar_mux_002:sink1_startofpacket
	wire   [81:0] cmd_xbar_demux_001_src2_data;                                                                                   // cmd_xbar_demux_001:src2_data -> cmd_xbar_mux_002:sink1_data
	wire    [5:0] cmd_xbar_demux_001_src2_channel;                                                                                // cmd_xbar_demux_001:src2_channel -> cmd_xbar_mux_002:sink1_channel
	wire          cmd_xbar_demux_001_src2_ready;                                                                                  // cmd_xbar_mux_002:sink1_ready -> cmd_xbar_demux_001:src2_ready
	wire          cmd_xbar_demux_001_src3_endofpacket;                                                                            // cmd_xbar_demux_001:src3_endofpacket -> cmd_xbar_mux_003:sink1_endofpacket
	wire          cmd_xbar_demux_001_src3_valid;                                                                                  // cmd_xbar_demux_001:src3_valid -> cmd_xbar_mux_003:sink1_valid
	wire          cmd_xbar_demux_001_src3_startofpacket;                                                                          // cmd_xbar_demux_001:src3_startofpacket -> cmd_xbar_mux_003:sink1_startofpacket
	wire   [81:0] cmd_xbar_demux_001_src3_data;                                                                                   // cmd_xbar_demux_001:src3_data -> cmd_xbar_mux_003:sink1_data
	wire    [5:0] cmd_xbar_demux_001_src3_channel;                                                                                // cmd_xbar_demux_001:src3_channel -> cmd_xbar_mux_003:sink1_channel
	wire          cmd_xbar_demux_001_src3_ready;                                                                                  // cmd_xbar_mux_003:sink1_ready -> cmd_xbar_demux_001:src3_ready
	wire          cmd_xbar_demux_001_src4_endofpacket;                                                                            // cmd_xbar_demux_001:src4_endofpacket -> cmd_xbar_mux_004:sink1_endofpacket
	wire          cmd_xbar_demux_001_src4_valid;                                                                                  // cmd_xbar_demux_001:src4_valid -> cmd_xbar_mux_004:sink1_valid
	wire          cmd_xbar_demux_001_src4_startofpacket;                                                                          // cmd_xbar_demux_001:src4_startofpacket -> cmd_xbar_mux_004:sink1_startofpacket
	wire   [81:0] cmd_xbar_demux_001_src4_data;                                                                                   // cmd_xbar_demux_001:src4_data -> cmd_xbar_mux_004:sink1_data
	wire    [5:0] cmd_xbar_demux_001_src4_channel;                                                                                // cmd_xbar_demux_001:src4_channel -> cmd_xbar_mux_004:sink1_channel
	wire          cmd_xbar_demux_001_src4_ready;                                                                                  // cmd_xbar_mux_004:sink1_ready -> cmd_xbar_demux_001:src4_ready
	wire          cmd_xbar_demux_001_src5_endofpacket;                                                                            // cmd_xbar_demux_001:src5_endofpacket -> cmd_xbar_mux_005:sink1_endofpacket
	wire          cmd_xbar_demux_001_src5_valid;                                                                                  // cmd_xbar_demux_001:src5_valid -> cmd_xbar_mux_005:sink1_valid
	wire          cmd_xbar_demux_001_src5_startofpacket;                                                                          // cmd_xbar_demux_001:src5_startofpacket -> cmd_xbar_mux_005:sink1_startofpacket
	wire   [81:0] cmd_xbar_demux_001_src5_data;                                                                                   // cmd_xbar_demux_001:src5_data -> cmd_xbar_mux_005:sink1_data
	wire    [5:0] cmd_xbar_demux_001_src5_channel;                                                                                // cmd_xbar_demux_001:src5_channel -> cmd_xbar_mux_005:sink1_channel
	wire          cmd_xbar_demux_001_src5_ready;                                                                                  // cmd_xbar_mux_005:sink1_ready -> cmd_xbar_demux_001:src5_ready
	wire          rsp_xbar_demux_src0_endofpacket;                                                                                // rsp_xbar_demux:src0_endofpacket -> rsp_xbar_mux:sink0_endofpacket
	wire          rsp_xbar_demux_src0_valid;                                                                                      // rsp_xbar_demux:src0_valid -> rsp_xbar_mux:sink0_valid
	wire          rsp_xbar_demux_src0_startofpacket;                                                                              // rsp_xbar_demux:src0_startofpacket -> rsp_xbar_mux:sink0_startofpacket
	wire   [81:0] rsp_xbar_demux_src0_data;                                                                                       // rsp_xbar_demux:src0_data -> rsp_xbar_mux:sink0_data
	wire    [5:0] rsp_xbar_demux_src0_channel;                                                                                    // rsp_xbar_demux:src0_channel -> rsp_xbar_mux:sink0_channel
	wire          rsp_xbar_demux_src0_ready;                                                                                      // rsp_xbar_mux:sink0_ready -> rsp_xbar_demux:src0_ready
	wire          rsp_xbar_demux_src1_endofpacket;                                                                                // rsp_xbar_demux:src1_endofpacket -> rsp_xbar_mux_001:sink0_endofpacket
	wire          rsp_xbar_demux_src1_valid;                                                                                      // rsp_xbar_demux:src1_valid -> rsp_xbar_mux_001:sink0_valid
	wire          rsp_xbar_demux_src1_startofpacket;                                                                              // rsp_xbar_demux:src1_startofpacket -> rsp_xbar_mux_001:sink0_startofpacket
	wire   [81:0] rsp_xbar_demux_src1_data;                                                                                       // rsp_xbar_demux:src1_data -> rsp_xbar_mux_001:sink0_data
	wire    [5:0] rsp_xbar_demux_src1_channel;                                                                                    // rsp_xbar_demux:src1_channel -> rsp_xbar_mux_001:sink0_channel
	wire          rsp_xbar_demux_src1_ready;                                                                                      // rsp_xbar_mux_001:sink0_ready -> rsp_xbar_demux:src1_ready
	wire          rsp_xbar_demux_001_src0_endofpacket;                                                                            // rsp_xbar_demux_001:src0_endofpacket -> rsp_xbar_mux:sink1_endofpacket
	wire          rsp_xbar_demux_001_src0_valid;                                                                                  // rsp_xbar_demux_001:src0_valid -> rsp_xbar_mux:sink1_valid
	wire          rsp_xbar_demux_001_src0_startofpacket;                                                                          // rsp_xbar_demux_001:src0_startofpacket -> rsp_xbar_mux:sink1_startofpacket
	wire   [81:0] rsp_xbar_demux_001_src0_data;                                                                                   // rsp_xbar_demux_001:src0_data -> rsp_xbar_mux:sink1_data
	wire    [5:0] rsp_xbar_demux_001_src0_channel;                                                                                // rsp_xbar_demux_001:src0_channel -> rsp_xbar_mux:sink1_channel
	wire          rsp_xbar_demux_001_src0_ready;                                                                                  // rsp_xbar_mux:sink1_ready -> rsp_xbar_demux_001:src0_ready
	wire          rsp_xbar_demux_001_src1_endofpacket;                                                                            // rsp_xbar_demux_001:src1_endofpacket -> rsp_xbar_mux_001:sink1_endofpacket
	wire          rsp_xbar_demux_001_src1_valid;                                                                                  // rsp_xbar_demux_001:src1_valid -> rsp_xbar_mux_001:sink1_valid
	wire          rsp_xbar_demux_001_src1_startofpacket;                                                                          // rsp_xbar_demux_001:src1_startofpacket -> rsp_xbar_mux_001:sink1_startofpacket
	wire   [81:0] rsp_xbar_demux_001_src1_data;                                                                                   // rsp_xbar_demux_001:src1_data -> rsp_xbar_mux_001:sink1_data
	wire    [5:0] rsp_xbar_demux_001_src1_channel;                                                                                // rsp_xbar_demux_001:src1_channel -> rsp_xbar_mux_001:sink1_channel
	wire          rsp_xbar_demux_001_src1_ready;                                                                                  // rsp_xbar_mux_001:sink1_ready -> rsp_xbar_demux_001:src1_ready
	wire          rsp_xbar_demux_002_src0_endofpacket;                                                                            // rsp_xbar_demux_002:src0_endofpacket -> rsp_xbar_mux:sink2_endofpacket
	wire          rsp_xbar_demux_002_src0_valid;                                                                                  // rsp_xbar_demux_002:src0_valid -> rsp_xbar_mux:sink2_valid
	wire          rsp_xbar_demux_002_src0_startofpacket;                                                                          // rsp_xbar_demux_002:src0_startofpacket -> rsp_xbar_mux:sink2_startofpacket
	wire   [81:0] rsp_xbar_demux_002_src0_data;                                                                                   // rsp_xbar_demux_002:src0_data -> rsp_xbar_mux:sink2_data
	wire    [5:0] rsp_xbar_demux_002_src0_channel;                                                                                // rsp_xbar_demux_002:src0_channel -> rsp_xbar_mux:sink2_channel
	wire          rsp_xbar_demux_002_src0_ready;                                                                                  // rsp_xbar_mux:sink2_ready -> rsp_xbar_demux_002:src0_ready
	wire          rsp_xbar_demux_002_src1_endofpacket;                                                                            // rsp_xbar_demux_002:src1_endofpacket -> rsp_xbar_mux_001:sink2_endofpacket
	wire          rsp_xbar_demux_002_src1_valid;                                                                                  // rsp_xbar_demux_002:src1_valid -> rsp_xbar_mux_001:sink2_valid
	wire          rsp_xbar_demux_002_src1_startofpacket;                                                                          // rsp_xbar_demux_002:src1_startofpacket -> rsp_xbar_mux_001:sink2_startofpacket
	wire   [81:0] rsp_xbar_demux_002_src1_data;                                                                                   // rsp_xbar_demux_002:src1_data -> rsp_xbar_mux_001:sink2_data
	wire    [5:0] rsp_xbar_demux_002_src1_channel;                                                                                // rsp_xbar_demux_002:src1_channel -> rsp_xbar_mux_001:sink2_channel
	wire          rsp_xbar_demux_002_src1_ready;                                                                                  // rsp_xbar_mux_001:sink2_ready -> rsp_xbar_demux_002:src1_ready
	wire          rsp_xbar_demux_003_src0_endofpacket;                                                                            // rsp_xbar_demux_003:src0_endofpacket -> rsp_xbar_mux:sink3_endofpacket
	wire          rsp_xbar_demux_003_src0_valid;                                                                                  // rsp_xbar_demux_003:src0_valid -> rsp_xbar_mux:sink3_valid
	wire          rsp_xbar_demux_003_src0_startofpacket;                                                                          // rsp_xbar_demux_003:src0_startofpacket -> rsp_xbar_mux:sink3_startofpacket
	wire   [81:0] rsp_xbar_demux_003_src0_data;                                                                                   // rsp_xbar_demux_003:src0_data -> rsp_xbar_mux:sink3_data
	wire    [5:0] rsp_xbar_demux_003_src0_channel;                                                                                // rsp_xbar_demux_003:src0_channel -> rsp_xbar_mux:sink3_channel
	wire          rsp_xbar_demux_003_src0_ready;                                                                                  // rsp_xbar_mux:sink3_ready -> rsp_xbar_demux_003:src0_ready
	wire          rsp_xbar_demux_003_src1_endofpacket;                                                                            // rsp_xbar_demux_003:src1_endofpacket -> rsp_xbar_mux_001:sink3_endofpacket
	wire          rsp_xbar_demux_003_src1_valid;                                                                                  // rsp_xbar_demux_003:src1_valid -> rsp_xbar_mux_001:sink3_valid
	wire          rsp_xbar_demux_003_src1_startofpacket;                                                                          // rsp_xbar_demux_003:src1_startofpacket -> rsp_xbar_mux_001:sink3_startofpacket
	wire   [81:0] rsp_xbar_demux_003_src1_data;                                                                                   // rsp_xbar_demux_003:src1_data -> rsp_xbar_mux_001:sink3_data
	wire    [5:0] rsp_xbar_demux_003_src1_channel;                                                                                // rsp_xbar_demux_003:src1_channel -> rsp_xbar_mux_001:sink3_channel
	wire          rsp_xbar_demux_003_src1_ready;                                                                                  // rsp_xbar_mux_001:sink3_ready -> rsp_xbar_demux_003:src1_ready
	wire          rsp_xbar_demux_004_src0_endofpacket;                                                                            // rsp_xbar_demux_004:src0_endofpacket -> rsp_xbar_mux:sink4_endofpacket
	wire          rsp_xbar_demux_004_src0_valid;                                                                                  // rsp_xbar_demux_004:src0_valid -> rsp_xbar_mux:sink4_valid
	wire          rsp_xbar_demux_004_src0_startofpacket;                                                                          // rsp_xbar_demux_004:src0_startofpacket -> rsp_xbar_mux:sink4_startofpacket
	wire   [81:0] rsp_xbar_demux_004_src0_data;                                                                                   // rsp_xbar_demux_004:src0_data -> rsp_xbar_mux:sink4_data
	wire    [5:0] rsp_xbar_demux_004_src0_channel;                                                                                // rsp_xbar_demux_004:src0_channel -> rsp_xbar_mux:sink4_channel
	wire          rsp_xbar_demux_004_src0_ready;                                                                                  // rsp_xbar_mux:sink4_ready -> rsp_xbar_demux_004:src0_ready
	wire          rsp_xbar_demux_004_src1_endofpacket;                                                                            // rsp_xbar_demux_004:src1_endofpacket -> rsp_xbar_mux_001:sink4_endofpacket
	wire          rsp_xbar_demux_004_src1_valid;                                                                                  // rsp_xbar_demux_004:src1_valid -> rsp_xbar_mux_001:sink4_valid
	wire          rsp_xbar_demux_004_src1_startofpacket;                                                                          // rsp_xbar_demux_004:src1_startofpacket -> rsp_xbar_mux_001:sink4_startofpacket
	wire   [81:0] rsp_xbar_demux_004_src1_data;                                                                                   // rsp_xbar_demux_004:src1_data -> rsp_xbar_mux_001:sink4_data
	wire    [5:0] rsp_xbar_demux_004_src1_channel;                                                                                // rsp_xbar_demux_004:src1_channel -> rsp_xbar_mux_001:sink4_channel
	wire          rsp_xbar_demux_004_src1_ready;                                                                                  // rsp_xbar_mux_001:sink4_ready -> rsp_xbar_demux_004:src1_ready
	wire          rsp_xbar_demux_005_src0_endofpacket;                                                                            // rsp_xbar_demux_005:src0_endofpacket -> rsp_xbar_mux:sink5_endofpacket
	wire          rsp_xbar_demux_005_src0_valid;                                                                                  // rsp_xbar_demux_005:src0_valid -> rsp_xbar_mux:sink5_valid
	wire          rsp_xbar_demux_005_src0_startofpacket;                                                                          // rsp_xbar_demux_005:src0_startofpacket -> rsp_xbar_mux:sink5_startofpacket
	wire   [81:0] rsp_xbar_demux_005_src0_data;                                                                                   // rsp_xbar_demux_005:src0_data -> rsp_xbar_mux:sink5_data
	wire    [5:0] rsp_xbar_demux_005_src0_channel;                                                                                // rsp_xbar_demux_005:src0_channel -> rsp_xbar_mux:sink5_channel
	wire          rsp_xbar_demux_005_src0_ready;                                                                                  // rsp_xbar_mux:sink5_ready -> rsp_xbar_demux_005:src0_ready
	wire          rsp_xbar_demux_005_src1_endofpacket;                                                                            // rsp_xbar_demux_005:src1_endofpacket -> rsp_xbar_mux_001:sink5_endofpacket
	wire          rsp_xbar_demux_005_src1_valid;                                                                                  // rsp_xbar_demux_005:src1_valid -> rsp_xbar_mux_001:sink5_valid
	wire          rsp_xbar_demux_005_src1_startofpacket;                                                                          // rsp_xbar_demux_005:src1_startofpacket -> rsp_xbar_mux_001:sink5_startofpacket
	wire   [81:0] rsp_xbar_demux_005_src1_data;                                                                                   // rsp_xbar_demux_005:src1_data -> rsp_xbar_mux_001:sink5_data
	wire    [5:0] rsp_xbar_demux_005_src1_channel;                                                                                // rsp_xbar_demux_005:src1_channel -> rsp_xbar_mux_001:sink5_channel
	wire          rsp_xbar_demux_005_src1_ready;                                                                                  // rsp_xbar_mux_001:sink5_ready -> rsp_xbar_demux_005:src1_ready
	wire          limiter_cmd_src_endofpacket;                                                                                    // limiter:cmd_src_endofpacket -> cmd_xbar_demux:sink_endofpacket
	wire          limiter_cmd_src_startofpacket;                                                                                  // limiter:cmd_src_startofpacket -> cmd_xbar_demux:sink_startofpacket
	wire   [81:0] limiter_cmd_src_data;                                                                                           // limiter:cmd_src_data -> cmd_xbar_demux:sink_data
	wire    [5:0] limiter_cmd_src_channel;                                                                                        // limiter:cmd_src_channel -> cmd_xbar_demux:sink_channel
	wire          limiter_cmd_src_ready;                                                                                          // cmd_xbar_demux:sink_ready -> limiter:cmd_src_ready
	wire          rsp_xbar_mux_src_endofpacket;                                                                                   // rsp_xbar_mux:src_endofpacket -> limiter:rsp_sink_endofpacket
	wire          rsp_xbar_mux_src_valid;                                                                                         // rsp_xbar_mux:src_valid -> limiter:rsp_sink_valid
	wire          rsp_xbar_mux_src_startofpacket;                                                                                 // rsp_xbar_mux:src_startofpacket -> limiter:rsp_sink_startofpacket
	wire   [81:0] rsp_xbar_mux_src_data;                                                                                          // rsp_xbar_mux:src_data -> limiter:rsp_sink_data
	wire    [5:0] rsp_xbar_mux_src_channel;                                                                                       // rsp_xbar_mux:src_channel -> limiter:rsp_sink_channel
	wire          rsp_xbar_mux_src_ready;                                                                                         // limiter:rsp_sink_ready -> rsp_xbar_mux:src_ready
	wire          limiter_001_cmd_src_endofpacket;                                                                                // limiter_001:cmd_src_endofpacket -> cmd_xbar_demux_001:sink_endofpacket
	wire          limiter_001_cmd_src_startofpacket;                                                                              // limiter_001:cmd_src_startofpacket -> cmd_xbar_demux_001:sink_startofpacket
	wire   [81:0] limiter_001_cmd_src_data;                                                                                       // limiter_001:cmd_src_data -> cmd_xbar_demux_001:sink_data
	wire    [5:0] limiter_001_cmd_src_channel;                                                                                    // limiter_001:cmd_src_channel -> cmd_xbar_demux_001:sink_channel
	wire          limiter_001_cmd_src_ready;                                                                                      // cmd_xbar_demux_001:sink_ready -> limiter_001:cmd_src_ready
	wire          rsp_xbar_mux_001_src_endofpacket;                                                                               // rsp_xbar_mux_001:src_endofpacket -> limiter_001:rsp_sink_endofpacket
	wire          rsp_xbar_mux_001_src_valid;                                                                                     // rsp_xbar_mux_001:src_valid -> limiter_001:rsp_sink_valid
	wire          rsp_xbar_mux_001_src_startofpacket;                                                                             // rsp_xbar_mux_001:src_startofpacket -> limiter_001:rsp_sink_startofpacket
	wire   [81:0] rsp_xbar_mux_001_src_data;                                                                                      // rsp_xbar_mux_001:src_data -> limiter_001:rsp_sink_data
	wire    [5:0] rsp_xbar_mux_001_src_channel;                                                                                   // rsp_xbar_mux_001:src_channel -> limiter_001:rsp_sink_channel
	wire          rsp_xbar_mux_001_src_ready;                                                                                     // limiter_001:rsp_sink_ready -> rsp_xbar_mux_001:src_ready
	wire          cmd_xbar_mux_src_endofpacket;                                                                                   // cmd_xbar_mux:src_endofpacket -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:cp_endofpacket
	wire          cmd_xbar_mux_src_valid;                                                                                         // cmd_xbar_mux:src_valid -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:cp_valid
	wire          cmd_xbar_mux_src_startofpacket;                                                                                 // cmd_xbar_mux:src_startofpacket -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:cp_startofpacket
	wire   [81:0] cmd_xbar_mux_src_data;                                                                                          // cmd_xbar_mux:src_data -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:cp_data
	wire    [5:0] cmd_xbar_mux_src_channel;                                                                                       // cmd_xbar_mux:src_channel -> nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:cp_channel
	wire          cmd_xbar_mux_src_ready;                                                                                         // nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent:cp_ready -> cmd_xbar_mux:src_ready
	wire          id_router_src_endofpacket;                                                                                      // id_router:src_endofpacket -> rsp_xbar_demux:sink_endofpacket
	wire          id_router_src_valid;                                                                                            // id_router:src_valid -> rsp_xbar_demux:sink_valid
	wire          id_router_src_startofpacket;                                                                                    // id_router:src_startofpacket -> rsp_xbar_demux:sink_startofpacket
	wire   [81:0] id_router_src_data;                                                                                             // id_router:src_data -> rsp_xbar_demux:sink_data
	wire    [5:0] id_router_src_channel;                                                                                          // id_router:src_channel -> rsp_xbar_demux:sink_channel
	wire          id_router_src_ready;                                                                                            // rsp_xbar_demux:sink_ready -> id_router:src_ready
	wire          cmd_xbar_mux_001_src_endofpacket;                                                                               // cmd_xbar_mux_001:src_endofpacket -> uart_0_s1_translator_avalon_universal_slave_0_agent:cp_endofpacket
	wire          cmd_xbar_mux_001_src_valid;                                                                                     // cmd_xbar_mux_001:src_valid -> uart_0_s1_translator_avalon_universal_slave_0_agent:cp_valid
	wire          cmd_xbar_mux_001_src_startofpacket;                                                                             // cmd_xbar_mux_001:src_startofpacket -> uart_0_s1_translator_avalon_universal_slave_0_agent:cp_startofpacket
	wire   [81:0] cmd_xbar_mux_001_src_data;                                                                                      // cmd_xbar_mux_001:src_data -> uart_0_s1_translator_avalon_universal_slave_0_agent:cp_data
	wire    [5:0] cmd_xbar_mux_001_src_channel;                                                                                   // cmd_xbar_mux_001:src_channel -> uart_0_s1_translator_avalon_universal_slave_0_agent:cp_channel
	wire          cmd_xbar_mux_001_src_ready;                                                                                     // uart_0_s1_translator_avalon_universal_slave_0_agent:cp_ready -> cmd_xbar_mux_001:src_ready
	wire          id_router_001_src_endofpacket;                                                                                  // id_router_001:src_endofpacket -> rsp_xbar_demux_001:sink_endofpacket
	wire          id_router_001_src_valid;                                                                                        // id_router_001:src_valid -> rsp_xbar_demux_001:sink_valid
	wire          id_router_001_src_startofpacket;                                                                                // id_router_001:src_startofpacket -> rsp_xbar_demux_001:sink_startofpacket
	wire   [81:0] id_router_001_src_data;                                                                                         // id_router_001:src_data -> rsp_xbar_demux_001:sink_data
	wire    [5:0] id_router_001_src_channel;                                                                                      // id_router_001:src_channel -> rsp_xbar_demux_001:sink_channel
	wire          id_router_001_src_ready;                                                                                        // rsp_xbar_demux_001:sink_ready -> id_router_001:src_ready
	wire          cmd_xbar_mux_002_src_endofpacket;                                                                               // cmd_xbar_mux_002:src_endofpacket -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:cp_endofpacket
	wire          cmd_xbar_mux_002_src_valid;                                                                                     // cmd_xbar_mux_002:src_valid -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:cp_valid
	wire          cmd_xbar_mux_002_src_startofpacket;                                                                             // cmd_xbar_mux_002:src_startofpacket -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:cp_startofpacket
	wire   [81:0] cmd_xbar_mux_002_src_data;                                                                                      // cmd_xbar_mux_002:src_data -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:cp_data
	wire    [5:0] cmd_xbar_mux_002_src_channel;                                                                                   // cmd_xbar_mux_002:src_channel -> onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:cp_channel
	wire          cmd_xbar_mux_002_src_ready;                                                                                     // onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent:cp_ready -> cmd_xbar_mux_002:src_ready
	wire          id_router_002_src_endofpacket;                                                                                  // id_router_002:src_endofpacket -> rsp_xbar_demux_002:sink_endofpacket
	wire          id_router_002_src_valid;                                                                                        // id_router_002:src_valid -> rsp_xbar_demux_002:sink_valid
	wire          id_router_002_src_startofpacket;                                                                                // id_router_002:src_startofpacket -> rsp_xbar_demux_002:sink_startofpacket
	wire   [81:0] id_router_002_src_data;                                                                                         // id_router_002:src_data -> rsp_xbar_demux_002:sink_data
	wire    [5:0] id_router_002_src_channel;                                                                                      // id_router_002:src_channel -> rsp_xbar_demux_002:sink_channel
	wire          id_router_002_src_ready;                                                                                        // rsp_xbar_demux_002:sink_ready -> id_router_002:src_ready
	wire          cmd_xbar_mux_003_src_endofpacket;                                                                               // cmd_xbar_mux_003:src_endofpacket -> pio_0_s1_translator_avalon_universal_slave_0_agent:cp_endofpacket
	wire          cmd_xbar_mux_003_src_valid;                                                                                     // cmd_xbar_mux_003:src_valid -> pio_0_s1_translator_avalon_universal_slave_0_agent:cp_valid
	wire          cmd_xbar_mux_003_src_startofpacket;                                                                             // cmd_xbar_mux_003:src_startofpacket -> pio_0_s1_translator_avalon_universal_slave_0_agent:cp_startofpacket
	wire   [81:0] cmd_xbar_mux_003_src_data;                                                                                      // cmd_xbar_mux_003:src_data -> pio_0_s1_translator_avalon_universal_slave_0_agent:cp_data
	wire    [5:0] cmd_xbar_mux_003_src_channel;                                                                                   // cmd_xbar_mux_003:src_channel -> pio_0_s1_translator_avalon_universal_slave_0_agent:cp_channel
	wire          cmd_xbar_mux_003_src_ready;                                                                                     // pio_0_s1_translator_avalon_universal_slave_0_agent:cp_ready -> cmd_xbar_mux_003:src_ready
	wire          id_router_003_src_endofpacket;                                                                                  // id_router_003:src_endofpacket -> rsp_xbar_demux_003:sink_endofpacket
	wire          id_router_003_src_valid;                                                                                        // id_router_003:src_valid -> rsp_xbar_demux_003:sink_valid
	wire          id_router_003_src_startofpacket;                                                                                // id_router_003:src_startofpacket -> rsp_xbar_demux_003:sink_startofpacket
	wire   [81:0] id_router_003_src_data;                                                                                         // id_router_003:src_data -> rsp_xbar_demux_003:sink_data
	wire    [5:0] id_router_003_src_channel;                                                                                      // id_router_003:src_channel -> rsp_xbar_demux_003:sink_channel
	wire          id_router_003_src_ready;                                                                                        // rsp_xbar_demux_003:sink_ready -> id_router_003:src_ready
	wire          cmd_xbar_mux_005_src_endofpacket;                                                                               // cmd_xbar_mux_005:src_endofpacket -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:cp_endofpacket
	wire          cmd_xbar_mux_005_src_valid;                                                                                     // cmd_xbar_mux_005:src_valid -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:cp_valid
	wire          cmd_xbar_mux_005_src_startofpacket;                                                                             // cmd_xbar_mux_005:src_startofpacket -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:cp_startofpacket
	wire   [81:0] cmd_xbar_mux_005_src_data;                                                                                      // cmd_xbar_mux_005:src_data -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:cp_data
	wire    [5:0] cmd_xbar_mux_005_src_channel;                                                                                   // cmd_xbar_mux_005:src_channel -> epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:cp_channel
	wire          cmd_xbar_mux_005_src_ready;                                                                                     // epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent:cp_ready -> cmd_xbar_mux_005:src_ready
	wire          id_router_005_src_endofpacket;                                                                                  // id_router_005:src_endofpacket -> rsp_xbar_demux_005:sink_endofpacket
	wire          id_router_005_src_valid;                                                                                        // id_router_005:src_valid -> rsp_xbar_demux_005:sink_valid
	wire          id_router_005_src_startofpacket;                                                                                // id_router_005:src_startofpacket -> rsp_xbar_demux_005:sink_startofpacket
	wire   [81:0] id_router_005_src_data;                                                                                         // id_router_005:src_data -> rsp_xbar_demux_005:sink_data
	wire    [5:0] id_router_005_src_channel;                                                                                      // id_router_005:src_channel -> rsp_xbar_demux_005:sink_channel
	wire          id_router_005_src_ready;                                                                                        // rsp_xbar_demux_005:sink_ready -> id_router_005:src_ready
	wire          cmd_xbar_mux_004_src_endofpacket;                                                                               // cmd_xbar_mux_004:src_endofpacket -> width_adapter:in_endofpacket
	wire          cmd_xbar_mux_004_src_valid;                                                                                     // cmd_xbar_mux_004:src_valid -> width_adapter:in_valid
	wire          cmd_xbar_mux_004_src_startofpacket;                                                                             // cmd_xbar_mux_004:src_startofpacket -> width_adapter:in_startofpacket
	wire   [81:0] cmd_xbar_mux_004_src_data;                                                                                      // cmd_xbar_mux_004:src_data -> width_adapter:in_data
	wire    [5:0] cmd_xbar_mux_004_src_channel;                                                                                   // cmd_xbar_mux_004:src_channel -> width_adapter:in_channel
	wire          cmd_xbar_mux_004_src_ready;                                                                                     // width_adapter:in_ready -> cmd_xbar_mux_004:src_ready
	wire          width_adapter_src_endofpacket;                                                                                  // width_adapter:out_endofpacket -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:cp_endofpacket
	wire          width_adapter_src_valid;                                                                                        // width_adapter:out_valid -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:cp_valid
	wire          width_adapter_src_startofpacket;                                                                                // width_adapter:out_startofpacket -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:cp_startofpacket
	wire  [117:0] width_adapter_src_data;                                                                                         // width_adapter:out_data -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:cp_data
	wire          width_adapter_src_ready;                                                                                        // barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:cp_ready -> width_adapter:out_ready
	wire    [5:0] width_adapter_src_channel;                                                                                      // width_adapter:out_channel -> barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent:cp_channel
	wire          id_router_004_src_endofpacket;                                                                                  // id_router_004:src_endofpacket -> width_adapter_001:in_endofpacket
	wire          id_router_004_src_valid;                                                                                        // id_router_004:src_valid -> width_adapter_001:in_valid
	wire          id_router_004_src_startofpacket;                                                                                // id_router_004:src_startofpacket -> width_adapter_001:in_startofpacket
	wire  [117:0] id_router_004_src_data;                                                                                         // id_router_004:src_data -> width_adapter_001:in_data
	wire    [5:0] id_router_004_src_channel;                                                                                      // id_router_004:src_channel -> width_adapter_001:in_channel
	wire          id_router_004_src_ready;                                                                                        // width_adapter_001:in_ready -> id_router_004:src_ready
	wire          width_adapter_001_src_endofpacket;                                                                              // width_adapter_001:out_endofpacket -> rsp_xbar_demux_004:sink_endofpacket
	wire          width_adapter_001_src_valid;                                                                                    // width_adapter_001:out_valid -> rsp_xbar_demux_004:sink_valid
	wire          width_adapter_001_src_startofpacket;                                                                            // width_adapter_001:out_startofpacket -> rsp_xbar_demux_004:sink_startofpacket
	wire   [81:0] width_adapter_001_src_data;                                                                                     // width_adapter_001:out_data -> rsp_xbar_demux_004:sink_data
	wire          width_adapter_001_src_ready;                                                                                    // rsp_xbar_demux_004:sink_ready -> width_adapter_001:out_ready
	wire    [5:0] width_adapter_001_src_channel;                                                                                  // width_adapter_001:out_channel -> rsp_xbar_demux_004:sink_channel
	wire    [5:0] limiter_cmd_valid_data;                                                                                         // limiter:cmd_src_valid -> cmd_xbar_demux:sink_valid
	wire    [5:0] limiter_001_cmd_valid_data;                                                                                     // limiter_001:cmd_src_valid -> cmd_xbar_demux_001:sink_valid
	wire          irq_mapper_receiver0_irq;                                                                                       // uart_0:irq -> irq_mapper:receiver0_irq
	wire   [31:0] nios2_qsys_0_d_irq_irq;                                                                                         // irq_mapper:sender_irq -> nios2_qsys_0:d_irq

	qsys_barco_nios2_qsys_0 nios2_qsys_0 (
		.clk                                   (clk_clk),                                                                     //                       clk.clk
		.reset_n                               (~rst_controller_reset_out_reset),                                             //                   reset_n.reset_n
		.d_address                             (nios2_qsys_0_data_master_address),                                            //               data_master.address
		.d_byteenable                          (nios2_qsys_0_data_master_byteenable),                                         //                          .byteenable
		.d_read                                (nios2_qsys_0_data_master_read),                                               //                          .read
		.d_readdata                            (nios2_qsys_0_data_master_readdata),                                           //                          .readdata
		.d_waitrequest                         (nios2_qsys_0_data_master_waitrequest),                                        //                          .waitrequest
		.d_write                               (nios2_qsys_0_data_master_write),                                              //                          .write
		.d_writedata                           (nios2_qsys_0_data_master_writedata),                                          //                          .writedata
		.jtag_debug_module_debugaccess_to_roms (nios2_qsys_0_data_master_debugaccess),                                        //                          .debugaccess
		.i_address                             (nios2_qsys_0_instruction_master_address),                                     //        instruction_master.address
		.i_read                                (nios2_qsys_0_instruction_master_read),                                        //                          .read
		.i_readdata                            (nios2_qsys_0_instruction_master_readdata),                                    //                          .readdata
		.i_waitrequest                         (nios2_qsys_0_instruction_master_waitrequest),                                 //                          .waitrequest
		.d_irq                                 (nios2_qsys_0_d_irq_irq),                                                      //                     d_irq.irq
		.jtag_debug_module_resetrequest        (nios2_qsys_0_jtag_debug_module_reset_reset),                                  //   jtag_debug_module_reset.reset
		.jtag_debug_module_address             (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_address),       //         jtag_debug_module.address
		.jtag_debug_module_begintransfer       (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_begintransfer), //                          .begintransfer
		.jtag_debug_module_byteenable          (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_byteenable),    //                          .byteenable
		.jtag_debug_module_debugaccess         (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_debugaccess),   //                          .debugaccess
		.jtag_debug_module_readdata            (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_readdata),      //                          .readdata
		.jtag_debug_module_select              (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_chipselect),    //                          .chipselect
		.jtag_debug_module_write               (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_write),         //                          .write
		.jtag_debug_module_writedata           (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_writedata),     //                          .writedata
		.no_ci_readra                          ()                                                                             // custom_instruction_master.readra
	);

	qsys_barco_uart_0 uart_0 (
		.clk           (clk_clk),                                                //                 clk.clk
		.reset_n       (~rst_controller_reset_out_reset),                        //               reset.reset_n
		.address       (uart_0_s1_translator_avalon_anti_slave_0_address),       //                  s1.address
		.begintransfer (uart_0_s1_translator_avalon_anti_slave_0_begintransfer), //                    .begintransfer
		.chipselect    (uart_0_s1_translator_avalon_anti_slave_0_chipselect),    //                    .chipselect
		.read_n        (~uart_0_s1_translator_avalon_anti_slave_0_read),         //                    .read_n
		.write_n       (~uart_0_s1_translator_avalon_anti_slave_0_write),        //                    .write_n
		.writedata     (uart_0_s1_translator_avalon_anti_slave_0_writedata),     //                    .writedata
		.readdata      (uart_0_s1_translator_avalon_anti_slave_0_readdata),      //                    .readdata
		.dataavailable (),                                                       //                    .dataavailable
		.readyfordata  (),                                                       //                    .readyfordata
		.rxd           (uart_0_external_connection_rxd),                         // external_connection.export
		.txd           (uart_0_external_connection_txd),                         //                    .export
		.irq           (irq_mapper_receiver0_irq)                                //                 irq.irq
	);

	qsys_barco_onchip_memory2_0 onchip_memory2_0 (
		.clk        (clk_clk),                                                       //   clk1.clk
		.address    (onchip_memory2_0_s1_translator_avalon_anti_slave_0_address),    //     s1.address
		.chipselect (onchip_memory2_0_s1_translator_avalon_anti_slave_0_chipselect), //       .chipselect
		.clken      (onchip_memory2_0_s1_translator_avalon_anti_slave_0_clken),      //       .clken
		.readdata   (onchip_memory2_0_s1_translator_avalon_anti_slave_0_readdata),   //       .readdata
		.write      (onchip_memory2_0_s1_translator_avalon_anti_slave_0_write),      //       .write
		.writedata  (onchip_memory2_0_s1_translator_avalon_anti_slave_0_writedata),  //       .writedata
		.byteenable (onchip_memory2_0_s1_translator_avalon_anti_slave_0_byteenable), //       .byteenable
		.reset      (rst_controller_001_reset_out_reset)                             // reset1.reset
	);

	qsys_barco_pio_0 pio_0 (
		.clk        (clk_clk),                                            //                 clk.clk
		.reset_n    (~rst_controller_001_reset_out_reset),                //               reset.reset_n
		.address    (pio_0_s1_translator_avalon_anti_slave_0_address),    //                  s1.address
		.write_n    (~pio_0_s1_translator_avalon_anti_slave_0_write),     //                    .write_n
		.writedata  (pio_0_s1_translator_avalon_anti_slave_0_writedata),  //                    .writedata
		.chipselect (pio_0_s1_translator_avalon_anti_slave_0_chipselect), //                    .chipselect
		.readdata   (pio_0_s1_translator_avalon_anti_slave_0_readdata),   //                    .readdata
		.out_port   (pio_0_external_connection_export)                    // external_connection.export
	);

	barco_avalon barco_avalon_0 (
		.avs_s0_address       (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_address),       // barco_avalon.address
		.avs_s0_read          (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_read),          //             .read
		.avs_s0_readdata      (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_readdata),      //             .readdata
		.avs_s0_writedata     (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_writedata),     //             .writedata
		.avs_s0_readdatavalid (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_readdatavalid), //             .readdatavalid
		.avs_s0_waitrequest   (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_waitrequest),   //             .waitrequest
		.avs_s0_byteEnable    (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_byteenable),    //             .byteenable
		.avs_s0_write         (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_write),         //             .write
		.clk                  (clk_clk),                                                                  //        clock.clk
		.reset                (rst_controller_reset_out_reset),                                           //        reset.reset
		.Red_DO               (barco_avalon_0_conduit_end_Red_DO),                                        //  conduit_end.export
		.Green_DO             (barco_avalon_0_conduit_end_Green_DO),                                      //             .export
		.Blue_DO              (barco_avalon_0_conduit_end_Blue_DO),                                       //             .export
		.Valid_SO             (barco_avalon_0_conduit_end_Valid_SO),                                      //             .export
		.Addr_DO              (barco_avalon_0_conduit_end_Addr_DO)                                        //             .export
	);

	qsys_barco_epcs_flash_controller_0 epcs_flash_controller_0 (
		.clk           (clk_clk),                                                                             //               clk.clk
		.reset_n       (~rst_controller_reset_out_reset),                                                     //             reset.reset_n
		.address       (epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_address),    // epcs_control_port.address
		.chipselect    (epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_chipselect), //                  .chipselect
		.dataavailable (),                                                                                    //                  .dataavailable
		.endofpacket   (),                                                                                    //                  .endofpacket
		.read_n        (~epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_read),      //                  .read_n
		.readdata      (epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_readdata),   //                  .readdata
		.readyfordata  (),                                                                                    //                  .readyfordata
		.write_n       (~epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_write),     //                  .write_n
		.writedata     (epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_writedata),  //                  .writedata
		.irq           ()                                                                                     //               irq.irq
	);

	altera_merlin_master_translator #(
		.AV_ADDRESS_W                (25),
		.AV_DATA_W                   (32),
		.AV_BURSTCOUNT_W             (1),
		.AV_BYTEENABLE_W             (4),
		.UAV_ADDRESS_W               (25),
		.UAV_BURSTCOUNT_W            (3),
		.USE_READ                    (1),
		.USE_WRITE                   (0),
		.USE_BEGINBURSTTRANSFER      (0),
		.USE_BEGINTRANSFER           (0),
		.USE_CHIPSELECT              (0),
		.USE_BURSTCOUNT              (0),
		.USE_READDATAVALID           (0),
		.USE_WAITREQUEST             (1),
		.AV_SYMBOLS_PER_WORD         (4),
		.AV_ADDRESS_SYMBOLS          (1),
		.AV_BURSTCOUNT_SYMBOLS       (0),
		.AV_CONSTANT_BURST_BEHAVIOR  (0),
		.UAV_CONSTANT_BURST_BEHAVIOR (0),
		.AV_LINEWRAPBURSTS           (1),
		.AV_REGISTERINCOMINGSIGNALS  (0)
	) nios2_qsys_0_instruction_master_translator (
		.clk                   (clk_clk),                                                                            //                       clk.clk
		.reset                 (rst_controller_reset_out_reset),                                                     //                     reset.reset
		.uav_address           (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_address),       // avalon_universal_master_0.address
		.uav_burstcount        (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_burstcount),    //                          .burstcount
		.uav_read              (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_read),          //                          .read
		.uav_write             (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_write),         //                          .write
		.uav_waitrequest       (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_waitrequest),   //                          .waitrequest
		.uav_readdatavalid     (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_readdatavalid), //                          .readdatavalid
		.uav_byteenable        (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_byteenable),    //                          .byteenable
		.uav_readdata          (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_readdata),      //                          .readdata
		.uav_writedata         (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_writedata),     //                          .writedata
		.uav_lock              (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_lock),          //                          .lock
		.uav_debugaccess       (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_debugaccess),   //                          .debugaccess
		.av_address            (nios2_qsys_0_instruction_master_address),                                            //      avalon_anti_master_0.address
		.av_waitrequest        (nios2_qsys_0_instruction_master_waitrequest),                                        //                          .waitrequest
		.av_read               (nios2_qsys_0_instruction_master_read),                                               //                          .read
		.av_readdata           (nios2_qsys_0_instruction_master_readdata),                                           //                          .readdata
		.av_burstcount         (1'b1),                                                                               //               (terminated)
		.av_byteenable         (4'b1111),                                                                            //               (terminated)
		.av_beginbursttransfer (1'b0),                                                                               //               (terminated)
		.av_begintransfer      (1'b0),                                                                               //               (terminated)
		.av_chipselect         (1'b0),                                                                               //               (terminated)
		.av_readdatavalid      (),                                                                                   //               (terminated)
		.av_write              (1'b0),                                                                               //               (terminated)
		.av_writedata          (32'b00000000000000000000000000000000),                                               //               (terminated)
		.av_lock               (1'b0),                                                                               //               (terminated)
		.av_debugaccess        (1'b0),                                                                               //               (terminated)
		.uav_clken             (),                                                                                   //               (terminated)
		.av_clken              (1'b1)                                                                                //               (terminated)
	);

	altera_merlin_master_translator #(
		.AV_ADDRESS_W                (25),
		.AV_DATA_W                   (32),
		.AV_BURSTCOUNT_W             (1),
		.AV_BYTEENABLE_W             (4),
		.UAV_ADDRESS_W               (25),
		.UAV_BURSTCOUNT_W            (3),
		.USE_READ                    (1),
		.USE_WRITE                   (1),
		.USE_BEGINBURSTTRANSFER      (0),
		.USE_BEGINTRANSFER           (0),
		.USE_CHIPSELECT              (0),
		.USE_BURSTCOUNT              (0),
		.USE_READDATAVALID           (0),
		.USE_WAITREQUEST             (1),
		.AV_SYMBOLS_PER_WORD         (4),
		.AV_ADDRESS_SYMBOLS          (1),
		.AV_BURSTCOUNT_SYMBOLS       (0),
		.AV_CONSTANT_BURST_BEHAVIOR  (0),
		.UAV_CONSTANT_BURST_BEHAVIOR (0),
		.AV_LINEWRAPBURSTS           (0),
		.AV_REGISTERINCOMINGSIGNALS  (1)
	) nios2_qsys_0_data_master_translator (
		.clk                   (clk_clk),                                                                     //                       clk.clk
		.reset                 (rst_controller_reset_out_reset),                                              //                     reset.reset
		.uav_address           (nios2_qsys_0_data_master_translator_avalon_universal_master_0_address),       // avalon_universal_master_0.address
		.uav_burstcount        (nios2_qsys_0_data_master_translator_avalon_universal_master_0_burstcount),    //                          .burstcount
		.uav_read              (nios2_qsys_0_data_master_translator_avalon_universal_master_0_read),          //                          .read
		.uav_write             (nios2_qsys_0_data_master_translator_avalon_universal_master_0_write),         //                          .write
		.uav_waitrequest       (nios2_qsys_0_data_master_translator_avalon_universal_master_0_waitrequest),   //                          .waitrequest
		.uav_readdatavalid     (nios2_qsys_0_data_master_translator_avalon_universal_master_0_readdatavalid), //                          .readdatavalid
		.uav_byteenable        (nios2_qsys_0_data_master_translator_avalon_universal_master_0_byteenable),    //                          .byteenable
		.uav_readdata          (nios2_qsys_0_data_master_translator_avalon_universal_master_0_readdata),      //                          .readdata
		.uav_writedata         (nios2_qsys_0_data_master_translator_avalon_universal_master_0_writedata),     //                          .writedata
		.uav_lock              (nios2_qsys_0_data_master_translator_avalon_universal_master_0_lock),          //                          .lock
		.uav_debugaccess       (nios2_qsys_0_data_master_translator_avalon_universal_master_0_debugaccess),   //                          .debugaccess
		.av_address            (nios2_qsys_0_data_master_address),                                            //      avalon_anti_master_0.address
		.av_waitrequest        (nios2_qsys_0_data_master_waitrequest),                                        //                          .waitrequest
		.av_byteenable         (nios2_qsys_0_data_master_byteenable),                                         //                          .byteenable
		.av_read               (nios2_qsys_0_data_master_read),                                               //                          .read
		.av_readdata           (nios2_qsys_0_data_master_readdata),                                           //                          .readdata
		.av_write              (nios2_qsys_0_data_master_write),                                              //                          .write
		.av_writedata          (nios2_qsys_0_data_master_writedata),                                          //                          .writedata
		.av_debugaccess        (nios2_qsys_0_data_master_debugaccess),                                        //                          .debugaccess
		.av_burstcount         (1'b1),                                                                        //               (terminated)
		.av_beginbursttransfer (1'b0),                                                                        //               (terminated)
		.av_begintransfer      (1'b0),                                                                        //               (terminated)
		.av_chipselect         (1'b0),                                                                        //               (terminated)
		.av_readdatavalid      (),                                                                            //               (terminated)
		.av_lock               (1'b0),                                                                        //               (terminated)
		.uav_clken             (),                                                                            //               (terminated)
		.av_clken              (1'b1)                                                                         //               (terminated)
	);

	altera_merlin_slave_translator #(
		.AV_ADDRESS_W                   (9),
		.AV_DATA_W                      (32),
		.UAV_DATA_W                     (32),
		.AV_BURSTCOUNT_W                (1),
		.AV_BYTEENABLE_W                (4),
		.UAV_BYTEENABLE_W               (4),
		.UAV_ADDRESS_W                  (25),
		.UAV_BURSTCOUNT_W               (3),
		.AV_READLATENCY                 (0),
		.USE_READDATAVALID              (0),
		.USE_WAITREQUEST                (0),
		.USE_UAV_CLKEN                  (0),
		.AV_SYMBOLS_PER_WORD            (4),
		.AV_ADDRESS_SYMBOLS             (0),
		.AV_BURSTCOUNT_SYMBOLS          (0),
		.AV_CONSTANT_BURST_BEHAVIOR     (0),
		.UAV_CONSTANT_BURST_BEHAVIOR    (0),
		.AV_REQUIRE_UNALIGNED_ADDRESSES (0),
		.CHIPSELECT_THROUGH_READLATENCY (0),
		.AV_READ_WAIT_CYCLES            (1),
		.AV_WRITE_WAIT_CYCLES           (0),
		.AV_SETUP_WAIT_CYCLES           (0),
		.AV_DATA_HOLD_CYCLES            (0)
	) nios2_qsys_0_jtag_debug_module_translator (
		.clk                   (clk_clk),                                                                                   //                      clk.clk
		.reset                 (rst_controller_reset_out_reset),                                                            //                    reset.reset
		.uav_address           (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_address),       // avalon_universal_slave_0.address
		.uav_burstcount        (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_burstcount),    //                         .burstcount
		.uav_read              (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_read),          //                         .read
		.uav_write             (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_write),         //                         .write
		.uav_waitrequest       (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_waitrequest),   //                         .waitrequest
		.uav_readdatavalid     (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_readdatavalid), //                         .readdatavalid
		.uav_byteenable        (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_byteenable),    //                         .byteenable
		.uav_readdata          (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_readdata),      //                         .readdata
		.uav_writedata         (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_writedata),     //                         .writedata
		.uav_lock              (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_lock),          //                         .lock
		.uav_debugaccess       (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_debugaccess),   //                         .debugaccess
		.av_address            (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_address),                     //      avalon_anti_slave_0.address
		.av_write              (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_write),                       //                         .write
		.av_readdata           (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_readdata),                    //                         .readdata
		.av_writedata          (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_writedata),                   //                         .writedata
		.av_begintransfer      (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_begintransfer),               //                         .begintransfer
		.av_byteenable         (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_byteenable),                  //                         .byteenable
		.av_chipselect         (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_chipselect),                  //                         .chipselect
		.av_debugaccess        (nios2_qsys_0_jtag_debug_module_translator_avalon_anti_slave_0_debugaccess),                 //                         .debugaccess
		.av_read               (),                                                                                          //              (terminated)
		.av_beginbursttransfer (),                                                                                          //              (terminated)
		.av_burstcount         (),                                                                                          //              (terminated)
		.av_readdatavalid      (1'b0),                                                                                      //              (terminated)
		.av_waitrequest        (1'b0),                                                                                      //              (terminated)
		.av_writebyteenable    (),                                                                                          //              (terminated)
		.av_lock               (),                                                                                          //              (terminated)
		.av_clken              (),                                                                                          //              (terminated)
		.uav_clken             (1'b0),                                                                                      //              (terminated)
		.av_outputenable       ()                                                                                           //              (terminated)
	);

	altera_merlin_slave_translator #(
		.AV_ADDRESS_W                   (3),
		.AV_DATA_W                      (16),
		.UAV_DATA_W                     (32),
		.AV_BURSTCOUNT_W                (1),
		.AV_BYTEENABLE_W                (1),
		.UAV_BYTEENABLE_W               (4),
		.UAV_ADDRESS_W                  (25),
		.UAV_BURSTCOUNT_W               (3),
		.AV_READLATENCY                 (0),
		.USE_READDATAVALID              (0),
		.USE_WAITREQUEST                (0),
		.USE_UAV_CLKEN                  (0),
		.AV_SYMBOLS_PER_WORD            (4),
		.AV_ADDRESS_SYMBOLS             (0),
		.AV_BURSTCOUNT_SYMBOLS          (0),
		.AV_CONSTANT_BURST_BEHAVIOR     (0),
		.UAV_CONSTANT_BURST_BEHAVIOR    (0),
		.AV_REQUIRE_UNALIGNED_ADDRESSES (0),
		.CHIPSELECT_THROUGH_READLATENCY (0),
		.AV_READ_WAIT_CYCLES            (1),
		.AV_WRITE_WAIT_CYCLES           (1),
		.AV_SETUP_WAIT_CYCLES           (0),
		.AV_DATA_HOLD_CYCLES            (0)
	) uart_0_s1_translator (
		.clk                   (clk_clk),                                                              //                      clk.clk
		.reset                 (rst_controller_reset_out_reset),                                       //                    reset.reset
		.uav_address           (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_address),       // avalon_universal_slave_0.address
		.uav_burstcount        (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_burstcount),    //                         .burstcount
		.uav_read              (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_read),          //                         .read
		.uav_write             (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_write),         //                         .write
		.uav_waitrequest       (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_waitrequest),   //                         .waitrequest
		.uav_readdatavalid     (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_readdatavalid), //                         .readdatavalid
		.uav_byteenable        (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_byteenable),    //                         .byteenable
		.uav_readdata          (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_readdata),      //                         .readdata
		.uav_writedata         (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_writedata),     //                         .writedata
		.uav_lock              (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_lock),          //                         .lock
		.uav_debugaccess       (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_debugaccess),   //                         .debugaccess
		.av_address            (uart_0_s1_translator_avalon_anti_slave_0_address),                     //      avalon_anti_slave_0.address
		.av_write              (uart_0_s1_translator_avalon_anti_slave_0_write),                       //                         .write
		.av_read               (uart_0_s1_translator_avalon_anti_slave_0_read),                        //                         .read
		.av_readdata           (uart_0_s1_translator_avalon_anti_slave_0_readdata),                    //                         .readdata
		.av_writedata          (uart_0_s1_translator_avalon_anti_slave_0_writedata),                   //                         .writedata
		.av_begintransfer      (uart_0_s1_translator_avalon_anti_slave_0_begintransfer),               //                         .begintransfer
		.av_chipselect         (uart_0_s1_translator_avalon_anti_slave_0_chipselect),                  //                         .chipselect
		.av_beginbursttransfer (),                                                                     //              (terminated)
		.av_burstcount         (),                                                                     //              (terminated)
		.av_byteenable         (),                                                                     //              (terminated)
		.av_readdatavalid      (1'b0),                                                                 //              (terminated)
		.av_waitrequest        (1'b0),                                                                 //              (terminated)
		.av_writebyteenable    (),                                                                     //              (terminated)
		.av_lock               (),                                                                     //              (terminated)
		.av_clken              (),                                                                     //              (terminated)
		.uav_clken             (1'b0),                                                                 //              (terminated)
		.av_debugaccess        (),                                                                     //              (terminated)
		.av_outputenable       ()                                                                      //              (terminated)
	);

	altera_merlin_slave_translator #(
		.AV_ADDRESS_W                   (9),
		.AV_DATA_W                      (32),
		.UAV_DATA_W                     (32),
		.AV_BURSTCOUNT_W                (1),
		.AV_BYTEENABLE_W                (4),
		.UAV_BYTEENABLE_W               (4),
		.UAV_ADDRESS_W                  (25),
		.UAV_BURSTCOUNT_W               (3),
		.AV_READLATENCY                 (1),
		.USE_READDATAVALID              (0),
		.USE_WAITREQUEST                (0),
		.USE_UAV_CLKEN                  (0),
		.AV_SYMBOLS_PER_WORD            (4),
		.AV_ADDRESS_SYMBOLS             (0),
		.AV_BURSTCOUNT_SYMBOLS          (0),
		.AV_CONSTANT_BURST_BEHAVIOR     (0),
		.UAV_CONSTANT_BURST_BEHAVIOR    (0),
		.AV_REQUIRE_UNALIGNED_ADDRESSES (0),
		.CHIPSELECT_THROUGH_READLATENCY (0),
		.AV_READ_WAIT_CYCLES            (0),
		.AV_WRITE_WAIT_CYCLES           (0),
		.AV_SETUP_WAIT_CYCLES           (0),
		.AV_DATA_HOLD_CYCLES            (0)
	) onchip_memory2_0_s1_translator (
		.clk                   (clk_clk),                                                                        //                      clk.clk
		.reset                 (rst_controller_001_reset_out_reset),                                             //                    reset.reset
		.uav_address           (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_address),       // avalon_universal_slave_0.address
		.uav_burstcount        (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_burstcount),    //                         .burstcount
		.uav_read              (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_read),          //                         .read
		.uav_write             (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_write),         //                         .write
		.uav_waitrequest       (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_waitrequest),   //                         .waitrequest
		.uav_readdatavalid     (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_readdatavalid), //                         .readdatavalid
		.uav_byteenable        (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_byteenable),    //                         .byteenable
		.uav_readdata          (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_readdata),      //                         .readdata
		.uav_writedata         (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_writedata),     //                         .writedata
		.uav_lock              (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_lock),          //                         .lock
		.uav_debugaccess       (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_debugaccess),   //                         .debugaccess
		.av_address            (onchip_memory2_0_s1_translator_avalon_anti_slave_0_address),                     //      avalon_anti_slave_0.address
		.av_write              (onchip_memory2_0_s1_translator_avalon_anti_slave_0_write),                       //                         .write
		.av_readdata           (onchip_memory2_0_s1_translator_avalon_anti_slave_0_readdata),                    //                         .readdata
		.av_writedata          (onchip_memory2_0_s1_translator_avalon_anti_slave_0_writedata),                   //                         .writedata
		.av_byteenable         (onchip_memory2_0_s1_translator_avalon_anti_slave_0_byteenable),                  //                         .byteenable
		.av_chipselect         (onchip_memory2_0_s1_translator_avalon_anti_slave_0_chipselect),                  //                         .chipselect
		.av_clken              (onchip_memory2_0_s1_translator_avalon_anti_slave_0_clken),                       //                         .clken
		.av_read               (),                                                                               //              (terminated)
		.av_begintransfer      (),                                                                               //              (terminated)
		.av_beginbursttransfer (),                                                                               //              (terminated)
		.av_burstcount         (),                                                                               //              (terminated)
		.av_readdatavalid      (1'b0),                                                                           //              (terminated)
		.av_waitrequest        (1'b0),                                                                           //              (terminated)
		.av_writebyteenable    (),                                                                               //              (terminated)
		.av_lock               (),                                                                               //              (terminated)
		.uav_clken             (1'b0),                                                                           //              (terminated)
		.av_debugaccess        (),                                                                               //              (terminated)
		.av_outputenable       ()                                                                                //              (terminated)
	);

	altera_merlin_slave_translator #(
		.AV_ADDRESS_W                   (3),
		.AV_DATA_W                      (32),
		.UAV_DATA_W                     (32),
		.AV_BURSTCOUNT_W                (1),
		.AV_BYTEENABLE_W                (1),
		.UAV_BYTEENABLE_W               (4),
		.UAV_ADDRESS_W                  (25),
		.UAV_BURSTCOUNT_W               (3),
		.AV_READLATENCY                 (0),
		.USE_READDATAVALID              (0),
		.USE_WAITREQUEST                (0),
		.USE_UAV_CLKEN                  (0),
		.AV_SYMBOLS_PER_WORD            (4),
		.AV_ADDRESS_SYMBOLS             (0),
		.AV_BURSTCOUNT_SYMBOLS          (0),
		.AV_CONSTANT_BURST_BEHAVIOR     (0),
		.UAV_CONSTANT_BURST_BEHAVIOR    (0),
		.AV_REQUIRE_UNALIGNED_ADDRESSES (0),
		.CHIPSELECT_THROUGH_READLATENCY (0),
		.AV_READ_WAIT_CYCLES            (1),
		.AV_WRITE_WAIT_CYCLES           (0),
		.AV_SETUP_WAIT_CYCLES           (0),
		.AV_DATA_HOLD_CYCLES            (0)
	) pio_0_s1_translator (
		.clk                   (clk_clk),                                                             //                      clk.clk
		.reset                 (rst_controller_001_reset_out_reset),                                  //                    reset.reset
		.uav_address           (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_address),       // avalon_universal_slave_0.address
		.uav_burstcount        (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_burstcount),    //                         .burstcount
		.uav_read              (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_read),          //                         .read
		.uav_write             (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_write),         //                         .write
		.uav_waitrequest       (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_waitrequest),   //                         .waitrequest
		.uav_readdatavalid     (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_readdatavalid), //                         .readdatavalid
		.uav_byteenable        (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_byteenable),    //                         .byteenable
		.uav_readdata          (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_readdata),      //                         .readdata
		.uav_writedata         (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_writedata),     //                         .writedata
		.uav_lock              (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_lock),          //                         .lock
		.uav_debugaccess       (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_debugaccess),   //                         .debugaccess
		.av_address            (pio_0_s1_translator_avalon_anti_slave_0_address),                     //      avalon_anti_slave_0.address
		.av_write              (pio_0_s1_translator_avalon_anti_slave_0_write),                       //                         .write
		.av_readdata           (pio_0_s1_translator_avalon_anti_slave_0_readdata),                    //                         .readdata
		.av_writedata          (pio_0_s1_translator_avalon_anti_slave_0_writedata),                   //                         .writedata
		.av_chipselect         (pio_0_s1_translator_avalon_anti_slave_0_chipselect),                  //                         .chipselect
		.av_read               (),                                                                    //              (terminated)
		.av_begintransfer      (),                                                                    //              (terminated)
		.av_beginbursttransfer (),                                                                    //              (terminated)
		.av_burstcount         (),                                                                    //              (terminated)
		.av_byteenable         (),                                                                    //              (terminated)
		.av_readdatavalid      (1'b0),                                                                //              (terminated)
		.av_waitrequest        (1'b0),                                                                //              (terminated)
		.av_writebyteenable    (),                                                                    //              (terminated)
		.av_lock               (),                                                                    //              (terminated)
		.av_clken              (),                                                                    //              (terminated)
		.uav_clken             (1'b0),                                                                //              (terminated)
		.av_debugaccess        (),                                                                    //              (terminated)
		.av_outputenable       ()                                                                     //              (terminated)
	);

	altera_merlin_slave_translator #(
		.AV_ADDRESS_W                   (7),
		.AV_DATA_W                      (64),
		.UAV_DATA_W                     (64),
		.AV_BURSTCOUNT_W                (1),
		.AV_BYTEENABLE_W                (8),
		.UAV_BYTEENABLE_W               (8),
		.UAV_ADDRESS_W                  (25),
		.UAV_BURSTCOUNT_W               (4),
		.AV_READLATENCY                 (0),
		.USE_READDATAVALID              (1),
		.USE_WAITREQUEST                (1),
		.USE_UAV_CLKEN                  (0),
		.AV_SYMBOLS_PER_WORD            (8),
		.AV_ADDRESS_SYMBOLS             (0),
		.AV_BURSTCOUNT_SYMBOLS          (0),
		.AV_CONSTANT_BURST_BEHAVIOR     (0),
		.UAV_CONSTANT_BURST_BEHAVIOR    (0),
		.AV_REQUIRE_UNALIGNED_ADDRESSES (0),
		.CHIPSELECT_THROUGH_READLATENCY (0),
		.AV_READ_WAIT_CYCLES            (1),
		.AV_WRITE_WAIT_CYCLES           (0),
		.AV_SETUP_WAIT_CYCLES           (0),
		.AV_DATA_HOLD_CYCLES            (0)
	) barco_avalon_0_barco_avalon_translator (
		.clk                   (clk_clk),                                                                                //                      clk.clk
		.reset                 (rst_controller_reset_out_reset),                                                         //                    reset.reset
		.uav_address           (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_address),       // avalon_universal_slave_0.address
		.uav_burstcount        (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_burstcount),    //                         .burstcount
		.uav_read              (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_read),          //                         .read
		.uav_write             (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_write),         //                         .write
		.uav_waitrequest       (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_waitrequest),   //                         .waitrequest
		.uav_readdatavalid     (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_readdatavalid), //                         .readdatavalid
		.uav_byteenable        (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_byteenable),    //                         .byteenable
		.uav_readdata          (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_readdata),      //                         .readdata
		.uav_writedata         (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_writedata),     //                         .writedata
		.uav_lock              (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_lock),          //                         .lock
		.uav_debugaccess       (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_debugaccess),   //                         .debugaccess
		.av_address            (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_address),                     //      avalon_anti_slave_0.address
		.av_write              (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_write),                       //                         .write
		.av_read               (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_read),                        //                         .read
		.av_readdata           (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_readdata),                    //                         .readdata
		.av_writedata          (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_writedata),                   //                         .writedata
		.av_byteenable         (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_byteenable),                  //                         .byteenable
		.av_readdatavalid      (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_readdatavalid),               //                         .readdatavalid
		.av_waitrequest        (barco_avalon_0_barco_avalon_translator_avalon_anti_slave_0_waitrequest),                 //                         .waitrequest
		.av_begintransfer      (),                                                                                       //              (terminated)
		.av_beginbursttransfer (),                                                                                       //              (terminated)
		.av_burstcount         (),                                                                                       //              (terminated)
		.av_writebyteenable    (),                                                                                       //              (terminated)
		.av_lock               (),                                                                                       //              (terminated)
		.av_chipselect         (),                                                                                       //              (terminated)
		.av_clken              (),                                                                                       //              (terminated)
		.uav_clken             (1'b0),                                                                                   //              (terminated)
		.av_debugaccess        (),                                                                                       //              (terminated)
		.av_outputenable       ()                                                                                        //              (terminated)
	);

	altera_merlin_slave_translator #(
		.AV_ADDRESS_W                   (9),
		.AV_DATA_W                      (32),
		.UAV_DATA_W                     (32),
		.AV_BURSTCOUNT_W                (1),
		.AV_BYTEENABLE_W                (4),
		.UAV_BYTEENABLE_W               (4),
		.UAV_ADDRESS_W                  (25),
		.UAV_BURSTCOUNT_W               (3),
		.AV_READLATENCY                 (0),
		.USE_READDATAVALID              (0),
		.USE_WAITREQUEST                (0),
		.USE_UAV_CLKEN                  (0),
		.AV_SYMBOLS_PER_WORD            (4),
		.AV_ADDRESS_SYMBOLS             (0),
		.AV_BURSTCOUNT_SYMBOLS          (0),
		.AV_CONSTANT_BURST_BEHAVIOR     (0),
		.UAV_CONSTANT_BURST_BEHAVIOR    (0),
		.AV_REQUIRE_UNALIGNED_ADDRESSES (0),
		.CHIPSELECT_THROUGH_READLATENCY (0),
		.AV_READ_WAIT_CYCLES            (1),
		.AV_WRITE_WAIT_CYCLES           (1),
		.AV_SETUP_WAIT_CYCLES           (0),
		.AV_DATA_HOLD_CYCLES            (0)
	) epcs_flash_controller_0_epcs_control_port_translator (
		.clk                   (clk_clk),                                                                                              //                      clk.clk
		.reset                 (rst_controller_reset_out_reset),                                                                       //                    reset.reset
		.uav_address           (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_address),       // avalon_universal_slave_0.address
		.uav_burstcount        (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_burstcount),    //                         .burstcount
		.uav_read              (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_read),          //                         .read
		.uav_write             (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_write),         //                         .write
		.uav_waitrequest       (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_waitrequest),   //                         .waitrequest
		.uav_readdatavalid     (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_readdatavalid), //                         .readdatavalid
		.uav_byteenable        (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_byteenable),    //                         .byteenable
		.uav_readdata          (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_readdata),      //                         .readdata
		.uav_writedata         (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_writedata),     //                         .writedata
		.uav_lock              (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_lock),          //                         .lock
		.uav_debugaccess       (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_debugaccess),   //                         .debugaccess
		.av_address            (epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_address),                     //      avalon_anti_slave_0.address
		.av_write              (epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_write),                       //                         .write
		.av_read               (epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_read),                        //                         .read
		.av_readdata           (epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_readdata),                    //                         .readdata
		.av_writedata          (epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_writedata),                   //                         .writedata
		.av_chipselect         (epcs_flash_controller_0_epcs_control_port_translator_avalon_anti_slave_0_chipselect),                  //                         .chipselect
		.av_begintransfer      (),                                                                                                     //              (terminated)
		.av_beginbursttransfer (),                                                                                                     //              (terminated)
		.av_burstcount         (),                                                                                                     //              (terminated)
		.av_byteenable         (),                                                                                                     //              (terminated)
		.av_readdatavalid      (1'b0),                                                                                                 //              (terminated)
		.av_waitrequest        (1'b0),                                                                                                 //              (terminated)
		.av_writebyteenable    (),                                                                                                     //              (terminated)
		.av_lock               (),                                                                                                     //              (terminated)
		.av_clken              (),                                                                                                     //              (terminated)
		.uav_clken             (1'b0),                                                                                                 //              (terminated)
		.av_debugaccess        (),                                                                                                     //              (terminated)
		.av_outputenable       ()                                                                                                      //              (terminated)
	);

	altera_merlin_master_agent #(
		.PKT_PROTECTION_H          (81),
		.PKT_PROTECTION_L          (81),
		.PKT_BEGIN_BURST           (74),
		.PKT_BURSTWRAP_H           (73),
		.PKT_BURSTWRAP_L           (70),
		.PKT_BYTE_CNT_H            (69),
		.PKT_BYTE_CNT_L            (66),
		.PKT_ADDR_H                (60),
		.PKT_ADDR_L                (36),
		.PKT_TRANS_COMPRESSED_READ (61),
		.PKT_TRANS_POSTED          (62),
		.PKT_TRANS_WRITE           (63),
		.PKT_TRANS_READ            (64),
		.PKT_TRANS_LOCK            (65),
		.PKT_DATA_H                (31),
		.PKT_DATA_L                (0),
		.PKT_BYTEEN_H              (35),
		.PKT_BYTEEN_L              (32),
		.PKT_SRC_ID_H              (77),
		.PKT_SRC_ID_L              (75),
		.PKT_DEST_ID_H             (80),
		.PKT_DEST_ID_L             (78),
		.ST_DATA_W                 (82),
		.ST_CHANNEL_W              (6),
		.AV_BURSTCOUNT_W           (3),
		.SUPPRESS_0_BYTEEN_RSP     (1),
		.ID                        (1),
		.BURSTWRAP_VALUE           (3)
	) nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent (
		.clk              (clk_clk),                                                                                     //       clk.clk
		.reset            (rst_controller_reset_out_reset),                                                              // clk_reset.reset
		.av_address       (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_address),                //        av.address
		.av_write         (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_write),                  //          .write
		.av_read          (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_read),                   //          .read
		.av_writedata     (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_writedata),              //          .writedata
		.av_readdata      (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_readdata),               //          .readdata
		.av_waitrequest   (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_waitrequest),            //          .waitrequest
		.av_readdatavalid (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_readdatavalid),          //          .readdatavalid
		.av_byteenable    (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_byteenable),             //          .byteenable
		.av_burstcount    (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_burstcount),             //          .burstcount
		.av_debugaccess   (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_debugaccess),            //          .debugaccess
		.av_lock          (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_lock),                   //          .lock
		.cp_valid         (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_valid),         //        cp.valid
		.cp_data          (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_data),          //          .data
		.cp_startofpacket (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_startofpacket), //          .startofpacket
		.cp_endofpacket   (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_endofpacket),   //          .endofpacket
		.cp_ready         (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_ready),         //          .ready
		.rp_valid         (limiter_rsp_src_valid),                                                                       //        rp.valid
		.rp_data          (limiter_rsp_src_data),                                                                        //          .data
		.rp_channel       (limiter_rsp_src_channel),                                                                     //          .channel
		.rp_startofpacket (limiter_rsp_src_startofpacket),                                                               //          .startofpacket
		.rp_endofpacket   (limiter_rsp_src_endofpacket),                                                                 //          .endofpacket
		.rp_ready         (limiter_rsp_src_ready)                                                                        //          .ready
	);

	altera_merlin_master_agent #(
		.PKT_PROTECTION_H          (81),
		.PKT_PROTECTION_L          (81),
		.PKT_BEGIN_BURST           (74),
		.PKT_BURSTWRAP_H           (73),
		.PKT_BURSTWRAP_L           (70),
		.PKT_BYTE_CNT_H            (69),
		.PKT_BYTE_CNT_L            (66),
		.PKT_ADDR_H                (60),
		.PKT_ADDR_L                (36),
		.PKT_TRANS_COMPRESSED_READ (61),
		.PKT_TRANS_POSTED          (62),
		.PKT_TRANS_WRITE           (63),
		.PKT_TRANS_READ            (64),
		.PKT_TRANS_LOCK            (65),
		.PKT_DATA_H                (31),
		.PKT_DATA_L                (0),
		.PKT_BYTEEN_H              (35),
		.PKT_BYTEEN_L              (32),
		.PKT_SRC_ID_H              (77),
		.PKT_SRC_ID_L              (75),
		.PKT_DEST_ID_H             (80),
		.PKT_DEST_ID_L             (78),
		.ST_DATA_W                 (82),
		.ST_CHANNEL_W              (6),
		.AV_BURSTCOUNT_W           (3),
		.SUPPRESS_0_BYTEEN_RSP     (1),
		.ID                        (2),
		.BURSTWRAP_VALUE           (15)
	) nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent (
		.clk              (clk_clk),                                                                              //       clk.clk
		.reset            (rst_controller_reset_out_reset),                                                       // clk_reset.reset
		.av_address       (nios2_qsys_0_data_master_translator_avalon_universal_master_0_address),                //        av.address
		.av_write         (nios2_qsys_0_data_master_translator_avalon_universal_master_0_write),                  //          .write
		.av_read          (nios2_qsys_0_data_master_translator_avalon_universal_master_0_read),                   //          .read
		.av_writedata     (nios2_qsys_0_data_master_translator_avalon_universal_master_0_writedata),              //          .writedata
		.av_readdata      (nios2_qsys_0_data_master_translator_avalon_universal_master_0_readdata),               //          .readdata
		.av_waitrequest   (nios2_qsys_0_data_master_translator_avalon_universal_master_0_waitrequest),            //          .waitrequest
		.av_readdatavalid (nios2_qsys_0_data_master_translator_avalon_universal_master_0_readdatavalid),          //          .readdatavalid
		.av_byteenable    (nios2_qsys_0_data_master_translator_avalon_universal_master_0_byteenable),             //          .byteenable
		.av_burstcount    (nios2_qsys_0_data_master_translator_avalon_universal_master_0_burstcount),             //          .burstcount
		.av_debugaccess   (nios2_qsys_0_data_master_translator_avalon_universal_master_0_debugaccess),            //          .debugaccess
		.av_lock          (nios2_qsys_0_data_master_translator_avalon_universal_master_0_lock),                   //          .lock
		.cp_valid         (nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_valid),         //        cp.valid
		.cp_data          (nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_data),          //          .data
		.cp_startofpacket (nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_startofpacket), //          .startofpacket
		.cp_endofpacket   (nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_endofpacket),   //          .endofpacket
		.cp_ready         (nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_ready),         //          .ready
		.rp_valid         (limiter_001_rsp_src_valid),                                                            //        rp.valid
		.rp_data          (limiter_001_rsp_src_data),                                                             //          .data
		.rp_channel       (limiter_001_rsp_src_channel),                                                          //          .channel
		.rp_startofpacket (limiter_001_rsp_src_startofpacket),                                                    //          .startofpacket
		.rp_endofpacket   (limiter_001_rsp_src_endofpacket),                                                      //          .endofpacket
		.rp_ready         (limiter_001_rsp_src_ready)                                                             //          .ready
	);

	altera_merlin_slave_agent #(
		.PKT_DATA_H                (31),
		.PKT_DATA_L                (0),
		.PKT_BEGIN_BURST           (74),
		.PKT_SYMBOL_W              (8),
		.PKT_BYTEEN_H              (35),
		.PKT_BYTEEN_L              (32),
		.PKT_ADDR_H                (60),
		.PKT_ADDR_L                (36),
		.PKT_TRANS_COMPRESSED_READ (61),
		.PKT_TRANS_POSTED          (62),
		.PKT_TRANS_WRITE           (63),
		.PKT_TRANS_READ            (64),
		.PKT_TRANS_LOCK            (65),
		.PKT_SRC_ID_H              (77),
		.PKT_SRC_ID_L              (75),
		.PKT_DEST_ID_H             (80),
		.PKT_DEST_ID_L             (78),
		.PKT_BURSTWRAP_H           (73),
		.PKT_BURSTWRAP_L           (70),
		.PKT_BYTE_CNT_H            (69),
		.PKT_BYTE_CNT_L            (66),
		.PKT_PROTECTION_H          (81),
		.PKT_PROTECTION_L          (81),
		.ST_CHANNEL_W              (6),
		.ST_DATA_W                 (82),
		.AVS_BURSTCOUNT_W          (3),
		.SUPPRESS_0_BYTEEN_CMD     (0),
		.PREVENT_FIFO_OVERFLOW     (1)
	) nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent (
		.clk                     (clk_clk),                                                                                             //             clk.clk
		.reset                   (rst_controller_reset_out_reset),                                                                      //       clk_reset.reset
		.m0_address              (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_address),                 //              m0.address
		.m0_burstcount           (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_burstcount),              //                .burstcount
		.m0_byteenable           (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_byteenable),              //                .byteenable
		.m0_debugaccess          (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_debugaccess),             //                .debugaccess
		.m0_lock                 (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_lock),                    //                .lock
		.m0_readdata             (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_readdata),                //                .readdata
		.m0_readdatavalid        (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_readdatavalid),           //                .readdatavalid
		.m0_read                 (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_read),                    //                .read
		.m0_waitrequest          (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_waitrequest),             //                .waitrequest
		.m0_writedata            (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_writedata),               //                .writedata
		.m0_write                (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_m0_write),                   //                .write
		.rp_endofpacket          (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_endofpacket),             //              rp.endofpacket
		.rp_ready                (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_ready),                   //                .ready
		.rp_valid                (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_valid),                   //                .valid
		.rp_data                 (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_data),                    //                .data
		.rp_startofpacket        (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_startofpacket),           //                .startofpacket
		.cp_ready                (cmd_xbar_mux_src_ready),                                                                              //              cp.ready
		.cp_valid                (cmd_xbar_mux_src_valid),                                                                              //                .valid
		.cp_data                 (cmd_xbar_mux_src_data),                                                                               //                .data
		.cp_startofpacket        (cmd_xbar_mux_src_startofpacket),                                                                      //                .startofpacket
		.cp_endofpacket          (cmd_xbar_mux_src_endofpacket),                                                                        //                .endofpacket
		.cp_channel              (cmd_xbar_mux_src_channel),                                                                            //                .channel
		.rf_sink_ready           (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //         rf_sink.ready
		.rf_sink_valid           (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //                .valid
		.rf_sink_startofpacket   (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //                .startofpacket
		.rf_sink_endofpacket     (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //                .endofpacket
		.rf_sink_data            (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //                .data
		.rf_source_ready         (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_ready),            //       rf_source.ready
		.rf_source_valid         (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_valid),            //                .valid
		.rf_source_startofpacket (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //                .startofpacket
		.rf_source_endofpacket   (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //                .endofpacket
		.rf_source_data          (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_data),             //                .data
		.rdata_fifo_sink_ready   (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       // rdata_fifo_sink.ready
		.rdata_fifo_sink_valid   (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_sink_data    (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data),        //                .data
		.rdata_fifo_src_ready    (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       //  rdata_fifo_src.ready
		.rdata_fifo_src_valid    (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_src_data     (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data)         //                .data
	);

	altera_avalon_sc_fifo #(
		.SYMBOLS_PER_BEAT    (1),
		.BITS_PER_SYMBOL     (83),
		.FIFO_DEPTH          (2),
		.CHANNEL_WIDTH       (0),
		.ERROR_WIDTH         (0),
		.USE_PACKETS         (1),
		.USE_FILL_LEVEL      (0),
		.EMPTY_LATENCY       (1),
		.USE_MEMORY_BLOCKS   (0),
		.USE_STORE_FORWARD   (0),
		.USE_ALMOST_FULL_IF  (0),
		.USE_ALMOST_EMPTY_IF (0)
	) nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo (
		.clk               (clk_clk),                                                                                             //       clk.clk
		.reset             (rst_controller_reset_out_reset),                                                                      // clk_reset.reset
		.in_data           (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_data),             //        in.data
		.in_valid          (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_valid),            //          .valid
		.in_ready          (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_ready),            //          .ready
		.in_startofpacket  (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //          .startofpacket
		.in_endofpacket    (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //          .endofpacket
		.out_data          (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //       out.data
		.out_valid         (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //          .valid
		.out_ready         (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //          .ready
		.out_startofpacket (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //          .startofpacket
		.out_endofpacket   (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //          .endofpacket
		.csr_address       (2'b00),                                                                                               // (terminated)
		.csr_read          (1'b0),                                                                                                // (terminated)
		.csr_write         (1'b0),                                                                                                // (terminated)
		.csr_readdata      (),                                                                                                    // (terminated)
		.csr_writedata     (32'b00000000000000000000000000000000),                                                                // (terminated)
		.almost_full_data  (),                                                                                                    // (terminated)
		.almost_empty_data (),                                                                                                    // (terminated)
		.in_empty          (1'b0),                                                                                                // (terminated)
		.out_empty         (),                                                                                                    // (terminated)
		.in_error          (1'b0),                                                                                                // (terminated)
		.out_error         (),                                                                                                    // (terminated)
		.in_channel        (1'b0),                                                                                                // (terminated)
		.out_channel       ()                                                                                                     // (terminated)
	);

	altera_merlin_slave_agent #(
		.PKT_DATA_H                (31),
		.PKT_DATA_L                (0),
		.PKT_BEGIN_BURST           (74),
		.PKT_SYMBOL_W              (8),
		.PKT_BYTEEN_H              (35),
		.PKT_BYTEEN_L              (32),
		.PKT_ADDR_H                (60),
		.PKT_ADDR_L                (36),
		.PKT_TRANS_COMPRESSED_READ (61),
		.PKT_TRANS_POSTED          (62),
		.PKT_TRANS_WRITE           (63),
		.PKT_TRANS_READ            (64),
		.PKT_TRANS_LOCK            (65),
		.PKT_SRC_ID_H              (77),
		.PKT_SRC_ID_L              (75),
		.PKT_DEST_ID_H             (80),
		.PKT_DEST_ID_L             (78),
		.PKT_BURSTWRAP_H           (73),
		.PKT_BURSTWRAP_L           (70),
		.PKT_BYTE_CNT_H            (69),
		.PKT_BYTE_CNT_L            (66),
		.PKT_PROTECTION_H          (81),
		.PKT_PROTECTION_L          (81),
		.ST_CHANNEL_W              (6),
		.ST_DATA_W                 (82),
		.AVS_BURSTCOUNT_W          (3),
		.SUPPRESS_0_BYTEEN_CMD     (0),
		.PREVENT_FIFO_OVERFLOW     (1)
	) uart_0_s1_translator_avalon_universal_slave_0_agent (
		.clk                     (clk_clk),                                                                        //             clk.clk
		.reset                   (rst_controller_reset_out_reset),                                                 //       clk_reset.reset
		.m0_address              (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_address),                 //              m0.address
		.m0_burstcount           (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_burstcount),              //                .burstcount
		.m0_byteenable           (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_byteenable),              //                .byteenable
		.m0_debugaccess          (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_debugaccess),             //                .debugaccess
		.m0_lock                 (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_lock),                    //                .lock
		.m0_readdata             (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_readdata),                //                .readdata
		.m0_readdatavalid        (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_readdatavalid),           //                .readdatavalid
		.m0_read                 (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_read),                    //                .read
		.m0_waitrequest          (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_waitrequest),             //                .waitrequest
		.m0_writedata            (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_writedata),               //                .writedata
		.m0_write                (uart_0_s1_translator_avalon_universal_slave_0_agent_m0_write),                   //                .write
		.rp_endofpacket          (uart_0_s1_translator_avalon_universal_slave_0_agent_rp_endofpacket),             //              rp.endofpacket
		.rp_ready                (uart_0_s1_translator_avalon_universal_slave_0_agent_rp_ready),                   //                .ready
		.rp_valid                (uart_0_s1_translator_avalon_universal_slave_0_agent_rp_valid),                   //                .valid
		.rp_data                 (uart_0_s1_translator_avalon_universal_slave_0_agent_rp_data),                    //                .data
		.rp_startofpacket        (uart_0_s1_translator_avalon_universal_slave_0_agent_rp_startofpacket),           //                .startofpacket
		.cp_ready                (cmd_xbar_mux_001_src_ready),                                                     //              cp.ready
		.cp_valid                (cmd_xbar_mux_001_src_valid),                                                     //                .valid
		.cp_data                 (cmd_xbar_mux_001_src_data),                                                      //                .data
		.cp_startofpacket        (cmd_xbar_mux_001_src_startofpacket),                                             //                .startofpacket
		.cp_endofpacket          (cmd_xbar_mux_001_src_endofpacket),                                               //                .endofpacket
		.cp_channel              (cmd_xbar_mux_001_src_channel),                                                   //                .channel
		.rf_sink_ready           (uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //         rf_sink.ready
		.rf_sink_valid           (uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //                .valid
		.rf_sink_startofpacket   (uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //                .startofpacket
		.rf_sink_endofpacket     (uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //                .endofpacket
		.rf_sink_data            (uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //                .data
		.rf_source_ready         (uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_ready),            //       rf_source.ready
		.rf_source_valid         (uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_valid),            //                .valid
		.rf_source_startofpacket (uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //                .startofpacket
		.rf_source_endofpacket   (uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //                .endofpacket
		.rf_source_data          (uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_data),             //                .data
		.rdata_fifo_sink_ready   (uart_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       // rdata_fifo_sink.ready
		.rdata_fifo_sink_valid   (uart_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_sink_data    (uart_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data),        //                .data
		.rdata_fifo_src_ready    (uart_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       //  rdata_fifo_src.ready
		.rdata_fifo_src_valid    (uart_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_src_data     (uart_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data)         //                .data
	);

	altera_avalon_sc_fifo #(
		.SYMBOLS_PER_BEAT    (1),
		.BITS_PER_SYMBOL     (83),
		.FIFO_DEPTH          (2),
		.CHANNEL_WIDTH       (0),
		.ERROR_WIDTH         (0),
		.USE_PACKETS         (1),
		.USE_FILL_LEVEL      (0),
		.EMPTY_LATENCY       (1),
		.USE_MEMORY_BLOCKS   (0),
		.USE_STORE_FORWARD   (0),
		.USE_ALMOST_FULL_IF  (0),
		.USE_ALMOST_EMPTY_IF (0)
	) uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo (
		.clk               (clk_clk),                                                                        //       clk.clk
		.reset             (rst_controller_reset_out_reset),                                                 // clk_reset.reset
		.in_data           (uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_data),             //        in.data
		.in_valid          (uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_valid),            //          .valid
		.in_ready          (uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_ready),            //          .ready
		.in_startofpacket  (uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //          .startofpacket
		.in_endofpacket    (uart_0_s1_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //          .endofpacket
		.out_data          (uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //       out.data
		.out_valid         (uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //          .valid
		.out_ready         (uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //          .ready
		.out_startofpacket (uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //          .startofpacket
		.out_endofpacket   (uart_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //          .endofpacket
		.csr_address       (2'b00),                                                                          // (terminated)
		.csr_read          (1'b0),                                                                           // (terminated)
		.csr_write         (1'b0),                                                                           // (terminated)
		.csr_readdata      (),                                                                               // (terminated)
		.csr_writedata     (32'b00000000000000000000000000000000),                                           // (terminated)
		.almost_full_data  (),                                                                               // (terminated)
		.almost_empty_data (),                                                                               // (terminated)
		.in_empty          (1'b0),                                                                           // (terminated)
		.out_empty         (),                                                                               // (terminated)
		.in_error          (1'b0),                                                                           // (terminated)
		.out_error         (),                                                                               // (terminated)
		.in_channel        (1'b0),                                                                           // (terminated)
		.out_channel       ()                                                                                // (terminated)
	);

	altera_merlin_slave_agent #(
		.PKT_DATA_H                (31),
		.PKT_DATA_L                (0),
		.PKT_BEGIN_BURST           (74),
		.PKT_SYMBOL_W              (8),
		.PKT_BYTEEN_H              (35),
		.PKT_BYTEEN_L              (32),
		.PKT_ADDR_H                (60),
		.PKT_ADDR_L                (36),
		.PKT_TRANS_COMPRESSED_READ (61),
		.PKT_TRANS_POSTED          (62),
		.PKT_TRANS_WRITE           (63),
		.PKT_TRANS_READ            (64),
		.PKT_TRANS_LOCK            (65),
		.PKT_SRC_ID_H              (77),
		.PKT_SRC_ID_L              (75),
		.PKT_DEST_ID_H             (80),
		.PKT_DEST_ID_L             (78),
		.PKT_BURSTWRAP_H           (73),
		.PKT_BURSTWRAP_L           (70),
		.PKT_BYTE_CNT_H            (69),
		.PKT_BYTE_CNT_L            (66),
		.PKT_PROTECTION_H          (81),
		.PKT_PROTECTION_L          (81),
		.ST_CHANNEL_W              (6),
		.ST_DATA_W                 (82),
		.AVS_BURSTCOUNT_W          (3),
		.SUPPRESS_0_BYTEEN_CMD     (0),
		.PREVENT_FIFO_OVERFLOW     (1)
	) onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent (
		.clk                     (clk_clk),                                                                                  //             clk.clk
		.reset                   (rst_controller_001_reset_out_reset),                                                       //       clk_reset.reset
		.m0_address              (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_address),                 //              m0.address
		.m0_burstcount           (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_burstcount),              //                .burstcount
		.m0_byteenable           (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_byteenable),              //                .byteenable
		.m0_debugaccess          (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_debugaccess),             //                .debugaccess
		.m0_lock                 (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_lock),                    //                .lock
		.m0_readdata             (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_readdata),                //                .readdata
		.m0_readdatavalid        (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_readdatavalid),           //                .readdatavalid
		.m0_read                 (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_read),                    //                .read
		.m0_waitrequest          (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_waitrequest),             //                .waitrequest
		.m0_writedata            (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_writedata),               //                .writedata
		.m0_write                (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_m0_write),                   //                .write
		.rp_endofpacket          (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_endofpacket),             //              rp.endofpacket
		.rp_ready                (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_ready),                   //                .ready
		.rp_valid                (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_valid),                   //                .valid
		.rp_data                 (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_data),                    //                .data
		.rp_startofpacket        (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_startofpacket),           //                .startofpacket
		.cp_ready                (cmd_xbar_mux_002_src_ready),                                                               //              cp.ready
		.cp_valid                (cmd_xbar_mux_002_src_valid),                                                               //                .valid
		.cp_data                 (cmd_xbar_mux_002_src_data),                                                                //                .data
		.cp_startofpacket        (cmd_xbar_mux_002_src_startofpacket),                                                       //                .startofpacket
		.cp_endofpacket          (cmd_xbar_mux_002_src_endofpacket),                                                         //                .endofpacket
		.cp_channel              (cmd_xbar_mux_002_src_channel),                                                             //                .channel
		.rf_sink_ready           (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //         rf_sink.ready
		.rf_sink_valid           (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //                .valid
		.rf_sink_startofpacket   (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //                .startofpacket
		.rf_sink_endofpacket     (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //                .endofpacket
		.rf_sink_data            (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //                .data
		.rf_source_ready         (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_ready),            //       rf_source.ready
		.rf_source_valid         (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_valid),            //                .valid
		.rf_source_startofpacket (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //                .startofpacket
		.rf_source_endofpacket   (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //                .endofpacket
		.rf_source_data          (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_data),             //                .data
		.rdata_fifo_sink_ready   (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       // rdata_fifo_sink.ready
		.rdata_fifo_sink_valid   (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_sink_data    (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data),        //                .data
		.rdata_fifo_src_ready    (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       //  rdata_fifo_src.ready
		.rdata_fifo_src_valid    (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_src_data     (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data)         //                .data
	);

	altera_avalon_sc_fifo #(
		.SYMBOLS_PER_BEAT    (1),
		.BITS_PER_SYMBOL     (83),
		.FIFO_DEPTH          (2),
		.CHANNEL_WIDTH       (0),
		.ERROR_WIDTH         (0),
		.USE_PACKETS         (1),
		.USE_FILL_LEVEL      (0),
		.EMPTY_LATENCY       (1),
		.USE_MEMORY_BLOCKS   (0),
		.USE_STORE_FORWARD   (0),
		.USE_ALMOST_FULL_IF  (0),
		.USE_ALMOST_EMPTY_IF (0)
	) onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo (
		.clk               (clk_clk),                                                                                  //       clk.clk
		.reset             (rst_controller_001_reset_out_reset),                                                       // clk_reset.reset
		.in_data           (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_data),             //        in.data
		.in_valid          (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_valid),            //          .valid
		.in_ready          (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_ready),            //          .ready
		.in_startofpacket  (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //          .startofpacket
		.in_endofpacket    (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //          .endofpacket
		.out_data          (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //       out.data
		.out_valid         (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //          .valid
		.out_ready         (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //          .ready
		.out_startofpacket (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //          .startofpacket
		.out_endofpacket   (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //          .endofpacket
		.csr_address       (2'b00),                                                                                    // (terminated)
		.csr_read          (1'b0),                                                                                     // (terminated)
		.csr_write         (1'b0),                                                                                     // (terminated)
		.csr_readdata      (),                                                                                         // (terminated)
		.csr_writedata     (32'b00000000000000000000000000000000),                                                     // (terminated)
		.almost_full_data  (),                                                                                         // (terminated)
		.almost_empty_data (),                                                                                         // (terminated)
		.in_empty          (1'b0),                                                                                     // (terminated)
		.out_empty         (),                                                                                         // (terminated)
		.in_error          (1'b0),                                                                                     // (terminated)
		.out_error         (),                                                                                         // (terminated)
		.in_channel        (1'b0),                                                                                     // (terminated)
		.out_channel       ()                                                                                          // (terminated)
	);

	altera_merlin_slave_agent #(
		.PKT_DATA_H                (31),
		.PKT_DATA_L                (0),
		.PKT_BEGIN_BURST           (74),
		.PKT_SYMBOL_W              (8),
		.PKT_BYTEEN_H              (35),
		.PKT_BYTEEN_L              (32),
		.PKT_ADDR_H                (60),
		.PKT_ADDR_L                (36),
		.PKT_TRANS_COMPRESSED_READ (61),
		.PKT_TRANS_POSTED          (62),
		.PKT_TRANS_WRITE           (63),
		.PKT_TRANS_READ            (64),
		.PKT_TRANS_LOCK            (65),
		.PKT_SRC_ID_H              (77),
		.PKT_SRC_ID_L              (75),
		.PKT_DEST_ID_H             (80),
		.PKT_DEST_ID_L             (78),
		.PKT_BURSTWRAP_H           (73),
		.PKT_BURSTWRAP_L           (70),
		.PKT_BYTE_CNT_H            (69),
		.PKT_BYTE_CNT_L            (66),
		.PKT_PROTECTION_H          (81),
		.PKT_PROTECTION_L          (81),
		.ST_CHANNEL_W              (6),
		.ST_DATA_W                 (82),
		.AVS_BURSTCOUNT_W          (3),
		.SUPPRESS_0_BYTEEN_CMD     (0),
		.PREVENT_FIFO_OVERFLOW     (1)
	) pio_0_s1_translator_avalon_universal_slave_0_agent (
		.clk                     (clk_clk),                                                                       //             clk.clk
		.reset                   (rst_controller_001_reset_out_reset),                                            //       clk_reset.reset
		.m0_address              (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_address),                 //              m0.address
		.m0_burstcount           (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_burstcount),              //                .burstcount
		.m0_byteenable           (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_byteenable),              //                .byteenable
		.m0_debugaccess          (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_debugaccess),             //                .debugaccess
		.m0_lock                 (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_lock),                    //                .lock
		.m0_readdata             (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_readdata),                //                .readdata
		.m0_readdatavalid        (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_readdatavalid),           //                .readdatavalid
		.m0_read                 (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_read),                    //                .read
		.m0_waitrequest          (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_waitrequest),             //                .waitrequest
		.m0_writedata            (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_writedata),               //                .writedata
		.m0_write                (pio_0_s1_translator_avalon_universal_slave_0_agent_m0_write),                   //                .write
		.rp_endofpacket          (pio_0_s1_translator_avalon_universal_slave_0_agent_rp_endofpacket),             //              rp.endofpacket
		.rp_ready                (pio_0_s1_translator_avalon_universal_slave_0_agent_rp_ready),                   //                .ready
		.rp_valid                (pio_0_s1_translator_avalon_universal_slave_0_agent_rp_valid),                   //                .valid
		.rp_data                 (pio_0_s1_translator_avalon_universal_slave_0_agent_rp_data),                    //                .data
		.rp_startofpacket        (pio_0_s1_translator_avalon_universal_slave_0_agent_rp_startofpacket),           //                .startofpacket
		.cp_ready                (cmd_xbar_mux_003_src_ready),                                                    //              cp.ready
		.cp_valid                (cmd_xbar_mux_003_src_valid),                                                    //                .valid
		.cp_data                 (cmd_xbar_mux_003_src_data),                                                     //                .data
		.cp_startofpacket        (cmd_xbar_mux_003_src_startofpacket),                                            //                .startofpacket
		.cp_endofpacket          (cmd_xbar_mux_003_src_endofpacket),                                              //                .endofpacket
		.cp_channel              (cmd_xbar_mux_003_src_channel),                                                  //                .channel
		.rf_sink_ready           (pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //         rf_sink.ready
		.rf_sink_valid           (pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //                .valid
		.rf_sink_startofpacket   (pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //                .startofpacket
		.rf_sink_endofpacket     (pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //                .endofpacket
		.rf_sink_data            (pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //                .data
		.rf_source_ready         (pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_ready),            //       rf_source.ready
		.rf_source_valid         (pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_valid),            //                .valid
		.rf_source_startofpacket (pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //                .startofpacket
		.rf_source_endofpacket   (pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //                .endofpacket
		.rf_source_data          (pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_data),             //                .data
		.rdata_fifo_sink_ready   (pio_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       // rdata_fifo_sink.ready
		.rdata_fifo_sink_valid   (pio_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_sink_data    (pio_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data),        //                .data
		.rdata_fifo_src_ready    (pio_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       //  rdata_fifo_src.ready
		.rdata_fifo_src_valid    (pio_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_src_data     (pio_0_s1_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data)         //                .data
	);

	altera_avalon_sc_fifo #(
		.SYMBOLS_PER_BEAT    (1),
		.BITS_PER_SYMBOL     (83),
		.FIFO_DEPTH          (2),
		.CHANNEL_WIDTH       (0),
		.ERROR_WIDTH         (0),
		.USE_PACKETS         (1),
		.USE_FILL_LEVEL      (0),
		.EMPTY_LATENCY       (1),
		.USE_MEMORY_BLOCKS   (0),
		.USE_STORE_FORWARD   (0),
		.USE_ALMOST_FULL_IF  (0),
		.USE_ALMOST_EMPTY_IF (0)
	) pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo (
		.clk               (clk_clk),                                                                       //       clk.clk
		.reset             (rst_controller_001_reset_out_reset),                                            // clk_reset.reset
		.in_data           (pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_data),             //        in.data
		.in_valid          (pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_valid),            //          .valid
		.in_ready          (pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_ready),            //          .ready
		.in_startofpacket  (pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //          .startofpacket
		.in_endofpacket    (pio_0_s1_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //          .endofpacket
		.out_data          (pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //       out.data
		.out_valid         (pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //          .valid
		.out_ready         (pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //          .ready
		.out_startofpacket (pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //          .startofpacket
		.out_endofpacket   (pio_0_s1_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //          .endofpacket
		.csr_address       (2'b00),                                                                         // (terminated)
		.csr_read          (1'b0),                                                                          // (terminated)
		.csr_write         (1'b0),                                                                          // (terminated)
		.csr_readdata      (),                                                                              // (terminated)
		.csr_writedata     (32'b00000000000000000000000000000000),                                          // (terminated)
		.almost_full_data  (),                                                                              // (terminated)
		.almost_empty_data (),                                                                              // (terminated)
		.in_empty          (1'b0),                                                                          // (terminated)
		.out_empty         (),                                                                              // (terminated)
		.in_error          (1'b0),                                                                          // (terminated)
		.out_error         (),                                                                              // (terminated)
		.in_channel        (1'b0),                                                                          // (terminated)
		.out_channel       ()                                                                               // (terminated)
	);

	altera_merlin_slave_agent #(
		.PKT_DATA_H                (63),
		.PKT_DATA_L                (0),
		.PKT_BEGIN_BURST           (110),
		.PKT_SYMBOL_W              (8),
		.PKT_BYTEEN_H              (71),
		.PKT_BYTEEN_L              (64),
		.PKT_ADDR_H                (96),
		.PKT_ADDR_L                (72),
		.PKT_TRANS_COMPRESSED_READ (97),
		.PKT_TRANS_POSTED          (98),
		.PKT_TRANS_WRITE           (99),
		.PKT_TRANS_READ            (100),
		.PKT_TRANS_LOCK            (101),
		.PKT_SRC_ID_H              (113),
		.PKT_SRC_ID_L              (111),
		.PKT_DEST_ID_H             (116),
		.PKT_DEST_ID_L             (114),
		.PKT_BURSTWRAP_H           (109),
		.PKT_BURSTWRAP_L           (106),
		.PKT_BYTE_CNT_H            (105),
		.PKT_BYTE_CNT_L            (102),
		.PKT_PROTECTION_H          (117),
		.PKT_PROTECTION_L          (117),
		.ST_CHANNEL_W              (6),
		.ST_DATA_W                 (118),
		.AVS_BURSTCOUNT_W          (4),
		.SUPPRESS_0_BYTEEN_CMD     (0),
		.PREVENT_FIFO_OVERFLOW     (1)
	) barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent (
		.clk                     (clk_clk),                                                                                          //             clk.clk
		.reset                   (rst_controller_reset_out_reset),                                                                   //       clk_reset.reset
		.m0_address              (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_address),                 //              m0.address
		.m0_burstcount           (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_burstcount),              //                .burstcount
		.m0_byteenable           (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_byteenable),              //                .byteenable
		.m0_debugaccess          (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_debugaccess),             //                .debugaccess
		.m0_lock                 (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_lock),                    //                .lock
		.m0_readdata             (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_readdata),                //                .readdata
		.m0_readdatavalid        (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_readdatavalid),           //                .readdatavalid
		.m0_read                 (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_read),                    //                .read
		.m0_waitrequest          (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_waitrequest),             //                .waitrequest
		.m0_writedata            (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_writedata),               //                .writedata
		.m0_write                (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_m0_write),                   //                .write
		.rp_endofpacket          (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_endofpacket),             //              rp.endofpacket
		.rp_ready                (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_ready),                   //                .ready
		.rp_valid                (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_valid),                   //                .valid
		.rp_data                 (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_data),                    //                .data
		.rp_startofpacket        (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_startofpacket),           //                .startofpacket
		.cp_ready                (width_adapter_src_ready),                                                                          //              cp.ready
		.cp_valid                (width_adapter_src_valid),                                                                          //                .valid
		.cp_data                 (width_adapter_src_data),                                                                           //                .data
		.cp_startofpacket        (width_adapter_src_startofpacket),                                                                  //                .startofpacket
		.cp_endofpacket          (width_adapter_src_endofpacket),                                                                    //                .endofpacket
		.cp_channel              (width_adapter_src_channel),                                                                        //                .channel
		.rf_sink_ready           (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //         rf_sink.ready
		.rf_sink_valid           (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //                .valid
		.rf_sink_startofpacket   (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //                .startofpacket
		.rf_sink_endofpacket     (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //                .endofpacket
		.rf_sink_data            (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //                .data
		.rf_source_ready         (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_ready),            //       rf_source.ready
		.rf_source_valid         (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_valid),            //                .valid
		.rf_source_startofpacket (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //                .startofpacket
		.rf_source_endofpacket   (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //                .endofpacket
		.rf_source_data          (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_data),             //                .data
		.rdata_fifo_sink_ready   (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       // rdata_fifo_sink.ready
		.rdata_fifo_sink_valid   (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_sink_data    (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data),        //                .data
		.rdata_fifo_src_ready    (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       //  rdata_fifo_src.ready
		.rdata_fifo_src_valid    (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_src_data     (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data)         //                .data
	);

	altera_avalon_sc_fifo #(
		.SYMBOLS_PER_BEAT    (1),
		.BITS_PER_SYMBOL     (119),
		.FIFO_DEPTH          (2),
		.CHANNEL_WIDTH       (0),
		.ERROR_WIDTH         (0),
		.USE_PACKETS         (1),
		.USE_FILL_LEVEL      (0),
		.EMPTY_LATENCY       (1),
		.USE_MEMORY_BLOCKS   (0),
		.USE_STORE_FORWARD   (0),
		.USE_ALMOST_FULL_IF  (0),
		.USE_ALMOST_EMPTY_IF (0)
	) barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo (
		.clk               (clk_clk),                                                                                          //       clk.clk
		.reset             (rst_controller_reset_out_reset),                                                                   // clk_reset.reset
		.in_data           (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_data),             //        in.data
		.in_valid          (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_valid),            //          .valid
		.in_ready          (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_ready),            //          .ready
		.in_startofpacket  (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //          .startofpacket
		.in_endofpacket    (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //          .endofpacket
		.out_data          (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //       out.data
		.out_valid         (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //          .valid
		.out_ready         (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //          .ready
		.out_startofpacket (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //          .startofpacket
		.out_endofpacket   (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //          .endofpacket
		.csr_address       (2'b00),                                                                                            // (terminated)
		.csr_read          (1'b0),                                                                                             // (terminated)
		.csr_write         (1'b0),                                                                                             // (terminated)
		.csr_readdata      (),                                                                                                 // (terminated)
		.csr_writedata     (32'b00000000000000000000000000000000),                                                             // (terminated)
		.almost_full_data  (),                                                                                                 // (terminated)
		.almost_empty_data (),                                                                                                 // (terminated)
		.in_empty          (1'b0),                                                                                             // (terminated)
		.out_empty         (),                                                                                                 // (terminated)
		.in_error          (1'b0),                                                                                             // (terminated)
		.out_error         (),                                                                                                 // (terminated)
		.in_channel        (1'b0),                                                                                             // (terminated)
		.out_channel       ()                                                                                                  // (terminated)
	);

	altera_merlin_slave_agent #(
		.PKT_DATA_H                (31),
		.PKT_DATA_L                (0),
		.PKT_BEGIN_BURST           (74),
		.PKT_SYMBOL_W              (8),
		.PKT_BYTEEN_H              (35),
		.PKT_BYTEEN_L              (32),
		.PKT_ADDR_H                (60),
		.PKT_ADDR_L                (36),
		.PKT_TRANS_COMPRESSED_READ (61),
		.PKT_TRANS_POSTED          (62),
		.PKT_TRANS_WRITE           (63),
		.PKT_TRANS_READ            (64),
		.PKT_TRANS_LOCK            (65),
		.PKT_SRC_ID_H              (77),
		.PKT_SRC_ID_L              (75),
		.PKT_DEST_ID_H             (80),
		.PKT_DEST_ID_L             (78),
		.PKT_BURSTWRAP_H           (73),
		.PKT_BURSTWRAP_L           (70),
		.PKT_BYTE_CNT_H            (69),
		.PKT_BYTE_CNT_L            (66),
		.PKT_PROTECTION_H          (81),
		.PKT_PROTECTION_L          (81),
		.ST_CHANNEL_W              (6),
		.ST_DATA_W                 (82),
		.AVS_BURSTCOUNT_W          (3),
		.SUPPRESS_0_BYTEEN_CMD     (0),
		.PREVENT_FIFO_OVERFLOW     (1)
	) epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent (
		.clk                     (clk_clk),                                                                                                        //             clk.clk
		.reset                   (rst_controller_reset_out_reset),                                                                                 //       clk_reset.reset
		.m0_address              (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_address),                 //              m0.address
		.m0_burstcount           (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_burstcount),              //                .burstcount
		.m0_byteenable           (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_byteenable),              //                .byteenable
		.m0_debugaccess          (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_debugaccess),             //                .debugaccess
		.m0_lock                 (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_lock),                    //                .lock
		.m0_readdata             (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_readdata),                //                .readdata
		.m0_readdatavalid        (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_readdatavalid),           //                .readdatavalid
		.m0_read                 (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_read),                    //                .read
		.m0_waitrequest          (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_waitrequest),             //                .waitrequest
		.m0_writedata            (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_writedata),               //                .writedata
		.m0_write                (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_m0_write),                   //                .write
		.rp_endofpacket          (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_endofpacket),             //              rp.endofpacket
		.rp_ready                (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_ready),                   //                .ready
		.rp_valid                (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_valid),                   //                .valid
		.rp_data                 (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_data),                    //                .data
		.rp_startofpacket        (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_startofpacket),           //                .startofpacket
		.cp_ready                (cmd_xbar_mux_005_src_ready),                                                                                     //              cp.ready
		.cp_valid                (cmd_xbar_mux_005_src_valid),                                                                                     //                .valid
		.cp_data                 (cmd_xbar_mux_005_src_data),                                                                                      //                .data
		.cp_startofpacket        (cmd_xbar_mux_005_src_startofpacket),                                                                             //                .startofpacket
		.cp_endofpacket          (cmd_xbar_mux_005_src_endofpacket),                                                                               //                .endofpacket
		.cp_channel              (cmd_xbar_mux_005_src_channel),                                                                                   //                .channel
		.rf_sink_ready           (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //         rf_sink.ready
		.rf_sink_valid           (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //                .valid
		.rf_sink_startofpacket   (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //                .startofpacket
		.rf_sink_endofpacket     (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //                .endofpacket
		.rf_sink_data            (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //                .data
		.rf_source_ready         (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_ready),            //       rf_source.ready
		.rf_source_valid         (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_valid),            //                .valid
		.rf_source_startofpacket (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //                .startofpacket
		.rf_source_endofpacket   (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //                .endofpacket
		.rf_source_data          (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_data),             //                .data
		.rdata_fifo_sink_ready   (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       // rdata_fifo_sink.ready
		.rdata_fifo_sink_valid   (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_sink_data    (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data),        //                .data
		.rdata_fifo_src_ready    (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rdata_fifo_src_ready),       //  rdata_fifo_src.ready
		.rdata_fifo_src_valid    (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rdata_fifo_src_valid),       //                .valid
		.rdata_fifo_src_data     (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rdata_fifo_src_data)         //                .data
	);

	altera_avalon_sc_fifo #(
		.SYMBOLS_PER_BEAT    (1),
		.BITS_PER_SYMBOL     (83),
		.FIFO_DEPTH          (2),
		.CHANNEL_WIDTH       (0),
		.ERROR_WIDTH         (0),
		.USE_PACKETS         (1),
		.USE_FILL_LEVEL      (0),
		.EMPTY_LATENCY       (1),
		.USE_MEMORY_BLOCKS   (0),
		.USE_STORE_FORWARD   (0),
		.USE_ALMOST_FULL_IF  (0),
		.USE_ALMOST_EMPTY_IF (0)
	) epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo (
		.clk               (clk_clk),                                                                                                        //       clk.clk
		.reset             (rst_controller_reset_out_reset),                                                                                 // clk_reset.reset
		.in_data           (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_data),             //        in.data
		.in_valid          (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_valid),            //          .valid
		.in_ready          (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_ready),            //          .ready
		.in_startofpacket  (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_startofpacket),    //          .startofpacket
		.in_endofpacket    (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rf_source_endofpacket),      //          .endofpacket
		.out_data          (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_data),          //       out.data
		.out_valid         (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_valid),         //          .valid
		.out_ready         (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_ready),         //          .ready
		.out_startofpacket (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_startofpacket), //          .startofpacket
		.out_endofpacket   (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rsp_fifo_out_endofpacket),   //          .endofpacket
		.csr_address       (2'b00),                                                                                                          // (terminated)
		.csr_read          (1'b0),                                                                                                           // (terminated)
		.csr_write         (1'b0),                                                                                                           // (terminated)
		.csr_readdata      (),                                                                                                               // (terminated)
		.csr_writedata     (32'b00000000000000000000000000000000),                                                                           // (terminated)
		.almost_full_data  (),                                                                                                               // (terminated)
		.almost_empty_data (),                                                                                                               // (terminated)
		.in_empty          (1'b0),                                                                                                           // (terminated)
		.out_empty         (),                                                                                                               // (terminated)
		.in_error          (1'b0),                                                                                                           // (terminated)
		.out_error         (),                                                                                                               // (terminated)
		.in_channel        (1'b0),                                                                                                           // (terminated)
		.out_channel       ()                                                                                                                // (terminated)
	);

	qsys_barco_addr_router addr_router (
		.sink_ready         (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_ready),         //      sink.ready
		.sink_valid         (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_valid),         //          .valid
		.sink_data          (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_data),          //          .data
		.sink_startofpacket (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_startofpacket), //          .startofpacket
		.sink_endofpacket   (nios2_qsys_0_instruction_master_translator_avalon_universal_master_0_agent_cp_endofpacket),   //          .endofpacket
		.clk                (clk_clk),                                                                                     //       clk.clk
		.reset              (rst_controller_reset_out_reset),                                                              // clk_reset.reset
		.src_ready          (addr_router_src_ready),                                                                       //       src.ready
		.src_valid          (addr_router_src_valid),                                                                       //          .valid
		.src_data           (addr_router_src_data),                                                                        //          .data
		.src_channel        (addr_router_src_channel),                                                                     //          .channel
		.src_startofpacket  (addr_router_src_startofpacket),                                                               //          .startofpacket
		.src_endofpacket    (addr_router_src_endofpacket)                                                                  //          .endofpacket
	);

	qsys_barco_addr_router addr_router_001 (
		.sink_ready         (nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_ready),         //      sink.ready
		.sink_valid         (nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_valid),         //          .valid
		.sink_data          (nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_data),          //          .data
		.sink_startofpacket (nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_startofpacket), //          .startofpacket
		.sink_endofpacket   (nios2_qsys_0_data_master_translator_avalon_universal_master_0_agent_cp_endofpacket),   //          .endofpacket
		.clk                (clk_clk),                                                                              //       clk.clk
		.reset              (rst_controller_reset_out_reset),                                                       // clk_reset.reset
		.src_ready          (addr_router_001_src_ready),                                                            //       src.ready
		.src_valid          (addr_router_001_src_valid),                                                            //          .valid
		.src_data           (addr_router_001_src_data),                                                             //          .data
		.src_channel        (addr_router_001_src_channel),                                                          //          .channel
		.src_startofpacket  (addr_router_001_src_startofpacket),                                                    //          .startofpacket
		.src_endofpacket    (addr_router_001_src_endofpacket)                                                       //          .endofpacket
	);

	qsys_barco_id_router id_router (
		.sink_ready         (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_ready),         //      sink.ready
		.sink_valid         (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_valid),         //          .valid
		.sink_data          (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_data),          //          .data
		.sink_startofpacket (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_startofpacket), //          .startofpacket
		.sink_endofpacket   (nios2_qsys_0_jtag_debug_module_translator_avalon_universal_slave_0_agent_rp_endofpacket),   //          .endofpacket
		.clk                (clk_clk),                                                                                   //       clk.clk
		.reset              (rst_controller_reset_out_reset),                                                            // clk_reset.reset
		.src_ready          (id_router_src_ready),                                                                       //       src.ready
		.src_valid          (id_router_src_valid),                                                                       //          .valid
		.src_data           (id_router_src_data),                                                                        //          .data
		.src_channel        (id_router_src_channel),                                                                     //          .channel
		.src_startofpacket  (id_router_src_startofpacket),                                                               //          .startofpacket
		.src_endofpacket    (id_router_src_endofpacket)                                                                  //          .endofpacket
	);

	qsys_barco_id_router id_router_001 (
		.sink_ready         (uart_0_s1_translator_avalon_universal_slave_0_agent_rp_ready),         //      sink.ready
		.sink_valid         (uart_0_s1_translator_avalon_universal_slave_0_agent_rp_valid),         //          .valid
		.sink_data          (uart_0_s1_translator_avalon_universal_slave_0_agent_rp_data),          //          .data
		.sink_startofpacket (uart_0_s1_translator_avalon_universal_slave_0_agent_rp_startofpacket), //          .startofpacket
		.sink_endofpacket   (uart_0_s1_translator_avalon_universal_slave_0_agent_rp_endofpacket),   //          .endofpacket
		.clk                (clk_clk),                                                              //       clk.clk
		.reset              (rst_controller_reset_out_reset),                                       // clk_reset.reset
		.src_ready          (id_router_001_src_ready),                                              //       src.ready
		.src_valid          (id_router_001_src_valid),                                              //          .valid
		.src_data           (id_router_001_src_data),                                               //          .data
		.src_channel        (id_router_001_src_channel),                                            //          .channel
		.src_startofpacket  (id_router_001_src_startofpacket),                                      //          .startofpacket
		.src_endofpacket    (id_router_001_src_endofpacket)                                         //          .endofpacket
	);

	qsys_barco_id_router id_router_002 (
		.sink_ready         (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_ready),         //      sink.ready
		.sink_valid         (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_valid),         //          .valid
		.sink_data          (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_data),          //          .data
		.sink_startofpacket (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_startofpacket), //          .startofpacket
		.sink_endofpacket   (onchip_memory2_0_s1_translator_avalon_universal_slave_0_agent_rp_endofpacket),   //          .endofpacket
		.clk                (clk_clk),                                                                        //       clk.clk
		.reset              (rst_controller_001_reset_out_reset),                                             // clk_reset.reset
		.src_ready          (id_router_002_src_ready),                                                        //       src.ready
		.src_valid          (id_router_002_src_valid),                                                        //          .valid
		.src_data           (id_router_002_src_data),                                                         //          .data
		.src_channel        (id_router_002_src_channel),                                                      //          .channel
		.src_startofpacket  (id_router_002_src_startofpacket),                                                //          .startofpacket
		.src_endofpacket    (id_router_002_src_endofpacket)                                                   //          .endofpacket
	);

	qsys_barco_id_router id_router_003 (
		.sink_ready         (pio_0_s1_translator_avalon_universal_slave_0_agent_rp_ready),         //      sink.ready
		.sink_valid         (pio_0_s1_translator_avalon_universal_slave_0_agent_rp_valid),         //          .valid
		.sink_data          (pio_0_s1_translator_avalon_universal_slave_0_agent_rp_data),          //          .data
		.sink_startofpacket (pio_0_s1_translator_avalon_universal_slave_0_agent_rp_startofpacket), //          .startofpacket
		.sink_endofpacket   (pio_0_s1_translator_avalon_universal_slave_0_agent_rp_endofpacket),   //          .endofpacket
		.clk                (clk_clk),                                                             //       clk.clk
		.reset              (rst_controller_001_reset_out_reset),                                  // clk_reset.reset
		.src_ready          (id_router_003_src_ready),                                             //       src.ready
		.src_valid          (id_router_003_src_valid),                                             //          .valid
		.src_data           (id_router_003_src_data),                                              //          .data
		.src_channel        (id_router_003_src_channel),                                           //          .channel
		.src_startofpacket  (id_router_003_src_startofpacket),                                     //          .startofpacket
		.src_endofpacket    (id_router_003_src_endofpacket)                                        //          .endofpacket
	);

	qsys_barco_id_router_004 id_router_004 (
		.sink_ready         (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_ready),         //      sink.ready
		.sink_valid         (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_valid),         //          .valid
		.sink_data          (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_data),          //          .data
		.sink_startofpacket (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_startofpacket), //          .startofpacket
		.sink_endofpacket   (barco_avalon_0_barco_avalon_translator_avalon_universal_slave_0_agent_rp_endofpacket),   //          .endofpacket
		.clk                (clk_clk),                                                                                //       clk.clk
		.reset              (rst_controller_reset_out_reset),                                                         // clk_reset.reset
		.src_ready          (id_router_004_src_ready),                                                                //       src.ready
		.src_valid          (id_router_004_src_valid),                                                                //          .valid
		.src_data           (id_router_004_src_data),                                                                 //          .data
		.src_channel        (id_router_004_src_channel),                                                              //          .channel
		.src_startofpacket  (id_router_004_src_startofpacket),                                                        //          .startofpacket
		.src_endofpacket    (id_router_004_src_endofpacket)                                                           //          .endofpacket
	);

	qsys_barco_id_router id_router_005 (
		.sink_ready         (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_ready),         //      sink.ready
		.sink_valid         (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_valid),         //          .valid
		.sink_data          (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_data),          //          .data
		.sink_startofpacket (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_startofpacket), //          .startofpacket
		.sink_endofpacket   (epcs_flash_controller_0_epcs_control_port_translator_avalon_universal_slave_0_agent_rp_endofpacket),   //          .endofpacket
		.clk                (clk_clk),                                                                                              //       clk.clk
		.reset              (rst_controller_reset_out_reset),                                                                       // clk_reset.reset
		.src_ready          (id_router_005_src_ready),                                                                              //       src.ready
		.src_valid          (id_router_005_src_valid),                                                                              //          .valid
		.src_data           (id_router_005_src_data),                                                                               //          .data
		.src_channel        (id_router_005_src_channel),                                                                            //          .channel
		.src_startofpacket  (id_router_005_src_startofpacket),                                                                      //          .startofpacket
		.src_endofpacket    (id_router_005_src_endofpacket)                                                                         //          .endofpacket
	);

	altera_merlin_traffic_limiter #(
		.PKT_DEST_ID_H             (80),
		.PKT_DEST_ID_L             (78),
		.PKT_TRANS_POSTED          (62),
		.MAX_OUTSTANDING_RESPONSES (5),
		.PIPELINED                 (0),
		.ST_DATA_W                 (82),
		.ST_CHANNEL_W              (6),
		.VALID_WIDTH               (6),
		.ENFORCE_ORDER             (0),
		.PKT_BYTE_CNT_H            (69),
		.PKT_BYTE_CNT_L            (66),
		.PKT_BYTEEN_H              (35),
		.PKT_BYTEEN_L              (32)
	) limiter (
		.clk                    (clk_clk),                        //       clk.clk
		.reset                  (rst_controller_reset_out_reset), // clk_reset.reset
		.cmd_sink_ready         (addr_router_src_ready),          //  cmd_sink.ready
		.cmd_sink_valid         (addr_router_src_valid),          //          .valid
		.cmd_sink_data          (addr_router_src_data),           //          .data
		.cmd_sink_channel       (addr_router_src_channel),        //          .channel
		.cmd_sink_startofpacket (addr_router_src_startofpacket),  //          .startofpacket
		.cmd_sink_endofpacket   (addr_router_src_endofpacket),    //          .endofpacket
		.cmd_src_ready          (limiter_cmd_src_ready),          //   cmd_src.ready
		.cmd_src_data           (limiter_cmd_src_data),           //          .data
		.cmd_src_channel        (limiter_cmd_src_channel),        //          .channel
		.cmd_src_startofpacket  (limiter_cmd_src_startofpacket),  //          .startofpacket
		.cmd_src_endofpacket    (limiter_cmd_src_endofpacket),    //          .endofpacket
		.rsp_sink_ready         (rsp_xbar_mux_src_ready),         //  rsp_sink.ready
		.rsp_sink_valid         (rsp_xbar_mux_src_valid),         //          .valid
		.rsp_sink_channel       (rsp_xbar_mux_src_channel),       //          .channel
		.rsp_sink_data          (rsp_xbar_mux_src_data),          //          .data
		.rsp_sink_startofpacket (rsp_xbar_mux_src_startofpacket), //          .startofpacket
		.rsp_sink_endofpacket   (rsp_xbar_mux_src_endofpacket),   //          .endofpacket
		.rsp_src_ready          (limiter_rsp_src_ready),          //   rsp_src.ready
		.rsp_src_valid          (limiter_rsp_src_valid),          //          .valid
		.rsp_src_data           (limiter_rsp_src_data),           //          .data
		.rsp_src_channel        (limiter_rsp_src_channel),        //          .channel
		.rsp_src_startofpacket  (limiter_rsp_src_startofpacket),  //          .startofpacket
		.rsp_src_endofpacket    (limiter_rsp_src_endofpacket),    //          .endofpacket
		.cmd_src_valid          (limiter_cmd_valid_data)          // cmd_valid.data
	);

	altera_merlin_traffic_limiter #(
		.PKT_DEST_ID_H             (80),
		.PKT_DEST_ID_L             (78),
		.PKT_TRANS_POSTED          (62),
		.MAX_OUTSTANDING_RESPONSES (5),
		.PIPELINED                 (0),
		.ST_DATA_W                 (82),
		.ST_CHANNEL_W              (6),
		.VALID_WIDTH               (6),
		.ENFORCE_ORDER             (0),
		.PKT_BYTE_CNT_H            (69),
		.PKT_BYTE_CNT_L            (66),
		.PKT_BYTEEN_H              (35),
		.PKT_BYTEEN_L              (32)
	) limiter_001 (
		.clk                    (clk_clk),                            //       clk.clk
		.reset                  (rst_controller_reset_out_reset),     // clk_reset.reset
		.cmd_sink_ready         (addr_router_001_src_ready),          //  cmd_sink.ready
		.cmd_sink_valid         (addr_router_001_src_valid),          //          .valid
		.cmd_sink_data          (addr_router_001_src_data),           //          .data
		.cmd_sink_channel       (addr_router_001_src_channel),        //          .channel
		.cmd_sink_startofpacket (addr_router_001_src_startofpacket),  //          .startofpacket
		.cmd_sink_endofpacket   (addr_router_001_src_endofpacket),    //          .endofpacket
		.cmd_src_ready          (limiter_001_cmd_src_ready),          //   cmd_src.ready
		.cmd_src_data           (limiter_001_cmd_src_data),           //          .data
		.cmd_src_channel        (limiter_001_cmd_src_channel),        //          .channel
		.cmd_src_startofpacket  (limiter_001_cmd_src_startofpacket),  //          .startofpacket
		.cmd_src_endofpacket    (limiter_001_cmd_src_endofpacket),    //          .endofpacket
		.rsp_sink_ready         (rsp_xbar_mux_001_src_ready),         //  rsp_sink.ready
		.rsp_sink_valid         (rsp_xbar_mux_001_src_valid),         //          .valid
		.rsp_sink_channel       (rsp_xbar_mux_001_src_channel),       //          .channel
		.rsp_sink_data          (rsp_xbar_mux_001_src_data),          //          .data
		.rsp_sink_startofpacket (rsp_xbar_mux_001_src_startofpacket), //          .startofpacket
		.rsp_sink_endofpacket   (rsp_xbar_mux_001_src_endofpacket),   //          .endofpacket
		.rsp_src_ready          (limiter_001_rsp_src_ready),          //   rsp_src.ready
		.rsp_src_valid          (limiter_001_rsp_src_valid),          //          .valid
		.rsp_src_data           (limiter_001_rsp_src_data),           //          .data
		.rsp_src_channel        (limiter_001_rsp_src_channel),        //          .channel
		.rsp_src_startofpacket  (limiter_001_rsp_src_startofpacket),  //          .startofpacket
		.rsp_src_endofpacket    (limiter_001_rsp_src_endofpacket),    //          .endofpacket
		.cmd_src_valid          (limiter_001_cmd_valid_data)          // cmd_valid.data
	);

	altera_reset_controller #(
		.NUM_RESET_INPUTS        (2),
		.OUTPUT_RESET_SYNC_EDGES ("deassert"),
		.SYNC_DEPTH              (2)
	) rst_controller (
		.reset_in0  (nios2_qsys_0_jtag_debug_module_reset_reset), // reset_in0.reset
		.reset_in1  (~reset_reset_n),                             // reset_in1.reset
		.clk        (clk_clk),                                    //       clk.clk
		.reset_out  (rst_controller_reset_out_reset),             // reset_out.reset
		.reset_in2  (1'b0),                                       // (terminated)
		.reset_in3  (1'b0),                                       // (terminated)
		.reset_in4  (1'b0),                                       // (terminated)
		.reset_in5  (1'b0),                                       // (terminated)
		.reset_in6  (1'b0),                                       // (terminated)
		.reset_in7  (1'b0),                                       // (terminated)
		.reset_in8  (1'b0),                                       // (terminated)
		.reset_in9  (1'b0),                                       // (terminated)
		.reset_in10 (1'b0),                                       // (terminated)
		.reset_in11 (1'b0),                                       // (terminated)
		.reset_in12 (1'b0),                                       // (terminated)
		.reset_in13 (1'b0),                                       // (terminated)
		.reset_in14 (1'b0),                                       // (terminated)
		.reset_in15 (1'b0)                                        // (terminated)
	);

	altera_reset_controller #(
		.NUM_RESET_INPUTS        (1),
		.OUTPUT_RESET_SYNC_EDGES ("deassert"),
		.SYNC_DEPTH              (2)
	) rst_controller_001 (
		.reset_in0  (~reset_reset_n),                     // reset_in0.reset
		.clk        (clk_clk),                            //       clk.clk
		.reset_out  (rst_controller_001_reset_out_reset), // reset_out.reset
		.reset_in1  (1'b0),                               // (terminated)
		.reset_in2  (1'b0),                               // (terminated)
		.reset_in3  (1'b0),                               // (terminated)
		.reset_in4  (1'b0),                               // (terminated)
		.reset_in5  (1'b0),                               // (terminated)
		.reset_in6  (1'b0),                               // (terminated)
		.reset_in7  (1'b0),                               // (terminated)
		.reset_in8  (1'b0),                               // (terminated)
		.reset_in9  (1'b0),                               // (terminated)
		.reset_in10 (1'b0),                               // (terminated)
		.reset_in11 (1'b0),                               // (terminated)
		.reset_in12 (1'b0),                               // (terminated)
		.reset_in13 (1'b0),                               // (terminated)
		.reset_in14 (1'b0),                               // (terminated)
		.reset_in15 (1'b0)                                // (terminated)
	);

	qsys_barco_cmd_xbar_demux cmd_xbar_demux (
		.clk                (clk_clk),                           //        clk.clk
		.reset              (rst_controller_reset_out_reset),    //  clk_reset.reset
		.sink_ready         (limiter_cmd_src_ready),             //       sink.ready
		.sink_channel       (limiter_cmd_src_channel),           //           .channel
		.sink_data          (limiter_cmd_src_data),              //           .data
		.sink_startofpacket (limiter_cmd_src_startofpacket),     //           .startofpacket
		.sink_endofpacket   (limiter_cmd_src_endofpacket),       //           .endofpacket
		.sink_valid         (limiter_cmd_valid_data),            // sink_valid.data
		.src0_ready         (cmd_xbar_demux_src0_ready),         //       src0.ready
		.src0_valid         (cmd_xbar_demux_src0_valid),         //           .valid
		.src0_data          (cmd_xbar_demux_src0_data),          //           .data
		.src0_channel       (cmd_xbar_demux_src0_channel),       //           .channel
		.src0_startofpacket (cmd_xbar_demux_src0_startofpacket), //           .startofpacket
		.src0_endofpacket   (cmd_xbar_demux_src0_endofpacket),   //           .endofpacket
		.src1_ready         (cmd_xbar_demux_src1_ready),         //       src1.ready
		.src1_valid         (cmd_xbar_demux_src1_valid),         //           .valid
		.src1_data          (cmd_xbar_demux_src1_data),          //           .data
		.src1_channel       (cmd_xbar_demux_src1_channel),       //           .channel
		.src1_startofpacket (cmd_xbar_demux_src1_startofpacket), //           .startofpacket
		.src1_endofpacket   (cmd_xbar_demux_src1_endofpacket),   //           .endofpacket
		.src2_ready         (cmd_xbar_demux_src2_ready),         //       src2.ready
		.src2_valid         (cmd_xbar_demux_src2_valid),         //           .valid
		.src2_data          (cmd_xbar_demux_src2_data),          //           .data
		.src2_channel       (cmd_xbar_demux_src2_channel),       //           .channel
		.src2_startofpacket (cmd_xbar_demux_src2_startofpacket), //           .startofpacket
		.src2_endofpacket   (cmd_xbar_demux_src2_endofpacket),   //           .endofpacket
		.src3_ready         (cmd_xbar_demux_src3_ready),         //       src3.ready
		.src3_valid         (cmd_xbar_demux_src3_valid),         //           .valid
		.src3_data          (cmd_xbar_demux_src3_data),          //           .data
		.src3_channel       (cmd_xbar_demux_src3_channel),       //           .channel
		.src3_startofpacket (cmd_xbar_demux_src3_startofpacket), //           .startofpacket
		.src3_endofpacket   (cmd_xbar_demux_src3_endofpacket),   //           .endofpacket
		.src4_ready         (cmd_xbar_demux_src4_ready),         //       src4.ready
		.src4_valid         (cmd_xbar_demux_src4_valid),         //           .valid
		.src4_data          (cmd_xbar_demux_src4_data),          //           .data
		.src4_channel       (cmd_xbar_demux_src4_channel),       //           .channel
		.src4_startofpacket (cmd_xbar_demux_src4_startofpacket), //           .startofpacket
		.src4_endofpacket   (cmd_xbar_demux_src4_endofpacket),   //           .endofpacket
		.src5_ready         (cmd_xbar_demux_src5_ready),         //       src5.ready
		.src5_valid         (cmd_xbar_demux_src5_valid),         //           .valid
		.src5_data          (cmd_xbar_demux_src5_data),          //           .data
		.src5_channel       (cmd_xbar_demux_src5_channel),       //           .channel
		.src5_startofpacket (cmd_xbar_demux_src5_startofpacket), //           .startofpacket
		.src5_endofpacket   (cmd_xbar_demux_src5_endofpacket)    //           .endofpacket
	);

	qsys_barco_cmd_xbar_demux cmd_xbar_demux_001 (
		.clk                (clk_clk),                               //        clk.clk
		.reset              (rst_controller_reset_out_reset),        //  clk_reset.reset
		.sink_ready         (limiter_001_cmd_src_ready),             //       sink.ready
		.sink_channel       (limiter_001_cmd_src_channel),           //           .channel
		.sink_data          (limiter_001_cmd_src_data),              //           .data
		.sink_startofpacket (limiter_001_cmd_src_startofpacket),     //           .startofpacket
		.sink_endofpacket   (limiter_001_cmd_src_endofpacket),       //           .endofpacket
		.sink_valid         (limiter_001_cmd_valid_data),            // sink_valid.data
		.src0_ready         (cmd_xbar_demux_001_src0_ready),         //       src0.ready
		.src0_valid         (cmd_xbar_demux_001_src0_valid),         //           .valid
		.src0_data          (cmd_xbar_demux_001_src0_data),          //           .data
		.src0_channel       (cmd_xbar_demux_001_src0_channel),       //           .channel
		.src0_startofpacket (cmd_xbar_demux_001_src0_startofpacket), //           .startofpacket
		.src0_endofpacket   (cmd_xbar_demux_001_src0_endofpacket),   //           .endofpacket
		.src1_ready         (cmd_xbar_demux_001_src1_ready),         //       src1.ready
		.src1_valid         (cmd_xbar_demux_001_src1_valid),         //           .valid
		.src1_data          (cmd_xbar_demux_001_src1_data),          //           .data
		.src1_channel       (cmd_xbar_demux_001_src1_channel),       //           .channel
		.src1_startofpacket (cmd_xbar_demux_001_src1_startofpacket), //           .startofpacket
		.src1_endofpacket   (cmd_xbar_demux_001_src1_endofpacket),   //           .endofpacket
		.src2_ready         (cmd_xbar_demux_001_src2_ready),         //       src2.ready
		.src2_valid         (cmd_xbar_demux_001_src2_valid),         //           .valid
		.src2_data          (cmd_xbar_demux_001_src2_data),          //           .data
		.src2_channel       (cmd_xbar_demux_001_src2_channel),       //           .channel
		.src2_startofpacket (cmd_xbar_demux_001_src2_startofpacket), //           .startofpacket
		.src2_endofpacket   (cmd_xbar_demux_001_src2_endofpacket),   //           .endofpacket
		.src3_ready         (cmd_xbar_demux_001_src3_ready),         //       src3.ready
		.src3_valid         (cmd_xbar_demux_001_src3_valid),         //           .valid
		.src3_data          (cmd_xbar_demux_001_src3_data),          //           .data
		.src3_channel       (cmd_xbar_demux_001_src3_channel),       //           .channel
		.src3_startofpacket (cmd_xbar_demux_001_src3_startofpacket), //           .startofpacket
		.src3_endofpacket   (cmd_xbar_demux_001_src3_endofpacket),   //           .endofpacket
		.src4_ready         (cmd_xbar_demux_001_src4_ready),         //       src4.ready
		.src4_valid         (cmd_xbar_demux_001_src4_valid),         //           .valid
		.src4_data          (cmd_xbar_demux_001_src4_data),          //           .data
		.src4_channel       (cmd_xbar_demux_001_src4_channel),       //           .channel
		.src4_startofpacket (cmd_xbar_demux_001_src4_startofpacket), //           .startofpacket
		.src4_endofpacket   (cmd_xbar_demux_001_src4_endofpacket),   //           .endofpacket
		.src5_ready         (cmd_xbar_demux_001_src5_ready),         //       src5.ready
		.src5_valid         (cmd_xbar_demux_001_src5_valid),         //           .valid
		.src5_data          (cmd_xbar_demux_001_src5_data),          //           .data
		.src5_channel       (cmd_xbar_demux_001_src5_channel),       //           .channel
		.src5_startofpacket (cmd_xbar_demux_001_src5_startofpacket), //           .startofpacket
		.src5_endofpacket   (cmd_xbar_demux_001_src5_endofpacket)    //           .endofpacket
	);

	qsys_barco_cmd_xbar_mux cmd_xbar_mux (
		.clk                 (clk_clk),                               //       clk.clk
		.reset               (rst_controller_reset_out_reset),        // clk_reset.reset
		.src_ready           (cmd_xbar_mux_src_ready),                //       src.ready
		.src_valid           (cmd_xbar_mux_src_valid),                //          .valid
		.src_data            (cmd_xbar_mux_src_data),                 //          .data
		.src_channel         (cmd_xbar_mux_src_channel),              //          .channel
		.src_startofpacket   (cmd_xbar_mux_src_startofpacket),        //          .startofpacket
		.src_endofpacket     (cmd_xbar_mux_src_endofpacket),          //          .endofpacket
		.sink0_ready         (cmd_xbar_demux_src0_ready),             //     sink0.ready
		.sink0_valid         (cmd_xbar_demux_src0_valid),             //          .valid
		.sink0_channel       (cmd_xbar_demux_src0_channel),           //          .channel
		.sink0_data          (cmd_xbar_demux_src0_data),              //          .data
		.sink0_startofpacket (cmd_xbar_demux_src0_startofpacket),     //          .startofpacket
		.sink0_endofpacket   (cmd_xbar_demux_src0_endofpacket),       //          .endofpacket
		.sink1_ready         (cmd_xbar_demux_001_src0_ready),         //     sink1.ready
		.sink1_valid         (cmd_xbar_demux_001_src0_valid),         //          .valid
		.sink1_channel       (cmd_xbar_demux_001_src0_channel),       //          .channel
		.sink1_data          (cmd_xbar_demux_001_src0_data),          //          .data
		.sink1_startofpacket (cmd_xbar_demux_001_src0_startofpacket), //          .startofpacket
		.sink1_endofpacket   (cmd_xbar_demux_001_src0_endofpacket)    //          .endofpacket
	);

	qsys_barco_cmd_xbar_mux cmd_xbar_mux_001 (
		.clk                 (clk_clk),                               //       clk.clk
		.reset               (rst_controller_reset_out_reset),        // clk_reset.reset
		.src_ready           (cmd_xbar_mux_001_src_ready),            //       src.ready
		.src_valid           (cmd_xbar_mux_001_src_valid),            //          .valid
		.src_data            (cmd_xbar_mux_001_src_data),             //          .data
		.src_channel         (cmd_xbar_mux_001_src_channel),          //          .channel
		.src_startofpacket   (cmd_xbar_mux_001_src_startofpacket),    //          .startofpacket
		.src_endofpacket     (cmd_xbar_mux_001_src_endofpacket),      //          .endofpacket
		.sink0_ready         (cmd_xbar_demux_src1_ready),             //     sink0.ready
		.sink0_valid         (cmd_xbar_demux_src1_valid),             //          .valid
		.sink0_channel       (cmd_xbar_demux_src1_channel),           //          .channel
		.sink0_data          (cmd_xbar_demux_src1_data),              //          .data
		.sink0_startofpacket (cmd_xbar_demux_src1_startofpacket),     //          .startofpacket
		.sink0_endofpacket   (cmd_xbar_demux_src1_endofpacket),       //          .endofpacket
		.sink1_ready         (cmd_xbar_demux_001_src1_ready),         //     sink1.ready
		.sink1_valid         (cmd_xbar_demux_001_src1_valid),         //          .valid
		.sink1_channel       (cmd_xbar_demux_001_src1_channel),       //          .channel
		.sink1_data          (cmd_xbar_demux_001_src1_data),          //          .data
		.sink1_startofpacket (cmd_xbar_demux_001_src1_startofpacket), //          .startofpacket
		.sink1_endofpacket   (cmd_xbar_demux_001_src1_endofpacket)    //          .endofpacket
	);

	qsys_barco_cmd_xbar_mux cmd_xbar_mux_002 (
		.clk                 (clk_clk),                               //       clk.clk
		.reset               (rst_controller_001_reset_out_reset),    // clk_reset.reset
		.src_ready           (cmd_xbar_mux_002_src_ready),            //       src.ready
		.src_valid           (cmd_xbar_mux_002_src_valid),            //          .valid
		.src_data            (cmd_xbar_mux_002_src_data),             //          .data
		.src_channel         (cmd_xbar_mux_002_src_channel),          //          .channel
		.src_startofpacket   (cmd_xbar_mux_002_src_startofpacket),    //          .startofpacket
		.src_endofpacket     (cmd_xbar_mux_002_src_endofpacket),      //          .endofpacket
		.sink0_ready         (cmd_xbar_demux_src2_ready),             //     sink0.ready
		.sink0_valid         (cmd_xbar_demux_src2_valid),             //          .valid
		.sink0_channel       (cmd_xbar_demux_src2_channel),           //          .channel
		.sink0_data          (cmd_xbar_demux_src2_data),              //          .data
		.sink0_startofpacket (cmd_xbar_demux_src2_startofpacket),     //          .startofpacket
		.sink0_endofpacket   (cmd_xbar_demux_src2_endofpacket),       //          .endofpacket
		.sink1_ready         (cmd_xbar_demux_001_src2_ready),         //     sink1.ready
		.sink1_valid         (cmd_xbar_demux_001_src2_valid),         //          .valid
		.sink1_channel       (cmd_xbar_demux_001_src2_channel),       //          .channel
		.sink1_data          (cmd_xbar_demux_001_src2_data),          //          .data
		.sink1_startofpacket (cmd_xbar_demux_001_src2_startofpacket), //          .startofpacket
		.sink1_endofpacket   (cmd_xbar_demux_001_src2_endofpacket)    //          .endofpacket
	);

	qsys_barco_cmd_xbar_mux cmd_xbar_mux_003 (
		.clk                 (clk_clk),                               //       clk.clk
		.reset               (rst_controller_001_reset_out_reset),    // clk_reset.reset
		.src_ready           (cmd_xbar_mux_003_src_ready),            //       src.ready
		.src_valid           (cmd_xbar_mux_003_src_valid),            //          .valid
		.src_data            (cmd_xbar_mux_003_src_data),             //          .data
		.src_channel         (cmd_xbar_mux_003_src_channel),          //          .channel
		.src_startofpacket   (cmd_xbar_mux_003_src_startofpacket),    //          .startofpacket
		.src_endofpacket     (cmd_xbar_mux_003_src_endofpacket),      //          .endofpacket
		.sink0_ready         (cmd_xbar_demux_src3_ready),             //     sink0.ready
		.sink0_valid         (cmd_xbar_demux_src3_valid),             //          .valid
		.sink0_channel       (cmd_xbar_demux_src3_channel),           //          .channel
		.sink0_data          (cmd_xbar_demux_src3_data),              //          .data
		.sink0_startofpacket (cmd_xbar_demux_src3_startofpacket),     //          .startofpacket
		.sink0_endofpacket   (cmd_xbar_demux_src3_endofpacket),       //          .endofpacket
		.sink1_ready         (cmd_xbar_demux_001_src3_ready),         //     sink1.ready
		.sink1_valid         (cmd_xbar_demux_001_src3_valid),         //          .valid
		.sink1_channel       (cmd_xbar_demux_001_src3_channel),       //          .channel
		.sink1_data          (cmd_xbar_demux_001_src3_data),          //          .data
		.sink1_startofpacket (cmd_xbar_demux_001_src3_startofpacket), //          .startofpacket
		.sink1_endofpacket   (cmd_xbar_demux_001_src3_endofpacket)    //          .endofpacket
	);

	qsys_barco_cmd_xbar_mux cmd_xbar_mux_004 (
		.clk                 (clk_clk),                               //       clk.clk
		.reset               (rst_controller_reset_out_reset),        // clk_reset.reset
		.src_ready           (cmd_xbar_mux_004_src_ready),            //       src.ready
		.src_valid           (cmd_xbar_mux_004_src_valid),            //          .valid
		.src_data            (cmd_xbar_mux_004_src_data),             //          .data
		.src_channel         (cmd_xbar_mux_004_src_channel),          //          .channel
		.src_startofpacket   (cmd_xbar_mux_004_src_startofpacket),    //          .startofpacket
		.src_endofpacket     (cmd_xbar_mux_004_src_endofpacket),      //          .endofpacket
		.sink0_ready         (cmd_xbar_demux_src4_ready),             //     sink0.ready
		.sink0_valid         (cmd_xbar_demux_src4_valid),             //          .valid
		.sink0_channel       (cmd_xbar_demux_src4_channel),           //          .channel
		.sink0_data          (cmd_xbar_demux_src4_data),              //          .data
		.sink0_startofpacket (cmd_xbar_demux_src4_startofpacket),     //          .startofpacket
		.sink0_endofpacket   (cmd_xbar_demux_src4_endofpacket),       //          .endofpacket
		.sink1_ready         (cmd_xbar_demux_001_src4_ready),         //     sink1.ready
		.sink1_valid         (cmd_xbar_demux_001_src4_valid),         //          .valid
		.sink1_channel       (cmd_xbar_demux_001_src4_channel),       //          .channel
		.sink1_data          (cmd_xbar_demux_001_src4_data),          //          .data
		.sink1_startofpacket (cmd_xbar_demux_001_src4_startofpacket), //          .startofpacket
		.sink1_endofpacket   (cmd_xbar_demux_001_src4_endofpacket)    //          .endofpacket
	);

	qsys_barco_cmd_xbar_mux cmd_xbar_mux_005 (
		.clk                 (clk_clk),                               //       clk.clk
		.reset               (rst_controller_reset_out_reset),        // clk_reset.reset
		.src_ready           (cmd_xbar_mux_005_src_ready),            //       src.ready
		.src_valid           (cmd_xbar_mux_005_src_valid),            //          .valid
		.src_data            (cmd_xbar_mux_005_src_data),             //          .data
		.src_channel         (cmd_xbar_mux_005_src_channel),          //          .channel
		.src_startofpacket   (cmd_xbar_mux_005_src_startofpacket),    //          .startofpacket
		.src_endofpacket     (cmd_xbar_mux_005_src_endofpacket),      //          .endofpacket
		.sink0_ready         (cmd_xbar_demux_src5_ready),             //     sink0.ready
		.sink0_valid         (cmd_xbar_demux_src5_valid),             //          .valid
		.sink0_channel       (cmd_xbar_demux_src5_channel),           //          .channel
		.sink0_data          (cmd_xbar_demux_src5_data),              //          .data
		.sink0_startofpacket (cmd_xbar_demux_src5_startofpacket),     //          .startofpacket
		.sink0_endofpacket   (cmd_xbar_demux_src5_endofpacket),       //          .endofpacket
		.sink1_ready         (cmd_xbar_demux_001_src5_ready),         //     sink1.ready
		.sink1_valid         (cmd_xbar_demux_001_src5_valid),         //          .valid
		.sink1_channel       (cmd_xbar_demux_001_src5_channel),       //          .channel
		.sink1_data          (cmd_xbar_demux_001_src5_data),          //          .data
		.sink1_startofpacket (cmd_xbar_demux_001_src5_startofpacket), //          .startofpacket
		.sink1_endofpacket   (cmd_xbar_demux_001_src5_endofpacket)    //          .endofpacket
	);

	qsys_barco_rsp_xbar_demux rsp_xbar_demux (
		.clk                (clk_clk),                           //       clk.clk
		.reset              (rst_controller_reset_out_reset),    // clk_reset.reset
		.sink_ready         (id_router_src_ready),               //      sink.ready
		.sink_channel       (id_router_src_channel),             //          .channel
		.sink_data          (id_router_src_data),                //          .data
		.sink_startofpacket (id_router_src_startofpacket),       //          .startofpacket
		.sink_endofpacket   (id_router_src_endofpacket),         //          .endofpacket
		.sink_valid         (id_router_src_valid),               //          .valid
		.src0_ready         (rsp_xbar_demux_src0_ready),         //      src0.ready
		.src0_valid         (rsp_xbar_demux_src0_valid),         //          .valid
		.src0_data          (rsp_xbar_demux_src0_data),          //          .data
		.src0_channel       (rsp_xbar_demux_src0_channel),       //          .channel
		.src0_startofpacket (rsp_xbar_demux_src0_startofpacket), //          .startofpacket
		.src0_endofpacket   (rsp_xbar_demux_src0_endofpacket),   //          .endofpacket
		.src1_ready         (rsp_xbar_demux_src1_ready),         //      src1.ready
		.src1_valid         (rsp_xbar_demux_src1_valid),         //          .valid
		.src1_data          (rsp_xbar_demux_src1_data),          //          .data
		.src1_channel       (rsp_xbar_demux_src1_channel),       //          .channel
		.src1_startofpacket (rsp_xbar_demux_src1_startofpacket), //          .startofpacket
		.src1_endofpacket   (rsp_xbar_demux_src1_endofpacket)    //          .endofpacket
	);

	qsys_barco_rsp_xbar_demux rsp_xbar_demux_001 (
		.clk                (clk_clk),                               //       clk.clk
		.reset              (rst_controller_reset_out_reset),        // clk_reset.reset
		.sink_ready         (id_router_001_src_ready),               //      sink.ready
		.sink_channel       (id_router_001_src_channel),             //          .channel
		.sink_data          (id_router_001_src_data),                //          .data
		.sink_startofpacket (id_router_001_src_startofpacket),       //          .startofpacket
		.sink_endofpacket   (id_router_001_src_endofpacket),         //          .endofpacket
		.sink_valid         (id_router_001_src_valid),               //          .valid
		.src0_ready         (rsp_xbar_demux_001_src0_ready),         //      src0.ready
		.src0_valid         (rsp_xbar_demux_001_src0_valid),         //          .valid
		.src0_data          (rsp_xbar_demux_001_src0_data),          //          .data
		.src0_channel       (rsp_xbar_demux_001_src0_channel),       //          .channel
		.src0_startofpacket (rsp_xbar_demux_001_src0_startofpacket), //          .startofpacket
		.src0_endofpacket   (rsp_xbar_demux_001_src0_endofpacket),   //          .endofpacket
		.src1_ready         (rsp_xbar_demux_001_src1_ready),         //      src1.ready
		.src1_valid         (rsp_xbar_demux_001_src1_valid),         //          .valid
		.src1_data          (rsp_xbar_demux_001_src1_data),          //          .data
		.src1_channel       (rsp_xbar_demux_001_src1_channel),       //          .channel
		.src1_startofpacket (rsp_xbar_demux_001_src1_startofpacket), //          .startofpacket
		.src1_endofpacket   (rsp_xbar_demux_001_src1_endofpacket)    //          .endofpacket
	);

	qsys_barco_rsp_xbar_demux rsp_xbar_demux_002 (
		.clk                (clk_clk),                               //       clk.clk
		.reset              (rst_controller_001_reset_out_reset),    // clk_reset.reset
		.sink_ready         (id_router_002_src_ready),               //      sink.ready
		.sink_channel       (id_router_002_src_channel),             //          .channel
		.sink_data          (id_router_002_src_data),                //          .data
		.sink_startofpacket (id_router_002_src_startofpacket),       //          .startofpacket
		.sink_endofpacket   (id_router_002_src_endofpacket),         //          .endofpacket
		.sink_valid         (id_router_002_src_valid),               //          .valid
		.src0_ready         (rsp_xbar_demux_002_src0_ready),         //      src0.ready
		.src0_valid         (rsp_xbar_demux_002_src0_valid),         //          .valid
		.src0_data          (rsp_xbar_demux_002_src0_data),          //          .data
		.src0_channel       (rsp_xbar_demux_002_src0_channel),       //          .channel
		.src0_startofpacket (rsp_xbar_demux_002_src0_startofpacket), //          .startofpacket
		.src0_endofpacket   (rsp_xbar_demux_002_src0_endofpacket),   //          .endofpacket
		.src1_ready         (rsp_xbar_demux_002_src1_ready),         //      src1.ready
		.src1_valid         (rsp_xbar_demux_002_src1_valid),         //          .valid
		.src1_data          (rsp_xbar_demux_002_src1_data),          //          .data
		.src1_channel       (rsp_xbar_demux_002_src1_channel),       //          .channel
		.src1_startofpacket (rsp_xbar_demux_002_src1_startofpacket), //          .startofpacket
		.src1_endofpacket   (rsp_xbar_demux_002_src1_endofpacket)    //          .endofpacket
	);

	qsys_barco_rsp_xbar_demux rsp_xbar_demux_003 (
		.clk                (clk_clk),                               //       clk.clk
		.reset              (rst_controller_001_reset_out_reset),    // clk_reset.reset
		.sink_ready         (id_router_003_src_ready),               //      sink.ready
		.sink_channel       (id_router_003_src_channel),             //          .channel
		.sink_data          (id_router_003_src_data),                //          .data
		.sink_startofpacket (id_router_003_src_startofpacket),       //          .startofpacket
		.sink_endofpacket   (id_router_003_src_endofpacket),         //          .endofpacket
		.sink_valid         (id_router_003_src_valid),               //          .valid
		.src0_ready         (rsp_xbar_demux_003_src0_ready),         //      src0.ready
		.src0_valid         (rsp_xbar_demux_003_src0_valid),         //          .valid
		.src0_data          (rsp_xbar_demux_003_src0_data),          //          .data
		.src0_channel       (rsp_xbar_demux_003_src0_channel),       //          .channel
		.src0_startofpacket (rsp_xbar_demux_003_src0_startofpacket), //          .startofpacket
		.src0_endofpacket   (rsp_xbar_demux_003_src0_endofpacket),   //          .endofpacket
		.src1_ready         (rsp_xbar_demux_003_src1_ready),         //      src1.ready
		.src1_valid         (rsp_xbar_demux_003_src1_valid),         //          .valid
		.src1_data          (rsp_xbar_demux_003_src1_data),          //          .data
		.src1_channel       (rsp_xbar_demux_003_src1_channel),       //          .channel
		.src1_startofpacket (rsp_xbar_demux_003_src1_startofpacket), //          .startofpacket
		.src1_endofpacket   (rsp_xbar_demux_003_src1_endofpacket)    //          .endofpacket
	);

	qsys_barco_rsp_xbar_demux rsp_xbar_demux_004 (
		.clk                (clk_clk),                               //       clk.clk
		.reset              (rst_controller_reset_out_reset),        // clk_reset.reset
		.sink_ready         (width_adapter_001_src_ready),           //      sink.ready
		.sink_channel       (width_adapter_001_src_channel),         //          .channel
		.sink_data          (width_adapter_001_src_data),            //          .data
		.sink_startofpacket (width_adapter_001_src_startofpacket),   //          .startofpacket
		.sink_endofpacket   (width_adapter_001_src_endofpacket),     //          .endofpacket
		.sink_valid         (width_adapter_001_src_valid),           //          .valid
		.src0_ready         (rsp_xbar_demux_004_src0_ready),         //      src0.ready
		.src0_valid         (rsp_xbar_demux_004_src0_valid),         //          .valid
		.src0_data          (rsp_xbar_demux_004_src0_data),          //          .data
		.src0_channel       (rsp_xbar_demux_004_src0_channel),       //          .channel
		.src0_startofpacket (rsp_xbar_demux_004_src0_startofpacket), //          .startofpacket
		.src0_endofpacket   (rsp_xbar_demux_004_src0_endofpacket),   //          .endofpacket
		.src1_ready         (rsp_xbar_demux_004_src1_ready),         //      src1.ready
		.src1_valid         (rsp_xbar_demux_004_src1_valid),         //          .valid
		.src1_data          (rsp_xbar_demux_004_src1_data),          //          .data
		.src1_channel       (rsp_xbar_demux_004_src1_channel),       //          .channel
		.src1_startofpacket (rsp_xbar_demux_004_src1_startofpacket), //          .startofpacket
		.src1_endofpacket   (rsp_xbar_demux_004_src1_endofpacket)    //          .endofpacket
	);

	qsys_barco_rsp_xbar_demux rsp_xbar_demux_005 (
		.clk                (clk_clk),                               //       clk.clk
		.reset              (rst_controller_reset_out_reset),        // clk_reset.reset
		.sink_ready         (id_router_005_src_ready),               //      sink.ready
		.sink_channel       (id_router_005_src_channel),             //          .channel
		.sink_data          (id_router_005_src_data),                //          .data
		.sink_startofpacket (id_router_005_src_startofpacket),       //          .startofpacket
		.sink_endofpacket   (id_router_005_src_endofpacket),         //          .endofpacket
		.sink_valid         (id_router_005_src_valid),               //          .valid
		.src0_ready         (rsp_xbar_demux_005_src0_ready),         //      src0.ready
		.src0_valid         (rsp_xbar_demux_005_src0_valid),         //          .valid
		.src0_data          (rsp_xbar_demux_005_src0_data),          //          .data
		.src0_channel       (rsp_xbar_demux_005_src0_channel),       //          .channel
		.src0_startofpacket (rsp_xbar_demux_005_src0_startofpacket), //          .startofpacket
		.src0_endofpacket   (rsp_xbar_demux_005_src0_endofpacket),   //          .endofpacket
		.src1_ready         (rsp_xbar_demux_005_src1_ready),         //      src1.ready
		.src1_valid         (rsp_xbar_demux_005_src1_valid),         //          .valid
		.src1_data          (rsp_xbar_demux_005_src1_data),          //          .data
		.src1_channel       (rsp_xbar_demux_005_src1_channel),       //          .channel
		.src1_startofpacket (rsp_xbar_demux_005_src1_startofpacket), //          .startofpacket
		.src1_endofpacket   (rsp_xbar_demux_005_src1_endofpacket)    //          .endofpacket
	);

	qsys_barco_rsp_xbar_mux rsp_xbar_mux (
		.clk                 (clk_clk),                               //       clk.clk
		.reset               (rst_controller_reset_out_reset),        // clk_reset.reset
		.src_ready           (rsp_xbar_mux_src_ready),                //       src.ready
		.src_valid           (rsp_xbar_mux_src_valid),                //          .valid
		.src_data            (rsp_xbar_mux_src_data),                 //          .data
		.src_channel         (rsp_xbar_mux_src_channel),              //          .channel
		.src_startofpacket   (rsp_xbar_mux_src_startofpacket),        //          .startofpacket
		.src_endofpacket     (rsp_xbar_mux_src_endofpacket),          //          .endofpacket
		.sink0_ready         (rsp_xbar_demux_src0_ready),             //     sink0.ready
		.sink0_valid         (rsp_xbar_demux_src0_valid),             //          .valid
		.sink0_channel       (rsp_xbar_demux_src0_channel),           //          .channel
		.sink0_data          (rsp_xbar_demux_src0_data),              //          .data
		.sink0_startofpacket (rsp_xbar_demux_src0_startofpacket),     //          .startofpacket
		.sink0_endofpacket   (rsp_xbar_demux_src0_endofpacket),       //          .endofpacket
		.sink1_ready         (rsp_xbar_demux_001_src0_ready),         //     sink1.ready
		.sink1_valid         (rsp_xbar_demux_001_src0_valid),         //          .valid
		.sink1_channel       (rsp_xbar_demux_001_src0_channel),       //          .channel
		.sink1_data          (rsp_xbar_demux_001_src0_data),          //          .data
		.sink1_startofpacket (rsp_xbar_demux_001_src0_startofpacket), //          .startofpacket
		.sink1_endofpacket   (rsp_xbar_demux_001_src0_endofpacket),   //          .endofpacket
		.sink2_ready         (rsp_xbar_demux_002_src0_ready),         //     sink2.ready
		.sink2_valid         (rsp_xbar_demux_002_src0_valid),         //          .valid
		.sink2_channel       (rsp_xbar_demux_002_src0_channel),       //          .channel
		.sink2_data          (rsp_xbar_demux_002_src0_data),          //          .data
		.sink2_startofpacket (rsp_xbar_demux_002_src0_startofpacket), //          .startofpacket
		.sink2_endofpacket   (rsp_xbar_demux_002_src0_endofpacket),   //          .endofpacket
		.sink3_ready         (rsp_xbar_demux_003_src0_ready),         //     sink3.ready
		.sink3_valid         (rsp_xbar_demux_003_src0_valid),         //          .valid
		.sink3_channel       (rsp_xbar_demux_003_src0_channel),       //          .channel
		.sink3_data          (rsp_xbar_demux_003_src0_data),          //          .data
		.sink3_startofpacket (rsp_xbar_demux_003_src0_startofpacket), //          .startofpacket
		.sink3_endofpacket   (rsp_xbar_demux_003_src0_endofpacket),   //          .endofpacket
		.sink4_ready         (rsp_xbar_demux_004_src0_ready),         //     sink4.ready
		.sink4_valid         (rsp_xbar_demux_004_src0_valid),         //          .valid
		.sink4_channel       (rsp_xbar_demux_004_src0_channel),       //          .channel
		.sink4_data          (rsp_xbar_demux_004_src0_data),          //          .data
		.sink4_startofpacket (rsp_xbar_demux_004_src0_startofpacket), //          .startofpacket
		.sink4_endofpacket   (rsp_xbar_demux_004_src0_endofpacket),   //          .endofpacket
		.sink5_ready         (rsp_xbar_demux_005_src0_ready),         //     sink5.ready
		.sink5_valid         (rsp_xbar_demux_005_src0_valid),         //          .valid
		.sink5_channel       (rsp_xbar_demux_005_src0_channel),       //          .channel
		.sink5_data          (rsp_xbar_demux_005_src0_data),          //          .data
		.sink5_startofpacket (rsp_xbar_demux_005_src0_startofpacket), //          .startofpacket
		.sink5_endofpacket   (rsp_xbar_demux_005_src0_endofpacket)    //          .endofpacket
	);

	qsys_barco_rsp_xbar_mux rsp_xbar_mux_001 (
		.clk                 (clk_clk),                               //       clk.clk
		.reset               (rst_controller_reset_out_reset),        // clk_reset.reset
		.src_ready           (rsp_xbar_mux_001_src_ready),            //       src.ready
		.src_valid           (rsp_xbar_mux_001_src_valid),            //          .valid
		.src_data            (rsp_xbar_mux_001_src_data),             //          .data
		.src_channel         (rsp_xbar_mux_001_src_channel),          //          .channel
		.src_startofpacket   (rsp_xbar_mux_001_src_startofpacket),    //          .startofpacket
		.src_endofpacket     (rsp_xbar_mux_001_src_endofpacket),      //          .endofpacket
		.sink0_ready         (rsp_xbar_demux_src1_ready),             //     sink0.ready
		.sink0_valid         (rsp_xbar_demux_src1_valid),             //          .valid
		.sink0_channel       (rsp_xbar_demux_src1_channel),           //          .channel
		.sink0_data          (rsp_xbar_demux_src1_data),              //          .data
		.sink0_startofpacket (rsp_xbar_demux_src1_startofpacket),     //          .startofpacket
		.sink0_endofpacket   (rsp_xbar_demux_src1_endofpacket),       //          .endofpacket
		.sink1_ready         (rsp_xbar_demux_001_src1_ready),         //     sink1.ready
		.sink1_valid         (rsp_xbar_demux_001_src1_valid),         //          .valid
		.sink1_channel       (rsp_xbar_demux_001_src1_channel),       //          .channel
		.sink1_data          (rsp_xbar_demux_001_src1_data),          //          .data
		.sink1_startofpacket (rsp_xbar_demux_001_src1_startofpacket), //          .startofpacket
		.sink1_endofpacket   (rsp_xbar_demux_001_src1_endofpacket),   //          .endofpacket
		.sink2_ready         (rsp_xbar_demux_002_src1_ready),         //     sink2.ready
		.sink2_valid         (rsp_xbar_demux_002_src1_valid),         //          .valid
		.sink2_channel       (rsp_xbar_demux_002_src1_channel),       //          .channel
		.sink2_data          (rsp_xbar_demux_002_src1_data),          //          .data
		.sink2_startofpacket (rsp_xbar_demux_002_src1_startofpacket), //          .startofpacket
		.sink2_endofpacket   (rsp_xbar_demux_002_src1_endofpacket),   //          .endofpacket
		.sink3_ready         (rsp_xbar_demux_003_src1_ready),         //     sink3.ready
		.sink3_valid         (rsp_xbar_demux_003_src1_valid),         //          .valid
		.sink3_channel       (rsp_xbar_demux_003_src1_channel),       //          .channel
		.sink3_data          (rsp_xbar_demux_003_src1_data),          //          .data
		.sink3_startofpacket (rsp_xbar_demux_003_src1_startofpacket), //          .startofpacket
		.sink3_endofpacket   (rsp_xbar_demux_003_src1_endofpacket),   //          .endofpacket
		.sink4_ready         (rsp_xbar_demux_004_src1_ready),         //     sink4.ready
		.sink4_valid         (rsp_xbar_demux_004_src1_valid),         //          .valid
		.sink4_channel       (rsp_xbar_demux_004_src1_channel),       //          .channel
		.sink4_data          (rsp_xbar_demux_004_src1_data),          //          .data
		.sink4_startofpacket (rsp_xbar_demux_004_src1_startofpacket), //          .startofpacket
		.sink4_endofpacket   (rsp_xbar_demux_004_src1_endofpacket),   //          .endofpacket
		.sink5_ready         (rsp_xbar_demux_005_src1_ready),         //     sink5.ready
		.sink5_valid         (rsp_xbar_demux_005_src1_valid),         //          .valid
		.sink5_channel       (rsp_xbar_demux_005_src1_channel),       //          .channel
		.sink5_data          (rsp_xbar_demux_005_src1_data),          //          .data
		.sink5_startofpacket (rsp_xbar_demux_005_src1_startofpacket), //          .startofpacket
		.sink5_endofpacket   (rsp_xbar_demux_005_src1_endofpacket)    //          .endofpacket
	);

	altera_merlin_width_adapter #(
		.IN_PKT_ADDR_H                 (60),
		.IN_PKT_ADDR_L                 (36),
		.IN_PKT_DATA_H                 (31),
		.IN_PKT_DATA_L                 (0),
		.IN_PKT_BYTEEN_H               (35),
		.IN_PKT_BYTEEN_L               (32),
		.IN_PKT_BYTE_CNT_H             (69),
		.IN_PKT_BYTE_CNT_L             (66),
		.IN_PKT_TRANS_COMPRESSED_READ  (61),
		.IN_PKT_BURSTWRAP_H            (73),
		.IN_PKT_BURSTWRAP_L            (70),
		.IN_ST_DATA_W                  (82),
		.OUT_PKT_ADDR_H                (96),
		.OUT_PKT_ADDR_L                (72),
		.OUT_PKT_DATA_H                (63),
		.OUT_PKT_DATA_L                (0),
		.OUT_PKT_BYTEEN_H              (71),
		.OUT_PKT_BYTEEN_L              (64),
		.OUT_PKT_BYTE_CNT_H            (105),
		.OUT_PKT_BYTE_CNT_L            (102),
		.OUT_PKT_TRANS_COMPRESSED_READ (97),
		.OUT_ST_DATA_W                 (118),
		.ST_CHANNEL_W                  (6),
		.OPTIMIZE_FOR_RSP              (0)
	) width_adapter (
		.clk               (clk_clk),                            //       clk.clk
		.reset             (rst_controller_reset_out_reset),     // clk_reset.reset
		.in_valid          (cmd_xbar_mux_004_src_valid),         //      sink.valid
		.in_channel        (cmd_xbar_mux_004_src_channel),       //          .channel
		.in_startofpacket  (cmd_xbar_mux_004_src_startofpacket), //          .startofpacket
		.in_endofpacket    (cmd_xbar_mux_004_src_endofpacket),   //          .endofpacket
		.in_ready          (cmd_xbar_mux_004_src_ready),         //          .ready
		.in_data           (cmd_xbar_mux_004_src_data),          //          .data
		.out_endofpacket   (width_adapter_src_endofpacket),      //       src.endofpacket
		.out_data          (width_adapter_src_data),             //          .data
		.out_channel       (width_adapter_src_channel),          //          .channel
		.out_valid         (width_adapter_src_valid),            //          .valid
		.out_ready         (width_adapter_src_ready),            //          .ready
		.out_startofpacket (width_adapter_src_startofpacket)     //          .startofpacket
	);

	altera_merlin_width_adapter #(
		.IN_PKT_ADDR_H                 (96),
		.IN_PKT_ADDR_L                 (72),
		.IN_PKT_DATA_H                 (63),
		.IN_PKT_DATA_L                 (0),
		.IN_PKT_BYTEEN_H               (71),
		.IN_PKT_BYTEEN_L               (64),
		.IN_PKT_BYTE_CNT_H             (105),
		.IN_PKT_BYTE_CNT_L             (102),
		.IN_PKT_TRANS_COMPRESSED_READ  (97),
		.IN_PKT_BURSTWRAP_H            (109),
		.IN_PKT_BURSTWRAP_L            (106),
		.IN_ST_DATA_W                  (118),
		.OUT_PKT_ADDR_H                (60),
		.OUT_PKT_ADDR_L                (36),
		.OUT_PKT_DATA_H                (31),
		.OUT_PKT_DATA_L                (0),
		.OUT_PKT_BYTEEN_H              (35),
		.OUT_PKT_BYTEEN_L              (32),
		.OUT_PKT_BYTE_CNT_H            (69),
		.OUT_PKT_BYTE_CNT_L            (66),
		.OUT_PKT_TRANS_COMPRESSED_READ (61),
		.OUT_ST_DATA_W                 (82),
		.ST_CHANNEL_W                  (6),
		.OPTIMIZE_FOR_RSP              (1)
	) width_adapter_001 (
		.clk               (clk_clk),                             //       clk.clk
		.reset             (rst_controller_reset_out_reset),      // clk_reset.reset
		.in_valid          (id_router_004_src_valid),             //      sink.valid
		.in_channel        (id_router_004_src_channel),           //          .channel
		.in_startofpacket  (id_router_004_src_startofpacket),     //          .startofpacket
		.in_endofpacket    (id_router_004_src_endofpacket),       //          .endofpacket
		.in_ready          (id_router_004_src_ready),             //          .ready
		.in_data           (id_router_004_src_data),              //          .data
		.out_endofpacket   (width_adapter_001_src_endofpacket),   //       src.endofpacket
		.out_data          (width_adapter_001_src_data),          //          .data
		.out_channel       (width_adapter_001_src_channel),       //          .channel
		.out_valid         (width_adapter_001_src_valid),         //          .valid
		.out_ready         (width_adapter_001_src_ready),         //          .ready
		.out_startofpacket (width_adapter_001_src_startofpacket)  //          .startofpacket
	);

	qsys_barco_irq_mapper irq_mapper (
		.clk           (clk_clk),                        //       clk.clk
		.reset         (rst_controller_reset_out_reset), // clk_reset.reset
		.receiver0_irq (irq_mapper_receiver0_irq),       // receiver0.irq
		.sender_irq    (nios2_qsys_0_d_irq_irq)          //    sender.irq
	);

endmodule
