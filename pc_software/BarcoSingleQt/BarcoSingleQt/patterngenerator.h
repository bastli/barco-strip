#ifndef PATTERNGENERATOR_H
#define PATTERNGENERATOR_H

#include <QObject>
#include <QColor>
#include <QDebug>

#include "definitions.h"

//-----------------------------------------------------------------------------

class PatternGenerator : public QObject
{
    Q_OBJECT

public:
    PatternGenerator(QObject* parent = NULL) : QObject(parent) {}

    virtual void generate(const int &strip_id, unsigned char *dest);
    virtual void generate(const int& strip_id, const int i, unsigned char &r, unsigned char &g, unsigned char &b);
};

//-----------------------------------------------------------------------------

class ConstPatternGenerator : public PatternGenerator
{
protected:
    QColor color;

public:
    ConstPatternGenerator(QColor col, QObject* parent = NULL) : PatternGenerator(parent), color(col) {}

    void generate(const int &strip_id, const int i, unsigned char &r, unsigned char &g, unsigned char &b);
};

//-----------------------------------------------------------------------------

class RampPatternGenerator : public ConstPatternGenerator
{

public:
    RampPatternGenerator(QColor col, QObject* parent = NULL) : ConstPatternGenerator(col, parent) {}

    void generate(const int &strip_id, const int i, unsigned char &r, unsigned char &g, unsigned char &b);
};

//-----------------------------------------------------------------------------

class NumberPatternGenerator : public PatternGenerator
{
public:
    NumberPatternGenerator(QObject* parent = NULL) : PatternGenerator(parent) {}

    void generate(const int &strip_id, const int i, unsigned char &r, unsigned char &g, unsigned char &b);
};

//-----------------------------------------------------------------------------


#endif // PATTERNGENERATOR_H
