**Please consult the Repository Wiki for more detailed information on setup, mechanics and protocols.**

# Barco-Strips

The Barco-Strips are Bastli's RGB stripes par excellence. 15 high brightness, 1.5 meter industrial RGB stripes from [Barco](http://www.barco.com/) with a custom hacked FPGA core are used to form a versatile nova display.

They are interconnected over a fast FPGA serial converter box and over network by a simple but powerful protocol to a computer calculating all the astonishing visuals by our own designed TouchDesigner effects.

## Getting Started

### Connect the hardware

1. Install all the barco-strips at their designated position. They have mounting carabiners and metal plates.
    * Wire them with the long serial cables to the distribution boxes which can be placed wherever you like. Don't forget to fasten the cables with the thumbscrews (M4) to the barco-strips.
2. Connect the distribution boxes to the serial converter box.
3. Connect the power supply to the serial converter box and the power socket

4. Setup a powerful Windows (7+) Workingstation somewhere near the installation and connect it via a Ethernet cable to the Serial. A new graphics card is recommended as well as fast processor and ASIO interface. Do not forget these peripherials:
    * Mouse & Keyboard
    * Microphone
    * MIDI Controller

### Setup the software

1. Clone this repository onto the Workstation.
2. Download & Install [Derivative TouchDesigner](https://www.derivative.ca/). Register for a free key and activate. Ensure it is working before going offline!
3. Run `main.toe` in the `touchdesigner` folder.

// ### Configuration & Debugging interface
// Point your browser to `http://barco.local` and open up a webinterface to easily configure and debug the barco-strips.


### Network ###

If Barco-Strip server has not DHCP activated:
Assign IP 10.6.66.101, netmask 255.255.255.0 to computer. The barco server is assigned to IP 10.6.66.10, port 1337 running the interface.


### Patch Barco Strips ###

Open a session over telnet to the barco-strip server. Ensure that you have a Telnet client installed (e.g. MobaXTerm).
Enter `banana` to ensure the server is running. The command `patc<Physical><Channel>` will remap the appropriate physical output to the specified virtual channel.

```
    telnet 10.64.128.2 1337
    
    patc0b patc1a patc29 patc38 patc47 patc56 patc65 patcd4 patc83 patc92 patca1 patcb0
```