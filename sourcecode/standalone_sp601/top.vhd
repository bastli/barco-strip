library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;
-------------------------------------------------------------------------------

entity standalone_sp601_top is

  port (
    SysClk_P         : in  std_logic;
    SysClk_N         : in  std_logic;
    ExtReset_RI      : in  std_logic;
    StripeClk_CO     : out std_logic;
    StripeReset_RO   : out std_logic;
    StripeData_DO    : out std_logic_vector(3 downto 0);
    StripeRed_DO     : out std_logic;
    StripeGreen_DO   : out std_logic;
    StripeBlue_DO    : out std_logic;
    StripeGE_DO      : out std_logic;
    StripeSegment_DO : out std_logic_vector(1 downto 0)
    );

end entity;

-------------------------------------------------------------------------------

architecture rtl of standalone_sp601_top is

  -----------------------------------------------------------------------------
  -- Components
  -----------------------------------------------------------------------------
  component BarcoIf is
    port (
      Reset_RI         : in  std_logic;
      InClk_CI         : in  std_logic;
      InAddr_DI        : in  unsigned(6 downto 0);
      Red_DI           : in  std_logic_vector(11 downto 0);
      Green_DI         : in  std_logic_vector(11 downto 0);
      Blue_DI          : in  std_logic_vector(11 downto 0);
      Valid_SI         : in  std_logic;
      StripeClk_CI     : in  std_logic;
      StripeData_DO    : out std_logic_vector(3 downto 0);
      StripeRed_DO     : out std_logic;
      StripeGreen_DO   : out std_logic;
      StripeBlue_DO    : out std_logic;
      StripeGE_DO      : out std_logic;
      StripeSegment_DO : out std_logic_vector(1 downto 0));
  end component BarcoIf;

  component LedCounter is
    port (
      Clk_CI   : in  std_logic;
      Reset_RI : in  std_logic;
      Addr_DO  : out unsigned(6 downto 0);
      Red_DO   : out std_logic_vector(11 downto 0);
      Green_DO : out std_logic_vector(11 downto 0);
      Blue_DO  : out std_logic_vector(11 downto 0);
      Valid_SO : out std_logic);
  end component LedCounter;

  component clk_core_0
    port
      (                                 -- Clock in ports
        CLK_IN1_P : in  std_logic;
        CLK_IN1_N : in  std_logic;
        -- Clock out ports
        CLK_40MHz : out std_logic;
        CLK_10MHz : out std_logic;
        -- Status and control signals
        RESET     : in  std_logic;
        LOCKED    : out std_logic
        );
  end component;

  component clk_core_1
    port
      (                                 -- Clock in ports
        CLK_IN_10MHz : in  std_logic;
        -- Clock out ports
        CLK_05MHz    : out std_logic;
        -- Status and control signals
        RESET        : in  std_logic;
        LOCKED       : out std_logic
        );
  end component;

  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------

  signal Addr_D  : unsigned(6 downto 0);
  signal Red_D   : std_logic_vector(11 downto 0);
  signal Green_D : std_logic_vector(11 downto 0);
  signal Blue_D  : std_logic_vector(11 downto 0);
  signal Valid_S : std_logic;

  signal StripeClk_C                        : std_logic;
  signal StripeData_DN, StripeData_DP       : std_logic_vector(3 downto 0);
  signal StripeRed_DN, StripeRed_DP         : std_logic;
  signal StripeGreen_DN, StripeGreen_DP     : std_logic;
  signal StripeBlue_DN, StripeBlue_DP       : std_logic;
  signal StripeGE_DN, StripeGE_DP           : std_logic;
  signal StripeSegment_DN, StripeSegment_DP : std_logic_vector(1 downto 0);

  signal Clk_40MHz_C     : std_logic;
  signal Clk_10MHz_C     : std_logic;
  signal Clk_05MHz_C     : std_logic;
  signal Clk_05MHz_Inv_C : std_logic;

  signal Locked0_R    : std_logic;
  signal Locked1_R    : std_logic;
  signal NotLocked0_R : std_logic;
  signal IntReset_R   : std_logic;

begin  -- architecture rtl

  BarcoIfInst : BarcoIf
    port map (
      Reset_RI         => IntReset_R,
      InClk_CI         => Clk_40MHz_C,
      InAddr_DI        => Addr_D,
      Red_DI           => Red_D,
      Green_DI         => Green_D,
      Blue_DI          => Blue_D,
      Valid_SI         => Valid_S,
      StripeClk_CI     => Clk_05MHz_C,
      StripeData_DO    => StripeData_DN,
      StripeRed_DO     => StripeRed_DN,
      StripeGreen_DO   => StripeGreen_DN,
      StripeBlue_DO    => StripeBlue_DN,
      StripeGE_DO      => StripeGE_DN,
      StripeSegment_DO => StripeSegment_DN);

  LedCounterInst : LedCounter
    port map (
      Clk_CI   => Clk_40MHz_C,
      Reset_RI => IntReset_R,
      Addr_DO  => Addr_D,
      Red_DO   => Red_D,
      Green_DO => Green_D,
      Blue_DO  => Blue_D,
      Valid_SO => Valid_S);

  -----------------------------------------------------------------------------
  -- Clock Generators
  -----------------------------------------------------------------------------
  clkCore0Inst : clk_core_0
    port map
    (                                   -- Clock in ports
      CLK_IN1_P => SysClk_P,
      CLK_IN1_N => SysClk_N,
      -- Clock out ports
      CLK_40MHz => Clk_40MHz_C,
      CLK_10MHz => Clk_10MHz_C,
      -- Status and control signals
      RESET     => ExtReset_RI,
      LOCKED    => Locked0_R);

  clkCore1Inst : clk_core_1
    port map
    (                                   -- Clock in ports
      CLK_IN_10MHz => Clk_10MHz_C,
      -- Clock out ports
      CLK_05MHz    => Clk_05MHz_C,
      -- Status and control signals
      RESET        => NotLocked0_R,
      LOCKED       => Locked1_R);

  resetSync : process (Clk_10MHz_C, Locked0_R) is
  begin  -- process resetSync
    if Locked0_R = '0' then              -- asynchronous reset (active low)
      NotLocked0_R <= '1';
    elsif rising_edge(Clk_10MHz_C) then  -- rising clock edge
      NotLocked0_R <= not Locked0_R;
    end if;
  end process resetSync;

  resetSync2 : process (Clk_05MHz_C, Locked1_R) is
  begin  -- process resetSync
    if Locked1_R = '0' then              -- asynchronous reset (active low)
      IntReset_R <= '1';
    elsif rising_edge(Clk_05MHz_C) then  -- rising clock edge
      IntReset_R <= not Locked1_R;
    end if;
  end process resetSync2;


  -- TODO: this is not a good idea
  -- There should be a way to generate a second clock for the DDR flipflop
  Clk_05MHz_Inv_C <= not Clk_05MHz_C;


  -----------------------------------------------------------------------------
  -- output assignements
  -----------------------------------------------------------------------------

  ODDR2Inst : ODDR2
    port map (
      Q  => StripeClk_CO,
      C0 => Clk_05MHz_Inv_C,            -- TODO: WRONG, change!
      C1 => Clk_05MHz_C,
      CE => '1',
      D0 => '1',
      D1 => '0',
      R  => '0',
      S  => '0');

  stripeOutReg : process (Clk_05MHz_C) is
  begin  -- process stripeOutReg
    if rising_edge(Clk_05MHz_C) then    -- rising clock edge
      StripeData_DP    <= StripeData_DN;
      StripeRed_DP     <= StripeRed_DN;
      StripeGreen_DP   <= StripeGreen_DN;
      StripeBlue_DP    <= StripeBlue_DN;
      StripeGE_DP      <= StripeGE_DN;
      StripeSegment_DP <= StripeSegment_DN;
    end if;
  end process stripeOutReg;

  StripeReset_RO   <= IntReset_R;
  StripeData_DO    <= StripeData_DP;
  StripeRed_DO     <= StripeRed_DP;
  StripeGreen_DO   <= StripeGreen_DP;
  StripeBlue_DO    <= StripeBlue_DP;
  StripeGE_DO      <= StripeGE_DP;
  StripeSegment_DO <= StripeSegment_DP;

end architecture rtl;
