﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Threading.Tasks;
using System.Threading;
using System.IO.Ports;
using System.Windows.Input;
using System.Net;

namespace BarcoSingle
{
    class MainWindowViewModel
    {
        private Thread _ServerThread;
        private Listener _Server;
        private readonly MainWindow _window;

        public MainWindowViewModel(MainWindow window)
        {
            _window = window;

            using (_Server = new Listener())
            {
                _ServerThread = new Thread(() => _Server.Run(1337));
                _ServerThread.IsBackground = true;
                _ServerThread.Priority = ThreadPriority.Normal;
                _ServerThread.Start();

                int i = 0;
                foreach (string name in SerialPort.GetPortNames())
                {
                    int j = i;
                    Thread _SerialThread = new Thread(() => _Server.OutputRun(name, j));
                    _SerialThread.Priority = ThreadPriority.Highest;
                    _SerialThread.Start();

                    i++;
                }
            }
        }
    }
}
