#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <QColor>
#include <QByteArray>

// GUI
///////////////////////////////////////////////////////////////////////////////

const QColor COLOR_ERR     = QColor("#ef2929");
const QColor COLOR_WAITING = QColor("#fcaf3e");
const QColor COLOR_OK      = QColor("#8ae234");

//const QColor COLOR_YELLOW  = QColor("#fce94f");

const QString status_default_style = "padding: 3px; border-radius: 3px;";

// BARCO HARDWARE / SERIAL
///////////////////////////////////////////////////////////////////////////////

const int BARCO_LEDS_PER_STRIP = 112;
const int BARCO_COL_NUM = 3;
const int BARCO_DATA_PAYLOAD_SIZE = BARCO_COL_NUM * BARCO_LEDS_PER_STRIP;

const unsigned char serial_sync_pattern[] = { 0x0f, 0xf1, 0x03, 0xf2 };
const unsigned char serial_null_pattern[] = { 0 }; // should be initalized to all zero by compiler...

const quint64 BARCO_BAUDRATE = 500000;

// NETWORK
///////////////////////////////////////////////////////////////////////////////

const quint16 DEFAULT_TCP_PORT = 1337;

const unsigned char network_sync_pattern[] = { 0xB1, 0x6B, 0x00, 0xB5 };

#endif // DEFINITIONS_H
