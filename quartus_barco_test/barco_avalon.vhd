-- barco_avalon.vhd

-- This file was auto-generated as a prototype implementation of a module
-- created in component editor.  It ties off all outputs to ground and
-- ignores all inputs.  It needs to be edited to make it do something
-- useful.
-- 
-- This file will not be automatically regenerated.  You should check it in
-- to your version control system if you want to keep it.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity barco_avalon is
  port (
    avs_s0_address       : in  std_logic_vector(6 downto 0)  := (others => '0');  -- barco_avalon.address
    avs_s0_read          : in  std_logic                     := '0';  --             .read
    avs_s0_readdata      : out std_logic_vector(63 downto 0);  --             .readdata
    avs_s0_writedata     : in  std_logic_vector(63 downto 0) := (others => '0');  --             .writedata
    avs_s0_write         : in  std_logic;
    avs_s0_byteEnable    : in  std_logic_vector(7 downto 0);
    avs_s0_readdatavalid : out std_logic;  --             .readdatavalid
    avs_s0_waitrequest   : out std_logic;  --             .waitrequest
    clk                  : in  std_logic                     := '0';  --        clock.clk
    reset                : in  std_logic                     := '0';  --        reset.reset
    Red_DO               : out std_logic_vector(11 downto 0);
    Green_DO             : out std_logic_vector(11 downto 0);
    Blue_DO              : out std_logic_vector(11 downto 0);
    Valid_SO             : out std_logic;
    Addr_DO              : out std_logic_vector(6 downto 0)
    );
end entity barco_avalon;

architecture rtl of barco_avalon is
  signal Addr_DN, Addr_DP   : std_logic_vector(6 downto 0);
  signal Red_DN, Red_DP     : std_logic_vector(11 downto 0);
  signal Green_DN, Green_DP : std_logic_vector(11 downto 0);
  signal Blue_DN, Blue_DP   : std_logic_vector(11 downto 0);
  signal Valid_SN, Valid_SP : std_logic;
begin

  byteEnableComb : process (Addr_DP, Blue_DP, Green_DP, Red_DP,
                            avs_s0_address(6 downto 0),
                            avs_s0_byteEnable(3 downto 0),
                            avs_s0_byteEnable(7 downto 4), avs_s0_write,
                            avs_s0_writedata(11 downto 0),
                            avs_s0_writedata(27 downto 16),
                            avs_s0_writedata(43 downto 32)) is
    variable i, j : integer;
  begin  -- process byteEnableComb
    Valid_SN <= '0';
    Addr_DN  <= Addr_DP;
    Red_DN   <= Red_DP;
    Green_DN <= Green_DP;
    Blue_DN  <= Blue_DP;

    if avs_s0_write = '1' and avs_s0_byteEnable(3 downto 0) = "1111" then
      bitRev1 : for i in 0 to 11 loop
        Red_DN(11 - i)   <= avs_s0_writedata(i);
        Green_DN(11 - i) <= avs_s0_writedata(16 + i);
      end loop bitRev1;
    end if;

    if avs_s0_write = '1' and avs_s0_byteEnable(7 downto 4) = "1111" then
      Addr_DN <= avs_s0_address(6 downto 0);

      bitRev2 : for j in 0 to 11 loop
        Blue_DN(11 - j) <= avs_s0_writedata(32 + j);
      end loop bitRev2;

      Valid_SN <= '1';
    end if;
  end process byteEnableComb;

  avs_s0_waitrequest <= '0';

  avs_s0_readdata      <= (others => '0');
  avs_s0_readdatavalid <= '0';

  outReg : process (clk, reset) is
  begin  -- process outReg
    if reset = '1' then                 -- asynchronous reset (active high)
      Addr_DP  <= (others => '0');
      Red_DP   <= (others => '0');
      Green_DP <= (others => '0');
      Blue_DP  <= (others => '0');
      Valid_SP <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      Addr_DP  <= Addr_DN;
      Red_DP   <= Red_DN;
      Green_DP <= Green_DN;
      Blue_DP  <= Blue_DN;
      Valid_SP <= Valid_SN;
    end if;
  end process outReg;

  Addr_DO  <= Addr_DP;
  Red_DO   <= Red_DP;
  Green_DO <= Green_DP;
  Blue_DO  <= Blue_DP;
  Valid_SO <= Valid_SP;

end architecture rtl;  -- of barco_avalon
