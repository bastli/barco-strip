
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name Barco-Strip -dir "/home/devora/barco-strip/hdl/Barco-Strip/planAhead_run_1" -part xc6slx16csg324-2
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "/home/devora/barco-strip/hdl/Barco-Strip/softcore_top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {/home/devora/barco-strip/hdl/Barco-Strip} }
set_property target_constrs_file "io_pins.ucf" [current_fileset -constrset]
add_files [list {io_pins.ucf}] -fileset [get_property constrset [current_run]]
link_design
