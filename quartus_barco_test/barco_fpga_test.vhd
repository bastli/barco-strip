

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-------------------------------------------------------------------------------

entity barco_fpga_test is
  port (

    LED    : out std_logic_vector(1 downto 0);
    CLK_IN : in  std_logic;


    uart_0_external_connection_rxd : in  std_logic;
    uart_0_external_connection_txd : out std_logic;


    StripeBufferEnable_DO : out std_logic;
    StripeReset_RO        : out std_logic;
    StripeClk_CO          : out std_logic;
    StripeData_DO         : out std_logic_vector(3 downto 0);
    StripeRed_DO          : out std_logic;
    StripeGreen_DO        : out std_logic;
    StripeBlue_DO         : out std_logic;
    StripeGE_DO           : out std_logic;
    StripeSegment_DO      : out std_logic_vector(1 downto 0)

    );

end entity barco_fpga_test;

-------------------------------------------------------------------------------

architecture rtl of barco_fpga_test is

  component qsys_barco is
    port (
      clk_clk                             : in  std_logic := 'X';  -- clk
      reset_reset_n                       : in  std_logic := 'X';  -- reset_n
      uart_0_external_connection_rxd      : in  std_logic := 'X';  -- rxd
      uart_0_external_connection_txd      : out std_logic;         -- txd
      pio_0_external_connection_export    : out std_logic_vector(1 downto 0);
      barco_avalon_0_conduit_end_Red_DO   : out std_logic_vector(11 downto 0);  -- Red_DO
      barco_avalon_0_conduit_end_Green_DO : out std_logic_vector(11 downto 0);  -- Green_DO
      barco_avalon_0_conduit_end_Blue_DO  : out std_logic_vector(11 downto 0);  -- Blue_DO
      barco_avalon_0_conduit_end_Valid_SO : out std_logic;         -- Valid_SO
      barco_avalon_0_conduit_end_Addr_DO  : out std_logic_vector(6 downto 0)  -- Addr_DO
      );
  end component qsys_barco;


  component ClkDivider is
    port (
      clk_in  : in  std_logic;
      reset   : in  std_logic;
      clk_out : out std_logic
      );
  end component ClkDivider;


  component BarcoIf is
    port (
      Reset_RI         : in  std_logic;
      InClk_CI         : in  std_logic;
      InAddr_DI        : in  unsigned(6 downto 0);
      Red_DI           : in  std_logic_vector(11 downto 0);
      Green_DI         : in  std_logic_vector(11 downto 0);
      Blue_DI          : in  std_logic_vector(11 downto 0);
      Valid_SI         : in  std_logic;
      StripeClk_CI     : in  std_logic;
      StripeData_DO    : out std_logic_vector(3 downto 0);
      StripeRed_DO     : out std_logic;
      StripeGreen_DO   : out std_logic;
      StripeBlue_DO    : out std_logic;
      StripeGE_DO      : out std_logic;
      StripeSegment_DO : out std_logic_vector(1 downto 0));
  end component BarcoIf;


  component LedCounter is
    port (
      Clk_CI   : in  std_logic;
      Reset_RI : in  std_logic;
      Addr_DO  : out unsigned(6 downto 0);
      Red_DO   : out std_logic_vector(11 downto 0);
      Green_DO : out std_logic_vector(11 downto 0);
      Blue_DO  : out std_logic_vector(11 downto 0);
      Valid_SO : out std_logic);
  end component LedCounter;

  component alt_pll
    port
      (
        areset : in  std_logic := '0';
        inclk0 : in  std_logic := '0';
        c0     : out std_logic;
        locked : out std_logic
        );
  end component;



  signal Addr_D      : unsigned(6 downto 0);
  signal Vect_Addr_D : std_logic_vector(6 downto 0);
  signal Red_D       : std_logic_vector(11 downto 0);
  signal Green_D     : std_logic_vector(11 downto 0);
  signal Blue_D      : std_logic_vector(11 downto 0);
  signal Valid_S     : std_logic;

  signal StripeClk_C                        : std_logic;
  signal StripeData_DN, StripeData_DP       : std_logic_vector(3 downto 0);
  signal StripeRed_DN, StripeRed_DP         : std_logic;
  signal StripeGreen_DN, StripeGreen_DP     : std_logic;
  signal StripeBlue_DN, StripeBlue_DP       : std_logic;
  signal StripeGE_DN, StripeGE_DP           : std_logic;
  signal StripeSegment_DN, StripeSegment_DP : std_logic_vector(1 downto 0);

  signal Clk_C       : std_logic;
  signal Clk_Barco_C : std_logic;

  signal locked   : std_logic;
  signal n_locked : std_logic;

begin


  BarcoIfInst : BarcoIf
    port map (
      Reset_RI         => n_locked,
      InClk_CI         => Clk_C,
      InAddr_DI        => Addr_D,
      Red_DI           => Red_D,
      Green_DI         => Green_D,
      Blue_DI          => Blue_D,
      Valid_SI         => Valid_S,
      StripeClk_CI     => Clk_Barco_C,
      StripeData_DO    => StripeData_DN,
      StripeRed_DO     => StripeRed_DN,
      StripeGreen_DO   => StripeGreen_DN,
      StripeBlue_DO    => StripeBlue_DN,
      StripeGE_DO      => StripeGE_DN,
      StripeSegment_DO => StripeSegment_DN);


  alt_pll_inst : alt_pll port map (
    areset => '0',
    inclk0 => CLK_IN,
    c0     => Clk_C,
    locked => locked
    );

  clkdiv_inst : ClkDivider port map (
    clk_in  => Clk_C,
    reset   => n_locked,
    clk_out => Clk_Barco_C
    );

  n_locked <= not locked;




  stripeOutReg : process (Clk_Barco_C) is
  begin  -- process stripeOutReg
    if rising_edge(Clk_Barco_C) then    -- rising clock edge
      StripeData_DP    <= StripeData_DN;
      StripeRed_DP     <= StripeRed_DN;
      StripeGreen_DP   <= StripeGreen_DN;
      StripeBlue_DP    <= StripeBlue_DN;
      StripeGE_DP      <= StripeGE_DN;
      StripeSegment_DP <= StripeSegment_DN;
    end if;
  end process stripeOutReg;

  StripeReset_RO   <= n_locked;
  StripeData_DO    <= StripeData_DP;
  StripeRed_DO     <= StripeRed_DP;
  StripeGreen_DO   <= StripeGreen_DP;
  StripeBlue_DO    <= StripeBlue_DP;
  StripeGE_DO      <= StripeGE_DP;
  StripeSegment_DO <= StripeSegment_DP;

  u0 : component qsys_barco
    port map (
      clk_clk                             => Clk_C,  --                        clk.clk
      reset_reset_n                       => locked,  --                      reset.reset_n
      uart_0_external_connection_rxd      => uart_0_external_connection_rxd,  -- uart_0_external_connection.rxd
      uart_0_external_connection_txd      => uart_0_external_connection_txd,  --                           .txd
      pio_0_external_connection_export    => LED,
      barco_avalon_0_conduit_end_Red_DO   => Red_D,  -- barco_avalon_0_conduit_end.Red_DO
      barco_avalon_0_conduit_end_Green_DO => Green_D,  --                           .Green_DO
      barco_avalon_0_conduit_end_Blue_DO  => Blue_D,  --                           .Blue_DO
      barco_avalon_0_conduit_end_Valid_SO => Valid_S,  --                           .Valid_SO
      barco_avalon_0_conduit_end_Addr_DO  => Vect_Addr_D  --                           .Addr_DO
      );

  StripeBufferEnable_DO <= '0';
  StripeClk_CO          <= not Clk_Barco_C;
  Addr_D                <= unsigned(Vect_Addr_D);

end architecture rtl;

